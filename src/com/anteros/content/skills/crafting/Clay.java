package com.anteros.content.skills.crafting;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Clay extends CraftingData {

	public Clay() {
		
	}
	
	public static void displayClayOptions(final Player p, int craftType) {
		final String s = "<br><br><br><br>";
		int j = 2;
		int k = 10;
		for (int i = 0; i < CLAY_ITEMS.length; i++) {
			p.getActionSender().itemOnInterface(306, j, 130, (Integer) CLAY_ITEMS[i][0]);
			p.getActionSender().modifyText(s + CLAY_ITEMS[i][5], 306, k);
			j++;
			k += 4;
		}
		p.getActionSender().sendChatboxInterface(306);
	}
	
	public static void craftClay(final Player p, int amount, int craftType, int craftItem, boolean newCraft) {
		if (newCraft) {
			if ((craftType != 1 && craftType != 2) || craftItem < 0 || craftItem > 4) {
				return;
			}
			int index = craftItem;
			int endItem = craftType == 1 ? 0 : 1;
			int xp = craftType == 1 ? 3 : 4;
			p.setTemporaryAttribute("craftItem", new CraftItem(craftType, craftItem, amount, (Double) CLAY_ITEMS[index][xp], (Integer) CLAY_ITEMS[index][endItem], (String) CLAY_ITEMS[index][5], (Integer) CLAY_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			return;
		}
		int neededItem = item.getCraftType() == 1 ? CLAY : (Integer) CLAY_ITEMS[item.getCraftItem()][0];
		String s = item.getCraftType() == 1 ? "You mould the clay into a " + item.getMessage() : "You bake the " + item.getMessage() + " in the oven"; 
		String s1 = item.getCraftType() == 1 ? "You need some soft clay to mould a " + item.getMessage() : "You need a pre-made " + item.getMessage() + " to put in the oven";
		int animation = item.getCraftType() == 1 ? 883 : 899;
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to make a " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(neededItem)) {
			p.getActionSender().sendMessage(s1 + ".");
			Crafting.resetCrafting(p);
			return;			
		}
		p.getActionSender().closeInterfaces();
		if (p.getInventory().deleteItem(neededItem)) {
			if (p.getInventory().addItem(item.getFinishedItem())) {
				p.getLevels().addXp(CRAFTING, item.getXp());
				p.getActionSender().sendMessage(s + ".");
				p.animate(animation);
			}
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {

				@Override
				public void execute() {
					craftClay(p, -1, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
