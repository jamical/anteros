package com.anteros.content.skills.crafting;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Spinning extends CraftingData {

	public Spinning() {
		
	}
	
	public static void interactWithWheel(final Player p, int objectX, int objectY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, objectX - 1, objectY - 1, objectX + 1, objectY + 1) {

			@Override
			public void run() {
				displaySpinningInterface(p);
			}
		});
	}
	
	public static void displaySpinningInterface(Player p) {
		int k = 2;
		int l = 8;
		String s = "<br><br><br><br>";
		for (int j = 0; j < 3; j++) {
			p.getActionSender().itemOnInterface(304, k, 180, (Integer) SPINNING_ITEMS[j][0]);
			p.getActionSender().modifyText(s + (String) SPIN_FINISH[j], 304, l);
			l += 4;
			k++;
		}
		p.setTemporaryAttribute("craftType", 6);
		p.getActionSender().sendChatboxInterface(304);
	}
	
	public static void craftSpinning(final Player p, int amount, int index, boolean newCraft) {
		if (newCraft) {
			p.setTemporaryAttribute("craftItem", new CraftItem(6, index, amount, (Double) SPINNING_ITEMS[index][3], (Integer) SPINNING_ITEMS[index][0], (String) SPINNING_ITEMS[index][4], (Integer) SPINNING_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 6) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		int i = item.getCraftItem();
		if (!p.getInventory().hasItem((Integer) SPINNING_ITEMS[i][1])) {
			p.getActionSender().sendMessage("You have no " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to spin that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem((Integer) SPINNING_ITEMS[i][1])) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			p.animate(894);
			p.getActionSender().sendMessage("You spin the " + item.getMessage() + " into a " + SPIN_FINISH[i] + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(750) {

				@Override
				public void execute() {
					craftSpinning(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
