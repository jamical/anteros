package com.anteros.content.skills.herblore;

import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class FillVial {

	public FillVial() {
		
	}
	// TODO make this use an AreaEvent so itll work from a distance.
	/**
	 * Will fill vials in a continuous motion from a water source.
	 */
	public static boolean fillingVial(final Player p, final Location loc) {
		if (!p.getInventory().hasItem(229) || !p.getLocation().withinDistance(loc, 2)) {
			return true;
		}
		if (p.getTemporaryAttribute("fillVialTimer") != null) {
			long lastFillTime = (Long) p.getTemporaryAttribute("fillVialTimer");
			if (System.currentTimeMillis() - lastFillTime < 600) {
				return true;
			}
		}
		p.setTemporaryAttribute("fillingVials", true);
		p.setFaceLocation(loc);
		
		World.getInstance().registerEvent(new Event(500) {
			int amountFilled = 0;
			@Override
			public void execute() {
				String s = amountFilled == 1 ? "vial" : "vials";
				if (p.getTemporaryAttribute("fillingVials") == null || !p.getLocation().withinDistance(loc, 2) || !p.getInventory().hasItem(229)) {
					p.animate(65535);
					if (amountFilled > 0) {
						p.getActionSender().sendMessage("You fill up the " + s + " with water.");
					}
					stop();
					return;
				}
				if (p.getInventory().replaceSingleItem(229, 227)) {
					p.animate(832);
					amountFilled++;
					p.setTemporaryAttribute("fillVialTimer", System.currentTimeMillis());
				} else {
					if (amountFilled > 0) {
						p.animate(65535);
						p.getActionSender().sendMessage("You fill up the " + s + " with water.");
					}
					stop();
				}
			}
		});
		return true;
	}
}
