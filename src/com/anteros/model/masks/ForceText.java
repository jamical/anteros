package com.anteros.model.masks;

/**
 * @author Luke132
 */
public class ForceText {

	private String text;
	
	public ForceText(String message) {
		this.text = message;
	}

	public String getText() {
		return text;
	}

}
