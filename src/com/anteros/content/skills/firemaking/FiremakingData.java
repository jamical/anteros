package com.anteros.content.skills.firemaking;

public class FiremakingData {

	public FiremakingData() {
		
	}
	
	protected static final int FIREMAKING = 11;
	protected static final int TINDERBOX = 590;
	protected static final int FIRE_OBJECT = 2732;
	protected static final int ASHES = 592;
	protected static final int START_DELAY = 4500;
	
	protected static final int[] LOGS = {
		//regular logs up to rev 530
		1511,2862,1521,1519,6333,10810,1517,6332,12581,1515,1513,
		//pyre
		3438,3440,3442,6211,10808,3444,6213,12583,3446,3448,
	};
	protected static final int[] FIRE_LEVEL = {
		//regular levels
		1,1,15,30,30,35,45,50,58,60,75,
		//pyre
		5,20,35,40,47,50,55,63,65,80,
	};
	protected static final double[] FIRE_XP = {
		//regular exp 
		40,40,60,90,105,105,141,175.5,202.5,303.8,
		//pyre
		50.5,70,100,120,158,175,210,216.5,225,404.5,
	};
	
	protected static final int[] COLOURED_LOGS = {
		7404, // Red
		7405, // Green
		7406, // Blue
		10329, // Purple
		10328, // White
	};
	
	protected static final int[] COLOURED_FIRES = {
		11404, // Red
		11405, // Green
		11406, // Blue
		20001, // Purple
		20000, // White
	};
	
	protected static final int[] FIRELIGHTERS = {
		7329,
		7330,
		7331,
		10326,
		10327
	};
	
	protected static final int[][] OTHER_ITEMS = {
		{596, 594}, // Torches
		{36, 33}, // Candles
		{4529, 4534}, // Candle lantern
		{4522, 4524}, // Oil lamp
		{4537, 4539}, // Oil lantern
		{7051, 7053}, // Bug lantern
		{4548, 4550}, // Bullseye lantern
		{5014, 5013}, // Mining helmet
	};
}
