package com.anteros.content.minigames.warriorguild;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.ItemDefinition;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class WarriorGuild extends WarriorGuildData {

	public WarriorGuild() {
		
	}
	
	public static boolean useAnimator(final Player p, final int itemId, int objectId, int objectX, int objectY) {
		if (objectId != ANIMATOR_ID) {
			return false;
		}
		int animatorIndex = -1;
		for (int i = 0; i < ANIMATOR_LOCATIONS.length; i++) {
			if (objectX == ANIMATOR_LOCATIONS[i][0] && objectY == ANIMATOR_LOCATIONS[i][1]) {
				animatorIndex = i;
				break;
			}
		}
		if (animatorIndex == -1) {
			return false;
		}
		p.setTemporaryAttribute("warriorGuildAnimator", animatorIndex);
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(ANIMATOR_LOCATIONS[animatorIndex][0], (ANIMATOR_LOCATIONS[animatorIndex][1] + 1), 0)) {
			@Override
			public void run() {
				int armourIndex = hasArmour(p, itemId);
				if (armourIndex != -1) {
					createdAnimatedArmour(p, armourIndex);
				}
			}
		});
		return true;
	}

	protected static void createdAnimatedArmour(final Player p, final int index) {
		if (p.getTemporaryAttribute("warriorGuildAnimator") == null) {
			return;
		}
		p.animate(827);
		p.setTemporaryAttribute("unmovable", true);
		for (int i = 0; i < ARMOUR_SETS[index].length; i++) {
			p.getInventory().deleteItem(ARMOUR_SETS[index][i]);
		}
		p.getActionSender().sendChatboxInterface(211);
		p.getActionSender().modifyText("You place the armour onto the platform where it", 211, 1);
		p.getActionSender().modifyText("dissapears...", 211, 2);
		final int animatorIndex = (Integer) p.getTemporaryAttribute("warriorGuildAnimator");
		World.getInstance().registerEvent(new Event(1500) {
			int i = 0;
			NPC npc = null;
			@Override
			public void execute() {
				if (i == 0) {
					p.getActionSender().sendChatboxInterface(211);
					p.getActionSender().modifyText("The animator hums, something appears to be working.", 211, 1);
					p.getActionSender().modifyText("You stand back.", 211, 2);
					this.setTick(500);
				} else if (i == 1) {
					p.getWalkingQueue().forceWalk(0, + 3);
					this.setTick(2000);
				} else if (i == 2) {
					this.setTick(500);
					Location minCoords = Location.location(2849, 3534, 0);
					Location maxCoords = Location.location(2861, 3545, 0);
					npc = new NPC(ANIMATED_ARMOUR[index]);
					npc.readResolve();
					npc.setMinimumCoords(minCoords);
					npc.setMaximumCoords(maxCoords);
					npc.setLocation(Location.location(ANIMATOR_LOCATIONS[animatorIndex][0], ANIMATOR_LOCATIONS[animatorIndex][1], 0));
					npc.setWalkType(NPC.WalkType.STATIC);
					npc.setForceText(new ForceText("I'm ALIVE!"));
					npc.animate(4166);
					npc.setEntityFocus(p.getClientIndex());
					npc.setOwner(p);
					npc.setTarget(p);
					p.getActionSender().setArrowOnEntity(1, npc.getClientIndex());
					World.getInstance().getNpcList().add(npc);
				} else {
					p.setEntityFocus(npc.getClientIndex());
					p.getActionSender().softCloseInterfaces();
					this.stop();
					p.removeTemporaryAttribute("unmovable");
					npc.getFollow().setFollowing(p);
				}
				i++;
			}
		});
	}

	protected static int hasArmour(Player p, int itemId) {
		int itemIndex = -1;
		for (int i = 0; i < ARMOUR_SETS.length; i++) {
			for (int j = 0; j < ARMOUR_SETS[i].length; j++) {
				if (itemId == ARMOUR_SETS[i][j]) {
					itemIndex = i;
					for (int k = 0; k < ARMOUR_SETS[i].length; k++) {
						if (!p.getInventory().hasItem(ARMOUR_SETS[i][k])) {
							p.getActionSender().sendMessage("You do not have a complete set of " + ARMOUR_TYPE[i] + " armour!");
							return -1;
						}
					}
					break;
				}
			}
		}
		return itemIndex;
	}
	
	public static boolean talkToWarriorGuildNPC(final Player p, final NPC n, int slot) {
		if (n.getId() != 4289) {
			return false;
		}
		p.setEntityFocus(n.getClientIndex());
		int npcX = n.getLocation().getX();
		int npcY = n.getLocation().getY();
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, npcX-1, npcY-1, npcX+1, npcY+1) {

			@Override
			public void run() {
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				switch(n.getId()) {
					case 4289: // Kamfreena
						Dialogue.doDialogue(p, 77);
						break;
				}
			}
		});
		return true;
	}

	private static int getDefenderStatus(Player p) {
		int currentDefender = -1;
		for (int i = 0; i < DEFENDERS.length; i++) {
			if (p.getInventory().hasItem(DEFENDERS[i]) || p.getEquipment().getItemInSlot(5) == DEFENDERS[i]) {
				currentDefender = i;
			}
		}
		if (currentDefender >= -1 && currentDefender != 6) {
			currentDefender++;
		}
		return currentDefender;
	}
	
	public static void talkToKamfreena(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		switch(status) {
			case 77:
				p.getActionSender().sendNPCHead(4289, 241, 2);
				p.getActionSender().modifyText("Kamfreena", 241, 3);
				p.getActionSender().modifyText("Hello! Can I help you?.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 78;
				break;
				
			case 78:
				p.getActionSender().modifyText("I'd like to kill some Cyclops please.", 228, 2);
				p.getActionSender().modifyText("Never mind, sorry to bother you.", 228, 3);
				p.getActionSender().sendChatboxInterface(228);
				newStatus = 79;
				break;
				
			case 79:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("I'd like to kill some Cyclops please.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 81;
				break;
				
			case 80:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Never mind, sorry to bother you.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				break;
				
			case 81:
				if (!p.getInventory().hasItemAmount(8851, 100)) {
					p.getActionSender().sendNPCHead(4289, 242, 2);
					p.getActionSender().modifyText("Kamfreena", 242, 3);
					p.getActionSender().modifyText("You require a minimum of 100 tokens in order", 242, 4);
					p.getActionSender().modifyText("to be able to enter the Cyclops' room.", 242, 5);
					p.getActionSender().animateInterface(9827, 242, 2);
					p.getActionSender().sendChatboxInterface2(242);
					break;
				} else {
					int currentDefenderStatus = getDefenderStatus(p);
					int lastDefenderStatus = p.getSettings().getDefenderWave();
					p.getSettings().setDefenderWave(currentDefenderStatus);
					String s = currentDefenderStatus != lastDefenderStatus ? " now " : " ";
					p.getActionSender().sendNPCHead(4289, 242, 2);
					p.getActionSender().modifyText("Kamfreena", 242, 3);
					p.getActionSender().modifyText("Very well. The Cyclops will" + s + "drop:", 242, 4);
					p.getActionSender().modifyText(ItemDefinition.forId(DEFENDERS[currentDefenderStatus]).getName() + ".", 242, 5);
					p.getActionSender().animateInterface(9827, 242, 2);
					p.getActionSender().sendChatboxInterface2(242);
					newStatus = 82;
				}
				break;
				
			case 82:
				if (p.getSettings().getDefenderWave() < 6) {
					p.getActionSender().sendNPCHead(4289, 242, 2);
					p.getActionSender().modifyText("Kamfreena", 242, 3);
					p.getActionSender().modifyText("Be sure to speak to me once you have retrieved one", 242, 4);
					p.getActionSender().modifyText("if you wish to advance!", 242, 5);
					p.getActionSender().animateInterface(9827, 242, 2);
					p.getActionSender().sendChatboxInterface2(242);
				} else {
					p.getActionSender().sendNPCHead(4289, 242, 2);
					p.getActionSender().modifyText("Kamfreena", 242, 3);
					p.getActionSender().modifyText("Since Rune is the highest Defender available, you don't", 242, 4);
					p.getActionSender().modifyText("need to speak to me once you have retrieved one.", 242, 5);
					p.getActionSender().animateInterface(9827, 242, 2);
					p.getActionSender().sendChatboxInterface2(242);
				}
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}
