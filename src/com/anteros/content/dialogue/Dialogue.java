package com.anteros.content.dialogue;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.anteros.event.AreaEvent;
import com.anteros.model.ItemDefinition;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.npc.NPCDefinition;
import com.anteros.model.player.Player;
import com.anteros.util.Utils;


public abstract class Dialogue {
	
	
	public static final String[] RANDOM_GREETINGS = { "Hello there.", "Howdy.", "Good day.", "Salutations!", "Greetings.", "Nice to meet you." };

	public static final String[] RANDOM_RESPONSES = { "Good day to you, adventurer.", "Well met, adventurer.", "Well, hello there." };


	public int getNPCID() {
		return -1;
	}
	
	protected Player player;
	protected int stage = -1;

	public Dialogue() {

	}
	
	public void sendNPCChat(Mood mood, String... text) {
		sendNPCDialogue(getNPCID(), mood.getIndex(), text);
	}
	
	public void sendPlayerChat(Mood mood, String... text) {
		StringBuilder dialogue = new StringBuilder(player.getUsername() + "%65%");
		for (int i = 0; i < text.length; i++)
			dialogue.append(text[i] + "%65%");
		sendEntityDialogue(SEND_1_TEXT_CHAT, dialogue.toString().split("%65%"), IS_PLAYER, player.getIndex(), mood.getIndex());
		
	}

	public boolean sendHandedItem(int item, String... text) {
		StringBuilder builder = new StringBuilder();
		for (int line = 0; line < text.length; line++)
			builder.append((line == 0 ? "<p=" + getP() + ">" : "<br>") + text[line]);
		String texts = builder.toString();
		int amount = 1;
		player.getActionSender().sendChatboxInterface(209);
		player.getActionSender().modifyText(texts, 209, 4);
		player.getActionSender().itemOnInterface(209, 1, amount, item);
		return true;
	}

	public Object[] parameters;

	public void setPlayer(Player player) {
		this.player = player;
	}

	public abstract void start();

	public abstract void run(int interfaceId, int componentId);

	public abstract void finish();

	protected final void end() {
		player.getDialogueManager().finishDialogue();
	}
	
	public static final int OPTION1 = 15, OPTION2 = 16;

	/*public boolean Options2(String title, String... options) {
		player.getInterfaceManager().sendChatBoxInterface(1185);
		player.getActionSender().modifyText(1185, 14, title);
		for (int line = 0; line < 2; line++) {
			if (line < options.length) {
				if (line == 0)
					player.getActionSender().modifyText(1185, 20, options[line]);
				else if (line == 1)
					player.getActionSender().modifyText(1185, 25, options[line]);
			}
		}
		return true;
	}*/

	protected static final short SEND_1_TEXT_INFO = 210;
	protected static final short SEND_2_TEXT_INFO = 211;
	protected static final short SEND_3_TEXT_INFO = 212;
	protected static final short SEND_4_TEXT_INFO = 213;
	protected static final String SEND_DEFAULT_OPTIONS_TITLE = "Select an Option";
	protected static final String DEFAULT = "Select an Option";
	protected static final short SEND_2_OPTIONS = 236;
	protected static final short SEND_3_OPTIONS = 230;
	protected static final short SEND_4_OPTIONS = 237;
	protected static final short SEND_5_OPTIONS = 238;
	protected static final short SEND_2_LARGE_OPTIONS = 229;
	protected static final short SEND_3_LARGE_OPTIONS = 231;
	protected static final short SEND_1_TEXT_CHAT = 241;
	protected static final short SEND_2_TEXT_CHAT = 1189;
	protected static final short SEND_3_TEXT_CHAT = 243;
	protected static final short SEND_4_TEXT_CHAT = 244;
	protected static final short SEND_NO_CONTINUE_1_TEXT_CHAT = 245;
	protected static final short SEND_NO_CONTINUE_2_TEXT_CHAT = 246;
	protected static final short SEND_NO_CONTINUE_3_TEXT_CHAT = 247;
	protected static final short SEND_NO_CONTINUE_4_TEXT_CHAT = 248;
	protected static final short SEND_NO_EMOTE = -1;
	protected static final byte IS_NOTHING = -1;
	protected static final byte IS_PLAYER = 0;
	protected static final byte IS_NPC = 1;
	protected static final byte IS_ITEM = 2;
	public static final int HAPPY_FACE = 9843;
	public static final int ASKING_FACE = 9829;
	public static final int BLANK_FACE = 9772;
	public static final int SAD_FACE = 9768;
	public static final int UPSET_FACE = 9776;
	public static final int SCARED_FACE = 9780;
	public static final int MILDLY_ANGRY_FACE = 9784;
	public static final int ANGRY_FACE = 9788;
	public static final int VERY_ANGRY_FACE = 9792;
	public static final int MANIAC_FACE = 9800;
	public static final int NOT_TALKING_JUST_LISTENING_FACE = 9804;
	public static final int PLAIN_TALKING_FACE = 9808;
	public static final int WTF_FACE = 9820;
	public static final int SHAKING_NO_FACE = 9824;
	public static final int UNSURE_FACE = 9836;
	public static final int LISTENS_THEN_LAUGHS_FACE = 9840;
	public static final int GOOFY_LAUGH_FACE = 9851;
	public static final int THINKING_THEN_TALKING_FACE = 9859;
	public static final int NONONO_FACE = 9844;
	public static final int SAD_WTF_FACE = 9776;
	public static final int NORMAL = 9827, 
			   HAPPY = 9843, 
			   WORRIED = 9775, 
			   CONFUSED = 9830,
			   DRUNK = 9835, 
			   MAD = 9785, 
			   ANGRY = 9790, 
			   SAD = 9775, 
			   SCARED = 9780,
			   ASKING = 9829,
			   BLANK = 9772,
			   UPSET = 9776,
			   MILDLY_ANGRY = 9784,
			   VERY_ANGRY = 9792,
			   MANIAC = 9800,
			   NOT_TALKING_JUST_LISTENING = 9804,
			   PLAIN_TALKING = 9808,
			   UNSURE= 9836,
			   LISTENS_THEN_LAUGHS = 9840,
			   GOOFY_LAUGH = 9851,
			   THINKING_THEN_TALKING = 9859,
			   NONONO = 9844,
			   SAD_SHOCKED = 9776;

	private static int[] getIComponentsIds(short interId) {
		int[] childOptions;
		switch (interId) {
		case SEND_1_TEXT_INFO:
			childOptions = new int[1];
			childOptions[0] = 1;
			break;
		case SEND_2_TEXT_INFO:
			childOptions = new int[2];
			childOptions[0] = 1;
			childOptions[1] = 2;
			break;
		case SEND_3_TEXT_INFO:
			childOptions = new int[3];
			childOptions[0] = 1;
			childOptions[1] = 2;
			childOptions[2] = 3;
			break;
		case SEND_4_TEXT_INFO:
			childOptions = new int[4];
			childOptions[0] = 1;
			childOptions[1] = 2;
			childOptions[2] = 3;
			childOptions[3] = 4;
			break;
		case SEND_2_LARGE_OPTIONS:
			childOptions = new int[3];
			childOptions[0] = 1;
			childOptions[1] = 2;
			childOptions[2] = 3;
			break;
		case SEND_3_LARGE_OPTIONS:
			childOptions = new int[4];
			childOptions[0] = 1;
			childOptions[1] = 2;
			childOptions[2] = 3;
			childOptions[3] = 4;
			break;
		case SEND_2_OPTIONS:
			childOptions = new int[3];
			childOptions[0] = 0;
			childOptions[1] = 1;
			childOptions[2] = 2;
			break;
		case SEND_3_OPTIONS:
			childOptions = new int[4];
			childOptions[0] = 1;
			childOptions[1] = 2;
			childOptions[2] = 3;
			childOptions[3] = 4;
			break;
		case SEND_4_OPTIONS:
			childOptions = new int[5];
			childOptions[0] = 0;
			childOptions[1] = 1;
			childOptions[2] = 2;
			childOptions[3] = 3;
			childOptions[4] = 4;
			break;
		case SEND_5_OPTIONS:
			childOptions = new int[6];
			childOptions[0] = 0;
			childOptions[1] = 1;
			childOptions[2] = 2;
			childOptions[3] = 3;
			childOptions[4] = 4;
			childOptions[5] = 5;
			break;
		case SEND_1_TEXT_CHAT:
		case SEND_NO_CONTINUE_1_TEXT_CHAT:
			childOptions = new int[2];
			childOptions[0] = 3;
			childOptions[1] = 4;
			break;
		case SEND_2_TEXT_CHAT:
		case SEND_NO_CONTINUE_2_TEXT_CHAT:
			childOptions = new int[3];
			childOptions[0] = 3;
			childOptions[1] = 4;
			childOptions[2] = 5;
			break;
		case SEND_3_TEXT_CHAT:
		case SEND_NO_CONTINUE_3_TEXT_CHAT:
			childOptions = new int[4];
			childOptions[0] = 3;
			childOptions[1] = 4;
			childOptions[2] = 5;
			childOptions[3] = 6;
			break;
		case SEND_4_TEXT_CHAT:
		case SEND_NO_CONTINUE_4_TEXT_CHAT:
			childOptions = new int[5];
			childOptions[0] = 3;
			childOptions[1] = 4;
			childOptions[2] = 5;
			childOptions[3] = 6;
			childOptions[4] = 7;
			break;
		default:
			return null;
		}
		return childOptions;
	}

	public boolean sendNPCDialogue(int npcId, int animationId, String... text) {
		return sendEntityDialogue(IS_NPC, npcId, animationId, text);
	}

	public boolean sendPlayerDialogue(int animationId, String... text) {
		return sendEntityDialogue(IS_PLAYER, -1, animationId, text);
	}
	
	public boolean sendItemDialogue(int itemId, String... text) {
		return sendEntityDialogue(IS_ITEM, itemId, -1, text);
	}
	
	/*
	 * 
	 * auto selects title, new dialogues
	 */
	public boolean sendEntityDialogue(int type, int entityId, int animationId,
			String... text) {
		String title = "";
		if (type == IS_PLAYER) {
			title = player.getUsername();
		} else if (type == IS_NPC) {
			title = NPCDefinition.forId(entityId).getName();
		} else if (type == IS_ITEM)
			title = ItemDefinition.forId(entityId).getName();
		return sendEntityDialogue(type, title, entityId, animationId, text);
	}

	/*
	 * idk what it for
	 */
	public int getP() {
		return 1;
	}

	public static final int OPTION_1 = 11, OPTION_2 = 13, OPTION_3 = 14, OPTION_4 = 15, OPTION_5 = 16;

	@SuppressWarnings("rawtypes")
	public boolean sendOptionsDialogue(String title, String... options) {
		int i = 0;
		player.getActionSender().sendChatboxInterface(209 + options.length);
		Object params[] = new Object[options.length + 1];
		int interfaceId = 209 + options.length;
		params[i++] = Integer.valueOf(options.length);
		List optionsList = Arrays.asList(options);
		Collections.reverse(optionsList);
		for (Iterator iterator = optionsList.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			params[i++] = string;
		}

		player.getActionSender().modifyText(title, interfaceId, 20);
		//player.getActionSender().sendClientScript(5589, params, title);
		return true;
	}

	public static boolean sendNPCDialogueNoContinue(Player player, int npcId, int animationId, String... text) {
		return sendEntityDialogueNoContinue(player, IS_NPC, npcId, animationId, text);
	}

	public static boolean sendPlayerDialogueNoContinue(Player player, int animationId, String... text) {
		return sendEntityDialogueNoContinue(player, IS_PLAYER, -1, animationId, text);
	}

	/*
	 * 
	 * auto selects title, new dialogues
	 */
	public static boolean sendEntityDialogueNoContinue(Player player, int type, int entityId, int animationId, String... text) {
		String title = "";
		if (type == IS_PLAYER) {
			title = player.getUsername();
		} else if (type == IS_NPC) {
			title = NPCDefinition.forId(entityId).getName();
		} else if (type == IS_ITEM)
			title = ItemDefinition.forId(entityId).getName();
		return sendEntityDialogueNoContinue(player, type, title, entityId, animationId, text);
	}

	public static boolean sendEntityDialogueNoContinue(Player player, int type, String title, int entityId, int animationId, String... texts) {
		StringBuilder builder = new StringBuilder();
		for (int line = 0; line < texts.length; line++)
			builder.append(" " + texts[line]);
		String text = builder.toString();
		int interfaceId = 209 + texts.length;
		player.getActionSender().sendChatboxInterface(interfaceId);
		player.getActionSender().modifyText(title, interfaceId, 16);
		player.getActionSender().modifyText(text, interfaceId, 12);
		player.getActionSender().sendPlayerHead(1192, 11);/*(type == IS_PLAYER, entityId, 1192, 11);*/
		//if (animationId != -1)
			//player.getActionSender().sendIComponentAnimation(animationId, 1192, 11);
		return true;
	}

	public static void closeNoContinueDialogue(Player player) {
		player.getActionSender().closeChatboxInterface();
	}

	/*
	 * new dialogues
	 */
	public boolean sendEntityDialogue(int type, String title, int entityId, int animationId, String... texts) {
	/*	
		if (messages.length < 1 || messages.length > 4) {
			return null;
		}
		boolean npc = npcId > -1;
		int interfaceId = (npc ? 240 : 63) + messages.length;
		if (expression == -1) {
			expression = FacialExpression.NORMAL.getAnimationId();
		}
		player.getPacketDispatch().sendAnimationInterface(expression, interfaceId, 2);
		if (npc) {
			player.getPacketDispatch().sendNpcOnInterface(npcId, interfaceId, 2);
			player.getPacketDispatch().sendString(NPCDefinition.forId(npcId).getName(), interfaceId, 3);
		} else {
			player.getPacketDispatch().sendPlayerOnInterface(interfaceId, 2);
			player.getPacketDispatch().sendString(player.getUsername(), interfaceId, 3);
		}
		for (int i = 0; i < messages.length; i++) {
			player.getPacketDispatch().sendString(messages[i].toString().replace("@name", player.getUsername()), interfaceId, (i + 4));
		}
		player.getInterfaceManager().openChatbox(interfaceId);
		if (player.getAttributes().containsKey("tut-island") || TutorialSession.getExtension(player).getStage() <= TutorialSession.MAX_STAGE) {
			Component.setUnclosable(player, player.getInterfaceManager().getChatbox());
		}
		player.getPacketDispatch().sendInterfaceConfig(player.getInterfaceManager().getChatbox().getId(), 3, false);*/
		
		/*StringBuilder builder = new StringBuilder();
		for (int line = 0; line < texts.length; line++)
			builder.append(" " + texts[line]);
		String text = builder.toString();*/
		NPC n = null;
		boolean npc = entityId > -1;
		int interfaceId = (npc ? 240 : 63) + texts.length;
		if (animationId == -1) {
			animationId = Mood.NORMAL.getIndex();
		}
		player.getActionSender().animateInterface(animationId, interfaceId, 2);
		if (type == IS_NPC) {
			player.getActionSender().sendChatboxInterface(interfaceId);
			player.getActionSender().modifyText(title, 240, 3);
			player.getActionSender().sendNPCHead(entityId, interfaceId, 2);
			if (animationId != -1)
				player.getActionSender().animateInterface(animationId, interfaceId, 11);
		} else if (type == IS_PLAYER) {
			player.getActionSender().sendChatboxInterface(interfaceId);
			player.getActionSender().modifyText(title, 240, 3);
			player.getActionSender().sendPlayerHead(interfaceId, 2);
		}
		for (int i = 0; i < texts.length; i++) {
			player.getActionSender().modifyText(texts[i].toString().replace("@name", player.getUsername()), interfaceId, (i + 4));
		}
		handleNpcInteraction(player, n, entityId, -1);
		return true;
	}

	public boolean sendDialogue(String... texts) {
		StringBuilder builder = new StringBuilder();
		for (int line = 0; line < texts.length; line++)
			builder.append((line == 0 ? "<p=" + getP() + ">" : "<br>") + texts[line]);
		String text = builder.toString();
		int interfaceId = 209 + texts.length;
		player.getActionSender().sendChatboxInterface(interfaceId);
		player.getActionSender().modifyText(text, interfaceId, 1);
		return true;
	}

	public boolean sendEntityDialogue(short interId, String[] talkDefinitons, byte type, int entityId, int animationId) {
		if (type == IS_PLAYER || type == IS_NPC) { // auto convert to new
													// dialogue all old
													// dialogues
			String[] texts = new String[talkDefinitons.length - 1];
			for (int i = 0; i < texts.length; i++)
				texts[i] = talkDefinitons[i + 1];
			sendEntityDialogue(type, talkDefinitons[0], entityId, animationId, texts);
			return true;
		}
		int[] componentOptions = getIComponentsIds(interId);
		if (componentOptions == null)
			return false;
		player.getActionSender().sendChatboxInterface(interId);
		if (talkDefinitons.length != componentOptions.length)
			return false;
		for (int childOptionId = 0; childOptionId < componentOptions.length; childOptionId++)
			player.getActionSender().modifyText(talkDefinitons[childOptionId], interId, componentOptions[childOptionId]);
		if (type == IS_PLAYER) {
			player.getActionSender().sendPlayerHead(interId, 2);
		} else if (type == IS_NPC) {
			 player.getActionSender().sendNPCHead(entityId, interId, 2);
			if (animationId != -1)
				player.getActionSender().animateInterface(animationId, interId, 2);
		} else if (type == IS_ITEM)
			player.getActionSender().itemOnInterface(interId, 1, entityId, animationId);
		return true;
	}
	
	public String generateRandomGreetings() {
		  return RANDOM_GREETINGS[Utils.random(4)];
	}

		 public String generateRandomResponses() {
		  return RANDOM_RESPONSES[Utils.random(2)];
	}

		 
	 public static void handleNpcInteraction(final Player p, final NPC n, final int npcId, final int status) {
			p.setEntityFocus(n.getClientIndex());
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

				@Override
				public void run() {
					n.setFaceLocation(p.getLocation());
					p.setFaceLocation(n.getLocation());
					p.setEntityFocus(65535);
				}
			});
		}


}
