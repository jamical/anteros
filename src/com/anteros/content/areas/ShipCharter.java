package com.anteros.content.areas;

import com.anteros.model.Location;
import com.anteros.model.player.Player;

/**
 * Handles the ship chartering system.
 * @author Aero
 */
public class ShipCharter {

	/**
	 * The interface id of the chartering system.
	 */
	public static final byte SHIP_CHARTER = 95;
	
	private ShipCharter() {
		/** Empty to stop instantiaton **/
	}
	
	/**
	 * Handles the buttons on the chartering interface.
	 * @param p The player.
	 * @param button The button id.
	 */
	public static void handleButton(Player p, int button) {
		DESTINATIONS destin = DESTINATIONS.getDestination(button);
		if (destin == null) throw new RuntimeException("Destination was null.");
		int cost = getCost(p, destin);
		if (!p.getInventory().hasItemAmount(995, cost)) {
			p.getActionSender().sendMessage("You need at least " + cost + " gp to travel there.");
			return;
		}
		p.teleport(destin.getTile());
		p.getActionSender().closeInterfaces();
		p.getInventory().deleteItem(995, cost);
	}
	
	/**
	 * Calculates the cost.
	 * @param p The player.
	 * @param d The destination.
	 * @return The cost of the trip.
	 */
	public static int getCost(Player p, DESTINATIONS d) {
		int cost = d.getCost();
		boolean cabinFever = true;
		if (p.getEquipment().playerRing != 6465 && !cabinFever || cabinFever && p.getEquipment().playerRing == 6465) {
			cost -= Math.round((cost / 2));
		} 
		return cost;
	}
	
	/**
	 * Enum which contains the list of destinations.
	 * @author Aero
	 */
	enum DESTINATIONS {
		
		CATHERBY(Location.location(2792, 3414, 0), 25, 200),
		PORT_PHASMATYS(Location.location(3702, 3503, 0), 24, 200),
		CRANDOR(Location.location(2853, 3238, 0), 32, 200),
		BRIMHAVEN(Location.location(2760, 3238,0), 28, 200),
		PORT_SARIM(Location.location(3038, 3192, 0), 30, 200),
		PORT_TYRAS(Location.location(2142, 3122, 0), 23, 200),
		KARAMJA(Location.location(2954, 3158, 0), 27, 200),
		PORT_KHAZZARD(Location.location(2674, 3144, 0), 29, 200),
		SHIPYARD(Location.location(3001, 3032, 0), 26, 200),
		OO_GLOG(Location.location(2623, 2857, 0), 33, 200),
		MOS_LE_HARMLESS(Location.location(3671, 2931, 0), 31, 200);
		
		public static DESTINATIONS getDestination(int button) {
			for (DESTINATIONS destination : DESTINATIONS.values()) {
				if (destination.getButtonID() == button) 
					return destination;
			}
			return null;
		}
		
		private Location tile;/** Destination tile. **/
		private int buttonID;/** Button id. **/
		private int cost;/** The cost of the trip. **/
	
		DESTINATIONS(Location loc, int button, int money) {
			this.tile = loc;
			this.buttonID = button;
			this.cost = money;
		}
		
		/**
		 * Gets the destination tile.
		 * @return The destination tile.
		 */
		public Location getTile() {
			return tile;
		}
		
		/**
		 * Gets the button id.
		 * @return The button id.
		 */
		public int getButtonID() {
			return buttonID;
		}
		
		/**
		 * Gets the cost of the trip.
		 * @return The cost.
		 */
		public int getCost() {
			return cost;
		}
		
	}
	
}
