package com.anteros.content.areas;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Edgevile {
public static void climbExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3097, 3469, 0));
		}
	});
}
public static void climbEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3096, 9867, 0));
		}
	});
}
}
