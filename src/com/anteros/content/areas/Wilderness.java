package com.anteros.content.areas;

import com.anteros.content.skills.Skills;
import com.anteros.content.skills.magic.Teleport;
import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.world.LaddersAndStairs;

public class Wilderness {
	
	private static final int[][] LEVER_COORDINATES = {
		{2538, 2832, 0}, // neitiznot lever
		{3097, 3475, 0}, // edgeville lever
	};
	
	private static final int[][] LEVER_FACE_COORDINATES = {
		{2538, 2831, 0}, // neitiznot lever
		{3098, 3475, 0}, // edgeville lever
	};

	public Wilderness() {
		
	}
	
	public static void handleLever(final Player p, int id, Location loc) {
		if ((p.getTemporaryAttribute("teleporting") != null )) {
			return;
		}
		for (int i = 0 ; i < LEVER_COORDINATES.length; i++) {
			Location loc1 = Location.location(LEVER_COORDINATES[i][0], LEVER_COORDINATES[i][1], LEVER_COORDINATES[i][2]);
			if (loc.equals(loc1)) {
				final int index = i;
				final Location loc2 = Location.location(LEVER_FACE_COORDINATES[i][0], LEVER_FACE_COORDINATES[i][1], LEVER_FACE_COORDINATES[i][2]);
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, loc) {

					@Override
					public void run() {
						p.setFaceLocation(loc2);
						displayWildernessLeverOptions(p, index);
					}
				});
				return;
			}
		}
		LaddersAndStairs.getInstance().useLever(p, id, loc); // Used for default levers/levers with no options
	}
	
	public static void displayWildernessLeverOptions(Player p, int leverIndex) {
		int dialogueIndex = 140 + leverIndex;
		p.setTemporaryAttribute("dialogue", dialogueIndex);
		String option1 = leverIndex == 0 ? "Edgeville" : "Home";
		p.getActionSender().modifyText(option1, 230, 2);
		p.getActionSender().modifyText("Mage bank", 230, 3);
		p.getActionSender().modifyText("Nowhere", 230, 4);
		p.getActionSender().sendChatboxInterface2(230);
	}
	
	public static void leverTeleport(final Player p, int option) {
		p.getActionSender().closeInterfaces();
		final Location teleLocation = Location.location(LEVER_COORDINATES[option][0], LEVER_COORDINATES[option][1], LEVER_COORDINATES[option][2]);
		World.getInstance().registerEvent(new Event(200) {

			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("teleblocked") != null) {
					p.getActionSender().sendMessage("A magical force prevents you from teleporting!");
					return;
				} else if ((p.getTemporaryAttribute("teleporting") != null )) {
					return;
				}
				p.animate(2140);
				p.getActionSender().closeInterfaces();
				p.setTemporaryAttribute("teleporting", true);
				p.getWalkingQueue().reset();
				p.getActionSender().clearMapFlag();
				Skills.resetAllSkills(p);
				World.getInstance().registerEvent(new Event(700) {

					@Override
					public void execute() {
						this.stop();
						p.animate(8939, 0);
						p.graphics(1576, 0);
						World.getInstance().registerEvent(new Event(1800) {

							@Override
							public void execute() {
								this.stop();
								p.teleport(teleLocation);
								p.animate(8941, 0);
								p.graphics(1577, 0);
								Teleport.resetTeleport(p);
							}
						});
					}
				});
			}
		});
	}

	public static void slashWeb(final Player p, final int webId, final Location webLocation) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, webLocation.getX() - 1, webLocation.getY() - 1, webLocation.getX() + 1, webLocation.getY() + 1) {

			@Override
			public void run() {
				long lastSlash = 0;
				p.setFaceLocation(webLocation);
				if (p.getTemporaryAttribute("lastWebSlash") != null) {
					lastSlash = (Long)p.getTemporaryAttribute("lastWebSlash");
				}
				if (System.currentTimeMillis() - lastSlash <= 800) {
					return;
				}
				if (World.getInstance().getGlobalObjects().originalObjectExists(webId, webLocation)) {
					p.animate(p.getAttackAnimation());
					p.setTemporaryAttribute("lastWebSlash", System.currentTimeMillis());
					World.getInstance().registerEvent(new Event(500) {
					@Override
					public void execute() {
						this.stop();
						boolean webExists = World.getInstance().getGlobalObjects().originalObjectExists(webId, webLocation);
						World.getInstance().getGlobalObjects().lowerHealth(webId, webLocation);
						if (World.getInstance().getGlobalObjects().originalObjectExists(webId, webLocation)) {
							p.getActionSender().sendMessage("You fail to cut through the web.");
						} else {
							if (webExists) { // This means we slashed it, if !webExists, someone else slashed it in the last 500ms
								p.getActionSender().sendMessage("You slash through the web!");
							}
						}
					}
					});
				}
			}
		});
	}
}
