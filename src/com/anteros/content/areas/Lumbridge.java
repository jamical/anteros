package com.anteros.content.areas;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Lumbridge {

	public Lumbridge() {
		
	}
	
	public static void castleGround(final Player p, int objectX, int objectY) {
		if (objectX == 3204 && objectY == 3207) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3209, 3206, 3209) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3209, 1));
				}
			});
		} else if (objectX == 3204 && objectY == 3229) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3228, 3206, 3229) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3228, 1));
				}
			});
		}
	}
	
	public static void castleMidToTop(final Player p, int objectX, int objectY) {
		if (objectX == 3204 && objectY == 3207) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3209, 3206, 3209) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3209, 2));
				}
			});
		} else if (objectX == 3204 && objectY == 3229) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3228, 3206, 3229) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3228, 2));
				}
			});
		}
	}
	
	public static void doLadder(Player p) {
		if (p.getLocation().getZ() == 2) {
			castleUpLadder(p);
		}
		if (p.getLocation().getZ() == 3) {
			castleDownLadder(p);
		}
	}
	
	public static void castleDownLadder(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3222, 3207, 3223) {

			@Override
			public void run() {
				p.teleport(Location.location(3207, 3224, 2));
			}
		});
	}
	public static void castleMiddleLadder(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3222, 3207, 3223) {

			@Override
			public void run() {
				p.teleport(Location.location(3207, 3224, 2));
			}
		});
	}
	
	public static void castleUpLadder(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3224, 3207, 3225) {

			@Override
			public void run() {
				p.teleport(Location.location(3207, 3222, 3));
			}
		});
	}
	
	public static void castleMidToGround(final Player p, int objectX, int objectY) {
		if (objectX == 3204 && objectY == 3207) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3209, 3206, 3209) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3209, 0));
				}
			});
		} else if (objectX == 3204 && objectY == 3229) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3228, 3206, 3229) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3228, 0));
				}
			});
		}
	}
	
	public static void castleTop(final Player p, int objectX, int objectY) {
		if (objectX == 3204 && objectY == 3207) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3209, 3206, 3209) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3209, 1));
				}
			});
		} else if (objectX == 3204 && objectY == 3229) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3205, 3228, 3206, 3229) {

				@Override
				public void run() {
					p.teleport(Location.location(3205, 3228, 1));
				}
			});
		}
	}
}
