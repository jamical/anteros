package com.anteros.content.quests;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
/**
 * 
 * @author Abexlry
 *
 */
public class DoricsQuest {

	Player c;
	
	public DoricsQuest(Player c) {
		this.c = c;
	}
	
	public static void showInformation(Player p) { 
			p.getActionSender().displayInterface(275);
			p.getActionSender().modifyText("Doric's Quest", 275, 2);
			p.getQuests().sendQuestInterface(p);
		if(p.doricsQuestInt == 0) {
			p.getActionSender().modifyText("Doric's Quest", 275, 2);
			p.getActionSender().modifyText("<col=342D7E>I can start this quest by speaking to Doric, the dwarf, near", 275, 12);
			p.getActionSender().modifyText("<col=342D7E>Goblin Village near the crossing of Falador to Varrock", 275, 13);
			p.getActionSender().modifyText("", 275, 14);
			p.getActionSender().modifyText("<col=342D7E>Minimum requirements are:", 275, 15);
			if (p.getLevels().getLevel(Skills.MINING) < 15) {
				p.getActionSender().modifyText("<col=342D7E>Level 15 mining is recommended", 275, 16);
			} else if (p.getLevels().getLevel(Skills.MINING) >= 15) {
				p.getActionSender().modifyText("<str>Level 15 mining is recommended", 275, 16);
			}
		} else if(p.doricsQuestInt == 1) {
			p.getActionSender().modifyText("Doric's Quest", 275, 2);
			p.getActionSender().modifyText("<str>I've spoken with Doric and accepted his request", 275, 12);
			p.getActionSender().modifyText("<col=342D7E>He wants me to gather the following materials:", 275, 13);
			if(p.getInventory().hasItemAmount(435,6)) {
			p.getActionSender().modifyText("<str>6 clay (noted)", 275, 14);
			} else {
			p.getActionSender().modifyText("<col=800517>6 clay (noted)", 275, 14);
			}
			if(p.getInventory().hasItemAmount(437,4)) {
			p.getActionSender().modifyText("<str>4 copper ores (noted)", 275, 15);
			} else {
				p.getActionSender().modifyText("<col=800517>4 copper ores (noted)", 275, 15);	
			}
			if(p.getInventory().hasItemAmount(441,2)) {
			p.getActionSender().modifyText("<str>2 iron ores (noted)", 275, 16);
			} else {
				p.getActionSender().modifyText("<col=800517>2 iron ores (noted)", 275, 16);
			}
		} else if(p.doricsQuestInt == 2) {
			p.getActionSender().modifyText("Doric's Quest", 275, 2);
			p.getActionSender().modifyText("<str>I've spoken with Doric and accepted his request", 275, 12);
			p.getActionSender().modifyText("<col=342D7E>He wants me to gather the following materials:", 275, 13);
			if(p.getInventory().hasItemAmount(435,6)) {
			p.getActionSender().modifyText("<str>6 clay (noted)", 275, 14);
			} else {
			p.getActionSender().modifyText("<col=800517>6 clay (noted)", 275, 14);
			}
			if(p.getInventory().hasItemAmount(437,4)) {
			p.getActionSender().modifyText("<str>4 copper ores (noted)", 275, 15);
			} else {
				p.getActionSender().modifyText("<col=800517>4 copper ores (noted)", 275, 15);	
			}
			if(p.getInventory().hasItemAmount(441,2)) {
			p.getActionSender().modifyText("<str>2 iron ores (noted)", 275, 16);
			} else {
				p.getActionSender().modifyText("<col=800517>2 iron ores (noted)", 275, 16);
			}
		} else if(p.doricsQuestInt == 3) {
			p.getActionSender().modifyText("Doric's Quest", 275, 2);
			p.getActionSender().modifyText("<str>I've spoken with Doric and accepted his request", 275, 12);
			p.getActionSender().modifyText("<str>I've given him the materials he requested", 275, 13);
			p.getActionSender().modifyText("<col=800517>QUEST COMPLETE", 275, 15);
			p.getActionSender().modifyText("<col=342D7E>As a reward, I gained 1300 Mining Experience!", 275, 16);
		}
		p.getActionSender().displayInterface(275);
	}
	
	public static void interactWithDoric(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.doricsQuestInt == 0) {
					showChats(p, 611);
				} else if (p.doricsQuestInt == 1) {
					showChats(p, 618);
				} else if (p.doricsQuestInt == 2) {
					
				}
			}
		});
	}
	
	public static void showChats(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {
		case 611:
			p.getActionSender().sendNpcChat3("Oy der, mate! Care to help a little fella who needs", "some clay and a couple o' ores? There's a good", "reward, there is. So, how 'bout it, mate?", 284, "Doric", 9850);
			newStatus = 612;
			break;
		case 612:
			p.getActionSender().sendOption2("Sure.", "Uh, I'll have to pass for now.");
			newStatus = 613;
			break;
		case 613:
			p.getActionSender().sendPlayerChat1("Sure.", 9847);
			p.doricsQuestInt = 1;
			newStatus = 614;
			break;
		case 614:
			p.getActionSender().sendNpcChat3("Oh, that's great, right 'der, that's great. Now, I", "will be needing some clay, some copper, and", "a couple o' iron ores.", 284, "Doric", 9850);
			newStatus = 615;
			break;
		case 615:
			p.getActionSender().sendPlayerChat1("Alright, I'm on my way.", 9847);
			break;
		case 616:
			p.getActionSender().sendPlayerChat1("Uh, I'll have to pass for now.", 9827);
			newStatus = 615;
			break;
		case 617:
			p.getActionSender().sendNpcChat3("You'll have to pass? Oh I see, it's 'cause I'm", "short, ain't it? You pesky humans need to learn", "some respect for us dwarfs. Get out of my house!", 284, "Doric", 9785);
			break;
		case 618:
			p.getActionSender().sendNpcChat1("So, you got my items, mate?", 284, "Doric", 9850);
			newStatus = 619;
			break;
		case 619:
			if(p.getInventory().hasItemAmount(437, 4) && p.getInventory().hasItemAmount(441, 2) && p.getInventory().hasItemAmount(435, 6)) {
				p.getActionSender().sendPlayerChat1("Here's all the items!", 9850);
				newStatus = 620;
				} else {
					p.getActionSender().sendPlayerChat1("I don't have all the items yet.", 9775);
				newStatus = 622;
				}
			break;
		case 620:
			p.getInventory().deleteItem(437, 4);
			p.getInventory().deleteItem(435, 6);
			p.getInventory().deleteItem(441, 2);
			p.getActionSender().sendNpcChat1("Thanks mate! I owe it to ya!", 284, "Doric", 9750);
			newStatus = 621;
			break;
		case 621:
			p.doricsQuestInt = 3;
			p.getQuests().addQuestPoint(p, 1);
			p.getLevels().addXp(Skills.MINING, 1300);
			p.getInventory().addItem(995, 180);
			p.getQuests().sendQuestCompleted(p);
			p.getQuests().loadList(p);
			p.getActionSender().questCompleted4(1, 1269, "You have completed Doric's Quest!", "1 Quest Point", "1300 Mining XP", "180 coins", "Use of Doric's Anvils");
			break;
		case 622:
			p.getActionSender().sendNpcChat1("Well then go get them, you baffoon!", 284, "Doric", 9785);
			break;
		case 623:
			p.getActionSender().sendNpcChat1("Y'know, you could always use my anvils.", 284, "Doric", 9847);
			break;
		
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}



}
