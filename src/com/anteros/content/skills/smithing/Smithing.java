package com.anteros.content.skills.smithing;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.EnterVariable;

public class Smithing extends SmithingData {

	public Smithing() {
		
	}

	public static boolean wantToSmithOnAnvil(final Player p, int item, Location anvilLoc) {
		for (int i = 0; i < BARS.length; i++) {
			if (item == BARS[i]) {
				final int j = i;
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, anvilLoc.getX() - 1, anvilLoc.getY() - 1, anvilLoc.getX() + 1, anvilLoc.getY() + 1) {

					@Override
					public void run() {
						displaySmithOptions(p, j);
					}
				});
				return true;
			}
		}
		return false;
	}

	private static void displaySmithOptions(Player p, int index) {
		Object[][] variables = getInterfaceVariables(p, index);
		if (variables == null) {
			return;
		}
		p.getActionSender().showChildInterface(300, 65, variables[6][5] != null);	
		p.getActionSender().showChildInterface(300, 81, variables[8][5] != null);
		p.getActionSender().showChildInterface(300, 89, variables[9][5] != null);
		p.getActionSender().showChildInterface(300, 97, variables[10][5] != null);
		p.getActionSender().showChildInterface(300, 161, variables[18][5] != null);
		p.getActionSender().showChildInterface(300, 169, variables[19][5] != null);
		p.getActionSender().showChildInterface(300, 209, variables[24][5] != null);
		p.getActionSender().showChildInterface(300, 266, variables[29][5] != null);
		for (int j = 0; j < variables.length; j++) {

			boolean canSmith = p.getLevels().getLevel(SMITHING) >= (Integer)variables[j][1];
			String barColour = p.getInventory().hasItemAmount(BARS[index], (Integer)variables[j][3]) ? "<col=00FF00>" : "";
			String amount = barColour + variables[j][3];
			String name = (String)variables[j][5];
			String s = (Integer)variables[j][3] > 1 ? "s" : "";
			int child = (Integer)variables[j][6];

			p.getActionSender().itemOnInterface(300, child, (Integer)variables[j][2], (Integer)variables[j][0]);
			p.getActionSender().modifyText(canSmith ? "<col=FFFFFF>" + name : name, 300, (child + 1));
			if (!barColour.equals("")) {
				p.getActionSender().modifyText(barColour + amount + " Bar" + s, 300, (child + 2));
			}
		}
		p.getActionSender().displayInterface(300);
		p.setTemporaryAttribute("smithingBarType", index);
	}
	
	
	public static void smithItem(final Player p, int button, int offset, boolean newSmith) {
		int index = -1;
		if (!newSmith && p.getTemporaryAttribute("smithingItem") == null) {
			return;
		}
		if (newSmith) {
			if (p.getTemporaryAttribute("smithingBarType") == null) {
				return;
			}
			index = (Integer) p.getTemporaryAttribute("smithingBarType");
			if (index == -1) {
				return;
			}
			Object[][] variables = getInterfaceVariables(p, index);
			int amountToMake = -1;
			int item = -1;
			for (int i = 6; i > 0; i--) {
				for (int j = 0; j < variables.length; j++) {
					if ((Integer)variables[j][6] + i == button) {
						offset = i;
						item = j;
						break;
					}
				}
			}
			if (offset == 4) {
				p.getActionSender().displayEnterAmount();
				p.setTemporaryAttribute("interfaceVariable", new EnterVariable(300, button));
			} else
				if (offset == 3) {
				amountToMake = 28;
			} else
				if (offset == 5) {
				amountToMake = 5;
			} else
				if (offset == 6) {
				amountToMake = 1;
			}
			if (offset >= 400) {
				amountToMake = offset - 400;	
			}
			p.setTemporaryAttribute("smithingItem", new SmithBar(BARS[index], (Integer)variables[item][3], (Integer)variables[item][1], (Integer)variables[item][4], (Integer)variables[item][0], (Integer)variables[item][2], amountToMake));
		}
		SmithBar item = (SmithBar) p.getTemporaryAttribute("smithingItem");
		if (!canSmith(p, item)) {
			return;
		}
		p.getActionSender().closeInterfaces();
		p.animate(898);
		for (int i = 0; i < item.getBarAmount(); i++) {
			if (!p.getInventory().deleteItem(item.getBarType())) {
				return;
			}
		}
		if (p.getInventory().addItem(item.getFinishedItem(), item.getFinishedItemAmount())) {
			p.getLevels().addXp(SMITHING, item.getXp());
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {
				@Override
				public void execute() {
					smithItem(p, -1, 1, false);
					this.stop();
				}
			});
		}
	}

	private static boolean canSmith(Player p, SmithBar item) {
		if (p == null || item == null) {
			return false;
		}
		if (item.getAmount() <= 0) {
			return false;
		}
		if (!p.getInventory().hasItem(HAMMER)) {
			p.getActionSender().sendMessage("You need a hammer if you wish to smith.");
			return false;
		}
		if (p.getLevels().getLevel(SMITHING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Smithing level of " + item.getLevel() + " to make that item.");
			return false;
		}
		if (!p.getInventory().hasItemAmount(item.getBarType(), item.getBarAmount())) {
			p.getActionSender().sendMessage("You don't have enough bars to make that item.");
			return false;
		}
		return true;
	}

	private static Object[][] getInterfaceVariables(Player p, int i) {
		switch(i) {
			case 0:
				return BRONZE;
				// 1 is blurite
			case 2:
				return IRON;
				// 3 is silver
			case 4:
				return STEEL;
				// 5 is gold
			case 6:
				return MITHRIL;
				
			case 7:
				return ADAMANT;
				
			case 8:
				return RUNE;
		}
		return null;
	}
	
	public static void resetSmithing(Player p) {
		p.removeTemporaryAttribute("smithingItem");
		p.removeTemporaryAttribute("smithingBarType");
	}
}
