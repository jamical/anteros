package com.anteros.content.quests;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
/**
 * 
 * @author Abexlry
 *
 */
public class DruidicRitual {

	public static void showInformation(Player p) { 
		p.getActionSender().displayInterface(275);
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getQuests().sendQuestInterface(p);
	if (p.druidicRitualInt == 0) {
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getActionSender().modifyText("<col=342D7E>I can start this quest by speaking to</col> <col=800517>Kaqemeex</col> <col=342D7E>who is at", 275, 12);
		p.getActionSender().modifyText("<col=342D7E>the</col> <col=800517>Druids Circle</col> <col=342D7E>just</col> <col=800517>North</col> <col=342D7E>of</col> <col=800517>Taverley</col>", 275, 13);
		p.getActionSender().modifyText("", 275, 14);
	}
	if (p.druidicRitualInt == 1) {
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getActionSender().modifyText("<str>I told Kaqemeex I would help them prepare their cremony.", 275, 12);
		p.getActionSender().modifyText("<col=342D7E>I should speak to</col> <col=800517>Sanfew</col> <col=342D7E>in the village to the</col> <col=800517>South</col>", 275, 13);
		p.getActionSender().modifyText("", 275, 14);
	}
	if (p.druidicRitualInt == 2) {
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getActionSender().modifyText("<str>I told Kaqemeex I would help them prepare their cremony.", 275, 12);
		p.getActionSender().modifyText("<col=342D7E>The ceremony requires various meats being placed in the", 275, 13);
		p.getActionSender().modifyText("<col=800517>Cauldron of Thunder. <col=342D7E>somewhere to the</col> <col=800517>South</col>", 275, 14);
		p.getActionSender().modifyText("", 275, 1);
	}
	if (p.druidicRitualInt == 3) {
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getActionSender().modifyText("<str>I told Kaqemeex I would help them prepare their cremony.", 275, 12);
		p.getActionSender().modifyText("<str>The ceremony required various meats being placed in the", 275, 13);
		p.getActionSender().modifyText("<str>Cauldron of Thunder. I did this and gave them to Sanfew.", 275, 14);
		p.getActionSender().modifyText("<col=342D7E>I should speak to</col> <col=800517>Kaqemeex</col> <col=342D7E>again and claim my</col> <col=800517>reward</col", 275, 15);
	}
	if (p.druidicRitualInt == 4) {
		p.getActionSender().modifyText("Druidic Ritual", 275, 2);
		p.getActionSender().modifyText("<str>I told Kaqemeex I would help them prepare their cremony.", 275, 12);
		p.getActionSender().modifyText("<str>The ceremony required various meats being placed in the", 275, 13);
		p.getActionSender().modifyText("<str>Cauldron of Thunder. I did this and gave them to Sanfew.", 275, 14);
		p.getActionSender().modifyText("<str>Kaqemeex then taught me the basics of the skill Herblore.", 275, 15);
		p.getActionSender().modifyText("", 275, 16);
		p.getActionSender().modifyText("<col=800517>QUEST COMPLETE", 275, 17);
	}
	p.getActionSender().displayInterface(275);
}
	
	public static void interactWithKaqemeex(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.druidicRitualInt == 0 || p.druidicRitualInt == 1 || p.druidicRitualInt == 3 || p.druidicRitualInt == 4) {
					showChats(p, 641);
				} 
			}
		});
	}
	
	public static void interactWithSanfew(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.druidicRitualInt == 1 || p.druidicRitualInt == 0 || p.druidicRitualInt == 4 || p.druidicRitualInt == 3) {
					showChats(p, 657);
				} else if (p.druidicRitualInt == 2) {
					showChats(p, 665);
				}
			}
		});
	}
	
	public static void showChats(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {
		//Kaqemeex
		case 641:
			p.getActionSender().sendPlayerChat1("Hello there.", Dialogue.DEFAULT_HAPPY);
			if (p.druidicRitualInt == 0) {
				newStatus = 642;
			} else if (p.druidicRitualInt == 1 || p.druidicRitualInt == 2) {
				newStatus = 655;
			} else if (p.druidicRitualInt == 3) {
				newStatus = 670;
			} else if (p.druidicRitualInt == 4) {
				newStatus = 690;
			}
			break;
		case 642:
			p.getActionSender().sendNpcChat1("What brings you to our holy monument?", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 643;
			break;
		case 643:
			p.getActionSender().sendOption3("Who are you?", "I'm in search of a quest.", "Did you build this?");
			newStatus = 644;
			break;
		case 644:
			p.getActionSender().sendPlayerChat1("Who are you?", Dialogue.THINKING);
			newStatus = 645;
			break;
		case 645:
			p.getActionSender().sendNpcChat3("We are the druids of Guthix. We worship our god at", "our famous stone circles. You will find them located", "throughout these lands.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 646;
			break;
		case 646:
			p.getActionSender().sendOption3("What about the stone circle full of dark wizards?", "So, what's so good about Guthix?", "Well, I'll be on my way now.");
			newStatus = 647;
			break;
		case 647:
			p.getActionSender().sendPlayerChat1("What about the stone circle full of dark wizards?", Dialogue.THINKING);
			newStatus = 648;
			break;
		case 648:
			p.getActionSender().sendNpcChat4("That used to be OUR stone circle. Unfortunately,", "many many years ago, dark wizards cast a wicked spell", "upon it so that they could corrupt its power for their", "own evil ends.", 455, "Kaqemeex", Dialogue.ANGRY_MILD);
			newStatus = 649;
			break;
		case 649:
			p.getActionSender().sendNpcChat4("When they cursed the rocks for their rituals they made", "them useless to us and our magics. We require a brave", "adventurer to go on a quest for us to help purify the", "circle of Varrock.", 455, "Kaqemeex", Dialogue.ANGRY_MILD);
			newStatus = 650;
			break;
		case 650:
			p.getActionSender().sendOption3("Ok I will try and help.", "No, that doesn't sound very interesting.", "So... is there anything in this for me?");
			newStatus = 651;
			break;
		case 651:
			p.getActionSender().sendPlayerChat1("Ok, I will try and help", Dialogue.HAPPY_JOYFUL);
			newStatus = 652;
			break;
		case 652:
			p.getActionSender().sendNpcChat4("Excellent. Go to the village south of this place and speak", "to my fellow Sanfew who is working on the purification", "ritual. He knows better than I what is required to", "complete it.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			p.druidicRitualInt = 1;
			newStatus = 653;
			break;
		case 653:
			p.getActionSender().sendPlayerChat1("Will do.", Dialogue.DEFAULT_HAPPY);
			newStatus = -1;
			break;
		case 654:
			p.getActionSender().sendNpcChat3("We require a brave", "adventurer to go on a quest for us to help purify the", "circle of Varrock.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 650;
			break;
		case 655:
			p.getActionSender().sendNpcChat1("Hello again.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 656;
			break;
		case 656:
			p.getActionSender().sendNpcChat3("You will need to speak to my fellow druid Sanfew in", "the village south of here to continue in your quest", "adventurer.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = -1;
			break;
			//Sanfew
		case 657:
			p.getActionSender().sendNpcChat1("What can I do for you young 'un?", 454, "Sanfew", Dialogue.CALM_TALK);
			if (p.druidicRitualInt == 1) {
				newStatus = 658;
			} else {
				newStatus = 683;
			}
			break;
		case 658: 
			if (p.druidicRitualInt > 0) {
				p.getActionSender().sendOption2("I've been sent to help purify the Varrock stone circle.", "Actually, I don't need to speak to you.");
				newStatus = 659;
			}
			break;
		case 659:
			p.getActionSender().sendPlayerChat2("I've been sent to assist you with the ritual to purify the", "Varrockian stone circle.", Dialogue.CALM_TALK);
			newStatus = 660;
			break;
		case 660:
			p.getActionSender().sendNpcChat4("Well, what I'm struggling with right now is the meats", "needed for the potion to honour Guthix. I need the raw", "meat of four different animals for it, but not just any", "old meats will do.", 454, "Sanfew", Dialogue.CALM_TALK);
			newStatus = 661;
			break;
		case 661:
			p.getActionSender().sendNpcChat2("Each meat has to be dipped individually into the", "Cauldron of Thunder for it to work correctly.", 454, "Sanfew", Dialogue.CALM_TALK);
			newStatus = 662;
			break;
		case 662:
			p.getActionSender().sendOption2("Where can I find this cauldron?", "Ok, I'll do that then.");
			newStatus = 663;
			break;
		case 663:
			p.getActionSender().sendPlayerChat1("Where can I find this cauldron?", Dialogue.THINKING);
			newStatus = 664;
			break;
		case 664:
			p.getActionSender().sendNpcChat4("It is located somewhere in the mysterious underground", "halls which are located somewhere in the woods just", "South of here. They are too dangerous for me to go", "myself however.", 454, "Sanfew", Dialogue.CALM_TALK);
			if (p.druidicRitualInt == 1) {
				newStatus = 662;
			} else {
				newStatus = 688;
			}
			break;
			//STILL SANFEW
		case 665:
			p.getActionSender().sendNpcChat2("Did you bring me the required ingredients for the", "potion?", 454, "Sanfew", Dialogue.CALM_TALK);
			if (p.getInventory().hasItem(522) && p.getInventory().hasItem(523) && p.getInventory().hasItem(524) && p.getInventory().hasItem(525)) {
				newStatus = 666; //has meats
			} else {
				newStatus = 686;
			}
			break;
		case 666:
			p.getActionSender().sendPlayerChat1("Yes, I have all four now!", Dialogue.HAPPY_JOYFUL);
			newStatus = 667;
			break;
		case 667:
			p.getActionSender().sendNpcChat1("Well hand 'em over then lad!", 454, "Sanfew", Dialogue.DEFAULT_HAPPY);
			newStatus = 668;
			break;
		case 668:
			p.getActionSender().sendNpcChat3("Thanks you so much adventurer! These meats will allow", "our potion to honour Guthix to be completed, and bring", "one step closer to reclaiming our stone circle!", 454, "Sanfew", Dialogue.DEFAULT_HAPPY);
			newStatus = 669;
			break;
		case 669:
			p.getActionSender().sendNpcChat3("Now go and talk to Kaqemeex and he will introduce", "you to the wonderful world of herblore and potion", "making!", 454, "Sanfew", Dialogue.DEFAULT_HAPPY);
			p.getInventory().deleteItem(522);
			p.getInventory().deleteItem(523);
			p.getInventory().deleteItem(524);
			p.getInventory().deleteItem(525);
			p.druidicRitualInt = 3;
			newStatus = -1;
			break;
			//BACK TO KAQEMEEX
		case 670:
			p.getActionSender().sendNpcChat4("I have word from Sanfew that you have been very", "helpful in assisting him with his preparations for the", "purification ritual. As promised I will now teach you the", "ancient arts of Herblore.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 671;
			break;
		case 671:
			p.druidicRitualInt = 4;
			p.getQuests().addQuestPoint(p, 4);
			p.getQuests().sendQuestCompleted(p);
			p.getQuests().loadList(p);
			p.getLevels().addXp(Skills.HERBLORE, 250);
			p.getActionSender().questCompleted3(4, 249, "You have completed the Druidic Ritual quest!", "4 Quest Points", "250 Herblore XP", "Access to Herblore skill");
			break;
		case 672:
			p.getActionSender().sendPlayerChat1("I'm in search of a quest.", Dialogue.HAPPY_JOYFUL);
			newStatus = 654;
			break;
		case 673:
			p.getActionSender().sendPlayerChat1("Did you build this?", Dialogue.THINKING);
			newStatus = 674;
			break;
		case 674:
			p.getActionSender().sendNpcChat2("This altar was made for Guthix, do you have", "any other questions?", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 643;
			break;
		case 675:
			p.getActionSender().sendPlayerChat1("So what's so good about Guthix?", Dialogue.THINKING);
			newStatus = 676;
			break;
		case 676:
			p.getActionSender().sendNpcChat2("Guthix is the God of Balance, which is what", "we seek after.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 646;
			break;
		case 677:
			p.getActionSender().sendPlayerChat1("Well, I'll be on my way now.", Dialogue.CALM_TALK_BARELY_MOVING);
			newStatus = 678;
			break;
		case 678:
			p.getActionSender().sendNpcChat1("Farewell brave adventurer.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = -1;
			break;
		case 679:
			p.getActionSender().sendPlayerChat1("No, that doesn't sound very interesting.", Dialogue.SAD);
			newStatus = 680;
			break;
		case 680:
			p.getActionSender().sendNpcChat1("Please come back if you change your mind.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = -1;
			break;
		case 681:
			p.getActionSender().sendPlayerChat1("So... is there anything in this for me?", Dialogue.THINKING);
			newStatus = 682;
			break;
		case 682:
			p.getActionSender().sendNpcChat2("Why yes of course, I would show you", "an ancient practice called Herblore." , 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = 650;
			break;
		case 683:
			p.getActionSender().sendPlayerChat1("Actually, I don't need to speak to you.", Dialogue.CALM_TALK_BARELY_MOVING);
			newStatus = -1;
			break;
		case 684:
			p.getActionSender().sendPlayerChat1("Ok, I'll do that then.", Dialogue.DEFAULT_HAPPY);
			p.druidicRitualInt = 2;
			newStatus = 685;
			break;
		case 685:
			p.getActionSender().sendNpcChat1("Great! Return to me when you have the items I require.", 454, "Sanfew", Dialogue.CALM_TALK);
			newStatus = -1;
			break;
		case 686:
			p.getActionSender().sendPlayerChat1("No, I haven't got the items which you asked for.", Dialogue.SAD);
			newStatus = 687;
			break;
		case 687:
			p.getActionSender().sendNpcChat2("I need that enchanted meat for the ritual, it", "can't go on without it!", 454, "Sanfew", Dialogue.CALM_TALK);
			newStatus = 688;
			break;
		case 688:
			p.getActionSender().sendOption2("Where can I find this cauldron?", "I'll be on my way then.");
			newStatus = 689;
			break;
		case 689:
			p.getActionSender().sendPlayerChat1("I'll be on my way then.", Dialogue.CALM_TALK_BARELY_MOVING);
			newStatus = -1;
			break;
		case 690:
			p.getActionSender().sendNpcChat1("Hello again.", 455, "Kaqemeex", Dialogue.CALM_TALK);
			newStatus = -1;
			break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
		}
	
	public static void itemOnCauldron(Player player, int slot) {
		if (player.getInventory().getItemInSlot(slot) == 2132) {
			player.getInventory().deleteItem(2132, slot, 1);
			player.getInventory().addItem(522, 1);
		}
		if (player.getInventory().getItemInSlot(slot) == 2134) {
			player.getInventory().deleteItem(2134, slot, 1);
			player.getInventory().addItem(523, 1);
		}
		if (player.getInventory().getItemInSlot(slot) == 2136) {
			player.getInventory().deleteItem(2136, slot, 1);
			player.getInventory().addItem(524, 1);
		}
		if (player.getInventory().getItemInSlot(slot) == 2138) {
			player.getInventory().deleteItem(2138, slot, 1);
			player.getInventory().addItem(525, 1);
		}
	}
	
}
