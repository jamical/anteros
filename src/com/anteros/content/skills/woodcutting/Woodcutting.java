package com.anteros.content.skills.woodcutting;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;
import com.anteros.world.GroundItem;

public class Woodcutting extends WoodcuttingData {

	public Woodcutting() {

	}

	public static void tryCutTree(final Player p, final int treeId, final Location treeLocation, final int i, final boolean newCut) {
		int index = getTreeIndex(treeId);
		if (index == -1) {
			return;
		}
		int s = TREE_SIZE[index];
		final int dis = s;
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, treeLocation.getX() - s, treeLocation.getY() - s, treeLocation.getX() + s, treeLocation.getY() + s) {
			@Override
			public void run() {
				cutTree(p, treeId, treeLocation, i, newCut, dis);
			}
		});
	}

	public static int getTreeIndex(int id) {
		for (int i = 0; i < TREES.length; i++) {
			if (id == TREES[i]) {
				return i;
			}
		}
		return -1;
	}

	public static void cutTree(final Player p, int treeId, final Location treeLocation, int i, boolean newCut, int distance) {
		if (!newCut && p.getTemporaryAttribute("cuttingTree") == null) {
			return;
		}
		if (newCut) {
			if (i == 10 || i == 11) { // Magic or Yew tree.
				if (!World.getInstance().getObjectLocations().objectExists(treeId, treeLocation)) {
					// Logger.getInstance().info(p.getUsername() +
					// " tried to cut a non existing Magic or Yew tree!");
					// return;
				}
			}
			Tree newTree = new Tree(i, treeId, treeLocation, LOGS[i], LEVEL[i], TREE_NAME[i], XP[i], distance);
			p.setTemporaryAttribute("cuttingTree", newTree);
		}
		final Tree treeToCut = (Tree) p.getTemporaryAttribute("cuttingTree");
		if (!canCut(p, treeToCut, null)) {
			resetWoodcutting(p);
			return;
		}
		if (newCut) {
			p.animate(getAxeAnimation(p));
			p.setFaceLocation(treeLocation);
			p.getActionSender().sendMessage("You begin to swing your axe at the tree..");
		}
		int delay = getCutTime(p, treeToCut.getTreeIndex());
		World.getInstance().registerEvent(new Event(delay) {
			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("cuttingTree") == null) {
					resetWoodcutting(p);
					return;
				}
				final Tree tree = (Tree) p.getTemporaryAttribute("cuttingTree");
				if (!canCut(p, treeToCut, tree)) {
					resetWoodcutting(p);
					return;
				}
				World.getInstance().getGlobalObjects().lowerHealth(tree.getTreeId(), tree.getTreeLocation());
				if (!World.getInstance().getGlobalObjects().originalObjectExists(tree.getTreeId(), tree.getTreeLocation())) {
					resetWoodcutting(p);
					p.animate(65535);
				}
				if (p.getInventory().addItem(tree.getLog())) {
					p.getActionSender().closeInterfaces();
					int index = tree.getTreeIndex();
					String s = index == 1 || index == 3 || index == 8 ? "an" : "a";
					p.getLevels().addXp(WOODCUTTING, tree.getXp());
					if (index == 6) {
						p.getActionSender().sendMessage("You retrieve some Hollow bark from the tree.");
					} else {
						p.getActionSender().sendMessage("You cut down " + s + " " + tree.getName() + " log.");
					}
					if (Misc.random(100) == 0) {
						int nestId = Misc.random(10) < 5 ? 5073 : 5074;
						GroundItem g = new GroundItem(nestId, 1, Location.location(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ()), p);
						World.getInstance().getGroundItems().newEntityDrop(g);
						p.getActionSender().sendMessage("Something falls out of the tree and lands at your feet.");
					}
				}
				cutTree(p, tree.getTreeId(), tree.getTreeLocation(), tree.getTreeIndex(), false, tree.getDistance());
			}
		});
		final int j = delay;
		if (delay >= 2550) {
			World.getInstance().registerEvent(new Event(2550) {
				int time = j;

				@Override
				public void execute() {
					time -= 2550;
					if (time <= 0) {
						this.stop();
					}
					final Tree tree = (Tree) p.getTemporaryAttribute("cuttingTree");
					if (!canCut(p, treeToCut, tree)) {
						this.stop();
						return;
					}
					p.setFaceLocation(treeLocation);
					p.animate(getAxeAnimation(p));
				}
			});
		}
	}

	private static boolean canCut(Player p, Tree tree, Tree tree2) {
		if (tree == null || p == null || !World.getInstance().getGlobalObjects().originalObjectExists(tree.getTreeId(), tree.getTreeLocation())) {
			return false;
		}
		if (!p.getLocation().withinDistance(tree.getTreeLocation(), tree.getDistance())) {
			return false;
		}
		if (tree2 != null) {
			if (!tree.equals(tree2)) {
				return false;
			}
		}
		if (p.getLevels().getLevel(WOODCUTTING) < tree.getLevel()) {
			p.getActionSender().sendMessage("You need a Woodcutting level of " + tree.getLevel() + " to cut that tree.");
			return false;
		}
		if (!hasAxe(p)) {
			p.getActionSender().sendMessage("You need an axe to cut down a tree!");
			return false;
		}
		if (!hasAxeLevel(p)) {
			p.getActionSender().sendMessage("You do not have a axe with the woodcutting level to use!");
			return false;
		}
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendChatboxInterface(210);
			p.getActionSender().modifyText("Your inventory is too full to carry any logs.", 210, 1);
			return false;
		}
		return true;
	}

	public static int getCutTime(Player p, int i) {
		int standardTime = TREE_DELAY[i];
		int randomTime = standardTime + Misc.random(standardTime);
		int levelTime = getLevelDelayTime(p);
		int axeTime = AXE_DELAY[getUsedAxe(p)];
		int finalTime = (randomTime -= axeTime) + levelTime;
		if (finalTime <= 1000 || finalTime <= 0) {
			finalTime = 1000;
		}
		return finalTime;
	}

	public static int getLevelDelayTime(Player p) {
		if (p.getLevels().getLevel(WOODCUTTING) < 10 && p.getLevels().getLevel(WOODCUTTING) > 0) {
			return LEVEL_TIME[0];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 10 && p.getLevels().getLevel(WOODCUTTING) < 20) {
			return LEVEL_TIME[1];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 20 && p.getLevels().getLevel(WOODCUTTING) < 30) {
			return LEVEL_TIME[2];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 30 && p.getLevels().getLevel(WOODCUTTING) < 40) {
			return LEVEL_TIME[3];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 40 && p.getLevels().getLevel(WOODCUTTING) < 50) {
			return LEVEL_TIME[4];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 50 && p.getLevels().getLevel(WOODCUTTING) < 60) {
			return LEVEL_TIME[5];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 60 && p.getLevels().getLevel(WOODCUTTING) < 70) {
			return LEVEL_TIME[6];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 70 && p.getLevels().getLevel(WOODCUTTING) < 80) {
			return LEVEL_TIME[7];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 80 && p.getLevels().getLevel(WOODCUTTING) < 90) {
			return LEVEL_TIME[8];
		}
		if (p.getLevels().getLevel(WOODCUTTING) >= 90 && p.getLevels().getLevel(WOODCUTTING) < 99) {
			return LEVEL_TIME[9];
		}
		if (p.getLevels().getLevel(WOODCUTTING) == 99) {
			return LEVEL_TIME[10];
		}

		else {
			return 0;
		}

	}

	/**
	 * Checks if the {@link Player} has the appropriate axe.
	 * 
	 * @param player
	 *            The {@link Player}.
	 * @return If there is no appropriate axe.
	 */
	private static boolean hasAxeLevel(final Player player) {
		for (int x = 0; x < AXES.length; x++) {
			if (player.getEquipment().getItemInSlot(3) == AXES[x] || player.getInventory().hasItem(AXES[x]) && player.getLevels().getLevel(WOODCUTTING) >= AXE_LVL[x]) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasAxe(Player p) {
		for (int i = 0; i < AXES.length; i++) {
			if (p.getEquipment().getItemInSlot(3) == AXES[i] || p.getInventory().hasItem(AXES[i])) {
				return true;
			}
		}
		return false;
	}

	public static void randomNestItem(Player p, int nest) {
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendMessage("You need at least 1 free inventory spot to search through a nest.");
			return;
		}
		int[] items = nest == 5073 ? NEST_SEEDS : NEST_JEWELLERY;
		int reward = items[(int) (Math.random() * items.length)];
		if (nest == 5073) {
			if (Misc.random(50) == 0) {
				reward = 1645;
			}
		}
		if (p.getInventory().replaceSingleItem(nest, 5075)) {
			if (reward != 0) {
				p.getInventory().addItem(reward);
			}
		}
	}

	public static boolean crushNest(final Player player, int itemUsed, int usedWith) {
		final int used = itemUsed;
		final int with = usedWith;
		if (used != 233 && with != 5075) {
			return false;
		}
		if (player.getInventory().hasItem(5075) && ((used == 233 && with == 5075) || (used == 5075 && with == 233))) {
			player.animate(364);
			player.getInventory().deleteItem(5075, 1);
			player.getInventory().addItem(6693, 1);
			World.getInstance().registerEvent(new Event(750) {
				@Override
				public void execute() {
					crushNest(player, used, with);
					stop();
				}
			});

		}
		return true;
	}

	public static int getUsedAxe(Player p) {
		int index = -1;
		for (int i = 0; i < AXES.length; i++) {
			if (p.getEquipment().getItemInSlot(3) == AXES[i] || p.getInventory().hasItem(AXES[i])) {
				index = i;
			}
		}
		return index;
	}

	public static int getAxeAnimation(Player p) {
		int axe = getUsedAxe(p);
		if (axe == -1) {
			return 65535;
		}
		return AXE_ANIMATION[axe];
	}

	public static void resetWoodcutting(Player p) {
		p.removeTemporaryAttribute("cuttingTree");
	}
}
