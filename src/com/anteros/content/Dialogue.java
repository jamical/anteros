package com.anteros.content;

import com.anteros.content.areas.AlKharid;
import com.anteros.content.areas.BoatOptions;
import com.anteros.content.events.StartingEvent;
import com.anteros.content.holiday.Easter;
import com.anteros.content.minigames.agilityarena.AgilityArena;
import com.anteros.content.minigames.barrows.BrokenBarrows;
import com.anteros.content.minigames.warriorguild.WarriorGuild;
import com.anteros.content.quests.DoricsQuest;
import com.anteros.content.quests.DruidicRitual;
import com.anteros.content.quests.LearningTheRopes;
import com.anteros.content.quests.RuneMysteries;
import com.anteros.model.player.Player;

public class Dialogue {

	public Dialogue() {
		
	}
	
	private static int interactingNpcId;
	
	/*
	 * Some chat head id's
	 */
	public static int HAPPY_JOYFUL = 9847;
	public static int CALM_TALK_BARELY_MOVING = 9808;
	public static int CALM_TALK = 9760;
	public static int DEFAULT_HAPPY = 9845;
	public static int ANNOYED = 9832;
	public static int WORRIED = 9770;
	public static int DISBELIEF = 9775;
	public static int CRYING = 9765;
	public static int BOWS_HEAD_SAD = 9768;
	public static int EVIL = 9796;
	public static int LAUGH_LISTEN = 9840;
	public static int LAUGH_GOOFY = 9851;
	public static int SAD = 9764;
	public static int ANGRY_MILD = 9784;
	public static int ANGRY = 9788;
	public static int VERY_ANGRY = 9792;
	public static int THINKING = 9828;
	public static int NOT_TALKING = 9804;
	
	public static void doDialogue(final Player p, int status) {
		if (status > 0 && status < 76) {
			AgilityArena.doDialogue(p, status);
		} else if (status > 76 && status < 100) {
			WarriorGuild.talkToKamfreena(p, status);
		} else if (status > 100 && status < 125) {
			BrokenBarrows.showBobDialogue(p, status);
		} else if (status > 125 && status < 150) {
			AlKharid.payToll(p, status);
		} else if (status > 150 && status < 239) {
			LearningTheRopes.showChats(p, status);
		} else if (status > 239 && status < 270) {
			BoatOptions.showBentleyDialogue(p, status);
		} else if (status > 270 && status < 279) {
			BoatOptions.showTobLorThrDialogue(p, getInteractingNpcId(), status);
		} else if (status > 279 && status < 300) {
			BoatOptions.showCanifisSailorDialogue(p, status);
		} else if (status > 299 && status < 330) {
			BoatOptions.showJarvaldDialogue(p, status);
		} else if (status > 330 && status < 340) {
			Easter.showChats(p, status);
		} else if (status > 339 && status < 360) {
			BoatOptions.showSquireDialogue(p, status);
		} else if (status > 370 && status < 400) {
			BoatOptions.showArnorDialogue(p, status);
		} else if (status > 410 && status < 430) {
			BoatOptions.showCaptainBarnabyDialogue(p, status);
		} else if (status > 600 && status < 610) {
			StartingEvent.showStartingChat(p, status);
		} else if (status > 610 && status < 625) {
			DoricsQuest.showChats(p, status);
		} else if (status > 625 && status < 640) {
			RuneMysteries.showChats(p, status);
		} else if (status > 640 && status < 691) {
			DruidicRitual.showChats(p, status);
		} else if (status > 749 && status < 800) {
			WorldDialogue.getNpcDialogue(p, getInteractingNpcId(), status);
		}
	}

	public static int getInteractingNpcId() {
		return interactingNpcId;
	}

	public static void setInteractingNpcId(Player p, int interactingNpcId) {
		Dialogue.interactingNpcId = interactingNpcId;
	}
}
