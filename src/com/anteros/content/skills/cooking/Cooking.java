package com.anteros.content.skills.cooking;

import com.anteros.content.skills.SkillItem;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.ItemDefinition;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class Cooking extends CookingData {

	public Cooking() {
		
	}
	
	public static boolean isCooking(final Player p, int item, boolean fire, final int fireX, final int fireY) {
		for (int i = 0; i < MEAT_RAW.length; i++) {
			if (item == MEAT_RAW[i]) {
				if (fire) {
					final int j = i;
					World.getInstance().registerCoordinateEvent(new AreaEvent(p, fireX-1, fireY-1, fireX+1, fireY+1) {

						@Override
						public void run() {
							p.setFaceLocation(Location.location(fireX, fireY, p.getLocation().getZ()));
							if (World.getInstance().getGlobalObjects().fireExists(Location.location(fireX, fireY, 0))) {
								setCookingItem(p, null);
								p.setTemporaryAttribute("cookingFireLocation", Location.location(fireX, fireY, p.getLocation().getZ()));
								displayCookOption(p, j);
							}
						}
							
					});
					return true;
				}
				setCookingItem(p, null);
				displayCookOption(p, i);
				return true;
			}
		}
		for (int i = 0; i < MEAT_COOKED.length; i++) {
			if (item == MEAT_COOKED[i]) {
				if (fire) {
					final int j = i;
					World.getInstance().registerCoordinateEvent(new AreaEvent(p, fireX-1, fireY-1, fireX+1, fireY+1) {

						@Override
						public void run() {
							p.setFaceLocation(Location.location(fireX, fireY, p.getLocation().getZ()));
							if (World.getInstance().getGlobalObjects().fireExists(Location.location(fireX, fireY, 0))) {
								p.getInventory().replaceSingleItem(MEAT_COOKED[j], MEAT_BURNT[j]);
								p.getActionSender().sendMessage("You accidentally burn the " + ItemDefinition.forId(MEAT_COOKED[j]).getName() + ".");
								p.animate(883);
							}
						}
							
					});
					return true;
				}
				setCookingItem(p, null);
				p.getInventory().replaceSingleItem(MEAT_COOKED[i], MEAT_BURNT[i]);
				p.getActionSender().sendMessage("You accidentally burn the " + ItemDefinition.forId(MEAT_COOKED[i]).getName() + ".");
				p.animate(883);
				return true;
			}
		}
		return false;
		
	}
	
	public static void cookItem(final Player p, int amount, boolean newCook, final boolean fire) {
		SkillItem item = null;
		if (newCook) {
			if ((Integer) p.getTemporaryAttribute("meatItem") == null) {
				return;
			}
			int i = (Integer) p.getTemporaryAttribute("meatItem");
			p.setTemporaryAttribute("cookCycles", 0);
			item = new SkillItem(MEAT_COOKED[i], MEAT_RAW[i], MEAT_BURNT[i], MEAT_LEVEL[i], COOKING, MEAT_XP[i], amount);
			setCookingItem(p, item);
		}
		item = (SkillItem) getCookingItem(p);
		if (item == null || p == null || !p.getInventory().hasItem(item.getItemOne())) {
			return;
		}
		if (fire) {
			if (p.getTemporaryAttribute("cookingFireLocation") == null) {
				setCookingItem(p, null);
				p.getActionSender().closeInterfaces();
				return;
			} else {
				Location fireLocation = (Location) p.getTemporaryAttribute("cookingFireLocation");
				if (!World.getInstance().getGlobalObjects().fireExists(Location.location(fireLocation.getX(), fireLocation.getY(), fireLocation.getZ()))) {
					p.getActionSender().sendMessage("The fire has burnt out..");
					setCookingItem(p, null);
					p.getActionSender().closeInterfaces();
					return;
				}
			}
		}
		if (p.getLevels().getLevel(COOKING) < item.getLevel()) {
			p.getActionSender().closeInterfaces();
			p.getActionSender().sendMessage("You need a Cooking level of " + item.getLevel() + " to cook that.");
			return;
		}
		boolean burn = getFormula(p, item);
		int newFood = burn ? item.getItemTwo() : item.getFinishedItem();
		String message = burn ? "accidentally burn" : "successfully cook";
		double xp = burn ? 0 : item.getXp();
		int cookCycles = p.getTemporaryAttribute("cookCycles") == null ? 1 : (Integer)p.getTemporaryAttribute("cookCycles");
		if (p.getInventory().replaceSingleItem(item.getItemOne(), newFood)) {
			if (cookCycles >= 1) {
				p.animate(fire ? 897 : 896);
				p.setTemporaryAttribute("cookCycles", 0);
			} else {
				p.setTemporaryAttribute("cookCycles", cookCycles + 1);
			}
			p.getLevels().addXp(COOKING, xp);
			p.getActionSender().sendMessage("You " + message + " the " + ItemDefinition.forId(item.getFinishedItem()).getName() + ".");
			item.decreaseAmount();
			p.getActionSender().closeInterfaces();
		}
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {
				@Override
				public void execute() {
					cookItem(p, -1, false, fire);
					this.stop();
				}
			});
		}
	}

	private static boolean getFormula(Player p, SkillItem item) {
		int foodLevel = item.getLevel();
		int cookLevel = p.getLevels().getLevel(COOKING);
		if (!wearingCookingGauntlets(p)) {
			return Misc.random(cookLevel) <= Misc.random((int) (foodLevel / 1.5));
		}
		return Misc.random(cookLevel) <= Misc.random(foodLevel / 3);
	}

	private static boolean wearingCookingGauntlets(Player p) {
		return p.getEquipment().getItemInSlot(9) == COOKING_GAUNTLETS;
	}

	private static void displayCookOption(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getActionSender().sendChatboxInterface(307);
		p.getActionSender().itemOnInterface(307, 2, 150, MEAT_COOKED[index]);
		p.getActionSender().modifyText(s + ItemDefinition.forId(MEAT_COOKED[index]).getName(), 307, 6);
		p.setTemporaryAttribute("meatItem", index);	
	}

	private static void resetAllCookingVariables(Player p) {
		p.removeTemporaryAttribute("meatItem");
		p.removeTemporaryAttribute("cookingFireLocation");
	}

	public static void setCookingItem(Player p, Object a) {
		if (a == null) {
			resetAllCookingVariables(p);
			p.removeTemporaryAttribute("cookingItem");
			return;
		}
		p.setTemporaryAttribute("cookingItem", (Object) a);
	}
	
	public static Object getCookingItem(Player p) {
		return (Object) p.getTemporaryAttribute("cookingItem");
	}
}
