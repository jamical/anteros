package com.anteros.content.skills.smithing;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.ShopSession;
import com.anteros.util.EnterVariable;
import com.anteros.util.Misc;

public class Smelting extends SmithingData {
	
	public Smelting() {
		
	}
	
	public static void walkToFurnace(final Player p, int objectX, int objectY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, objectX -1, objectY -1, objectX + 1, objectY + 1) {

			@Override
			public void run() {
				displaySmeltOptions(p);
			}
		});
	}

	public static void displaySmeltOptions(Player p) {
		int gfxChild = 4;
		int textChild = 16;
		String s = "<br><br><br><br>";
		for (int i = 0 ; i < BARS.length; i++) {
			boolean enabled = p.getLevels().getLevel(SMITHING) >= SMELT_LEVELS[i];
			String colour = enabled ? "<col=000000>" : "<col=A00000>";
			p.getActionSender().itemOnInterface(311, gfxChild, 130, BARS[i]);
			p.getActionSender().modifyText(s + colour + BAR_NAMES[i], 311, textChild);
			gfxChild++;
			textChild += 4;
		}
		p.getActionSender().sendChatboxInterface(311);
	}

	public static void smeltOre(final Player p, int buttonId, boolean newSmelt, int amount) {
		p.getActionSender().closeInterfaces();
		int index = -1;
		if (!newSmelt && p.getTemporaryAttribute("smeltingBar") == null) {
			return;
		}
		if (newSmelt) {
			resetSmelting(p);
			index = getBarToMake(buttonId);
			if (index == -1) {
				return;
			}
			if (amount == -1) {
				amount =  getAmount(buttonId);
			}
			if (amount == -2) {
				p.getActionSender().displayEnterAmount();
				p.setTemporaryAttribute("interfaceVariable", new EnterVariable(311, buttonId));
				return;
			}
			BarToSmelt bar = new BarToSmelt(index, BARS[index], SMELT_LEVELS[index], SMELT_XP[index], amount, SMELT_ORES[index], SMELT_ORE_AMT[index], BAR_NAMES[index]);
			p.setTemporaryAttribute("smeltingBar", bar);
		}
		final BarToSmelt bar = (BarToSmelt) p.getTemporaryAttribute("smeltingBar");
		if (!canSmelt(p, bar)) {
			resetSmelting(p);
			return;
		}
		for (int i = 0; i < bar.getOre().length; i++) {
			if (bar.getOreAmount()[i] != 0) {
				for (int j = 0; j < bar.getOreAmount()[i]; j++) {
					if (!p.getInventory().deleteItem(bar.getOre()[i], 1)) {
						resetSmelting(p);
						return;
					}
				}
			}
		}
		String s = bar.getOre()[1] == 0 ? "ore" : "ore and coal";
		p.getActionSender().sendMessage("You place the " + bar.getName().toLowerCase()+ " " + s + " into the furnace..");
		p.animate(3243);
		World.getInstance().registerEvent(new Event(1800) {
			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("smeltingBar") == null) {
					resetSmelting(p);
					return;
				}
				final BarToSmelt myBar = (BarToSmelt) p.getTemporaryAttribute("smeltingBar");
				if (!bar.equals(myBar)) {
					resetSmelting(p);
					return;
				}
				boolean dropIron = false;
				if (bar.getIndex() == 2) {
					if (!wearingForgingRing(p)) {
						if (Misc.random(1) == 0) {
							p.getActionSender().sendMessage("You accidentally drop the iron ore deep into the furnace..");
							dropIron = true;
						}
					} else {
						lowerForgingRing(p);
					}
				}
				if (!dropIron) {
					String s1 = bar.getIndex() == 2 || bar.getIndex() == 8 ? "an" : "a";
					p.getActionSender().sendMessage("You retrieve " + s1 + " " + bar.getName().toLowerCase() + " bar from the furnace..");
					p.getInventory().addItem(bar.getBarId());
					p.getLevels().addXp(SMITHING, bar.getXp());
				}
				bar.decreaseAmount();
				if (bar.getAmount() >= 1) {
					smeltOre(p, -1, false, 1);
				}
				if (bar.getAmount() <= 0) {
					resetSmelting(p);
				}
			}
		});
	}

	private static boolean canSmelt(Player p, BarToSmelt bar) {
		if (bar == null || bar.getAmount() <= 0) {
			return false;
		}
		if (p.getLevels().getLevel(SMITHING) < bar.getLevel()) {
			p.getActionSender().sendMessage("You need a Smithing level of " + bar.getLevel() + " to make that bar.");
			return false;
		}
		for (int i = 0; i < bar.getOre().length; i++) {
			if (bar.getOreAmount()[i] != 0) {
				if (!p.getInventory().hasItemAmount(bar.getOre()[i], bar.getOreAmount()[i])) {
					p.getActionSender().sendMessage("You don't have enough ore to make that bar.");
					return false;
				}
			}
		}
		return true;
	}

	private static int getAmount(int buttonId) {
		for (int i = 13; i <= 48; i++) {
			if (buttonId == i) {
				return SMELT_BUTTON_AMOUNT[i - 13];
			}
		}
		return 0;
	}
	
	private static int getBarToMake(int buttonId) {
		int counter = 0, barIndex = 0;
		for (int i = 13; i <= 48; i++) {
			if (counter == 4) {
				barIndex++;
				counter = 0;
			}
			if (buttonId == i) {
				return barIndex;
			}
			counter++;
		}
		return -1;
	}
	
	private static void lowerForgingRing(Player p) {
		p.getSettings().setForgeCharge(p.getSettings().getForgeCharge() - 1);
		if (p.getSettings().getForgeCharge() <= 0) {
			p.getSettings().setForgeCharge(40);
			p.getActionSender().sendMessage("Your Ring of forging has shattered!");
			p.getEquipment().getSlot(12).setItemId(-1);
			p.getEquipment().getSlot(12).setItemAmount(0);
			p.getActionSender().refreshEquipment();
		}
	}
	
	private static boolean wearingForgingRing(Player p) {
		return p.getEquipment().getItemInSlot(12) == 2568;
	}
	
	public static void setAmountToZero(Player p) {
		if (p.getTemporaryAttribute("smeltingBar") != null) {
			BarToSmelt bar = (BarToSmelt) p.getTemporaryAttribute("smeltingBar");
			bar.setAmount(0);
		}
	}
	
	public static boolean wantToSmelt(Player p, int item) {
		for (int i = 0; i < SMELT_ORES.length; i++) {
			for (int j = 0; j < SMELT_ORES[i].length; j++) {
				if (SMELT_ORES[i][j] != 0) {
					if (item == SMELT_ORES[i][j]) {
						displaySmeltOptions(p);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static void resetSmelting(Player p) {
		p.setTemporaryAttribute("smeltingBar", null);
	}

}
