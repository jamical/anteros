package com.anteros.content;

import java.util.ArrayList;
import java.util.List;

import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class DwarfCannon {
	
	private transient Player p;
	private Location cannonLocation;
	private Location fakeCannonLocation;
	@SuppressWarnings("unused")
	private long setupTime;
	private int constructionStage;
	private int cannonballs;
	private transient boolean stopCannon;
	private transient int direction;
	private transient boolean firing;
	private static final int MAX_CANNONBALLS = 30;
	@SuppressWarnings("unused")
	private static final int HEALTH_TIME = 3600000;
	private static final int CANNONBALL_ID = 2;
	private transient List<NPC> npcsInArea;
	private static final int[] CANNON_PIECES = {
		6, 8, 10, 12
	};
	private static final String[] CONSTRUCTION_MESSAGE = {
		"Cannon base", "Stand", "Barrels", "Furnace"
	};
	private static final int[] CANNON_OBJECTS = {
		7, 8, 9, 6
	};

	public DwarfCannon(Player player) {
		p = player;
		cannonLocation = p.getLocation();
		fakeCannonLocation = Location.location(cannonLocation.getX()+1, cannonLocation.getY()+1, cannonLocation.getY());
		firing = false;
		cannonballs = 0;
		constructionStage = 0;
		direction = 0;
		setNpcsInArea();
		newCannon();
	}
	
	private void setNpcsInArea() {
		npcsInArea = new ArrayList<NPC>();
		for (NPC n : World.getInstance().getNpcList()) {
			//Location l1 = n.getMinimumCoords();
			//Location l2 = n.getMaximumCoords();
			if (cannonLocation.inArea(cannonLocation.getX()-8, cannonLocation.getY()-8, cannonLocation.getX()+8, cannonLocation.getY()+8)) {
				npcsInArea.add(n);
			}
		}
	}

	public void newCannon() {
		setupTime = System.currentTimeMillis();
		World.getInstance().registerEvent(new Event(1000) {
			int i = constructionStage;
			@Override
			public void execute() {
				String s = constructionStage == 0 ? "You place the " : "You add the ";
				String s1 = constructionStage == 0 ? " on the ground." : ".";
				if (p.getInventory().deleteItem(CANNON_PIECES[i])) {
					p.getActionSender().createObject(CANNON_OBJECTS[i], cannonLocation, 0, 10);
					p.getActionSender().sendMessage(s + CONSTRUCTION_MESSAGE[constructionStage] + s1);
					constructionStage++;
					i++;
					if (constructionStage >= 4) {
						constructionStage--;
						this.stop();
						return;
					}
					p.animate(827);
				} else {
					this.stop();
				}
			}
		});
	}
	
	public void fireCannon() {
		if (firing) {
			loadCannon();
			return;
		}
		cannonLoop();
	}
	
	public void loadCannon() {
		if (cannonballs >= MAX_CANNONBALLS) {
			p.getActionSender().sendMessage("The cannon cannot hold any more ammo.");
			return;
		}
		int ballsInInven = p.getInventory().getItemAmount(CANNONBALL_ID);
		if (ballsInInven == 0) {
			p.getActionSender().sendMessage("You don't have any cannonballs to restock the cannon with!");
			return;
		}
		int difference = MAX_CANNONBALLS - cannonballs;
		int amountToLoad = difference;
		if (ballsInInven < (difference)) {
			amountToLoad = ballsInInven;
		}
		if (p.getInventory().deleteItem(CANNONBALL_ID, amountToLoad)) {
			cannonballs += difference;
			p.getActionSender().sendMessage("You load the cannon with " + amountToLoad + " cannonballs.");
			stopCannon = false;
		}
	}
	
	public void pickupCannon() {
		if (p.getInventory().getTotalFreeSlots() < (constructionStage + (cannonballs > 0 ? 1 : 0))) {
			p.getActionSender().sendMessage("You don't have enough room to pick up the cannon.");
			return;
		}
		for (int i = 0; i <= constructionStage; i++) {
			if (!p.getInventory().addItem(CANNON_PIECES[i])) {
				return;
			}
		}
		firing = false;
		p.getInventory().addItem(2, cannonballs);
		p.getActionSender().removeObject(cannonLocation, 0, 10);
		p.getActionSender().sendMessage("You pick up the cannon.");
		p.getSettings().setCannon(null);
	}

	
	public void cannonLoop() {
		if (firing) {
			return;
		}
		firing = true;
		World.getInstance().registerEvent(new Event(1000) {
			int i = 515;
			@Override
			public void execute() {
				if (!firing) {
					this.stop();
					return;
				}
				p.getActionSender().newObjectAnimation(cannonLocation, i);
				World.getInstance().registerEvent(new Event(600) {
					
				@Override
				public void execute() {
					if (!firing) {
						this.stop();
						return;
					}
					if (stopCannon && i == 514) {
						i = 514;
						this.stop();
						firing = false;
						return;
					}
					if (!stopCannon) {
						if (checkHitTarget()) {
							checkCannonballs();
						}
					}
					i++;
					direction++;
					if (direction == 7) {
						direction = 0;
					}
					if (i > 521) {
						i = 514;
					}
					this.stop();
				}
			});
		}
	});
	}

	private void checkCannonballs() {
		cannonballs--;
		if (cannonballs <= 0) {
			p.getActionSender().sendMessage("Your cannon has run out of ammo!");
			stopCannon = true;
		}
	}

	@SuppressWarnings("unused")
	protected boolean checkHitTarget() {
		int i = 0;
		int cannonX = fakeCannonLocation.getX();
		int cannonY = fakeCannonLocation.getY();
		NPC[] npcsToAttack = new NPC[npcsInArea.size()];
		boolean hit = false;
		for (NPC n : World.getInstance().getNpcList()) {
			hit = false;
			Location l = n.getLocation();
			if (n == null || n.isHidden() || n.isDead() || !n.getLocation().withinDistance(fakeCannonLocation, 8)) {
				//continue;
			}
			switch(direction) {
				case 0: // North
					hit = l.inArea(cannonX-1, cannonY, cannonX+1, cannonY+8);
					break;
					
				case 1: // North east
					break;
				
				case 2: // East:
					hit = l.inArea(cannonX, cannonY-1, cannonX+8, cannonY+1);
					break;
					
				case 3: // South east
					break;
					
				case 4: // South
					hit = l.inArea(cannonX-1, cannonY-8, cannonX+1, cannonY);
					break;
					
				case 5: // South west
					break;
					
				case 6: // West
					hit = l.inArea(cannonX-8, cannonY-1, cannonX, cannonY+1);
					break;
					
				case 7: // North west
					break;
			}
			if (hit) {
				final NPC npc = n;
				p.getActionSender().sendProjectile(fakeCannonLocation, n.getLocation(), 30, 53, 50, 38, 38, 40, n);
				World.getInstance().registerEvent(new Event(1000) {

					@Override
					public void execute() {
						int damage = Misc.random(30);
						p.getLevels().addXp(4, damage * 2);
						npc.hit(damage);
						npc.animate(npc.getDefenceAnimation());
						this.stop();
					}
				});
				return true;
			}
		}
		return true;
	}
	
	public Location getLocation() {
		return cannonLocation;
	}
}
