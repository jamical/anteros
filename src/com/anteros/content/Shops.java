package com.anteros.content;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.ShopSession;

public class Shops {
	
	public static void openShop(final Player p, final NPC n, final int npcId) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				switch(npcId) { //npc's id
					case 520: //gen stores
					case 521:
					case 522:
					case 523:
					case 563:
						p.setShopSession(new ShopSession(p, 1)); //shop's int in the shops.xml file
						break;
					case 553: //aubury
						p.setShopSession(new ShopSession(p, 3));
						break;
					case 519: //bob
						p.setShopSession(new ShopSession(p, 4));
						break;
					case 546: //zaff
						p.setShopSession(new ShopSession(p, 5));
						break;
					case 551: //varrock swordshop shopkeeper
						p.setShopSession(new ShopSession(p, 6));
						break;
					case 548: //thessalia
						p.setShopSession(new ShopSession(p, 7));
						break;
					case 549: //horvik
						p.setShopSession(new ShopSession(p, 8));
						break;
					case 550: //lowe
						p.setShopSession(new ShopSession(p, 9));
						break;
					case 576: //harry
						p.setShopSession(new ShopSession(p, 10));
						break;
					case 541: //zeke
						p.setShopSession(new ShopSession(p, 11));
						break;
					case 545: //dommik
						p.setShopSession(new ShopSession(p, 12));
						break;
					case 542: //louie legs
						p.setShopSession(new ShopSession(p, 13));
						break;
					case 587: //jatix
						p.setShopSession(new ShopSession(p,14));
						break;
					case 1783: //ricard
						p.setShopSession(new ShopSession(p,15));
						break;
					case 1972: //rasolo
						p.setShopSession(new ShopSession(p,16));
						break;
					case 586: //gauis 2h shop
						p.setShopSession(new ShopSession(p,17));
						break;
					case 2305: //farming shops
						p.setShopSession(new ShopSession(p,18));
						break;
				}
			}
		});
	}

}
