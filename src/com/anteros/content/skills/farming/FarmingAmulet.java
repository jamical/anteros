package com.anteros.content.skills.farming;

import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class FarmingAmulet {

	public FarmingAmulet() {
		
	}
	
	private static final Object[][] PATCHES = {
		// min x, min y, extra x, extra y, name
		{3064, 3311, 2, 3, "Draynor allotment"}, // Draynor allotment
		{2801, 3472, 2, 2, "Catherby allotment"}, // Catherby allotment
		{2671, 3364, 4, 1, "Ardougne allotment"}, // Ardougne allotment
		{3610, 3531, 3, 3, "Canifis allotment"}, // Canifis allotment
		{3225, 3461, 3, 3, "Varrock tree patch"}, // Varrock tree
		{3184, 3229, 4, 3, "Lumbridge tree patch"}, // Lumbridge tree
		{3002, 3376, 2, 3, "Falador tree patch"}, // Falador tree
		{2932, 3433, 2, 3, "Taverly tree patch"}, // Taverly tree
		{2858, 3427, 4, 4, "Catherby fruit tree patch"}, // Catherby fruit tree
		{2478, 3466, 3, 4, "Gnome stronghold fruit tree patch"}, // Gnome stronghold fruit tree
		{2762, 3208, 3, 1, "Brimhaven fruit tree patch"}, // Brimhaven fruit tree
		{2489, 3182, 3, 2, "Yanille fruit tree patch"}, // Yanille fruit tree
	};

	public static boolean showOptions(Player p, int item) {
		if (item != 12622 || p.getTemporaryAttribute("unmovable") != null) {
			return false;
		}
		p.getActionSender().softCloseInterfaces();
		p.getActionSender().sendChatboxInterface2(232);
		p.getActionSender().modifyText("Allotments", 232, 2);
		p.getActionSender().modifyText("Trees", 232, 3);
		p.getActionSender().modifyText("Fruit trees", 232, 4);
		p.getActionSender().modifyText("Exit", 232, 5);
		p.setTemporaryAttribute("dialogue", 450);
		return true;
	}

	public static void displayAllotmentOptions(Player p) {
		p.getActionSender().sendChatboxInterface2(235);
		p.getActionSender().modifyText("Draynor", 235, 2);
		p.getActionSender().modifyText("Catherby", 235, 3);
		p.getActionSender().modifyText("Ardougne", 235, 4);
		p.getActionSender().modifyText("Canifis", 235, 5);
		p.getActionSender().modifyText("Go back", 235, 6);
		p.setTemporaryAttribute("dialogue", 451);
	}
	
	public static void displayTreeOptions(Player p) {
		p.getActionSender().sendChatboxInterface2(235);
		p.getActionSender().modifyText("Varrock", 235, 2);
		p.getActionSender().modifyText("Lumbridge", 235, 3);
		p.getActionSender().modifyText("Falador", 235, 4);
		p.getActionSender().modifyText("Taverly", 235, 5);
		p.getActionSender().modifyText("Go back", 235, 6);
		p.setTemporaryAttribute("dialogue", 452);
	}
	
	public static void displayFruitTreeOptions(Player p) {
		p.getActionSender().sendChatboxInterface2(235);
		p.getActionSender().modifyText("Catherby", 235, 2);
		p.getActionSender().modifyText("Gnome Stronghold", 235, 3);
		p.getActionSender().modifyText("Brimhaven", 235, 4);
		p.getActionSender().modifyText("Yanille", 235, 5);
		p.getActionSender().modifyText("Go back", 235, 6);
		p.setTemporaryAttribute("dialogue", 453);
	}
	
	public static void teleportToPatch(final Player p, final int option) {
		p.setTemporaryAttribute("unmovable", true);
		p.getWalkingQueue().reset();
		p.getActionSender().softCloseInterfaces();
		p.getActionSender().displayInterface(120);
		World.getInstance().registerEvent(new Event(2000) {
			int i = 0;
			@Override
			public void execute() {
				if (i == 0) {
					i++;
					this.setTick(600);
					p.teleport(Location.location((Integer)PATCHES[option][0] + Misc.random((Integer)PATCHES[option][2]), (Integer)PATCHES[option][1] + Misc.random((Integer)PATCHES[option][3]), 0));
				} else {
					this.stop();
					p.removeTemporaryAttribute("unmovable");
					p.getActionSender().sendMessage("You are teleported to the " + PATCHES[option][4] + ".");
					p.getActionSender().closeInterfaces();
				}
			}		
		});
	}
}
