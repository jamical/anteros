package com.anteros.content.skills.fishing;

/**
 * @author Abexlry (Barbarian Fishing)
 *
 */

public class FishingData {

	public FishingData() {
		
	}
	
	protected static final int FISHING = 10;
	
	protected static final int[] SPOT_IDS ={
		/*
		 * 233, // Bait - sardine
		952, // Net - Shrimp, anchovie
		309, // Lure/bait - Trout, salmon / 
		312, // Cage/Harpoon - Lobster / Tuna/Swordfish/Shark
		313, // Net/Harpoon - Big net / Shark
		952, // Net - Monkfish
		*/
		
		316, // Net/Bait - Shrimps, Anchovies - Sardine, Herring
		309, // Lure/Bait - Trout, Salmon, Rainbow Fish/Pike
		324, // Cage/Harpoon - Lobsters/Tuna, Swordfish
		313, // Net/Harpoon - Mackerel, Cod, Bass, Casket, Seaweed, Oysters/Sharks
		952, // Net - Monkfish
		6267, // Cage - Crayfish
		
	};
	
	protected static final int[][] FIRST_SPOT_FISH ={
		{317, 321}, // Shrimps, Anchovies
		{335, 331}, // Trout, Salmon
		{377}, // Lobster
		{405, 353, 407, 401, 341, 363}, // Casket, Mackerel, Oyster, Seaweed, Cod, Bass
		{7944}, // Monkfish
		{13435}, // Crayfish
		
		
		
		/*{327, 345, 349}, // Sardine, herring, pike
		{317, 321}, // shrimp, anchovies
		{335, 331}, // trout, salmon
		{377}, // Lobster
		{405, 353, 407, 401, 341, 363}, // Casket, mackerel, oyster, seaweed, cod, bass
		{7944}, // Monkfish
		*/
	};
	
	protected static final String[][] FIRST_CATCH_NAME = {
		{"Shrimps", "Anchovies"},
		{"Trout", "Salmon"},
		{"Lobster"},
		{"Casket", "Mackerel", "Oyster", "Seaweed", "Cod", "Bass"},
		{"Monkfish"},
		{"Crayfish"},
	};
	
	protected static final int[] FIRST_SPOT_TIME = {
		5000, // shrimp, anchovies
		6000, // trout, salmon
		4000, // Lobster
		4000, // Casket, mackerel, oyster, seaweed, cod, bass
		6000, // Monkfish
		5000, // Crayfish
	};
	
	protected static final int[] SECOND_SPOT_TIME = {
		6000,
		4000,
		4000,
		9000,
		-1,
		-1,
	};

	protected static final int[] FIRST_SPOT_MINTIME = {
		3000,
		3500,
		3000,
		2000,
		3000,
		3500,
	};
	
	protected static final int[] SECOND_SPOT_MINTIME = {
		3500,
		3500,
		2000,
		4000,
		-1,
		-1,
	};
	
	protected static final int[][] SECOND_SPOT_FISH ={
		{327, 345}, // Sardine, Herring
		{349}, // Pike
		{359, 371}, // Tuna, Swordfish
		{383}, //Sharks
		{-1}, // None
		{-1},
	};
	
	protected static final String[][] SECOND_CATCH_NAME = {
		{"Sardine", "Herring"},
		{"Pike"},
		{"Tuna", "Swordfish"},
		{"Shark"},
		{""},
		{""},
	};
	
	protected static final int[][] FIRST_SPOT_LEVEL ={
		{1, 15},
		{20, 30},
		{40},
		{16, 16, 16, 16, 23, 46},
		{62},
		{1},
	};
	
	protected static final int[][] SECOND_SPOT_LEVEL ={
		{5, 10},
		{25},
		{35, 50},
		{76},
		{-1},
		{-1},
	};
	
	protected static final double[][] FIRST_SPOT_XP ={
		{10, 40},
		{50, 70},
		{90},
		{10, 20, 10, 1, 45, 100},
		{120},
		{10},
	};
	
	protected static final double[][] SECOND_SPOT_XP ={
		{20, 30},
		{25},
		{80, 100},
		{110},
		{-1},
		{-1},
	};
	
	protected static final int[][] PRIMARY_ITEM ={
		{303, 307}, // Small net / fishing rod
		{309, 307}, // Fly fishing rod / fishing rod
		{301, 311}, // Lobster pot / Harpoon
		{305, 311}, // Big net / Harpoon
		{303}, // Small net
		{13431}, // crayfish cage
	};
	
	protected static final int[][] SECONDARY_ITEM ={
		{-1, 313}, // nothing / bait
		{314, 313},
		{-1, -1},
		{-1, -1},
		{-1},
		{-1},
	};
	
	protected static final String[][] PRIMARY_NAME = {
		{"a small fishing net", "a fishing rod"},
		{"a fly-fishing rod", "a fishing rod"},
		{"a lobster pot", "a harpoon"},
		{"a big fishing net", "a harpoon"},
		{"a small fishing net"},
		{"a crayfish cage"},
	};
	
	protected static final String[][] SECONDARY_NAME = {
		{"", "some fishing bait"},
		{"feathers", "some fishing bait"},
		{"", ""},
		{"", ""},
		{""},
		{""},
	};
	
	protected static final int[][] FISHING_ANIMATION ={
		{621, 622}, // Small net
		{622, 622}, // Rod casting anim
		{619, 618}, // Lobster pot/harpoon
		{620, 618}, // Big net
		{621}, // Small net
		{619}, // Cage fishing
	};
	
	protected static final int[][] FISHING_ANIMATION2 ={
		{621, 623}, // Small net
		{623, 623}, // Rod continue fishing anim
		{619, 618}, // Lobster pot
		{620, 618}, // Big net
		{621}, // Small net
		{619}, // Cage fishing
	};
	
	/**
	 * Start of Barbarian segment
	 */
	protected static final int[][] BARBARIAN_CATCHES ={
		{6710}, // Barehanded Tuna
		{6707}, // Barehanded Swordfish
		{6705}, // Barehanded Shark
		{}, // Leaping Trout (some kind of rod)
		{}, // Leaping Salmon (some kind of rod)
		{}, // Leaping Sturgeon (some kind of rod)
	};
	
	protected static final int[][] BARBARIAN_ANIMATION ={
		{6703}, // short pulling water
		{6704}, // pulling water
		{6709}, // hit the water and pull back faster, tries a few times and brushes off
	};
	/**
	 * End of Barbarian segment
	 */
}
