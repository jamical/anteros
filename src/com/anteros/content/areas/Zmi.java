package com.anteros.content.areas;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Zmi {
	public static void climbExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

			@Override
			public void run() { 
				player.teleport(Location.location(2452, 3232, 0));
				player.animate(828);
			}
		});
	}
	public static void climbEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

			@Override
			public void run() { 
				player.teleport(Location.location(3271, 4861, 0));
				player.animate(827);
			}
		});
	}
	}