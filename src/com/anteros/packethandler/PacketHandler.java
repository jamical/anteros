package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.model.player.Player;
import com.anteros.net.Packet;

/**
 * Packet handler interface.
 * @author Graham
 *
 */
public interface PacketHandler {
	
	public void handlePacket(Player player, IoSession session, Packet packet);

}
