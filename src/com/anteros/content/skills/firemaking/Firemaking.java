package com.anteros.content.skills.firemaking;

import com.anteros.GameEngine;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.world.GroundItem;

public class Firemaking extends FiremakingData {

	public Firemaking() {
		
	}
	
	public static boolean isFiremaking(Player p, int itemUsed, int usedWith, int usedSlot, int withSlot) {
		int itemOne = itemUsed;
		int itemTwo = usedWith;
		int slotType;
		int slot;
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				itemOne = usedWith;
				itemTwo = itemUsed;
			}
			for (int j = 0; j < LOGS.length; j++) {
				if (itemOne == LOGS[j] && itemTwo == TINDERBOX) {
					slotType = itemUsed == TINDERBOX ? 1 : usedWith == TINDERBOX ? 2 : -1;
					slot = slotType == 1 ? withSlot : usedSlot;
					lightFire(p, j, false, slot);
					return true;
				}
			}
			for (int j = 0; j < COLOURED_LOGS.length; j++) {
				if (itemOne == COLOURED_LOGS[j] && itemTwo == TINDERBOX) {
					slotType = itemUsed == TINDERBOX ? 1 : usedWith == TINDERBOX ? 2 : -1;
					slot = slotType == 1 ? withSlot : usedSlot;
					lightFire(p, j, true, slot);
					return true;
				}
			}
			for (int j = 0; j < LOGS.length; j++) {
				for (int k = 0; k < FIRELIGHTERS.length; k++) {
					if (itemOne == LOGS[j] && itemTwo == FIRELIGHTERS[k]) {
						useFirelighter(p, j, k, usedSlot, withSlot);
						return true;
					}
				}
			}
			for (int j = 0; j < OTHER_ITEMS.length; j++) {
				if (itemOne == OTHER_ITEMS[j][0] && itemTwo == TINDERBOX) {
					slotType = itemUsed == TINDERBOX ? 1 : usedWith == TINDERBOX ? 2 : -1;
					slot = slotType == 1 ? withSlot : usedSlot;
					lightLightSource(p, j, slot);
					return true;
				}
			}
		}
		return false;
	}

	private static void lightLightSource(Player p, int j, int slot) {
		if (p.getInventory().replaceItemSlot(OTHER_ITEMS[j][0], OTHER_ITEMS[j][1], slot)) {
			p.getActionSender().sendMessage("You ignite the light source.");
		}
	}

	private static void useFirelighter(Player p, int j, int k, int usedSlot, int withSlot) {
		if (p.getInventory().deleteItem(FIRELIGHTERS[k], usedSlot, 1)) {
			p.getInventory().replaceItemSlot(LOGS[j], COLOURED_LOGS[k], withSlot);
			p.getActionSender().sendMessage("You cover the log in the strange goo..it changes colour.");
		} else {
			if (p.getInventory().deleteItem(FIRELIGHTERS[k], withSlot, 1)) {
				p.getInventory().replaceItemSlot(LOGS[j], COLOURED_LOGS[k], usedSlot);
				p.getActionSender().sendMessage("You cover the log in the strange goo..it changes colour.");
			}
		}
	}
	
	private static boolean blocked;
	
	private static boolean pathBlocked(Player p) {
		if (!GameEngine.noClipHandler.checkPos(p.getLocation().getX() - 1, p.getLocation().getY(), p.getLocation().getZ())) {
			blocked = true;
			return true;
		} else if (GameEngine.noClipHandler.checkPos(p.getLocation().getX() - 1, p.getLocation().getY(), p.getLocation().getZ())) {
			blocked = false;
			return false;
		} else {
			return false;
		}
		
	}
	
	private static Location getFireLoc(Player p) {
		Location fireLocation = null;
		if (blocked) {
			fireLocation = Location.location(p.getLocation().getX() - 1, p.getLocation().getY(), p.getLocation().getZ());
		} else if (!blocked) {
			fireLocation = Location.location(p.getLocation().getX() + 1, p.getLocation().getY(), p.getLocation().getZ());
		}
		return fireLocation;
	}

	private static void lightFire(final Player p, final int index, boolean colouredFire, final int slot) {
		if (!GameEngine.noClipHandler.checkPos(p.getLocation().getX() - 1, p.getLocation().getY(), p.getLocation().getZ()) &&
				!GameEngine.noClipHandler.checkPos(p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ())) {
			p.getActionSender().sendMessage("You cannot light a fire here.");
			return;
		}
			p.getActionSender().closeInterfaces();
			final int log = colouredFire ? COLOURED_LOGS[index] : LOGS[index];
			if (!canMakeFire(p, index, colouredFire) || !p.getInventory().deleteItem(log, slot, 1)) {
				return;
			}
			int delay = getDelay(p, index);
			p.getWalkingQueue().reset();
			p.getActionSender().clearMapFlag();
			if (delay == START_DELAY) {
				p.animate(733);
			}
			p.getActionSender().sendMessage("You attempt to light the logs.");
			final int fireObject = colouredFire ? COLOURED_FIRES[index] : FIRE_OBJECT;
			GroundItem item = new GroundItem(log, 1, p.getLocation(), p);
			World.getInstance().getGroundItems().newEntityDrop(item);
			p.setTemporaryAttribute("unmovable", true);
			if (pathBlocked(p)) {
				p.getWalkingQueue().forceWalk(1, 0);
			} else if (!pathBlocked(p)) {
				p.getWalkingQueue().forceWalk(-1, 0);
			}
			World.getInstance().registerEvent(new Event(delay) {

				@Override
				public void execute() {
					this.stop();
					p.setTemporaryAttribute("lastFiremake", System.currentTimeMillis());
					p.animate(65535);
					World.getInstance().registerEvent(new Event(1000) {

						@Override
						public void execute() {
							this.stop();
							Location fireLocation = getFireLoc(p);
							if (World.getInstance().getGroundItems().deleteItem(log, fireLocation)) {
								p.getActionSender().sendMessage("The fire catches and the logs begin to burn.");
								p.setFaceLocation(fireLocation);
								p.getLevels().addXp(FIREMAKING, FIRE_XP[index]);
								World.getInstance().getGlobalObjects().newFire(p, fireObject, fireLocation);
							}
							p.removeTemporaryAttribute("unmovable");
						}	
					});
				}
			});
	}

	private static int getDelay(Player p, int index) {
		long lastFireTime = 0;
		if (p.getTemporaryAttribute("lastFiremake") != null) {
			lastFireTime = (Long) p.getTemporaryAttribute("lastFiremake");
			if (System.currentTimeMillis() - lastFireTime < 2500) {
				return 1000;
			}
		}
		return START_DELAY;
	}

	private static boolean canMakeFire(Player p, int index, boolean colouredLog) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return false;
		}
		if (World.getInstance().getGlobalObjects().fireExists(p.getLocation())) {
			p.getActionSender().sendMessage("You cannot light a fire here.");
			return false;
		}
		if ((p.getLevels().getLevel(FIREMAKING) < FIRE_LEVEL[index]) && !colouredLog) {
			p.getActionSender().sendMessage("You need a Firemaking level of " + FIRE_LEVEL[index] + " to light this log.");
			return false;
		}
		if (!p.getInventory().hasItem(TINDERBOX)) {
			p.getActionSender().sendMessage("You need a tinderbox if you intend on actually make a fire!");
			return false;
		}
		return true;
	}
}
