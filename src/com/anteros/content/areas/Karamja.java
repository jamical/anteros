package com.anteros.content.areas;

import java.util.ArrayList;

import com.anteros.content.skills.woodcutting.Woodcutting;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class Karamja {
public static void climbTzhaarExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(2859, 3168, 0));
		}
	});
}
public static void climbTzhaarEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(2856, 9570, 0));
		}
	});
}

public static ArrayList <int[]> vineRemoved = new ArrayList<int[]>();

public static void chopVines(final Player player, final int objectX, final int objectY) {
	final int playerX = player.getLocation().getX();
	final int playerY = player.getLocation().getY();
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, objectX - 1, objectY - 1, objectX + 1, objectY + 1) {
		@Override
		public void run() {
			if (Woodcutting.hasAxe(player)) {
				player.animate(Woodcutting.getAxeAnimation(player));
				int cutTime = 1000 + Misc.random(1550); //time it takes to cut the vine
				World.getInstance().registerEvent(new Event(cutTime) {
					@Override
					public void execute() {
						World.getInstance().getGlobalObjects().newTempObject(player, 5107, Location.location(objectX, objectY, player.getLocation().getZ()), 2000);
						vineRemoved.add(new int[] { objectX, objectY });
						player.getActionSender().removeObject(Location.location(objectX, objectY, player.getLocation().getZ()), 0, 10);
						if (playerX > objectX) {
							player.getWalkingQueue().forceWalk(-2, 0);
						} else if (playerY > objectY) {
							player.getWalkingQueue().forceWalk(0, -2);
						} else if (playerX < objectX) {
							player.getWalkingQueue().forceWalk(+2, 0);
						} else if (playerY < objectY) {
							player.getWalkingQueue().forceWalk(0, +2);
						}
						this.stop();
					}
				});
				} else {
				player.getActionSender().sendMessage("You need an axe to chop down these vines. ");
				return;
			}
		}
	});
}
}