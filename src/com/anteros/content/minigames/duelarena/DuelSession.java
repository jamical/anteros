package com.anteros.content.minigames.duelarena;

import com.anteros.content.skills.prayer.Prayer;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Item;
import com.anteros.model.ItemDefinition;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
import com.anteros.util.Misc;
import com.anteros.util.log.Logger;

public class DuelSession {

	/**
	 * STATUSES :
	 * 0 - open interface / stake items / set rules
	 * 1 - one player has accepted
	 * 2 - both players on confirmation interface
	 * 3 - one player has accepted
	 * 4 - both players accepted
	 * 5 - duel countdown
	 * 6 - able to attack
	 * 7 - end of duel
	 * 8 - displayed winning screen
	 */
	
	private Player player;
	private Player p2;
	private Player winner;
	private Item[] items = new Item[28];
	private Item[] winnings;
	private int status;
	private boolean[] rules = new boolean[23];
	private int config;
	
	private static final int[] RULE_CONFIGS = {
		1, // no forfeit
		2, // no movement
		16, // no range
		32, // no melee
		64, // no magic
		128, // no drinks
		256, // no food
		512, // no prayer
		1024, // obstacles
		4096, // fun weapons
		8192, // special attacks
		65536, // amulet
		131072, // weapon
		262144, // body
		524288, // shield
		2097152, // legs
		8388608, // gloves
		16777216,// boots
		67108864, // ring
		134217728, // arrows
		268435456, // summoning
		16384, // Hat
		32768, // Cape
	};
	
	private static final String[] DUEL_TEXT = {
		//DURING
		"You cannot forfeit the duel.",
		"You cannot move.",
		"You cannot use Ranged attacks.", // "You cannot have no Ranged, no Melee and no Magic - how would you fight?
		"You cannot use Melee attacks.",
		"You cannot use Magic attacks.",
		"You cannot use drinks.",
		"You cannot use food.",
		"You cannot use Prayer.",
		"There will be obstacles in the arena.",
		"You can only use 'fun weapons.'", // if neither player has a fun weapon, it says "Fun weapons is selected, but neither player has a fun weapon".
		"You cannot use special attacks.",
		//END OF DURING
		//BEFORE THE DUEL
		//ALWAYS SAYS - BOOSTED STATS WILL BE RESTORED
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Some worn items will be taken off.", // DURING - You can't use two-handed weapons, like bows.
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
		"Summoning familiars can assist you in battle.", // DURING
		"Some worn items will be taken off.",
		"Some worn items will be taken off.",
	};
	
	private static final int[] DUEL_TEXT_VAR = {
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		1,
		0,
		0,
	};
	
	public DuelSession(Player p, Player p2) {
		this.player = p;
		this.p2 = p2;
		this.config = 0;
		this.status = 0;
		player.getDuelRequests().clear();
		openInterface();
	}

	private void openInterface() {
		player.getActionSender().modifyText("", 631, 28);
		player.getActionSender().modifyText("unlimited!", 631, 92);
		player.getActionSender().modifyText("unlimited!", 631, 93);
		player.getActionSender().modifyText(p2.getPlayerDetails().getDisplayName(), 631, 25);
		player.getActionSender().modifyText(""+p2.getLevels().getCombatLevel(), 631, 27);
		player.getActionSender().sendConfig(286, 0);
		player.getActionSender().configureDuel();
		refreshDuel();
	}
	
	public boolean ruleEnabled(int i) {
		return rules[i];
	}

	public void refreshDuel() {
		player.getActionSender().sendItems(-1, 64209, 93, player.getInventory().getItems());
		player.getActionSender().sendItems(-1, -70135, 134, items);
		p2.getActionSender().sendItems(-2, -70135, 134, items);
	}

	public void toggleDuelRules(int buttonId) {
		if (buttonId == 102) {
			acceptDuel();
			return;
		}
		if (buttonId == 107) {
			declineDuel();
			return;
		}
		buttonId = getArrayId(buttonId);
		if (buttonId == -1) {
			return;
		}
		config = rules[buttonId] ? config - RULE_CONFIGS[buttonId] : config + RULE_CONFIGS[buttonId];
		rules[buttonId] = rules[buttonId] ? false : true;
		if (buttonId == 14 && rules[14]) {
			player.getActionSender().sendMessage("You will not be able to use two-handed weapons, such as bows.");
			p2.getActionSender().sendMessage("You will not be able to use two-handed weapons, such as bows.");
		}
		if (buttonId == 19 && rules[19]) {
			player.getActionSender().sendMessage("You will not be able to use any weapon which uses arrows.");
			p2.getActionSender().sendMessage("You will not be able to use any weapon which uses arrows.");
		}
		p2.getDuel().setRule(buttonId, rules[buttonId]);
		if (config > 0) {
			player.getActionSender().sendConfig(286, config);
			p2.getActionSender().sendConfig(286, config);
		} else {
			player.getActionSender().sendConfig(286, 0);
			p2.getActionSender().sendConfig(286, 0);
		}
		p2.getDuel().setConfig(config);
		resetStatus();
	}
	
	public void declineDuel() {
		status = 0;
		p2.getDuel().setStatus(0);
		player.getActionSender().sendMessage("You decline the stake and duel options.");
		p2.getActionSender().sendMessage("Other player declined the stake and duel options.");
		giveBack();
		p2.getDuel().giveBack();
		player.setTemporaryAttribute("challengeUpdate", true);
		p2.setTemporaryAttribute("challengeUpdate", true);
		p2.getActionSender().closeInterfaces();
		player.getActionSender().closeInterfaces();
	}

	private void giveBack() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (!player.getInventory().addItem(items[i].getItemId(), items[i].getItemAmount())) {
					logger.info("Possible stake dupe " + player.getUsername());
				}
			}
		}
	}

	public void acceptDuel() {
		if (status == 0) {
			if (stakingOverMaxAmount()) {
				player.getActionSender().sendMessage("You or your opponent dosen't have enough room for the stake.");
				return;
			}
			if (notEnoughRoom()) {
				player.getActionSender().sendMessage("You don't have enough inventory space for the stake and/or duel options.");
				return;
			}
			if (p2.getDuel().notEnoughRoom()) {
				player.getActionSender().sendMessage("Other player dosen't have enough inventory space for the stake and/or duel options.");	
				return;
			}
			if (rules[2] && rules[3] && rules[4]) {
				player.getActionSender().modifyText("You cannot have no Ranged, no Melee and no Magic - how would you fight?", 631, 28);
				p2.getActionSender().modifyText("You cannot have no Ranged, no Melee and no Magic - how would you fight?", 631, 28);
				return;
			}
			this.status = 1;
		}
		if (status == 1) {
			player.getActionSender().modifyText("Waiting for other player...", 631, 28);
			p2.getActionSender().modifyText("Other player has accepted...", 631, 28);
			if (p2.getDuel().getStatus() == 1) {
				displayConfirmation();
				p2.getDuel().displayConfirmation();
			}
			return;
		}
		if (status == 2 && p2.getDuel().getStatus() == 2) {
			player.getActionSender().modifyText("Waiting for other player...", 626, 45);
			p2.getActionSender().modifyText("Other player has accepted...", 626, 45);
			status = 3;
			return;
		}
		if (status == 3 || (status == 2 && p2.getDuel().getStatus() == 3)) {
			status = 4;
			p2.getDuel().setStatus(4);
			player.getActionSender().softCloseInterfaces();
			p2.getActionSender().softCloseInterfaces();
			player.getActionSender().sendMessage("You accept the stake and duel options.");
			p2.getActionSender().sendMessage("You accept the stake and duel options.");
			if (rules[1]) {
				player.setTemporaryAttribute("unmovable", true);
				p2.setTemporaryAttribute("unmovable", true);
			}
			Location[] teleports = getArenaTeleport();
			int random = Misc.random(1);
			player.teleport(random == 0 ? teleports[0] : teleports[1]);
			p2.teleport(random == 0 ? teleports[1] : teleports[0]);
			player.getActionSender().setArrowOnEntity(10, p2.getIndex());
			p2.getActionSender().setArrowOnEntity(10, player.getIndex());
			resetPlayerVariables(true);
			removeWornItems();
			p2.getDuel().removeWornItems();
			beginCountdownEvent(false);
			p2.getDuel().beginCountdownEvent(true);
		}
	}
	
	public void removeWornItems() {
		int[] slot = {2,3,4,5,7,9,10,12,13,0,1};
		int[] rule = {11,12,13,14,15,16,17,18,19,21,22};
		for (int j = 0; j < rule.length; j++) {
			if (rules[rule[j]]) {
				if (j == 20) {
					continue;
				}
				player.getEquipment().unequipItem(slot[j]);
			}
		}
	}

	private void beginCountdownEvent(boolean b) {
		World.getInstance().registerEvent(new Event(1000) {
			@Override
			public void execute() {
				this.stop();
				countdown();
			}
		});
	}
	
	private void countdown() {
		status = 5;
		World.getInstance().registerEvent(new Event(1000) {
			int i = 4;
			@Override
			public void execute() {
				if (i > 0) {
					player.forceChat(""+i);
				} else {
					player.forceChat("Fight!");
					status = 6;
					this.stop();
				}
				i--;
			}
		});
	}
	
	private Location[] getArenaTeleport() {
		final int arenaChoice = Misc.random(2);
		Location[] locations = new Location[2];
		int[] arenaBoundariesX = {3337, 3367, 3336};
		int[] arenaBoundariesY = {3246, 3227, 3208};
		int[] maxOffsetX = {14, 14, 16};
		int[] maxOffsetY = {10, 10, 10};
		int finalX = arenaBoundariesX[arenaChoice] + Misc.random(maxOffsetX[arenaChoice]);
		int finalY = arenaBoundariesY[arenaChoice] + Misc.random(maxOffsetY[arenaChoice]);
		locations[0] = Location.location(finalX, finalY, 0);
		if (rules[1]) {
			int direction = Misc.random(1);
			if (direction == 0) {
				finalX--;
			} else {
				finalY++;
			}
		} else {
			finalX = arenaBoundariesX[arenaChoice] + Misc.random(maxOffsetX[arenaChoice]);
			finalY = arenaBoundariesY[arenaChoice] + Misc.random(maxOffsetY[arenaChoice]);
		}
		locations[1] = Location.location(finalX, finalY, 0);
		return locations;
	}
	
	public int getExtraSlots() {
		int i = 0;
		int[] slot = {2,3,4,5,7,9,10,12,13,0,1};
		int[] rule = {11,12,13,14,15,16,17,18,19,21,22};
		for (int j = 0; j < rule.length; j++) {
			if (rules[rule[j]]) {
				i += player.getEquipment().getSlot(slot[j]).getItemId() > 0 ? 1 : 0;
			}
		}
		return i;
	}

	private void displayConfirmation() {
		this.status = 2;
		int duringDuelOffset = 33;
		int beforeDuelOffset = 41;
		boolean wornItemWarning = false;
		for (int i = 28; i <= 44; i++) {
			if (i != 32) {
				player.getActionSender().modifyText("", 626, i);
			}
		}
		player.getActionSender().modifyText("Modified stats will be restored.", 626, beforeDuelOffset);
		beforeDuelOffset++;
		if (beforeDuelOffset == 42) {
			beforeDuelOffset = 28;
		}
		for (int i = 0; i < rules.length; i++) {
			if (rules[i]) {
				if (i == 14) {
					player.getActionSender().modifyText("You can't use two-handed weapons, like bows.", 626, duringDuelOffset);
					duringDuelOffset++;
				}
				if (DUEL_TEXT_VAR[i] == 0) {
					if ((i >= 11 && i <= 22) && i != 20) {
						if (wornItemWarning) {
							continue;
						} else {
							wornItemWarning = true;
						}
					}
					player.getActionSender().modifyText(DUEL_TEXT[i], 626, beforeDuelOffset);
					beforeDuelOffset++;
					if (beforeDuelOffset == 42) {
						beforeDuelOffset = 28;
					}
				} else
				if (DUEL_TEXT_VAR[i] == 1) {
					player.getActionSender().modifyText(DUEL_TEXT[i], 626, duringDuelOffset);
					duringDuelOffset++;
				}
			}
		}
		p2.getActionSender().modifyText("", 626, 45); // Accepted text.
		if (getAmountOfItems() > 0) {
			player.getActionSender().modifyText("", 626, 25); // 'Absoloutely nothing!' text.
			p2.getActionSender().modifyText("", 626, 26); // 'Absoloutely nothing!' text.
		}
		player.getActionSender().displayInterface(626);
	}
	
	public void resetPlayerVariables(boolean justThisPlayer) {
		player.getSettings().setSkullCycles(0);
		player.getSpecialAttack().resetSpecial();
		player.setLastkiller(null);
		player.setEntityFocus(65535);
		player.setDead(false);
		player.setPoisonAmount(0);
		player.clearKillersHits();
		player.getSettings().setLastVengeanceTime(0);
		player.getSettings().setVengeance(false);
		player.removeTemporaryAttribute("willDie");
		player.setFrozen(false);
		player.removeTemporaryAttribute("unmovable");
		player.getSettings().setTeleblockTime(0);
		player.removeTemporaryAttribute("teleblocked");
		player.setTarget(null);
		player.setAttacker(null);
		player.setHp(player.getMaxHp());
		for (int i = 0; i < Skills.SKILL_COUNT; i ++) {
			player.getLevels().setLevel(i, player.getLevels().getLevelForXp(i));
		}
		player.getActionSender().sendSkillLevels();
		Prayer.deactivateAllPrayers(player);
		if (!justThisPlayer) {
			p2.getSettings().setSkullCycles(0);
			p2.getSpecialAttack().resetSpecial();
			p2.setLastkiller(null);
			p2.setDead(false);
			p2.setPoisonAmount(0);
			p2.setEntityFocus(65535);
			p2.clearKillersHits();
			p2.setTarget(null);
			p2.getSettings().setLastVengeanceTime(0);
			p2.getSettings().setVengeance(false);
			p2.removeTemporaryAttribute("willDie");
			p2.setFrozen(false);
			p2.removeTemporaryAttribute("unmovable");
			player.getSettings().setTeleblockTime(0);
			player.removeTemporaryAttribute("teleblocked");
			p2.getSettings().setTeleblockTime(0);
			p2.removeTemporaryAttribute("teleblocked");
			p2.setAttacker(null);
			p2.setHp(p2.getMaxHp());
			for (int i = 0; i < Skills.SKILL_COUNT; i ++) {
				p2.getLevels().setLevel(i, p2.getLevels().getLevelForXp(i));
			}
			p2.getActionSender().sendSkillLevels();
			Prayer.deactivateAllPrayers(p2);
		}
	}
	
	public void finishDuel(boolean lost, boolean forfeit) {
		status = 8;
		p2.getDuel().setStatus(8);
		if (forfeit) {
			p2.getActionSender().sendMessage(player.getPlayerDetails().getDisplayName() + " has forfeited the duel!");
		}
		if (lost) {
			p2.getDuel().setWinner(p2);
			p2.getDuel().setWinnings(items);
			p2.getActionSender().sendMessage("Well done! You have defeated " + player.getPlayerDetails().getDisplayName() + "!");
			player.getActionSender().sendMessage("Oh dear you are dead!");
		}
		player.teleport(Location.location(3360 + Misc.random(19), 3274 + Misc.random(3), 0));
		p2.teleport(Location.location(3360 + Misc.random(19), 3274 + Misc.random(3), 0));
		p2.getActionSender().setArrowOnEntity(10, -1);
		player.getActionSender().setArrowOnEntity(10, -1);
		p2.removeTemporaryAttribute("unmovable");
		player.removeTemporaryAttribute("unmovable");
		p2.getActionSender().modifyText(""+player.getLevels().getCombatLevel(), 634, 22);
		p2.getActionSender().modifyText(player.getPlayerDetails().getDisplayName(), 634, 23);
		final Object[] opts1 = new Object[]{"", "", "", "", "", -1, 0, 6, 6, 136, 41549857};
		p2.getActionSender().displayInterface(634);
		if (p2.getDuel().getWinnings() != null) {
			p2.getActionSender().setRightClickOptions(1026, 41549857, 0, 35);
			p2.getActionSender().sendClientScript2(566, 149, opts1, "IviiiIsssss");
			p2.getActionSender().sendItems(-1, 64000, 136, p2.getDuel().getWinnings());
		}
		resetPlayerVariables(false);
		player.setTemporaryAttribute("challengeUpdate", true);
		p2.setTemporaryAttribute("challengeUpdate", true);
		if (lost) {
			player.getActionSender().closeInterfaces();
			p2.getDuel().giveBack();
		}
	}
	
	public void resetStatus() {
		if (status == 1 || p2.getDuel().getStatus() == 1) {
			this.status = 0;
			p2.getDuel().setStatus(0);
			player.getActionSender().modifyText("", 631, 28);
			p2.getActionSender().modifyText("", 631, 28);
		}
	}
	
	public void recieveWinnings(Player p) {
		if (!winner.equals(p)) {
			logger.info(p.getPlayerDetails().getDisplayName() + " tried to claim stake winnings that weren't his.");
			return;
		}
		if (status != 8) {
			return;
		}
		for (int i = 0; i < winnings.length; i++) {
			if (winnings[i] != null) {
				if (!player.getInventory().addItem(winnings[i].getItemId(), winnings[i].getItemAmount())) {
					logger.info("Possible stake winnings dupe " + player.getUsername());
				} else {
					winnings[i] = null;
				}
			}
		}
	}

	private int getArrayId(int buttonId) {
		int[] buttons = {67,66,65,64,61,62,63,58,59,60,57,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,52,53,50,51};
		int[] rule = {16,17,18,15,12,13,14,22,11,19,21,2,2,3,3,4,4,9,9,0,0,5,5,6,6,7,7,1,1,8,8,20,20,10,10};
		for (int i = 0; i < buttons.length; i++) {
			if (buttonId == buttons[i]) {
				return rule[i];
			}
		}
		return -1;
	}
	
	public void forfeitDuel(int x, int y) {
		// if (status == 5 || status == 4) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(player, x-1, y-1, x+1, y+1) {
				@Override
				public void run() {
					if (rules[0]) {
						player.getActionSender().sendMessage("You cannot forfeit this duel!");
						return;
					}
					player.getActionSender().modifyText("Forfeit duel?", 228, 1);
					player.getActionSender().modifyText("Yes", 228, 2);
					player.getActionSender().modifyText("No", 228, 3);
					player.getActionSender().sendChatboxInterface2(228);
				}
			});
		//}
	}
	
	public boolean stakeItem(int slot, int amount) {
		int itemId = player.getInventory().getItemInSlot(slot);
		boolean stackable = ItemDefinition.forId(itemId).isStackable();
		int stakeSlot = findItem(itemId);
		if (amount <= 0 || itemId == -1 || status > 2) {
			return false;
		}
		if (ItemDefinition.forId(itemId).isPlayerBound()) {
			player.getActionSender().sendMessage("You cannot stake that item.");
			return false;
		}
		if (!stackable) {
			stakeSlot = findFreeSlot();
			if(stakeSlot == -1) {
				//player.getActionSender().sendMessage("An error occured whilst trying to find free a stake slot.");
				//theoretically this should never happen since if there are no slots available, you should have no inventory items.
				return false;
			}
			if (amount > player.getInventory().getItemAmount(itemId)) {
				amount = player.getInventory().getItemAmount(itemId);
			}
			for (int i = 0; i < amount; i++) {
				stakeSlot = findFreeSlot();
				if (!player.getInventory().deleteItem(itemId) || stakeSlot == -1) {
					break;
				}
				items[stakeSlot] = new Item(itemId, 1);
			}
			resetStatus();
			refreshDuel();
			return true;
		}
		else if(stackable) {
			stakeSlot = findItem(itemId);
			if(stakeSlot == -1) {
				stakeSlot = findFreeSlot();
				if (stakeSlot == -1) {
					//player.getActionSender().sendMessage("An error occured whilst trying to find free a stake slot.");
					return false;
				}
			}
			if (amount > player.getInventory().getAmountInSlot(slot)) {
				amount = player.getInventory().getAmountInSlot(slot);
			}
			if (player.getInventory().deleteItem(itemId, amount)) {
				if (items[stakeSlot] == null) {
					items[stakeSlot] = new Item(itemId, amount);
				} else {
					if (items[stakeSlot].getItemId() == itemId) {
						items[stakeSlot].setItemId(itemId);
						items[stakeSlot].setItemAmount(items[stakeSlot].getItemAmount() + amount);
					}
				}
				resetStatus();
				refreshDuel();
				return true;
			}
		}
		return false;
	}
	
	public void removeItem(int slot, int amount) {
		if (status > 2 || items[slot] == null) {
			return;
		}
		int itemId = getItemInSlot(slot);
		int tradeSlot = findItem(itemId);
		boolean stackable = ItemDefinition.forId(itemId).isStackable();
		if (tradeSlot == -1) {
			logger.info("user tried to remove non-existing item from duel! " + player.getUsername());
			return;
		}
		if (amount > getItemAmount(itemId)) {
			amount = getItemAmount(itemId);
		}
		if (!stackable) {
			for (int i = 0; i < amount; i++) {
				tradeSlot = findItem(itemId);
				if (player.getInventory().addItem(itemId, amount)) {
					items[tradeSlot].setItemAmount(getAmountInSlot(tradeSlot) - amount);
					if (getAmountInSlot(tradeSlot) <= 0) {
						items[tradeSlot] = null;
					}
				}
			}
			resetStatus();
			refreshDuel();
		} else {
			tradeSlot = findItem(itemId);
			if (player.getInventory().addItem(itemId, amount)) {
				items[tradeSlot].setItemAmount(getAmountInSlot(tradeSlot) - amount);
				if (getAmountInSlot(tradeSlot) <= 0) {
					items[tradeSlot] = null;
				}
				p2.getActionSender().tradeWarning(tradeSlot);
			}
		}
		resetStatus();
		refreshDuel();
	}
	
	public Item[] getWinnings() {
		return winnings;
	}
	
	public void setWinnings(Item[] items) {
		Item[] winnings = new Item[28];
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() > 0) {
					winnings[j] = items[i];
					j++;
				}
			}
		}
		this.winnings = winnings;
	}
	
	public void setWinner(Player p) {
		this.winner = p;
	}
	
	public Player getWinner() {
		return winner;
	}
	
	public void setConfig(int c) {
		this.config = c;
	}
	
	public void setRule(int rule, boolean b) {
		this.rules[rule] = b;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getTotalFreeSlots() {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				j++;
			}
		}
		return j;
	}
	
	public int findFreeSlot() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean hasItem(int itemId) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() == itemId) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasItemAmount(int itemId, int amount) {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() == itemId) {
					j += items[i].getItemAmount();
				}
			}
		}
		return j >= amount;
	}
	
	public int findItem(int itemId) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() == itemId) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public int getItemAmount(int itemId) {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() == itemId) {
					j += items[i].getItemAmount();
				}
			}
		} 
		return j;
	}
	
	public int getAmountOfItems() {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() > -1) {
					j++;
				}
			}
		} 
		return j;		
	}
	
	public boolean stakingOverMaxAmount() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				int id = items[i].getItemId();
				// if you're both staking the item
				long l1 = (long)p2.getDuel().getItemAmount(id) + (long)items[i].getItemAmount();
				// you're staking the item, p2 has it in inventory
				long l2 = (long)p2.getInventory().getItemAmount(id) + (long)items[i].getItemAmount();
				if (l1 > Integer.MAX_VALUE || l2 > Integer.MAX_VALUE) {
					return true;
				}
			}
		}
		for (int i = 0; i < p2.getDuel().getItems().length; i++) {
			if (p2.getDuel().getSlot(i) != null) {
				// p2 is staking the item, you have it in inventory
				long l3 = (long)p2.getDuel().getSlot(i).getItemAmount() + (long)player.getInventory().getItemAmount(p2.getDuel().getSlot(i).getItemId());
				if (l3 > Integer.MAX_VALUE) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean notEnoughRoom() {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getItemId() > -1) {
					if (items[i].getDefinition().isStackable()) {
						if (player.getInventory().hasItem(items[i].getItemId())) {
							continue;
						}
					}
					j++;
				}
			}
		}
		for (int i = 0; i < p2.getDuel().getItems().length; i++) {
			if (p2.getDuel().getSlot(i) != null) {
				if (p2.getDuel().getSlot(i).getItemId() > -1) {
					if (p2.getDuel().getSlot(i).getDefinition().isStackable()) {
						if (player.getInventory().hasItem(p2.getDuel().getSlot(i).getItemId()) || hasItem(p2.getDuel().getSlot(i).getItemId())) {
							continue;
						}
					}
					j++;
				}
			}
		} 
		j += getExtraSlots();
		return j > player.getInventory().getTotalFreeSlots();		
	}
	
	private Item[] getItems() {
		return items;
	}

	public int getAmountInSlot(int slot) {
		return items[slot].getItemAmount();
	}

	public int getItemInSlot(int slot) {
		return items[slot].getItemId();
	}
	
	public Item getSlot(int slot) {
		return items[slot];
	}
	
	private Logger logger = Logger.getInstance();

	public Player getPlayer2() {
		return p2;
	}
}
