package com.anteros.content.minigames.barrows;

import com.anteros.content.skills.prayer.PrayerData;
import com.anteros.event.Event;
import com.anteros.model.Entity;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class BarrowNPCAttacks {

	public BarrowNPCAttacks() {
		
	}

	public static void attack(final NPC npc, final Entity target) {
		if (npc.isDead() || npc.getOwner() == null || npc.isDestroyed() || target.isDead() || target.isDestroyed()) {
			return;
		}
		int damage = Misc.random(npc.getMaxHit());
		int prayer = ((Player)target).getPrayers().getHeadIcon();
		int hitDelay = npc.getHitDelay();
		boolean special = false;
		switch(npc.getId()) {
			case 2026: // Dharok
				int healthHit = (npc.getMaxHp() - npc.getHp()) / 2;
				damage = Misc.random(damage + healthHit);
				if (Misc.random(1) == 0) {
					if (damage < (npc.getMaxHp() / 3)) {
						damage = (npc.getMaxHp() / 4) + Misc.random(damage + healthHit) - (npc.getMaxHp() / 4);
					}
				}
				if (prayer == PrayerData.MELEE) {
					damage = 0;
				}
				break;
				
			case 2025: // Ahrim
				hitDelay = 1000;
				((Player)target).getActionSender().sendProjectile(npc.getLocation(), target.getLocation(), 32, 156, 50, 40, 34, 60, target);
				if (Misc.random(3) == 0) {
					special = true;
					int skill = Misc.random(2);
					int currentLevel = ((Player)target).getLevels().getLevel(skill);
					int newLevel = currentLevel - Misc.random(((Player)target).getLevels().getLevelForXp(skill) / 12);
					((Player)target).getLevels().setLevel(skill, newLevel);
					((Player)target).getActionSender().sendSkillLevel(skill);
				}
				if (prayer == PrayerData.MAGIC) {
					damage = 0;
				}
				break;
				
			case 2027: // Guthan
				if (prayer == PrayerData.MELEE) {
					damage = 0;
				}
				if (Misc.random(3) == 0) {
					special = true;
					target.graphics(398);
					npc.heal(Misc.random(damage));
				}
				break;
				
			case 2030: // Verac
				if (Misc.random(1) == 0 && prayer != 0) {
					if (damage <= npc.getMaxHit() / 2) {
						damage += npc.getMaxHit() / 2;
						if (damage > npc.getMaxHit()) {
							damage = npc.getMaxHit();
						}
					}
				}
				break;
				
			case 2029: // Torag
				if (Misc.random(3) == 0) {
					special = true;
				}
				if (prayer == PrayerData.MELEE) {
					damage = 0;
				}
				break;
				
			case 2028: // Karil
				hitDelay = 700;
				((Player)target).getActionSender().sendProjectile(npc.getLocation(), target.getLocation(), 32, 27, 50, 40, 34, 40, target);
				if (Misc.random(10) == 0) {
					special = true;
					int agility = ((Player)target).getLevels().getLevel(16);
					int newAgility = agility / 4;
					if (newAgility <= 1) {
						newAgility = 1;
					}
					((Player)target).getLevels().setLevel(16, newAgility);
					((Player)target).getActionSender().sendSkillLevel(16);
				}
				if (Misc.random(1) == 0) {
					damage = damage > 0 ? damage : Misc.random(npc.getMaxHit());
				}
				if (prayer == PrayerData.RANGE) {
					damage = 0;
				}
				break;
		}
		npc.animate(npc.getAttackAnimation());
		target.setLastAttacked(System.currentTimeMillis());
		npc.setLastAttack(System.currentTimeMillis());
		npc.resetCombatTurns();
		target.setAttacker(npc);
		if ((target.getCombatTurns() > 2 || target.getCombatTurns() < 0)) {
			target.animate(target.getDefenceAnimation());
		}
		if (damage > target.getHp()) {
			damage = target.getHp();
		}
		final int hit = damage;
		final boolean spec = special;
		World.getInstance().registerEvent(new Event(hitDelay) {
			@Override
			public void execute() {
				if (npc.getId() == 2025) {
					if (spec) {
						target.graphics(400, 0, 100);
						((Player)target).getActionSender().sendMessage("You feel weakened.");
						
					}
					target.graphics(hit == 0 ? 85 : 157, 0, 100);
				} else if (npc.getId() == 2027) {
					if (spec) {
						if (!npc.isDead()) {
							int newHp = npc.getHp() + hit;
							if (newHp > npc.getMaxHp()) {
								newHp = npc.getMaxHp();
							}
							npc.setHp(newHp);
						}
					}
				} else if (npc.getId() == 2029) {
					if (spec) {
						target.graphics(399, 0, 100);
						int energy = ((Player)target).getRunEnergy();
						int newEnergy = energy - (int) (energy * 0.50);
						if (newEnergy < 0) {
							newEnergy = 0;
						}
						((Player)target).getActionSender().sendMessage("You feel drained of energy.");
						((Player)target).setRunEnergy(newEnergy);
						((Player)target).getActionSender().sendEnergy();
					}
				} else if (npc.getId() == 2028) {
					if (spec) {
						target.graphics(399);
						((Player)target).getActionSender().sendMessage("You feel less agile.");
					}
				}
				target.hit(hit);
				this.stop();
			}
		});
	}
}
