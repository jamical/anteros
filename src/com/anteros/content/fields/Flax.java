package com.anteros.content.fields;

import java.util.ArrayList;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class Flax {
	 /**
	  * Flaxpicking.
	  * @author Derive
	  * @author Abexlry
	  *
	  */
	public static ArrayList <int[]> flaxRemoved = new ArrayList<int[]>();
	
	public static void pickFlax(final Player player, final int objectX, final int objectY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, objectX - 1, objectY - 1, objectX + 1, objectY + 1) {
			@Override
			public void run() {
				if (player.getInventory().getTotalFreeSlots() != 0) {
		            player.getInventory().addItem(1779, 1);
		            player.animate(827);
					player.getActionSender().sendMessage("You pick some flax.");
					if (Misc.random(3) == 1) {
						World.getInstance().getGlobalObjects().newFieldObject(player, 2646, Location.location(objectX, objectY, player.getLocation().getZ()), 5000);
						flaxRemoved.add(new int[] { objectX, objectY });
						player.getActionSender().removeObject(Location.location(objectX, objectY, player.getLocation().getZ()), 0, 10);
					}
				} else {
					player.getActionSender().sendMessage("Not enough space in your inventory.");
					return;
				}
			}
		});
	}
}
