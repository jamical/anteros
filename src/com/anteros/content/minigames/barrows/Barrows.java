package com.anteros.content.minigames.barrows;

import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class Barrows extends BarrowsData {
	
	public Barrows() {
		
	}
	
	/*
	 * The config to remove roofs is 1270
	 * The door is 6713
	 * 
	 * Random door configs 
	 * CONFIG = 452    0
CONFIG = 452    32
CONFIG = 452    96
CONFIG = 452    16480
CONFIG = 452    278624
CONFIG = 452    802912
CONFIG = 452    2900064
CONFIG = 452    2637920
CONFIG = 452    2638944
CONFIG = 452    2640992
CONFIG = 452    2645088
CONFIG = 452    2653280
CONFIG = 452    2649184
	 */
	
	
	
	public static boolean enterCrypt(Player p) {
		for (int i = 0; i < MOUND_COORDS.length; i++) {
			for (int j = 0; j < MOUND_COORDS[i].length; j++) {
				if (p.getLocation().inArea(MOUND_COORDS[i][0], MOUND_COORDS[i][1], MOUND_COORDS[i][2], MOUND_COORDS[i][3]) && p.getLocation().getZ() == 0) {
					p.teleport(Location.location(STAIR_COORDS[i][0], STAIR_COORDS[i][1], 3));
					if (p.getSettings().getBarrowTunnel() == -1) {
						p.getSettings().setBarrowTunnel(Misc.random(5));
					}
					return true;
				}
			}
		}
		return false;
	}

	public static boolean leaveCrypt(Player player, int stairs, final int x, final int y) {
		if (stairs != 6707 && stairs != 6703 && stairs != 6702 && stairs != 6704 && stairs != 6705 && stairs != 6706) {
			return false;
		}
		final Player p = player;
		Location stairLocation;
		Location moundLocation;
		final int cryptIndex = getCryptIndex(p);
		if (cryptIndex == -1) {
			return false;
		}
		stairLocation = Location.location(STAIR_COORDS[cryptIndex][0], STAIR_COORDS[cryptIndex][1], 3);
		moundLocation = Location.location(MOUND_COORDS[cryptIndex][0] + Misc.random(3), MOUND_COORDS[cryptIndex][1] + Misc.random(3), 0);
		final Location teleLoc = moundLocation;
		if (p.getLocation().equals(stairLocation)) {
			p.setFaceLocation(Location.location(x, y, 3));
			p.teleport(teleLoc);
			return true;
		}
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, stairLocation) {
			@Override
			public void run() {
				p.teleport(teleLoc);
			}
		});
		return true;
	}
	
	public static void removeBrotherFromGame(Player p) {
		for (NPC n : World.getInstance().getNpcList()) {
			if (n != null) {
				if (n.getId() >= 2025 && n.getId() <= 2030) {
					if (n.getOwner() == null || n.getOwner().equals(p) || n.getOwner().isDestroyed()) {
						if (!n.isDead()) {
							n.setHidden(true);
							World.getInstance().getNpcList().remove(n);
						}
					}
				}
			}
		}
	}
	
	public static boolean tryOpenCoffin(Player player, final int objectId) {
		if (objectId != 6823 && objectId != 6771 && objectId != 6821 && objectId != 6773 && objectId != 6822 && objectId != 6772) {
			return false;
		}
		final Player p = player;
		int cryptIndex = getCryptIndex(p);
		if (cryptIndex == -1) {
			return false;
		}
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, COFFIN_AREA[cryptIndex][0], COFFIN_AREA[cryptIndex][1], COFFIN_AREA[cryptIndex][2], COFFIN_AREA[cryptIndex][3]) {
			@Override
			public void run() {
				openCoffin(p, objectId);
			}
		});
		return true;
	}

	public static boolean openCoffin(Player p, int objectId) {
		if (objectId != 6823 && objectId != 6771 && objectId != 6821 && objectId != 6773 && objectId != 6822 && objectId != 6772) {
			return false;
		}
		int cryptIndex = getCryptIndex(p);
		if (cryptIndex == -1) {
			return false;
		}
		if (p.getSettings().getBarrowBrothersKilled(cryptIndex)) {
			p.getActionSender().sendMessage("You don't find anything.");
			return true;
		}
		if (p.getSettings().getBarrowTunnel() == cryptIndex){
			p.getActionSender().modifyText("You find a hidden tunnel, do you want to enter?", 210, 1);
			p.getActionSender().sendChatboxInterface(210);
			p.setTemporaryAttribute("barrowTunnel", 1);
			return true;
		}
		for (NPC n : World.getInstance().getNpcList()) {
			if (n.getId() == BROTHER_ID[cryptIndex]) {
				if (n.getOwner().equals(p)) {
					p.getActionSender().sendMessage("You don't find anything.");
					return true;
				}
			}
		}
		NPC npc = new NPC(BROTHER_ID[cryptIndex]);
		npc.readResolve();
		npc.setLocation(p.getLocation());
		npc.setEntityFocus(p.getClientIndex());
		npc.setOwner(p);
		npc.setTarget(p);
		npc.setCombatTurns(npc.getAttackSpeed());
		World.getInstance().getNpcList().add(npc);
		p.getActionSender().setArrowOnEntity(1, npc.getClientIndex());
		return true;
	}
	
	public static int getCryptIndex(Player p) {
		if (p.getLocation().inArea(3567, 9701, 3580, 9711)) {
			return VERAC; 
		} else if (p.getLocation().inArea(3548, 9709, 3561, 9721)) {
			return DHAROK;
		} else if (p.getLocation().inArea(3549, 9691, 3562, 9706)) {
			return AHRIM;
		} else if (p.getLocation().inArea(3532, 9698, 3546, 9710)) {
			return GUTHAN;
		} else if (p.getLocation().inArea(3544, 9677, 3559, 9689)) {
			return KARIL;
		} else if (p.getLocation().inArea(3563, 9680, 3577, 9694)) {
			return TORAG;
		}
		return -1;
	}

	public static void verifyEnterTunnel(Player p) {
		p.getActionSender().closeInterfaces();
		if (p.getTemporaryAttribute("barrowTunnel") != null) {
			if ((Integer) p.getTemporaryAttribute("barrowTunnel") == 2) {
				p.teleport(Location.location(3568, 9712, 0));
				p.removeTemporaryAttribute("barrowTunnel");
				return;
			}
		}
		p.getActionSender().sendChatboxInterface(228);
		p.getActionSender().modifyText("Yeah, I'm fearless!", 228, 2);
		p.getActionSender().modifyText("No way, that looks scary!", 228, 3);
		p.setTemporaryAttribute("barrowTunnel", 2);
	}

	public static void openChest(Player player) {
		final Player p = player;
		if (p.getLocation().getZ() != 0 || p.getTemporaryAttribute("lootedBarrowChest") != null) {
			return;
		}
		if (!p.getLocation().inArea(3551, 9694, 3552, 9694)) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3551, 9694, 3552, 9694) {
				@Override
				public void run() {
					openChest(p);
				}
			});
			return;
		}
		for (int i = 0; i < 6; i++) {
			if (!p.getSettings().getBarrowBrothersKilled(i)) {
				for (NPC n : World.getInstance().getNpcList()) {
					if (n != null) {
						if (n.getId() == BROTHER_ID[i]) {
							if (n.getOwner().equals(p)) {
								return;
							}
						}
					}
				}
				NPC npc = new NPC(BROTHER_ID[i]);
				npc.readResolve();
				npc.setLocation(p.getLocation());
				npc.setEntityFocus(p.getClientIndex());
				npc.setOwner(p);
				npc.setTarget(p);
				npc.setCombatTurns(npc.getAttackSpeed());
				World.getInstance().getNpcList().add(npc);
				p.getActionSender().setArrowOnEntity(1, npc.getClientIndex());
				return;
			}
		}
		p.getActionSender().sendMessage("You begin to lift open the massive chest...");
		p.animate(833);
		World.getInstance().registerEvent(new Event(1000) {
			
			@Override
			public void execute() {
				this.stop();
				p.getActionSender().sendMessage("..You loot the chest and the tomb begins to shake!");
				p.getActionSender().createObject(6775, Location.location(3551, 9695, 0), 0, 10);
				getBarrowReward(p);
				startEarthQuake(p);
			}
		});
	}

	protected static void getBarrowReward(Player p) {
		int barrowChance = Misc.random(BARROWS_CHANCE);
		int killCount = p.getSettings().getBarrowKillCount();
		if (barrowChance == 0) {
			int reward = BARROW_REWARDS[(int) (Math.random() * BARROW_REWARDS.length)];
			p.getInventory().addItemOrGround(reward);
		}
		if (Misc.random(20) == 0) {
			p.getInventory().addItemOrGround(1149); // Dragon med helm.
		} else if (Misc.random(15) == 0) {
			int halfKey = Misc.random(1) == 0 ? 985 : 987; 
			p.getInventory().addItemOrGround(halfKey); // Half key.
		}
		if (Misc.random(3) == 0 || p.getSettings().getBarrowTunnel() == KARIL) { // Bolt racks.
			int amount = getAmountOfReward(4740, killCount);
			p.getInventory().addItemOrGround(4740, amount);
		}
		if (Misc.random(3) == 0) { // Blood runes
			int amount = getAmountOfReward(565, killCount);
			p.getInventory().addItemOrGround(565, amount);
		}
		if (Misc.random(2) == 0) { // Death runes
			int amount = getAmountOfReward(560, killCount);
			p.getInventory().addItemOrGround(560, amount);
		}
		if (Misc.random(1) == 0) { // Chaos runes
			int amount = getAmountOfReward(562, killCount);
			p.getInventory().addItemOrGround(562, amount);
		}
		if (Misc.random(1) == 0) { // Coins
			int amount = getAmountOfReward(995, killCount);
			p.getInventory().addItemOrGround(995, amount);
		}
		if (Misc.random(1) == 0) {
			int amount = getAmountOfReward(558, killCount); // Mind runes.
			p.getInventory().addItemOrGround(558, amount);
		}
	}

	private static int getAmountOfReward(int item, int killCount) {
		int amount = 0;
		for (int i = 0; i < OTHER_REWARDS.length; i++) {
			if (OTHER_REWARDS[i] == item) {
				for (int j = 0; j < REWARD_KILLCOUNT[i].length; j++) {
					if (killCount >= REWARD_KILLCOUNT[i][j]) {
						amount = REWARD_AMOUNT[i][j];
					}
				}
				if (amount < MINIMUM_AMOUNT[i]) {
					amount = MINIMUM_AMOUNT[i];
				}
				break;
			}
		}
		return Misc.random(amount);
	}

	public static void startEarthQuake(Player player) {
		final Player p = player;
		p.getActionSender().newEarthquake(4, 4, 4, 4, 4);
		p.setTemporaryAttribute("lootedBarrowChest", true);
		p.getActionSender().createObject(6775, Location.location(3551, 9695, 0), 0, 10);
		p.getSettings().setBarrowKillCount(0);
		p.getActionSender().sendConfig(453, 0);
		World.getInstance().registerEvent(new Event(3000+ Misc.random(10000)) {
			
			@Override
			public void execute() {
				if (p.getTemporaryAttribute("lootedBarrowChest") == null) {
					this.stop();
					return;
				}
				p.graphics(405);
				p.animate(p.getDefenceAnimation());
				p.hit(7 + Misc.random(15));
				this.setTick(8000 + Misc.random(20000));
			}
		});
	}

	public static void killBrother(Player p, int id) {
		for (int i = 0; i < BROTHER_ID.length; i++) {
			if (id == BROTHER_ID[i]) {
				p.getSettings().setBarrowBrothersKilled(i, true);
				p.getSettings().setBarrowKillCount(p.getSettings().getBarrowKillCount() + 1);
				p.getActionSender().modifyText("Kill Count: "+p.getSettings().getBarrowKillCount(), 24, 0);
				break;
			}
		}
	}

	public static boolean openTunnelDoor(Player player, int doorId, int x, int y) {
		if (doorId < 6716 || (doorId > 6731 && doorId < 6735) || doorId > 6750) {
			return false;
		}
		final Player p = player;
		final int index = getDoorIndex(doorId, x, y);
		final int index2 = getOtherDoor(x, y); // index of the door next to the one you clicked.
		if (index == -1 || index2 == -1) {
			return false;
		}
		final boolean betweenDoors = p.getTemporaryAttribute("betweenDoors") != null;
		final Location clickedDoor = Location.location(DOOR_LOCATION[index][0], DOOR_LOCATION[index][1], 0);
		final Location otherDoor = Location.location(DOOR_LOCATION[index2][0], DOOR_LOCATION[index2][1], 0);
		final int openDoorId = DOOR_OPEN_DIRECTION[index][0];
		final int openDoorId2 = DOOR_OPEN_DIRECTION[index2][0];
		final int openDirection = DOOR_OPEN_DIRECTION[index][2];
		final int newX = openDirection == 1 ? x+1 : openDirection == 2 ? x : openDirection == 3 ? x-1 : openDirection == 4 ? x : x;
		final int newY = openDirection == 1 ? y : openDirection == 2 ? y+1 : openDirection == 3 ? y : openDirection == 4 ? y-1 : y;
		final int newX2 = openDirection == 1 ? DOOR_LOCATION[index2][0]+1 : openDirection == 2 ? DOOR_LOCATION[index2][0] : openDirection == 3 ? DOOR_LOCATION[index2][0]-1 : openDirection == 4 ? DOOR_LOCATION[index2][0] : DOOR_LOCATION[index2][0];
		final int newY2 = openDirection == 1 ? DOOR_LOCATION[index2][1] : openDirection == 2 ? DOOR_LOCATION[index2][1]+1 : openDirection == 3 ? DOOR_LOCATION[index2][1] : openDirection == 4 ? DOOR_LOCATION[index2][1]-1 : DOOR_LOCATION[index2][1];
		int[] doorStandCoordinates = getDoorCoordinates(p, index, index2, betweenDoors);
		final int[] walkDirections = getWalkDirections(p, index, index2, betweenDoors);
		p.setFaceLocation(clickedDoor);
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, doorStandCoordinates[0], doorStandCoordinates[1], doorStandCoordinates[2] + 1, doorStandCoordinates[3] + 1) {
			@Override
			public void run() {
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(800) {

					@Override
					public void execute() {
						p.getWalkingQueue().reset();
						for (Player p : World.getInstance().getPlayerList()) {
							p.getActionSender().removeObject(clickedDoor, openDoorId == 6713 ? 4 : 3, 0);
							p.getActionSender().removeObject(otherDoor, openDoorId2 == 6732 ? 3 : 4, 0);
							p.getActionSender().createObject(openDoorId, Location.location(newX, newY, 0), DOOR_OPEN_DIRECTION[index][1], 0);
							p.getActionSender().createObject(openDoorId2, Location.location(newX2, newY2, 0), DOOR_OPEN_DIRECTION[index2][1], 0);
						}
						p.getWalkingQueue().forceWalk(walkDirections[0], walkDirections[1]);
						stop();
					}
				});

				World.getInstance().registerEvent(new Event(betweenDoors ? 2200 : 1900) {

					@Override
					public void execute() {
						int face = openDirection == 3 ? 0 : openDirection == 4 ? 3 : openDirection == 2 ? 1 : 2;
						for (Player p : World.getInstance().getPlayerList()) {
							p.getActionSender().removeObject(Location.location(newX, newY, 0), openDoorId == 6713 ? 4 : 3, 0);
							p.getActionSender().removeObject(Location.location(newX2, newY2, 0), openDoorId2 == 6732 ? 3 : 4, 0);
							p.getActionSender().createObject(DOORS[index], clickedDoor, face, 0);
							p.getActionSender().createObject(DOORS[index2], otherDoor, face, 0);
						}
						p.removeTemporaryAttribute("unmovable");
						if (!betweenDoors) {
							p.getActionSender().sendConfig(1270, 1);
							p.setTemporaryAttribute("betweenDoors", true);
						} else {
							p.getActionSender().sendConfig(1270, 0);
							p.removeTemporaryAttribute("betweenDoors");
						}
						stop();
					}
				});
			}
		});
		return true;
	}

	private static int[] getWalkDirections(Player p, int index, int index2, boolean betweenDoors) {
		int openDirection = DOOR_OPEN_DIRECTION[index][2];
		int[] direction = new int[2];
		if (openDirection == 0) {
			/*Nothing*/
		} else if (openDirection == 1) { // doors open east.
			direction[0] = betweenDoors ? +1 : -1;
			direction[1] = 0; 
		} else if (openDirection == 2) { // doors open north.
			direction[0] = 0;
			direction[1] = betweenDoors ? +1 : -1;
		} else if (openDirection == 3) { // doors open west.
			direction[0] = betweenDoors ? -1 : +1;
			direction[1] = 0;
		} else if (openDirection == 4) { // doors open south.
			direction[0] = 0;
			direction[1] = betweenDoors ? -1 : +1;
		}
		return direction;
	}

	/**
	 * Returns the coordinates a player must be stood on to use a door, this varies
	 * due to the direction of the doors and which side of the door you are on.
	 */
	private static int[] getDoorCoordinates(Player p, int index, int index2, boolean betweenDoors) {
		int openDirection = DOOR_OPEN_DIRECTION[index][2];
		int doorX = DOOR_LOCATION[index][0];
		int doorY = DOOR_LOCATION[index][1];
		int otherDoorX = DOOR_LOCATION[index2][0];
		int otherDoorY = DOOR_LOCATION[index2][1];
		int[] coordinates = new int[4];
		coordinates[0] = getLowest(doorX, otherDoorX);
		coordinates[1] = getLowest(doorY, otherDoorY);
		coordinates[2] = getHighest(doorX, otherDoorX);
		coordinates[3] = getHighest(doorY, otherDoorY);
		if (!betweenDoors) { // Player isn't between doors, and is 1 coord from the door.
			if (openDirection == 3) {
				coordinates[0] -= 1;
			} else if (openDirection == 4) {
				coordinates[1] -= 1;
			}
		}
		return coordinates;
	}
	
	public static void prayerDrainEvent(final Player p) {
		World.getInstance().registerEvent(new Event(5000 + Misc.random(5000)) {

			@Override
			public void execute() {
				if (p.getTemporaryAttribute("atBarrows") == null) {
					this.stop();
					return;
				}
				int currentPrayer = p.getLevels().getLevel(5);
				int maxLevel = p.getLevels().getLevelForXp(5);
				int levelBy10 = currentPrayer - (maxLevel / 6);
				if (currentPrayer > 0) {
					p.getLevels().setLevel(5, levelBy10);
					p.getActionSender().sendSkillLevel(5);
				}
				int[] array = p.getLocation().getZ() == 0 ? HEADS[1] : HEADS[0];
				int head = array[(int) (Math.random() * array.length)];
				int slot = Misc.random(5);
				if (slot == 0) {
					slot=1;
				}
				p.getActionSender().itemOnInterface(24, slot, 100,head);
				p.getActionSender().animateInterface(2085, 24, slot);
				this.setTick(5000 + Misc.random(15000));
				
				final int s = slot;
				World.getInstance().registerEvent(new Event(4000) {
					@Override
					public void execute() {
						p.getActionSender().itemOnInterface(24, s, 100, -1);
						p.getActionSender().animateInterface(-1, 24, s);
						this.stop();
					}
				});
			}
		});
	}
	
	private static int getLowest(int i, int j) {
		return i > j ? j : i;
	}
	
	private static int getHighest(int i, int j) {
		return i > j ? i : j;
	}

	private static int getOtherDoor(int x, int y) {
		for (int i = 0; i < DOOR_LOCATION.length; i++) {
			if ((x == DOOR_LOCATION[i][0] && y + 1 == DOOR_LOCATION[i][1]) ||
				(x + 1 == DOOR_LOCATION[i][0] && y == DOOR_LOCATION[i][1]) ||
				(x == DOOR_LOCATION[i][0] && y - 1 == DOOR_LOCATION[i][1]) ||
				(x - 1 == DOOR_LOCATION[i][0] && y == DOOR_LOCATION[i][1])) {
				return i;
			}
		}
		return -1;
	}

	private static int getDoorIndex(int doorId, int x, int y) {
		for (int i = 0; i < DOORS.length; i++) {
			if (doorId == DOORS[i]) {
				if (x == DOOR_LOCATION[i][0] && y == DOOR_LOCATION[i][1]) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public static boolean betweenDoors(Player p) {
		for (int i = 0; i < DB.length; i++) {
			if (p.getLocation().inArea(DB[i][0], DB[i][1], DB[i][2], DB[i][3])) {
				return true;
			}
		}
		return false;
	}
}
