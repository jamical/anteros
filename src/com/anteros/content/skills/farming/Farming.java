package com.anteros.content.skills.farming;

import com.anteros.content.skills.woodcutting.Tree;
import com.anteros.content.skills.woodcutting.Woodcutting;
import com.anteros.content.skills.woodcutting.WoodcuttingData;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Item;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class Farming extends FarmingData {

	public Farming() {
		
	}
	
	public static boolean interactWithPatch(final Player p, final int objectId, int objectX, int objectY, final int seedId) {
		for (int i = 0; i < PATCHES.length; i++) {
			if (objectId == (Integer)PATCHES[i][1]) {
				final Location patchLocation = Location.location(objectX, objectY, 0);
				final int j = i;
				int[] data = getPatchDistances(i, objectX, objectY);
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, data[0], data[1], data[2], data[3]) {
		
					@Override
					public void run() {
						tendToPatch(p, patchLocation, seedId, j);
					}
				});
				return true;
			}
		}
		return false;
	}

	protected static void tendToPatch(Player p, Location patchLocation, int item, int i) {
		Patch patch = null;
		i = getPatchIndex(patchLocation, i);
		if (i == -1) {
			return;
		}
		patch = World.getInstance().getGlobalObjects().getFarmingPatches().patchExists(p, i);
		if (patch == null) {
			patch = new Patch(p.getUsername(), (PatchType)PATCHES[i][0], i, (Integer)PATCHES[i][7], (Integer)PATCHES[i][6]);
			patch.setConfigArray(WEEDS_CONFIG);
			World.getInstance().getGlobalObjects().getFarmingPatches().addPatch(patch);
			rakePatch(p, patch);
		} else {
			if (!patch.patchOccupied() && (item == -1 || item == TOOLS[0])) {
				rakePatch(p, patch);
			} else if (patch.isBlankPatch() && item != -1) {
				plantCrop(p, patch, item);
			} else if (patch.isFullyGrown() && (item == -1 || item == TOOLS[3])) {
				harvestCrop(p, patch);
			} else if (patch.isFruitTree() && patch.getStatus() >= 6 && patch.getStatus() <= 11) {
				harvestFruit(p, patch);
			} else if ((patch.isTree() || patch.isFruitTree()) && patch.getStatus() == patch.checkHealthStatus() && !patch.isHealthChecked()) {
				checkTreeHealth(p, patch);
			} else if ((patch.isTree() || patch.isFruitTree()) && patch.getStatus() == patch.chopStatus() && patch.isHealthChecked()) {
				chopTree(p, patch);
			} else if ((patch.isTree() || patch.isFruitTree()) && patch.getStatus() == patch.stumpStatus() && patch.isHealthChecked() && item == TOOLS[3]) {
				digUpTree(p, patch);
			} else if (patch.isFruitTree() && patch.getStatus() == patch.stumpStatus() && !patch.isHealthChecked()) {
				p.getActionSender().sendMessage("You must check the tree's health before you can chop it down.");
			} else {
				p.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
	}
	
	private static void harvestFruit(final Player p, final Patch patch) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		p.animate(2282);
		p.setTemporaryAttribute("unmovable", true);
		patch.setWeeding(true);
		World.getInstance().registerEvent(new Event(1700) {
			@Override
			public void execute() {
				if (patch.getStatus() == patch.chopStatus() || p.isDisconnected() || p.isDestroyed() || p.isDead() || p.getTemporaryAttribute("teleporting") != null) {
					this.stop();
					return;
				}
				p.animate(2282);
				final String s = patch.getSeedIndex() == 41 ? "leaf " : "";
				World.getInstance().registerEvent(new Event(800) {
					@Override
					public void execute() {
						this.stop();
						p.getActionSender().sendMessage("You pick " + (String)SEEDS[patch.getSeedIndex()][9] + " " + (String)SEEDS[patch.getSeedIndex()][7] + s + " from the tree.");
						p.getLevels().addXp(FARMING, (Double)SEEDS[patch.getSeedIndex()][8]);
						p.getInventory().addItemOrGround((Integer)SEEDS[patch.getSeedIndex()][2], 1);
						patch.setStatus(patch.getStatus() - 1);
						if (patch.getStatus() == 5) { // We have taken all the fruit (it is 5 after we lower the status above)
							patch.setStatus(13); // Chop option
							p.removeTemporaryAttribute("unmovable");
							patch.setWeeding(false);
						}
						setConfig(p, patch);
					}
				});
			}
		});
	}
	
	private static void chopFruitTree(final Player p, final Patch patch) {
		if (!Woodcutting.hasAxe(p)) {
			p.getActionSender().sendMessage("You don't have an axe.");
			return;
		}
		patch.setWeeding(true); // prevents it from growing which makes me rage
		p.animate(Woodcutting.getAxeAnimation(p));
		p.setTemporaryAttribute("harvesting", true);
		World.getInstance().registerEvent(new Event(2550) {

			@Override
			public void execute() {
				if (p.getTemporaryAttribute("harvesting") != null) {
					patch.setStatus(patch.stumpStatus());
					setConfig(p, patch);
				}
				this.stop();
				patch.setWeeding(false);
			}
		});
	}

	private static void digUpTree(final Player p, final Patch patch) {
		p.animate(830);
		World.getInstance().registerEvent(new Event(1000) {

			@Override
			public void execute() {
				patch.setStatus(2);
				patch.setConfigArray(WEEDS_CONFIG);
				setConfig(p, patch);
				p.getActionSender().sendMessage("You dig up the tree.");
				this.stop();
			}
		});
	}

	private static void chopTree(final Player p, final Patch patch) {
		if (patch.isFruitTree()) {
			chopFruitTree(p, patch);
			return;
		}
		if (!Woodcutting.hasAxe(p)) {
			p.getActionSender().sendMessage("You don't have an axe.");
			return;
		}
		if (!hasLevelToCutTree(p, patch)) {
			p.getActionSender().sendMessage("You will recieve no logs from this tree, due to your Woodcutting level.");
		}
		final Tree newTree = new Tree(0, 0, null, (Integer)SEEDS[patch.getSeedIndex()][2], 0, (String)SEEDS[patch.getSeedIndex()][7], (Double)SEEDS[patch.getSeedIndex()][11], 0);
		p.setTemporaryAttribute("cuttingTree", newTree);
		p.animate(Woodcutting.getAxeAnimation(p));
		p.getActionSender().sendMessage("You begin to swing your axe at the tree..");
		long delay = getCutTime(p, patch);
		final boolean canRecieveLog = hasLevelToCutTree(p, patch);
		World.getInstance().registerEvent(new Event(delay) {
			long timeSinceLastAnimation = System.currentTimeMillis();
			@Override
			public void execute() {
				if (!Woodcutting.hasAxe(p)) {
					p.getActionSender().sendMessage("You don't have an axe.");
					Woodcutting.resetWoodcutting(p);
					this.stop();
					return;
				}
				if (p.getTemporaryAttribute("cuttingTree") == null) {
					Woodcutting.resetWoodcutting(p);
					this.stop();
					return;
				}
				Tree tree = (Tree)p.getTemporaryAttribute("cuttingTree");
				if (!newTree.equals(tree)) {
					this.stop();
					return;
				}
				if (canRecieveLog) {
					String s = tree.getLog() == 1521 ? "an" : "a";
					if (p.getInventory().addItem(tree.getLog())) {
						p.getLevels().addXp(8, tree.getXp());
						p.getActionSender().sendMessage("You cut down " + s + " " + tree.getName() + " log.");
					} else {
						p.getActionSender().sendChatboxInterface(210);
						p.getActionSender().modifyText("Your inventory is too full to carry any logs.", 210, 1);
						p.animate(65535);
						this.stop();
						return;
					}
				}
				if (Misc.random(canRecieveLog ? 2 : 0) == 0) {
					p.animate(65535);
					patch.setStatus(patch.getConfigLength() - 1);
					setConfig(p, patch);
					this.stop();
					return;
				}
				if (System.currentTimeMillis() - timeSinceLastAnimation >= 2550) {
					p.animate(Woodcutting.getAxeAnimation(p));
					timeSinceLastAnimation = System.currentTimeMillis();
				}
			}
		});
	}

	public static int getCutTime(Player p, Patch patch) {
		int standardTime = (Integer)SEEDS[patch.getSeedIndex()][12];
		int randomTime = standardTime + Misc.random(standardTime);
		int axeTime = WoodcuttingData.AXE_DELAY[Woodcutting.getUsedAxe(p)];
		int finalTime = randomTime -= axeTime;
		if (finalTime <= 1000 || finalTime <= 0) {
			finalTime = 1000;
		}
		return finalTime;
	}

	private static boolean hasLevelToCutTree(Player p, Patch patch) {
		if (patch.isFruitTree()) {
			return true;
		}
		return p.getLevels().getLevel(8) >= (Integer)SEEDS[patch.getSeedIndex()][10];
	}

	private static void checkTreeHealth(Player p, Patch patch) {
		patch.setHealthChecked(true);
		if (patch.isFruitTree()) {
			patch.setStatus(11);
		} else {
			patch.setStatus(patch.getConfigLength() - 2);
		}
		p.getActionSender().sendMessage("You check the health of the tree.");
		setConfig(p, patch);
		p.getLevels().addXp(FARMING, (Double)SEEDS[patch.getSeedIndex()][5]);
	}

	private static void harvestCrop(final Player p, final Patch patch) {
		if (patch.getPatchType().equals(PatchType.VEGATABLE) || patch.getPatchType().equals(PatchType.VEGATABLE_1)) {
			if (!p.getInventory().hasItem(TOOLS[3])) {
				p.getActionSender().sendMessage("You need a spade to harvest your crops.");
				return;
			}
		}
		final PatchType patchType = patch.getPatchType();
		final int emote = (patchType.equals(PatchType.HERB) || patchType.equals(PatchType.FLOWER)) ? 2282 : 830;
		int delay = patchType.equals(PatchType.HERB) ? 2250 : 1500;
		final int amount = patchType.equals(PatchType.FLOWER) ? 1 : (Integer)PATCHES[patch.getPatchIndex()][8] + Misc.random(10);
		final int del = delay;
		final String s =  patchType.equals(PatchType.FLOWER) ? "flower patch" : patchType.equals(PatchType.HERB) ? "herb patch" : "allotment";
		final String s1 =  patchType.equals(PatchType.FLOWER) || patchType.equals(PatchType.HERB) ? "pick" : "harvest";
		p.animate(emote);
		p.setTemporaryAttribute("harvesting", true);
		World.getInstance().registerEvent(new Event(del) {
			int i = 1;
			public void execute() {
				if (p.getTemporaryAttribute("harvesting") == null) {
					this.stop();
					return;
				}
				p.animate(emote);
				World.getInstance().registerEvent(new Event(800) {

					public void execute() {
						this.stop();
						p.getLevels().addXp(FARMING, (Double)SEEDS[patch.getSeedIndex()][5]);
						p.getInventory().addItemOrGround((Integer)SEEDS[patch.getSeedIndex()][2], 1);
						p.getActionSender().sendMessage("You " + s1 + " " +(String)SEEDS[patch.getSeedIndex()][8] + " " + (String)SEEDS[patch.getSeedIndex()][7] + " from the " + s + " .");
						if (i++ >= amount) {
							p.getActionSender().sendMessage("The patch has been cleared.");
							patch.setStatus(2);
							patch.setConfigArray(WEEDS_CONFIG);
							setConfig(p, patch);
						}
					}
				});
				if (i >= amount) {
					this.stop();
					return;
				}
			}
		});
	}

	private static void plantCrop(final Player p, final Patch patch, int seedId) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		for (int i = 0; i < SEEDS.length; i++) {
			if (seedId == (Integer)SEEDS[i][1]) {
				final PatchType type = patch.getPatchType();
				final boolean sapling = type.equals(PatchType.TREE) || type.equals(PatchType.FRUIT_TREE);
				String s = sapling ? "sapling" : "seeds.";
				String s1 = s.equals("sapling") ? patch.getSeedIndex() == 33 || patch.getSeedIndex() == 38 || patch.getSeedIndex() == 40 ? "an " : "a " : "";
				if (!patch.getPatchType().equals(PatchType.VEGATABLE) && !patch.getPatchType().equals(PatchType.VEGATABLE_1)) {
					if (!patch.getPatchType().equals((PatchType)SEEDS[i][0])) {
						String s2 = !((PatchType)SEEDS[i][0]).equals(PatchType.FRUIT_TREE) && !((PatchType)SEEDS[i][0]).equals(PatchType.TREE)? "seed" : "tree";
						p.getActionSender().sendMessage("This type of " + s2 + " cannot be planted here.");
						return;
					}
				} else if (patch.getPatchType().equals(PatchType.VEGATABLE) || patch.getPatchType().equals(PatchType.VEGATABLE_1)) {
					if (!SEEDS[i][0].equals(PatchType.VEGATABLE) && !SEEDS[i][0].equals(PatchType.VEGATABLE_1)) {
						p.getActionSender().sendMessage("This type of seed  cannot be planted here.");
						return;
					}
				}
				final int[] data = getDataForPatch(patch, i);
				if (data == null) {
					return;
				}
				patch.setSeedIndex(i);
				if (p.getLevels().getLevel(FARMING) < (Integer)SEEDS[patch.getSeedIndex()][6]) {
					p.getActionSender().sendMessage("You need a Farming level of " + (Integer)SEEDS[patch.getSeedIndex()][6] + " to plant " + s1 + "" + (String)SEEDS[patch.getSeedIndex()][7] + " seeds.");
					return;
				}
				final int seedAmount = (Integer)PATCHES[patch.getPatchIndex()][8];
				if (!p.getInventory().hasItemAmount((Integer)SEEDS[i][1], seedAmount)) {
					p.getActionSender().sendMessage("This patch requires " + seedAmount + " seeds.");
					return;
				}
				if (!sapling) {
					if (!p.getInventory().hasItem(TOOLS[1])) {
						p.getActionSender().sendMessage("You need a seed dibber to plant seeds.");
						return;
					}
				} else {
					if (!p.getInventory().hasItem(TOOLS[2])) {
						p.getActionSender().sendMessage("You need a trowel to transfer the sapling from the pot to a farming patch.");
						return;
					}
					if (!p.getInventory().hasItem(TOOLS[3])) {
						p.getActionSender().sendMessage("You need a spade to plant the salping.");
						return;
					}
				}
				final int j = i;
				p.animate(2291);
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(1000) {

					@Override
					public void execute() {
						this.stop();
						if (p.getInventory().deleteItem((Integer)SEEDS[j][1], seedAmount)) {
							if (sapling) {
								p.getInventory().addItemOrGround(TOOLS[4]);
							}
							patch.setStatus(0);
							patch.setConfigArray(data);
							patch.setTimeToGrow((Long)SEEDS[j][3]);
							setConfig(p, patch);
							p.removeTemporaryAttribute("unmovable");
							String s = seedAmount > 1 ? "seeds." : "seed.";
							String s1 = seedAmount > 1 ? "" + seedAmount : "a";
							if ((patch.getPatchType().equals(PatchType.HERB) && (patch.getSeedIndex() == 20 || patch.getSeedIndex() == 21))) {
								s1 = "an";
							}
							String message = sapling ? "You plant the " + (String)SEEDS[patch.getSeedIndex()][7] + " sapling." : "You plant " + s1 + " " + (String)SEEDS[patch.getSeedIndex()][7] + " " + s;
							p.getActionSender().sendMessage(message);
							p.getLevels().addXp(FARMING, (Double)SEEDS[patch.getSeedIndex()][4]);
						}
					}
				});
				break;
			}
		}
	}


	private static void rakePatch(final Player p, final Patch patch) {
		if (patch.isBlankPatch()) {
			p.getActionSender().sendMessage("This patch is clear of weeds.");
			return;
		}
		if (!p.getInventory().hasItem(TOOLS[0])) {
			p.getActionSender().sendMessage("You need a rake to clear the weeds from this patch.");
			return;
		}
		p.animate(2273);
		patch.setWeeding(true);
		p.setTemporaryAttribute("harvesting", true);
		World.getInstance().registerEvent(new Event(1300) {
			
			@Override
			public void execute() {
				if (p.isDestroyed() || p.isDisconnected() || p.getTemporaryAttribute("harvesting") == null) {
					this.stop();
					patch.setWeeding(false);
					return;
				}
				if (!p.getInventory().hasItem(TOOLS[0])) {
					p.getActionSender().sendMessage("You need a rake to clear the weeds from this patch.");
					patch.setWeeding(false);
					this.stop();
					return;
				}
				p.getInventory().addItemOrGround(6055);
				p.animate(2273);
				setConfig(p, patch);
				patch.setStatus(patch.getStatus() + 1);
				if (patch.getStatus() >= 3) {
					p.getActionSender().sendMessage("You clear the weeds from the patch, this patch is now suitable for farming.");
					patch.setHasWeeds(false);
					patch.setWeeding(false);
					patch.setStatus(2);
					setConfig(p, patch);
					p.animate(65535);
					this.stop();
					return;
				}
			}
		});
	}
	
	public static boolean regrowWeeds(Patch patch) {
		boolean shouldRemoveFromList = false;
		if (!patch.patchOccupied()) {
			if (Misc.random(4) != 0 || patch.isWeeding()) {
				return false;
			}
			Player owner = World.getInstance().getPlayerForName(patch.getOwnerName());
			patch.setHasWeeds(true);
			patch.setStatus(patch.getStatus() - 1);
			if (patch.getStatus() <= -1) {
				patch.setStatus(0);
				shouldRemoveFromList = true;
			}
			if (owner != null) {
				setConfig(owner, patch);
			}
		}
		return shouldRemoveFromList;
	}
	
	public static void growPatch(Patch patch) {
		if (patch.isTree()) {
			if (patch.getStatus() == patch.checkHealthStatus()) {
				if (!patch.isHealthChecked()) {
					return;
				}
			} else if  (patch.getStatus() == patch.chopStatus()) {
				return;
			} else if (patch.getStatus() == patch.stumpStatus()) {
				patch.setStatus(patch.chopStatus());
			} else {
				patch.setStatus(patch.getStatus() + 1);
			}
		} else if (patch.isFruitTree()) {
			if (patch.isWeeding()) {
				return;
			}
			if (patch.getStatus() == patch.checkHealthStatus()) {
				if (!patch.isHealthChecked()) {
					return;
				}
			}
			if (patch.isHealthChecked()) {
				if (patch.getStatus() == patch.checkHealthStatus() - 1) { // all fruit
					return;
				}
			}
			if (patch.getStatus() == patch.stumpStatus()) {
				patch.setStatus(patch.chopStatus());
			} else if (patch.getStatus() == patch.chopStatus()) {
				patch.setStatus(6);
			} else {
				patch.setStatus(patch.getStatus() + 1);
			}
		} else {
			patch.setStatus(patch.getStatus() + 1);
		}
		Player owner = World.getInstance().getPlayerForName(patch.getOwnerName());
		if (owner != null) {
			setConfig(owner, patch);
		}
		patch.setLastUpdate(System.currentTimeMillis());
	}
	
	public static void setConfig(Player p, Patch patch) {
		int[] bounds = getIndexBoundaries(patch);
		Patch[] patches = World.getInstance().getGlobalObjects().getFarmingPatches().getPatchesForPlayer(p, bounds[0], bounds[1]);
		int config = 0;
		for (int i = 0; i < patches.length; i++) {
			if (patches[i] != null && !patches[i].isSapling()) {
				config += (patches[i].getConfigElement(patches[i].getStatus()) * patches[i].getMultiplyer());
			}
		}
		p.getActionSender().sendConfig(patch.getConfigId(), config);
	}
	
	public static boolean plantSapling(Player p, int itemUsed, int usedWith) {
		if (itemUsed != TOOLS[4] && usedWith != TOOLS[4]) {
			return false;
		}
		int itemOne = itemUsed;
		int itemTwo = usedWith;
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				itemOne = usedWith;
				itemTwo = itemUsed;
			}
			for (int j = 0; j < SAPLING_DATA.length; j++) {
				if (itemOne == (Integer)SAPLING_DATA[j][0] && itemTwo == TOOLS[4]) {
					if (!p.getInventory().hasItem(TOOLS[2])) {
						p.getActionSender().sendMessage("You don't have a trowel.");
						return true;
					}
					if (!p.getInventory().hasItem(TOOLS[4])) {
						p.getActionSender().sendMessage("You need a plant pot filled with compost to do this.");
						return true;
					}
					String s = j == 0 ? "" : "seed";
					if (p.getInventory().deleteItem((Integer)SAPLING_DATA[j][0])) {
						Patch patch = new Patch(p.getUsername(), j);
						World.getInstance().getGlobalObjects().getFarmingPatches().addPatch(patch);
						p.getInventory().replaceSingleItem(TOOLS[4], (Integer)SAPLING_DATA[j][1]);
						p.getActionSender().sendMessage("You place the " + (String)SAPLING_DATA[j][3] + " " + s + " into the plant pot and cover it with soil...now wait.");
					}
					return true;
				}
			}
		}
		return false;
	}
	
	private static int[] getPatchDistances(int index, int objectX, int objectY) {
		int[] data = new int[4];
		if (index >= 0 && index <= 7) { // Vegatable
			data[0] = objectX-1;
			data[1] = objectY-1;
			data[2] = objectX+1;
			data[3] = objectY+1;
		} else if (index >= 8 && index <= 15) { // Herb & flower
			data[0] = objectX-1;
			data[1] = objectY-1;
			data[2] = objectX+2;
			data[3] = objectY+2;
		} else if (index >= 12 && index <= 15) { // Flower
			data[0] = objectX-1;
			data[1] = objectY-1;
			data[2] = objectX+2;
			data[3] = objectY+2;
		} else if (index >= 16 && index <= 19) { // Trees
			data[0] = objectX-1;
			data[1] = objectY-1;
			data[2] = objectX+3;
			data[3] = objectY+3;
		} else if (index >= 20 && index <= 23) { // Fruit Trees
			data[0] = objectX-1;
			data[1] = objectY-1;
			data[2] = objectX+2;
			data[3] = objectY+2;
		}
		return data;
	}
	
	private static int getPatchIndex(Location patchLocation, int i) {
		if (patchLocation.inArea((Integer)PATCHES[i][2], (Integer)PATCHES[i][3], (Integer)PATCHES[i][4], (Integer)PATCHES[i][5])) {
			return i;
		}
		return -1;
	}
	
	private static int[] getIndexBoundaries(Patch patch) {
		int[] indexes = new int[2];
		switch(patch.getConfigId()) {
			case 504: // Catherby + Draynor allotments
				indexes[0] = 0;
				indexes[1] = 3;
				break;
				
			case 505: // Ardougne + Canifis allotments
				indexes[0] = 4;
				indexes[1] = 7;
				break;
				
			case 515: // Herb patches
				indexes[0] = 8;
				indexes[1] = 11;
				break;
				
			case 508: // Flower patches
				indexes[0] = 12;
				indexes[1] = 15;
				break;
				
			case 502: // Tree patches
				indexes[0] = 16;
				indexes[1] = 19;
				break;
				
			case 503: // Fruit tree patches
				indexes[0] = 20;
				indexes[1] = 23;
				break;
		}
		return indexes;
	}
	
	public static void refreshPatches(Player p) {
		int[] configs = {502, 503, 504, 505, 508, 515};
		for (int i = 0; i < configs.length; i++) {
			Patch patch = new Patch(p.getUsername(), 0);
			patch.setConfig(configs[i]);
			setConfig(p, patch);
		}
	}
	
	public static boolean growSapling(Patch patch) {
		Player owner = World.getInstance().getPlayerForName(patch.getOwnerName());
		if (owner != null) {
			if (owner.getInventory().replaceSingleItem((Integer)SAPLING_DATA[patch.getPatchIndex()][1], (Integer)SAPLING_DATA[patch.getPatchIndex()][2])) {
				return true;
			} else if (owner.getBank().findItem((Integer)SAPLING_DATA[patch.getPatchIndex()][1]) != -1) {
				Item item = owner.getBank().getSlot(owner.getBank().findItem((Integer)SAPLING_DATA[patch.getPatchIndex()][1]));
				if (item.getItemId() == (Integer)SAPLING_DATA[patch.getPatchIndex()][1]) {
					item.setItemId((Integer)SAPLING_DATA[patch.getPatchIndex()][2]);
					owner.getBank().refreshBank();
					return true;
				}
			}
		}
		return true;
	}
	
	private static int[] getDataForPatch(Patch patch, int seed) {
		PatchType type = patch.getPatchType();
		if (type.equals(PatchType.VEGATABLE)) { // Draynor + Catherby
			return ALLOTMENT_PATCH_CONFIGS[seed];
		} else 
		if (type.equals(PatchType.VEGATABLE_1)) { // Canifis + Ardougne
			return ALLOTMENT_PATCH_CONFIGS[seed];
		} else
		if (type.equals(PatchType.HERB)) {
			return HERB_PATCH_CONFIGS[seed - 14];
		} else 
		if (type.equals(PatchType.FLOWER)) {
			return FLOWER_PATCH_CONFIGS[seed - 28];
		} else 
		if (type.equals(PatchType.TREE)) {
			return TREE_PATCH_CONFIGS[seed - 33];
		} else 
		if (type.equals(PatchType.FRUIT_TREE)) {
			return FRUIT_TREE_PATCH_CONFIGS[seed - 38];
		}
		return null;
	}
}
