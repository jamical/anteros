package com.anteros.content.skills.fishing;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

/**
 * @author Abexlry (Barbarian Fishing)
 *
 */

public class Fishing extends FishingData{
	
	public Fishing() {
		
	}
	
	public static boolean wantToFish(final Player p, final NPC npc, final boolean secondOption) {
		for (int i = 0; i < SPOT_IDS.length; i++) {
			if (npc.getId() == SPOT_IDS[i]){
				p.setFaceLocation(npc.getLocation());
				final int j = i;
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, npc.getLocation().getX()-1, npc.getLocation().getY()-1, npc.getLocation().getX()+1, npc.getLocation().getY()+1) {
					@Override
					public void run() {
						//startSpotTimer(p, npc);
						startFishing(p, j, npc, true, secondOption);
					}
				});
				return true;
			}
		}
		return false;
	}
	
	private static void startSpotTimer(final Player p, final NPC npc) { //TODO remove spots randomly to prevent bots/etc
		long spotTimer = 30000 + Misc.random(500000);
		System.out.println(spotTimer);
		World.getInstance().registerEvent(new Event(spotTimer) {
			@Override
			public void execute() {
				System.out.println("killing spot");
				npc.setDead(true);
			}
		});	
	}

	private static void startFishing(final Player p, final int i, NPC npc, boolean newFish, final boolean secondOption) {
		if (!newFish && p.getTemporaryAttribute("fishingSpot") == null) {
			return;
		}
		if (newFish) {
			int j = secondOption ? 1 : 0;
			int[] fish = secondOption ? SECOND_SPOT_FISH[i] : FIRST_SPOT_FISH[i];
			int[] level = secondOption ? SECOND_SPOT_LEVEL[i] : FIRST_SPOT_LEVEL[i];
			double[] xp = secondOption ? SECOND_SPOT_XP[i] : FIRST_SPOT_XP[i];
			Spot fishingSpot = new Spot(fish, level, i, SPOT_IDS[i], xp, npc.getLocation(), PRIMARY_ITEM[i][j], SECONDARY_ITEM[i][j], PRIMARY_NAME[i][j], SECONDARY_NAME[i][j], secondOption);
			p.setTemporaryAttribute("fishingSpot", fishingSpot);
		}
		final Spot fishingSpot = (Spot) p.getTemporaryAttribute("fishingSpot");
		final Spot fishingSpot2 = (Spot) p.getTemporaryAttribute("fishingSpot");
		final int j = fishingSpot.isSecondOption() ? 1 : 0;
		final int index =  getFishToAdd(p, fishingSpot);
		/**
		 * Start of Barbarian segment
		 */
		if (!Barbarian.canUseSpot(p, fishingSpot)) {
			if (!canFish (p, fishingSpot, null, index) && !Barbarian.canUseSpot(p, fishingSpot)){
				resetFishing(p);
				p.animate(65535);
				return;
			}
			return;
		} 
		/**
		 * End of Barbarian segment
		 */
		if (newFish) {
			/**
			 * Start of Barbarian segment
			 */
			if (Barbarian.checkFish(p) > 0 && secondOption == true) {
				if (p.getInventory().findFreeSlot() == -1) {
					p.getActionSender().sendChatboxInterface(210);
					p.getActionSender().modifyText("Your inventory is too full to catch any more fish.", 210, 1);
					return;
				}
				if (fishingSpot.getSpotId() == 324 || fishingSpot.getSpotId() == 313) {
					Barbarian.startFishing(p, index, npc, newFish, secondOption);
				}
				return;
			}
			/**
			 * End of Barbarian segment
			 */
			p.getActionSender().sendMessage("You attempt to catch a fish...");
			p.animate(FISHING_ANIMATION[i][j]); 
		}
		final String name = fishingSpot.isSecondOption() ? SECOND_CATCH_NAME[fishingSpot.getSpotindex()][index] : FIRST_CATCH_NAME[fishingSpot.getSpotindex()][index];
		final String s = fishingSpot.getSpotindex() == 1 && !fishingSpot.isSecondOption() ? "some" : "a";
		World.getInstance().registerEvent(new Event(getFishingDelay(p, fishingSpot)) {
			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("fishingSpot") == null) {
					resetFishing(p);
					p.animate(65535);
					return;
				}
				final Spot fishingSpot2 = (Spot) p.getTemporaryAttribute("fishingSpot");			
				if (!canFish(p, fishingSpot, fishingSpot2, index)) {
					resetFishing(p);
					p.animate(65535);
					return;
				} 
				p.getActionSender().closeInterfaces();
				p.getInventory().deleteItem(fishingSpot2.getSecondaryItem());
				p.animate(FISHING_ANIMATION2[fishingSpot2.getSpotindex()][j]);
				p.getActionSender().sendMessage("You catch " + s + " " + name + ".");
				if (p.getInventory().addItem(fishingSpot2.getFish()[index])) {
					p.getLevels().addXp(FISHING, fishingSpot2.getFishingXp()[index]);	
				}
				startFishing(p, i, null, false, secondOption);
			}
		});	
	}

	
	
	protected static long getFishingDelay(Player p, Spot fishingSpot) {
		int[] time = fishingSpot.isSecondOption() ? SECOND_SPOT_TIME : FIRST_SPOT_TIME;
		int[] minTime = fishingSpot.isSecondOption() ? SECOND_SPOT_MINTIME : FIRST_SPOT_MINTIME;
		int baseTime = time[fishingSpot.getSpotindex()];
		int min = minTime[fishingSpot.getSpotindex()];
		int finalDelay = baseTime -= Misc.random(min);
		return finalDelay;
	}

	protected static int getFishToAdd(Player p, Spot fishingSpot) {
		int fishingLevel = p.getLevels().getLevel(FISHING);
		int[] canCatch = new int[fishingSpot.getFish().length];
		int j = 0;
		for (int i = 0; i < fishingSpot.getFish().length; i++) {
			if (fishingLevel >= fishingSpot.getLevel()[i]) {
				canCatch[j] = fishingSpot.getFish()[i];
				j++;
			}
		}
		int[] canCatch2 = new int[j];
		for (int i = 0; i < canCatch.length; i++) {
			if (canCatch[i] > 0) {
				canCatch2[i] = canCatch[i];
			}
		}
		int fish = Misc.random(canCatch2.length-1);
		for (int i = 0; i < fishingSpot.getFish().length; i++) {
			if (fish == fishingSpot.getFish()[i]) {
				return i;
			}
		}
		return Misc.random(canCatch2.length-1);
	}

	public static boolean canFish(Player p, Spot fishingSpot, Spot fishingSpot2, int index){
		if (p == null || fishingSpot == null) {
			return false;
		}
		if (!p.getLocation().withinDistance(fishingSpot.getSpotLocation(), 2)) {
			return false;
		}
		if (fishingSpot2 != null) {
			if (!fishingSpot.equals(fishingSpot2)) {
				return false;
			}
		}
		if (p.getLevels().getLevel(FISHING) < fishingSpot.getLevel()[index]){
			p.getActionSender ().sendMessage ("You need a fishing level of " +fishingSpot.getLevel()[index] + " to fish here.");
			return false;
		}
		if (fishingSpot.getPrimaryItem() != -1) {
			if (!p.getInventory().hasItem (fishingSpot.getPrimaryItem())){
				p.getActionSender ().sendMessage ("You need " + fishingSpot.getPrimaryName() + " to fish here.");	
				return false;
			}
		}
		if (fishingSpot.getSecondaryItem() != -1) {
			if (!p.getInventory().hasItem (fishingSpot.getSecondaryItem()) && !Barbarian.canUseSpot(p, fishingSpot)){
				p.getActionSender ().sendMessage ("You need " + fishingSpot.getSecondaryName() + " to fish here.");	
				return false;
			} 
		}
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendChatboxInterface(210);
			p.getActionSender().modifyText("Your inventory is too full to catch any more fish.", 210, 1);
			return false;
		}
		return true;
	}
	
	public static void resetFishing(Player p) {
		p.removeTemporaryAttribute("fishingSpot");
	}
}
