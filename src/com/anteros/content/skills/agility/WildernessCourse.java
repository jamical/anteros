package com.anteros.content.skills.agility;

import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceMovement;
import com.anteros.model.player.Player;

public class WildernessCourse extends AgilityData {

	public WildernessCourse() {
		
	}

	public static void doCourse(final Player p, final int objectX, final int objectY, final Object[] objectArray) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch((Integer)objectArray[0]) {
			case 2288: // Tunnel
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3003, 3937, 3005, 3938) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX + 1 && pY == objectY) { // right side
							newMove = 1;
						} else if (pX == objectX - 1 && pY == objectY) { // left side
							newMove = 2;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										p.getWalkingQueue().forceWalk(0, -1);
									} else if (i == 1) {
										p.getWalkingQueue().forceWalk(path == 1 ? -1 : +1, 0);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("You squeeze into the pipe...");
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY + 3, 10, 60, 0);
								p.setForceMovement(movement);		
								p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() + 1, 0));
								p.animate(10578);
								final boolean running = p.getWalkingQueue().isRunToggled();
								p.getWalkingQueue().setRunToggled(false);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								World.getInstance().registerEvent(new Event(1000) {
									int i = 0;
									@Override
									public void execute() {
										if (i == 0) {
											p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() + 9, 0));
											this.setTick(850);
										} else if (i == 1){
											ForceMovement movement = new ForceMovement(lX, lY + 9, lX, lY + 12, 10, 90, 0);
											p.setForceMovement(movement);
											this.setTick(1100);
										} else if (i == 2) {
											this.setTick(500);
											p.animate(10579);
											ForceMovement movement = new ForceMovement(lX, lY + 12, lX, lY + 13, 10, 40, 0);
											p.setForceMovement(movement);
										} else {
											p.getActionSender().sendMessage("...You squeeze out of the pipe.");
											this.stop();
											p.removeTemporaryAttribute("unmovable");
											p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
											p.getWalkingQueue().setRunToggled(running);
											p.teleport(Location.location(3004, 3950, 0));
										}
										i++;
									}					
								});	
							}
						});
					}
				});
				break;
				
			case 2283: // Rope swing
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3004, 3951, 3006, 3953) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX - 1 && pY == objectY + 1) { // front left
							newMove = 1;
						} else if (pX == objectX + 1 && pY == objectY + 1) { // front right
							newMove = 2;
						} else if (pX == objectX - 1 && pY == objectY) { // back left
							newMove = 3;
						} else if (pX == objectX + 1 && pY == objectY) { // back right
							newMove = 4;
						} else if (pX == objectX && pY == objectY - 1) { // very back middle
							newMove = 5;
						} else if (pX == objectX && pY == objectY) { //  middle
							newMove = 6;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									p.setFaceLocation(Location.location(3005, 3958, 0));
									if (path == 1 || path == 2) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(path == 1 ? +1 : -1, 0);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									} else if (path == 3 || path == 4) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(path == 3 ? +1 : -1, +1);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									} else if (path == 5 || path == 6) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(0, path == 5 ? +2 : +1);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								final boolean running = p.getWalkingQueue().isRunToggled();
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								final int newY = p.getLocation().getY() + 5;
								int dir = 4;
								p.animate(751);
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY+5, 25, 57, dir);
								p.setForceMovement(movement);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								final Location objectLocation = Location.location(3005, 3952, 0);
								World.getInstance().registerEvent(new Event(30) {

									@Override
									public void execute() {
										this.stop();
										for (Player nearbyPlayers : World.getInstance().getPlayerList()) {
											if (nearbyPlayers != null) {
												nearbyPlayers.getActionSender().newObjectAnimation(objectLocation, 497);
											}
										}
									}
								});
								World.getInstance().registerEvent(new Event(1300) {

									@Override
									public void execute() {
										this.stop();
										p.getAppearance().setWalkAnimation(-1);
										p.getUpdateFlags().setAppearanceUpdateRequired(true);
										p.removeTemporaryAttribute("unmovable");
										p.teleport(Location.location(p.getLocation().getX(), newY, 0));
										p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
										p.getWalkingQueue().setRunToggled(running);
									}		
								});
							}
						});
					}
				});
				break;
				
			case 37704: // Stepping stones
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(3002, 3960, 0)) {
					
					@Override
					public void run() {
						final boolean running = p.getWalkingQueue().isRunToggled();
						final int dir = 4;
						p.getWalkingQueue().setRunToggled(false);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(600) {
							int i = 0;
							@Override
							public void execute() {
								if (i >= 6) {
									this.stop();
									p.removeTemporaryAttribute("unmovable");
									p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
									p.getWalkingQueue().setRunToggled(running);
								} else {
									int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
									int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
									int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
									int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
									p.animate(741);
									ForceMovement movement = new ForceMovement(lX, lY, lX - 1, lY, 5, 35, dir);
									p.setForceMovement(movement);
									World.getInstance().registerEvent(new Event(550) {
										@Override
										public void execute() {
											this.stop();
											p.teleport(Location.location(p.getLocation().getX()-1, p.getLocation().getY(), 0));
										}
									});
									this.setTick(i == 6 ? 300 : 1100);
									i++;
									p.setFaceLocation(Location.location(2995, 3960, 0));
								}
							}
	
						});	
					}
				});
				break;
				
			case 2297: // Log
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 3001, 3944, 3002, 3946) {
					int distanceToWalk = p.getLocation().getX() == 3001 ? -7 : -8;
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX && pY == objectY - 1) {
							newMove = 1;
						} else if (pX == objectX && pY == objectY + 1) {
							newMove = 2;
						} else if (pX == objectX + 1 && pY == objectY + 1) {
							newMove = 3;
						} else if (pX == objectX + 1 && pY == objectY - 1) {
							newMove = 4;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										if (path == 1 || path == 2) {
											p.getWalkingQueue().forceWalk(0, path == 1 ? +1 : -1);
										} else if (path == 3 || path == 4) {
											p.getWalkingQueue().forceWalk(0, path == 4 ? +1 : -1);
										}
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								final boolean running = p.getWalkingQueue().isRunToggled();
								p.getWalkingQueue().setRunToggled(false);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								p.getAppearance().setWalkAnimation(155);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.getWalkingQueue().forceWalk(distanceToWalk, 0);
								int delay = distanceToWalk == -7 ? 4100 : 4600;
								World.getInstance().registerEvent(new Event(delay) {

									@Override
									public void execute() {
										this.stop();
										p.getAppearance().setWalkAnimation(-1);
										p.getUpdateFlags().setAppearanceUpdateRequired(true);
										p.removeTemporaryAttribute("unmovable");
										p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
										p.getWalkingQueue().setRunToggled(running);
									}		
								});
							}
						});
					}
				});
				break;
				
			case 2328: // Rocks
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2993, 3937, 2995, 3937) {
					@Override
					public void run() {
						World.getInstance().registerEvent(new Event(0) {
							int i = 0;
							@Override
							public void execute() {
								if (i == 0) {
									p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() - 5, 0));
									i++;
									this.setTick(500);
									return;
								}
								this.stop();
								p.animate(740);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.getAppearance().setWalkAnimation(0);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.setTemporaryAttribute("unmovable", true);
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY - 4, 5, 80, 4);
								p.setForceMovement(movement);
								World.getInstance().registerEvent(new Event(1500) {
			
									@Override
									public void execute() {
										this.stop();
										p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() - 4, 0));
										p.animate(65535);
										p.removeTemporaryAttribute("unmovable");
										p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
									}		
								});
							}
						});
					}
				});
				break;
		}
	}
}
