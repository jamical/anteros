package com.anteros.content.skills.crafting;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Glass extends CraftingData {

	public Glass() {
		
	}
	
	public static void displayGlassOption(Player p) {
		p.getActionSender().displayInterface(542);
	}

	public static void craftGlass(final Player p, int amount, int index, boolean newCraft) {
		if (newCraft) {
			p.setTemporaryAttribute("craftItem", new CraftItem(3, index, amount, (Double) GLASS_ITEMS[index][2], (Integer) GLASS_ITEMS[index][0], (String) GLASS_ITEMS[index][3], (Integer) GLASS_ITEMS[index][1]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 3) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		if (!p.getInventory().hasItem(MOLTEN_GLASS)) {
			p.getActionSender().sendMessage("You have no molten glass.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(GLASSBLOWING_PIPE)) {
			p.getActionSender().sendMessage("You need a glassblowing pipe if you wish to make a glass item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(MOLTEN_GLASS)) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			p.animate(884);
			p.getActionSender().sendMessage("You blow through the pipe, shaping the molten glass into a " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {

				@Override
				public void execute() {
					craftGlass(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
