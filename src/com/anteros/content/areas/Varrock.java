package com.anteros.content.areas;

import com.anteros.content.Dialogue;
import com.anteros.content.skills.runecrafting.RuneCraft;
import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class Varrock {
	
	
	public static void teleportAubury(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.runeMysteriesInt == 4) {
					RuneCraft.teleportToEssMine(p, n);
				} else {
					p.getActionSender().sendNpcChat2("Sorry, you do not have access to the", "essence mines yet.", 553, "Aubury", Dialogue.CALM_TALK_BARELY_MOVING);
				}
				}
		});
	}
		public static void hillEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

				@Override
				public void run() { 
					player.teleport(Location.location(3117, 9852, 0));
				}
			});
		}
		
		public static void hillLeave(final Player player, int objectId, Location loc, final int oX, final int oY) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

				@Override
				public void run() { 
					player.teleport(Location.location(3117, 9852, 0));
				}
			});
		}
		
		public static void hillGiantDoor(final Player player, int objectId, Location loc, final int oX, final int oY) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

				@Override
				public void run() { 
					if (player.getInventory().hasItem(983) && player.getLocation().getY() < oY) {
						player.teleport(Location.location(oX, oY, 0));
					} else if (!player.getInventory().hasItem(983) && player.getLocation().getY() < oY) {
						player.getActionSender().sendMessage("This door looks like it needs a key to open it.");
					} else if (player.getLocation().getY() >= oY) {
						player.teleport(Location.location(oX, oY - 1, 0));
					}
				}
			});
		}
	}


