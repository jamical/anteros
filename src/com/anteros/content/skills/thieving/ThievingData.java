package com.anteros.content.skills.thieving;

public class ThievingData {

	public ThievingData() {
		
	}
	
	protected static final int THIEVING = 17;
	protected static final int LOCKPICK = 1523;
	
	protected static final int[][] NPCS = {
		{1, 2, 3, 7875, 7877, 7879}, // Men.
		{4, 5, 6, 7881, 7883}, // Women.
		{7}, // Farmer.
		{1715}, // Female HAM members.
		{1716}, // Male HAM members.
		{15}, // Warrior
		{187}, // Rogue
		//Cave goblin here (no need for them in an RSPS..)
		{2234}, // Master farmer.
		{9, 32, 296, 297, 298, 299}, // Guard.
		//'Fremmenik' here
		{6174, 1880}, // Bandit guard.
		{1926, 1931}, // Desert bandits.
		{23, 26}, // Knights
		{34}, // Watchman
		//Menaphite thug here.
		{20}, // Paladin
		{66, 67, 68}, // Gnomes.
		{21}, // Hero.
		{2363, 2364, 2365, 2366, 2367}, // Elf
		
	};
	
	protected static final int[] NPC_LVL = {
		1, // Men.
		1, // Women.
		10, // Farmer.
		15, // Female HAM.
		20, // Male HAM.
		25, // Warrior.
		32, // Rogue.
		38, // Master farmer.
		40, // Guard.
		45, // Bandit guard.
		53, // Desert bandits.
		55, // Knights.
		65, // Watchman.
		70, // Paladin.
		75, // Gnome.
		80, // Hero.
		85, // Elf.
	};
	
	protected static final int[][] NPC_REWARD = {
		{995}, // Men.
		{995}, // Women.
		{995, 5318}, // Farmer
		{4298, 4300, 4302, 4304, 4306, 4308, 4310, 1511, 688, 689, 687, 686, 1605, 314, 946, 995, 1267, 371, 199, 453, 444, 201, 203, 205, 175, 884, 2138, 385}, // Female H.A.M TODO - lvl 1 clue
		{4298, 4300, 4302, 4304, 4306, 4308, 4310, 1511, 688, 689, 687, 686, 1605, 314, 946, 995, 1267, 371, 199, 453, 444, 201, 203, 205, 175, 884, 2138, 385}, // Male H.A.M TODO - lvl 1 clue
		{995}, // Warrior
		{995, 1523, 954, 554, 555, 556, 175}, // Rogue
		{5318, 5319, 5320, 5321, 5322, 5323, 5324, 5305, 5306, 5307, 5308, 5309, 5310, 5311, 5096, 5097, 5098, 5099, 5100, 5101, 5102, 5103, 5104, 5105, 5106}, // Master farmer.
		{995}, // Guard
		{995, 175, 1523, 1823}, // Bandit guard
		{995, 175, 1523, 1823}, // Bandits
		{995}, // Knight
		{995, 2309}, // Watchman
		{995, 562}, // Paladin
		{995, 2357, 561}, // Gnome
		{995}, // Hero
		{995}, // Elf
	};
	
	protected static final int[][] NPC_REWARD_AMOUNT = {
		{3}, // Men.
		{3}, // Women.
		{9, 1}, // Farmer
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 26, 1, 1, 1, 1, 1, 1, 1, 1, 1, 100, 1, 1}, // Female H.A.M
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 1, 26, 1, 1, 1, 1, 1, 1, 1, 1, 1, 100, 1, 1}, // Male H.A.M
		{18}, // Warrior
		{26, 1, 1, 10, 10, 10, 1}, // Rogue
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, // Master farmer.
		{30}, // Guard
		{32, 1, 1, 1}, // Bandit guard
		{32, 1, 1, 1}, // Bandit
		{50, 1}, // Knight
		{60, 1}, // Watchman
		{80, 5}, // Paladin
		{300, 1, 2}, // Gnome
		{200}, // Hero
		{345}, // Elf
	};
	
	protected static final double[] NPC_XP = {
		8, // Men & women.
		8, // Women.
		14.5, // Farmer.
		18.5, // Female HAM.
		22.5, // Male HAM.
		26, // Warrior.
		36.5, // Rogue.
		43, // Master farmer.
		46.8, // Guard.
		65, // Bandit guard.
		79.4, // Desert bandit.
		84.3, // Knights.
		137.5, // Watchman.
		151.8, // Paladin.
		198.3, // Gnome.
		273.3, // Hero
	};
	
	protected static final String[] NPC_NAMES = {
		"man's",
		"woman's",
		"farmer's",
		"H.A.M member's",
		"H.A.M member's", 
		"warrior's",
		"rogue's",
		"master farmer's",
		"guard's",
		"guard's",
		"bandit's",
		"knight's",
		"watchman's",
		"paladin's",
		"gnome's",
		"hero's",
		"elf's"
	};
	
	protected static final int[][] STALLS = {
		{4708, 4706}, // Vegatable
		{-31152}, // Baker
		{4876}, // AA general
		{635}, // Tea
		{4874}, // AA crafting
		{4875}, // AA food
		{-31153}, // Silk
		{14011}, // Wine
		{7053}, // Seed
		{-31149}, // Fur
		{4707, 4705}, // Fish
		{-31154}, // Silver
		{4877}, // AA magic
		{4878}, // AA scimitar
		{-31150}, // Spice stall
		{-31151}, // Gem stall
	};
	
	protected static final int[] STALL_LVL = {
		2, // Vegatable
		5, // Baker
		5, // AA General
		5, // Tea
		5, // AA crafting
		5, // AA food
		20, // Silk
		22, // Wine
		27, // Seed
		35, // Fur
		42, // Fish
		50, // Silver
		65, // AA magic
		65, // AA scimitar
		65, // Spice stall
		75, // Gem stall
	};
	
	protected static final int[][] STALL_REWARD = {
		{1942, 1965, 1957, 1550, 1982}, // Vegatable
		{2309, 1891, 1901}, // Baker
		{2347, 1931, 590}, // AA General
		{1978}, // Tea
		{1755, 1592, 1597}, // AA crafting
		{1963}, // AA food
		{950}, // Silk
		{1935, 1937, 1987, 1993, 7919}, // Wine
		{5318, 5319, 5320, 5321, 5322, 5323, 5324, 5096, 5097, 5098, 5099, 5100, 5101, 5102, 5103, 5104, 5105, 5106}, // Seed
		{958}, // Fur
		{377, 371, 359}, // Fish
		{442}, // Silver
		{554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566}, // AA magic
		{1321, 1323, 1325, 1327, 1329, 1331}, // AA scimitar
		{2007}, // Spice stall
		{1619, 1621, 1623}, // Gem stall
	};
	
	protected static final int[][] STALL_REWARD_AMOUNTS = {
		{1, 1, 1, 1, 1}, // Vegatable
		{1, 1, 1}, // Baker
		{1, 1, 1}, // AA General
		{1}, // Tea
		{1, 1, 1}, // AA crafting
		{1}, // AA food
		{1}, // Silk
		{1, 1, 1, 1, 1}, // Wine
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, // Seed
		{1}, // Fur
		{1, 1, 1}, // Fish
		{1}, // Silver
		{10, 10, 10, 10, 10, 10, 1, 1, 1, 1, 1, 1, 1}, // AA magic
		{1, 1, 1, 1, 1, 1}, // AA scimitar
		{1}, // Spice
		{1, 1, 1}, // Gem stall
	};
	
	protected static final double[] STALL_XP = {
		10, // Vegatable
		16, // Baker
		10, // AA General
		16, // Tea
		10, // AA crafting
		16, // AA food
		24, // Silk
		27, // Wine
		32, // Seed
		36, // Fur
		42, // Fish
		54, // Silver
		100, // AA magic
		160, // AA scimitar
		81.3, // Spice stall
		120, // Gem stall
	};
	
	protected static final String[] STALL_NAMES = {
	
	};
	
	protected static final int[] CHESTS = {
		2566, // Coin chest.
		2567, // Nature rune chest.
		2568, // Coin chest #2.
		2569, // Blood rune chest.
		2570, // King Lathas chest.
	};
	
	protected static final int[][] CHEST_REWARD = {
		{995}, // Ardougne coin chest.
		{995, 561}, // Nature rune chest.
		{995}, // Coin chest #2.
		{995, 565}, // Blood rune chest.
		{995, 383, 449, 1623}, // King Lathas chest.
	};
	
	protected static final int[][] CHEST_REWARD_AMOUNTS = {
		{10}, // Ardougne coin chest.
		{3, 2}, // Nature rune chest.
		{9000}, // Coin chest #2.
		{500, 10}, // Blood rune chest.
		{765, 1, 1, 1}, // King Lathas chest.
	};
	
	protected static final int[] CHEST_LVL = {
		13, // Ardougne coin chest.
		28, // Nature rune chest.
		43, // Coin chest #2.
		59, // Blood rune chest.
		72, // King Lathas chest.
		47, // Arrowheads chest.
	};
	
	protected static final double[] CHEST_XP = {
		7.5, // Ardougne coin chest.
		25, // Nature rune chest.
		125, // Coin chest #2.
		250, // Blood rune chest.
		500, // King Lathas chest
		150, // Arrowheads chest.
	};
	
	
	
	
	protected static final int[] HERB_SEEDS = {
		5291, 5292, 5293, 5294, 5295, 5296, 5297, 5298, 5299, 5300, 5301
	};
}
