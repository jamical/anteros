package com.anteros.content.skills.agility;

public class AgilityData {

	public AgilityData() {
		
	}
	
	protected static final int AGILITY = 16;
	
	protected static final int AGILITY_ARENA_PRICE = 1250;
	
	protected static final Object[][] GNOME_COURSE = {
		//obstacle, x, y, x to be stood, y to be stood, render emote, emote, xp
		{2295, 2474, 3435, 2474, 3436, 155, -1, 7.5}, // log
		{2285, -1, -1, -1, -1, -1, 158, 7.5}, // net
		{35970, 2473, 3422, 2473, 3423, -1, 158, 5.0}, // tree
		{2312, 2478, 3420, 2477, 3420, 155, -1, 7.5}, // rope
		{2314, 2486, 3419, 2485, 3419, -1, 158, 5.0}, // branch #2
		{2286, -1, -1, -1, -1, -1, 158, 7.5}, // net #2
		{4058, 2487, 3431, -1, -1, -1, 10580, 7.5}, // right obstacle pipe
		{154, 2487, 3431, -1, -1, -1, 10580, 7.5} // left obstacle pipe
	};
	
	protected static final Object[][] BARBARIAN_COURSE = {
		//obstacle, x, y, x to be stood, y to be stood, render emote, emote, xp
		{20210, 2552, 3559, -1, -1, -1, -1, 0.0}, // entrance tunnel
		{2282, 2551, 3550, -1, -1, -1, -1, 22.0}, // swing
		{2294, 2550, -1, -1, -1, 155, -1, 13.7}, // log
		{20211, 2538, -1, -1, -1, -1, -1, 8.2}, // net
		{2302, 2535, 3547, 2536, 3547, -1, -1, 22.0}, // balance plank
		{1948, -1, -1, -1, -1, -1, -1, 13.7}, // Crumbling wall
	};
	
	protected static final Object[][] WILDERNESS_COURSE = {
		//obstacle, x, y, x to be stood, y to be stood, render emote, emote, xp
		{2288, 3004, 3939, -1, -1, -1, -1, 12.5}, // tunnel
		{2283, 3005, 3952, -1, -1, -1, -1, 20.0}, // swing
		{37704, 3001, 3960, -1, -1, -1, -1, 20.0}, // stepping stone
		{2297, 3001, -1, -1, -1, 155, -1, 20.0}, // log
		{2328, -1, -1, -1, -1, 155, -1, 20.0}, // rocks
	};
	
	protected static final Object[][] APE_ATOLL_COURSE = {
		//obstacle, x, y, x to be stood, y to be stood, render emote, emote, xp
	};
	
	protected static final int[][] AGILITY_ARENA_PILLARS = {
		// id, x, y
		{3608, 2805, 9579},
		{3608, 2805, 9568},
		{3608, 2805, 9557},
		{3608, 2805, 9546},
		{3608, 2794, 9546},
		{3581, 2794, 9557},
		{3581, 2794, 9568},
		{3581, 2794, 9579},
		{3608, 2794, 9590},
		{3608, 2783, 9590},
		{3608, 2783, 9579},
		{3581, 2783, 9568},
		{3581, 2783, 9557},
		{3608, 2783, 9546},
		{3608, 2772, 9546},
		{3608, 2772, 9557},
		{3581, 2772, 9568},
		{3608, 2772, 9579},
		{3608, 2772, 9590},
		{3608, 2761, 9590},
		{3608, 2761, 9579},
		{3608, 2761, 9568},
		{3608, 2761, 9557},
		{3608, 2761, 9546}
	};
	
	protected static final Object[][] AGILITY_ARENA_OBJECTS = {
		// id, x, y, xp
		{3572, 2802, 9591, 6.0}, // 3 logs, northern (east side)
		{3572, 2802, 9590, 6.0}, // 3 logs, middle (east side)
		{3572, 2802, 9589, 6.0}, // 3 logs, southern (east side)
		{3572, 2797, 9591, 6.0}, // 3 logs, northern (east side)
		{3572, 2797, 9590, 6.0}, // 3 logs, middle (east side)
		{3572, 2797, 9589, 6.0}, // 3 logs, southern (east side)
		{3583, 2792, 9592, 22.0}, // Handholds west of planks.
	};
}
