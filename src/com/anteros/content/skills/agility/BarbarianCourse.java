package com.anteros.content.skills.agility;

import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceMovement;
import com.anteros.model.player.Player;

public class BarbarianCourse extends AgilityData {

	public BarbarianCourse() {
		
	}
	
	public static void doCourse(final Player p, final int objectX, final int objectY, final Object[] objectArray) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch((Integer)objectArray[0]) {
			case 20210: // Entrance tunnel
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2551, 3558, 2553, 3561) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX + 1 && pY == objectY) {
							newMove = 1;
						} else if (pX == objectX - 1 && pY == objectY) {
							newMove = 2;
						} else if (pX == objectX - 1 && pY == objectY + 1) {
							newMove = 3;
						} else if (pX == objectX + 1 && pY == objectY + 1) {
							newMove = 4;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										p.getWalkingQueue().forceWalk(0, (path == 1 || path == 2) ? -1 : (path == 3 || path == 4) ? +1 : 0);
									} else if (i == 1) {
										p.getWalkingQueue().forceWalk((path == 1 || path == 4) ? -1 : (path == 2 || path == 3) ? +1 : 0, 0);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							int i = 0;
							@Override
							public void execute() {
								if (i == 0) {
									p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() <= 3558 ? 3561 : 3558, 0));
									this.setTick(500);
									i++;
								} else {
									this.stop();
									final boolean running = p.getWalkingQueue().isRunToggled();
									int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
									int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
									final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
									final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
									final int newLocalY = p.getLocation().getY() == 3558 ? lY + 3 : lY - 3;
									final int newY = newLocalY > lY ? p.getLocation().getY() + 3 : p.getLocation().getY() - 3;
									int dir = newLocalY > lY ? 0 : 4;
									ForceMovement movement = new ForceMovement(lX, lY, lX, newLocalY, 10, 60, dir);
									p.setForceMovement(movement);
									p.animate(10580);
									p.getWalkingQueue().reset();
									p.getActionSender().clearMapFlag();
									p.setTemporaryAttribute("unmovable", true);
									World.getInstance().registerEvent(new Event(1500) {
				
										@Override
										public void execute() {
											this.stop();
											p.removeTemporaryAttribute("unmovable");
											p.teleport(Location.location(p.getLocation().getX(), newY, 0));
											p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
											p.getWalkingQueue().setRunToggled(running);
										}		
									});
								}
							}
						});
					}
				});
				break;

			case 2282: // Swing
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2550, 3554, 2552, 3555) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX - 1 && pY == objectY + 4) { // front left
							newMove = 1;
						} else if (pX == objectX + 1 && pY == objectY + 4) { // front right
							newMove = 2;
						} else if (pX == objectX - 1 && pY == objectY + 5) { // back left
							newMove = 3;
						} else if (pX == objectX + 1 && pY == objectY + 5) { // back right
							newMove = 4;
						} else if (pX == objectX && pY == objectY + 5) { // back middle
							newMove = 5;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									p.setFaceLocation(Location.location(2551, 3549, 0));
									if (path == 1 || path == 2) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(path == 1 ? +1 : -1, 0);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									} else if (path == 3 || path == 4) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(path == 3 ? +1 : -1, -1);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									} else if (path == 5) {
										if (i == 0) {
											p.getWalkingQueue().forceWalk(0, -1);
										} else if (i >= 1) {
											this.stop();
											doCourse(p, objectX, objectY, objectArray);
										}
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {

							@Override
							public void execute() {
								this.stop();
								final boolean running = p.getWalkingQueue().isRunToggled();
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								final int newY = p.getLocation().getY() - 5;
								int dir = 4;
								p.animate(751);
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY-5, 25, 57, dir);
								p.setForceMovement(movement);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								final Location objectLocation = Location.location(2551, 3550, 0);
								World.getInstance().registerEvent(new Event(30) {

									@Override
									public void execute() {
										this.stop();
										for (Player nearbyPlayers : World.getInstance().getPlayerList()) {
											if (nearbyPlayers != null) {
												nearbyPlayers.getActionSender().newObjectAnimation(objectLocation, 497);
											}
										}
									}
								});
								World.getInstance().registerEvent(new Event(1300) {

									@Override
									public void execute() {
										this.stop();
										p.getAppearance().setWalkAnimation(-1);
										p.getUpdateFlags().setAppearanceUpdateRequired(true);
										p.removeTemporaryAttribute("unmovable");
										p.teleport(Location.location(p.getLocation().getX(), newY, 0));
										p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
										p.getWalkingQueue().setRunToggled(running);
									}		
								});
							}
						});
					}
				});
				break;

			case 2294: // Log
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2550, 3545, 2551, 3547) {
					int distanceToWalk = p.getLocation().getX() == 2550 ? -9 : -10;
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX && pY == objectY - 1) {
							newMove = 1;
						} else if (pX == objectX&& pY == objectY + 1) {
							newMove = 2;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										p.getWalkingQueue().forceWalk(0, path == 1 ? +1 : -1);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								final boolean running = p.getWalkingQueue().isRunToggled();
								p.getWalkingQueue().setRunToggled(false);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								p.getAppearance().setWalkAnimation(155);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.getWalkingQueue().forceWalk(distanceToWalk, 0);
								int delay = distanceToWalk == -9 ? 5400 : 5900;
								World.getInstance().registerEvent(new Event(delay) {

									@Override
									public void execute() {
										this.stop();
										p.getAppearance().setWalkAnimation(-1);
										p.getUpdateFlags().setAppearanceUpdateRequired(true);
										p.removeTemporaryAttribute("unmovable");
										p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
										p.getWalkingQueue().setRunToggled(running);
									}		
								});
							}
						});
					}
				});
				break;
				
			case 20211: // Net
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2539, 3545, 2539, 3546) {
					@Override
					public void run() {
						p.animate(828);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setFaceLocation(Location.location(p.getLocation().getX()-1, p.getLocation().getY(), 0));
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1000) {
	
							@Override
							public void execute() {
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(p.getLocation().getX()-2, p.getLocation().getY(), 1));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
				
			case 2302: // Balance beam
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location((Integer)objectArray[3], (Integer)objectArray[4], 1)) {
					
					@Override
					public void run() {
						final boolean running = p.getWalkingQueue().isRunToggled();
						p.getWalkingQueue().setRunToggled(false);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setTemporaryAttribute("unmovable", true);
						p.getAppearance().setWalkAnimation(157);
						p.getUpdateFlags().setAppearanceUpdateRequired(true);
						p.getWalkingQueue().forceWalk(-4, 0);
						World.getInstance().registerEvent(new Event(2450) {
	
							@Override
							public void execute() {
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.getAppearance().setWalkAnimation(-1);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
								p.getWalkingQueue().setRunToggled(running);
							}
	
						});	
					}
				});
				break;
				
			case 1948: // Crumbling wall
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, objectX - 1, objectY - 1, objectX, objectY + 1) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX && pY == objectY - 1) {
							newMove = 1;
						} else if (pX == objectX && pY == objectY + 1) {
							newMove = 2;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										p.getWalkingQueue().forceWalk(-1, 0);
									} else if (i == 1) {
										p.getWalkingQueue().forceWalk(0, path == 1 ? +1 : -1);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
						int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
						final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
						final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
						ForceMovement movement = new ForceMovement(lX, lY, lX+2, lY, 10, 60, 1);
						p.setForceMovement(movement);		
						p.animate(4853);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1250) {
	
							@Override
							public void execute() {
								this.stop();
								p.setFaceLocation(Location.location(p.getLocation().getX()+1, p.getLocation().getY(), 0));
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(p.getLocation().getX()+2, p.getLocation().getY(), 0));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
		}
	}

	public static void useLadder(final Player p) {
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(2532, 3546, 1)) {
			
			@Override
			public void run() {
				p.animate(828);
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(1000) {

					@Override
					public void execute() {
						this.stop();
						p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY(), 0));
						p.removeTemporaryAttribute("unmovable");
					}
				});	
			}
		});
	}
}
