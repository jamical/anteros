package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.model.player.Player;
import com.anteros.net.Packet;

/**
 * 
 * Unused packets.
 * @author Graham
 */
public class Unused implements PacketHandler {

	@Override
	public void handlePacket(Player player, IoSession session, Packet packet) {
		
	}

}
