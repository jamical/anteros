package com.anteros.content.cluescrolls;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class ClueScrollChecks extends ClueScroll {
	
	  public static void checkClueLocation(Player p) {
	    	int playerX = p.getLocation().getX();
	    	int playerY = p.getLocation().getY();
	        switch (p.currentClue) {
	        case 2677:
	        case 2692:
	        case 2697:
	        	if (playerX == 2965 && playerY == 3378) {
	        		clueDig(p); p.getInventory().deleteItem(p.currentClue, 1); p.currentClue = 0;
	        	}
	        break;
	        case 2678:
	        case 2693:
	        case 2698:
	        	if (playerX == 3022 && playerY == 3912) {
	        		clueDig(p); p.getInventory().deleteItem(p.currentClue, 1); p.currentClue = 0;
	        	}
	        break;
	        case 2679:
	        case 2694:
	        case 2699:
	        	if (playerX == 2723 && playerY == 3338) {
	        		clueDig(p); p.getInventory().deleteItem(p.currentClue, 1);  p.currentClue = 0;
	        	}
	        break;
	        case 2680:
	        case 2695:
	        case 2700:
	        	if (playerX == 3278 && playerY == 3434) {
	        		clueDig(p); p.getInventory().deleteItem(p.currentClue, 1); p.currentClue = 0;
	        	}
	        break;
	        case 2681:
	        case 2696:
	        case 2701:
	        	if (playerX == 3309 && playerY == 3502) {
	              	clueDig(p); p.getInventory().deleteItem(p.currentClue, 1); p.currentClue = 0;
	        	}
	        break;
	       }  
	     }
	    
	    public static void showMapClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	case 2677:
	    	case 2692:
	        case 2697:
	            p.getActionSender().displayInterface(337);
	        break;
	        case 2678:
	        case 2693:
	        case 2698:
	            p.getActionSender().displayInterface(338);
	        break;
	        case 2679:
	        case 2694:
	        case 2699:
	        	p.getActionSender().displayInterface(339);
	        break;
	        case 2680:
	        case 2695:
	        case 2700:
	            p.getActionSender().displayInterface(340);
	        break;
	        case 2681:
	        case 2696:
	        case 2701:
	            p.getActionSender().displayInterface(350);
	        break;
	    	}
	    }
	    
	    public static void showEmoteClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	
	    	}
	    }
	    
	    public static void showCrypticClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	
	    	}
	    }
	    
	    public static void showSimpleClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	
	    	}
	    }
	    
	    public static void showCoordinateClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) { //TODO
			case 2687: //Coordinate clues
				
				break;
			case 2688:
				
				break;
			case 2689:
				
				break;
			case 2690:
				
				break;
			case 2691:
				
				break;
	    	}
	    }
	    
	    /*public static void showPuzzleClue(Player p, int itemId) { //TODO
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	
	    	}
	    }*/
	    
	    public static void showAnagramClue(Player p, int itemId) {
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	case 2682:
	    		p.getActionSender().sendTextScroll1("Aha Jar");
	    		break;
	    	case 2683:
	    		p.getActionSender().sendTextScroll1("A BAS");
	    		break;
	    	case 2684:
	    		p.getActionSender().sendTextScroll1("Bail Trims");
	    		break;
	    	case 2685:
	    		p.getActionSender().sendTextScroll1("El Ow");
	    		break;
	    	case 2686:
	    		p.getActionSender().sendTextScroll1("Goblin Kern");
	    		break;
	    	}
	    }
	    
	    /*public static void showChallengeClue(Player p, int itemId) { //TODO
	    	setCurrentClue(p, itemId);
	    	p.currentClue = itemId;
	    	switch(itemId) {
	    	
	    	}
	    }*/
	    
	    public static void showCongratsNpcChat(final Player p, final NPC npc) {
	    	final int random = Misc.random(5);
	    	World.getInstance().registerCoordinateEvent(new AreaEvent(p, npc.getLocation().getX() - 1, npc.getLocation().getY() - 1, npc.getLocation().getX() + 1, npc.getLocation().getY() + 1) {
    			@Override
    			public void run() {
    				npc.setFaceLocation(p.getLocation());
    				p.setFaceLocation(npc.getLocation());
    				p.setEntityFocus(65535);if (p.getInventory().getTotalFreeSlots() > 0) {
    		        	if (random > 0) {
    		        		p.getInventory().deleteItem(p.currentClue);
    		        		if (p.clueDifficulty == 1) {
    		        			p.getInventory().addItem(RandomEasyClue());
    		        		} else if (p.clueDifficulty == 2) {
    		        			p.getInventory().addItem(RandomMediumClue());
    		        		} else if (p.clueDifficulty == 3) {
    		        			p.getInventory().addItem(RandomHardClue());
    		        		}
    		        		p.currentClue = 0;
    		        		p.getActionSender().sendNpcChat1("Congratulations! Here's your next clue.", npc.getId(), npc.getDefinition().getName(), Dialogue.HAPPY_JOYFUL);
    		        	} else {
    		        		p.getInventory().deleteItem(p.currentClue);
    		        		p.getInventory().addItem(405);
    		        		p.currentClue = 0;
    		        		p.getActionSender().sendNpcChat1("Contratulations! Here's your reward.", npc.getId(), npc.getDefinition().getName(), Dialogue.HAPPY_JOYFUL);
    		        	}
    		    	} else {
    		    		p.getActionSender().sendNpcChat2("I have something for you, but you need", "another inventory slot to take it.", npc.getId(), npc.getDefinition().getName(), Dialogue.CALM_TALK);
    		    	}
    			}
    		});
	    }
	    
	    public static boolean currentClueNpc(Player p, NPC npc) {
	    	int curClue = p.currentClue;
	    	for (int i = 0; i < ClueScrollData.AnagramClue.length; i++) {
	    		if (curClue == ClueScrollData.AnagramClue[i] && npc.getId() == ClueScrollData.AnagramNpcs[i]) {
	    			showCongratsNpcChat(p, npc);
	    			return true;
	    		}
	    	}
	    	return false;
	    }

}
