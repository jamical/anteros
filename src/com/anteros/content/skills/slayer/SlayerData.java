package com.anteros.content.skills.slayer;

public class SlayerData {

	public SlayerData() {
		
	}
	
	protected static final int SLAYER = 18;
	
	protected static final int BUY_XP_POINTS = 400;
	protected static final int BUY_RING_POINTS = 75;
	protected static final int BUY_DART_POINTS = 35;
	protected static final int BUY_BOLT_POINTS = 35;
	protected static final int BUY_ARROW_POINTS = 35;
	protected static final int REASSIGN_POINTS = 30;
	protected static final int PERM_REMOVE_POINTS = 100;
	
	protected static final Object[][] SLAYER_MASTERS = {
		//Master id, combat level needed, name, min amount, max amount (plus min amount)
		{8273, 3, "Turael", 15, 35, "east Burthorpe", "Travel north of Taverly to get here.", "Burthorpe"},
		{8274, 30, "Mazchna", 20, 35, "north west Canifis", "Canifis is quite a walk, east of Varrock.", "Canifis"},
		{1597, 60, "Vannaka", 30, 40, "Edgeville dungeon", "Use the trapdoor in Edgeville to get here.", "Edgeville"},
		{1598, 80, "Chaeldar", 50, 35, "Zanaris", "You must have completed Lost City quest to get here.", "Zanaris"},
		{8275, 100, "Duradel", 40, 60, "Shilo Village", "I hear there is a Gnome who can direct you here.", "Shilo"},
	};
	
	protected static final Object[][] TURAEL_TASKS = {
		{"Rats"},
		{"Birds", 41},
		{"Bats"},
		{"Bears"},
		{"Cows", 81, 397},
		{"Crawling hands"},
		{"Cave bugs"},
		{"Cave slime"},
		{"Banshees"},
		{"Dwarves"},
		{"Ghosts"},
		{"Goblins"},
		{"Spiders"},
		{"Monkeys"},
		{"Scorpions"},
	};
	
	protected static final Object[][] MAZCHNA_TASKS = {
		{"Bats"},
		{"Bears"},
		{"Crawling hands"},
		{"Cave bugs"},
		{"Cave slime"},
		{"Banshees"},
		{"Ghosts"},
		{"Moss giants", 4688},
		{"Spiders"},
		{"Cave crawlers"},
		{"Cockatrice"},
		{"Zombies"},
		{"Hill giants"},
		{"Hobgoblins"},
		{"Icefiends"},
		{"Pyrefiends"},
		{"Skeletons"},
		{"Wolves"},
		{"Kalphites"},
		{"Dogs", 1594},
	};
	
	protected static final Object[][] VANNAKA_TASKS = {
		{"Bats"},
		{"Crawling hands"},
		{"Cave bugs"},
		{"Cave slime"},
		{"Banshees"},
		{"Rock slug"},
		{"Spiders"},
		{"Cave crawlers"},
		{"Cockatrice"},
		{"Zombies"},
		{"Hill giants"},
		{"Hobgoblins"},
		{"Dagannoth"},
		{"Icefiends"},
		{"Pyrefiends"},
		{"Gargoyles"},
		{"Skeletons"},
		{"Wolves"},
		{"Kalphites"},
		{"Aberrant spectres"},
		{"Basilisks"},
		{"Dogs", 1594},
		{"Bloodvelds"},
		{"Dust devils"},
		{"Green dragons"},
		{"Ice giants"},
		{"Ice warriors"},
		{"Jellies"},
		{"Infernal mages"},
		{"Lesser demons"},
		{"Moss giants", 4688},
		{"Fire giants", 1584, 1586, 7003},
		{"Turoth"},
	};
	
	protected static final Object[][] CHAELDAR_TASKS = {
		{"Banshees"},
		{"Spiders"},
		{"Cave crawlers"},
		{"Cockatrice"},
		{"Rock slug"},
		{"Hill giants"},
		{"Hobgoblins"},
		{"Pyrefiends"},
		{"Kalphites"},
		{"Aberrant spectres"},
		{"Basilisks"},
		{"Bloodvelds"},
		{"Dust devils"},
		{"Nechryael"},
		{"Dogs", 1594},
		{"Blue dragons"},
		{"Ice giants"},
		{"Gargoyles"},
		{"Jellies"},
		{"Infernal mages"},
		{"Lesser demons"},
		{"Fire giants", 1584, 1586, 7003},
		{"Turoth"},
		{"Kurasks"},
		{"Dagannoth"},
		{"Cave horrors"},
		{"Bronze dragons"},
		{"Waterfiends"},
	};
	
	protected static final Object[][] DURADEL_TASKS = {
		{"Aberrant spectres"},
		{"Gargoyles"},
		{"Abyssal demons"},
		{"Black demons"},
		{"Black Dragons"},
		{"Bloodvelds"},
		{"Dogs", 1594},
		{"Dark beasts"},
		{"Goraks"},
		{"Dagannoth"},
		{"Kalphites"},
		{"Iron dragons"},
		{"Steel dragons"},
		{"Mithril dragons"},
		{"Nechryael"},
		{"Spiritual mages"},
		{"Suqahs"},
		{"Greater demons"},
		{"Fire giants", 1584, 1586, 7003},
		{"Hellhounds"},
	};
}
