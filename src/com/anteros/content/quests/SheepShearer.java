package com.anteros.content.quests;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;

public class SheepShearer {

	Player c;

	public SheepShearer(Player c) {
		this.c = c;
	}

	public static void showInformation(Player p) {
		p.getActionSender().displayInterface(275);
		p.getActionSender().modifyText("Sheep Shearer", 275, 2);
		p.getQuests().sendQuestInterface(p);
		if (p.SheepShearerInt == 0) {
			p.getActionSender().modifyText("Sheep Shearer", 275, 2);
			p.getActionSender().modifyText("<col=342D7E>I can start this quest by speaking to Farmer Fred", 275, 12);
			p.getActionSender().modifyText("<col=342D7E>He is located on a farm somewhere outside Lumbridge", 275, 13);
			p.getActionSender().modifyText("", 275, 14);
		}
	}

	// } else if(p.SheepShearerInt == 1) {
	// p.getActionSender().modifyText("Sheep Shearer", 275, 2);
	// p.getActionSender().modifyText("<str>I've spoken with Farmer Fred", 275,
	// 12);
	// p.getActionSender().modifyText("<col=342D7E>He wants me to gather the following materials:",
	// 275, 13);
	// if(p.getInventory().hasItemAmount(435,6)) {
	// p.getActionSender().modifyText("<str>20 balls of wool", 275, 14);
	// } else {
	// p.getActionSender().modifyText("<col=800517>20 balls of woll", 275, 14);
	// }

	public static void showChats(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {
		case 611:
			p.getActionSender().sendNpcChat3("Oy der, mate! Care to help a little fella who needs", "some wool? There's a good", "reward, there is. So, how 'bout it, mate?", 758, "Doric", 9850);
			newStatus = 1000;
			break;
		case 612:
			p.getActionSender().sendOption2("Sure.", "Uh, I'll have to pass for now.");
			newStatus = 1001;
			break;
		case 613:
			p.getActionSender().sendPlayerChat1("Sure.", 9847);
			p.doricsQuestInt = 1;
			newStatus = 614;
			break;
		case 614:
			p.getActionSender().sendNpcChat3("Oh, that's great, right 'der, that's great. Now, I", "will be needing some clay, some copper, and", "a couple o' iron ores.", 758, "Doric", 9850);
			newStatus = 615;
			break;
		case 615:
			p.getActionSender().sendPlayerChat1("Alright, I'm on my way.", 9847);
			break;
		case 616:
			p.getActionSender().sendPlayerChat1("Uh, I'll have to pass for now.", 9827);
			newStatus = 615;
			break;
		case 617:
			p.getActionSender().sendNpcChat3("You'll have to pass? Oh I see, it's 'cause I'm", "short, ain't it? You pesky humans need to learn", "some respect for us dwarfs. Get out of my house!", 758, "Doric", 9785);
			break;
		case 618:
			p.getActionSender().sendNpcChat1("So, you got my items, mate?", 758, "Doric", 9850);
			newStatus = 619;
			break;
		case 619:
			if (p.getInventory().hasItemAmount(1759, 20)) {
				p.getActionSender().sendPlayerChat1("Here's all the items!", 9850);
				newStatus = 620;
			} else {
				p.getActionSender().sendPlayerChat1("I don't have all the items yet.", 9775);
				newStatus = 622;
			}
			break;
		case 620:
			p.getInventory().deleteItem(437, 4);
			p.getActionSender().sendNpcChat1("Thanks mate! I owe it to ya!", 758, "Doric", 9750);
			newStatus = 621;
			break;
		case 621:
			p.doricsQuestInt = 3;
			p.getQuests().addQuestPoint(p, 1);
			p.getLevels().addXp(Skills.CRAFTING, 150);
			p.getInventory().addItem(995, 2000);
			p.getQuests().sendQuestCompleted(p);
			p.getQuests().loadList(p);
			p.getActionSender().questCompleted3(1, 1269, "You have completed The Sheep Shearer Quest!", "1 Quest Point", "150 Crafting XP", "2000 coins");
			break;
		case 622:
			p.getActionSender().sendNpcChat1("Well then go get them, you baffoon!", 758, "Doric", 9785);
			break;
		case 623:
			p.getActionSender().sendNpcChat1("Y'know, you could always use my anvils.", 758, "Doric", 9847);
			break;

		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}