package com.anteros.content.minigames.agilityarena;

import java.text.NumberFormat;

import com.anteros.content.skills.Skills;
import com.anteros.content.skills.agility.AgilityData;
import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Area;
import com.anteros.util.Misc;

public class AgilityArena extends AgilityData {

	private static int currentPillar;
	
	public AgilityArena() {
		startArena();
	}

	private void startArena() {
		World.getInstance().registerEvent(new Event(0) {

			@Override
			public void execute() {
				currentPillar = Misc.random(AGILITY_ARENA_PILLARS.length - 1);
				updateArrow();
				this.setTick(30000 + Misc.random(30000));
			}
			
		});
	}
	
	public static void tagPillar(final Player p, final int pillarIndex) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p,  AGILITY_ARENA_PILLARS[pillarIndex][1]-1, AGILITY_ARENA_PILLARS[pillarIndex][2]-1, AGILITY_ARENA_PILLARS[pillarIndex][1]+1, AGILITY_ARENA_PILLARS[pillarIndex][2]+1) {

			@Override
			public void run() {
				if (p.getLocation().getZ() == 3) {
					p.setFaceLocation(Location.location(AGILITY_ARENA_PILLARS[pillarIndex][1], AGILITY_ARENA_PILLARS[pillarIndex][2], 3));
					if (pillarIndex != currentPillar) {
						p.getActionSender().sendMessage("You can only get a ticket when the flashing arrow is above the pillar!");
						return;
					}
					if (p.getSettings().taggedLastAgilityPillar()) {
						p.getActionSender().sendMessage("You have already tagged this pillar, wait until the arrow moves again.");
						return;
					}
					int currentStatus = p.getSettings().getAgilityArenaStatus();
					if (currentStatus == 0) {
						p.getActionSender().sendConfig(309, 4);
						p.getActionSender().sendMessage("You get tickets by tagging more than one pillar in a row. Tag the next pillar!");
					} else {
						p.getInventory().addItem(2996);
						p.getActionSender().sendMessage("You recieve an Agility Arena ticket!");
					}
					p.getSettings().setAgilityArenaStatus(currentStatus == 0 ? 1 : 1);
					p.getSettings().setTaggedLastAgilityPillar(true);
				}
			}
		});
	}
	
	public static void updatePillarForPlayer(Player p) {
		int[] pillarVars = AGILITY_ARENA_PILLARS[currentPillar];
		p.getActionSender().setArrowOnPosition(pillarVars[1], pillarVars[2], 80);
	}

	public void updateArrow() {
		int[] pillarVars = AGILITY_ARENA_PILLARS[currentPillar];
		for (Player p : World.getInstance().getPlayerList()) {
			if (p != null) {
				if (Area.atAgilityArena(p.getLocation())) {
					p.getActionSender().setArrowOnPosition(pillarVars[1], pillarVars[2], 80);
					if (p.getSettings().getAgilityArenaStatus() > 0) {
						if (!p.getSettings().taggedLastAgilityPillar()) {
							p.getSettings().setAgilityArenaStatus(0);
							p.getActionSender().sendConfig(309, 0);
						}
					}
					p.getSettings().setTaggedLastAgilityPillar(false);
				}
			}
		}
	}
	
	public static void enterArena(final Player p, int objectX, int objectY) {
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(2809, 3193, 0)) {
			@Override
			public void run() {
				if (!p.getSettings().hasPaidAgilityArena()) {
					p.getActionSender().sendMessage("You must pay Cap'n Izzy the entrance fee before you can enter the Agility Arena.");
					return;
				}
				p.animate(827);
				World.getInstance().registerEvent(new Event(1000) {

					@Override
					public void execute() {
						p.getSettings().setPaidAgilityArena(false);
						p.teleport(Location.location(2805, 9589, 3));
						this.stop();
					}
				});
			}
		});
	}
	
	public static void exitArena(final Player p, int objectX, int objectY) {
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(2805, 9589, 3)) {
			@Override
			public void run() {
				p.animate(828);
				World.getInstance().registerEvent(new Event(1000) {

					@Override
					public void execute() {
						p.teleport(Location.location(2809, 3193, 0));
						this.stop();
					}
				});
			}
		});
	}
	
	public static boolean dialogue(final Player p, final NPC npc, final boolean rightClickPay) {
		if ((npc.getId() != 1055 && npc.getId() != 437) || (rightClickPay && npc.getId() != 437)) {
			return false;
		}
		p.setEntityFocus(npc.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, npc.getLocation().getX()-1, npc.getLocation().getY()-1, npc.getLocation().getX()+1, npc.getLocation().getY()+1) {
			@Override
			public void run() {
				npc.setFaceLocation(p.getLocation());
				int status = npc.getId() == 1055 ? 43 : 1;
				if (rightClickPay) {
					if (!p.getInventory().hasItemAmount(995, AGILITY_ARENA_PRICE)) {
						p.getActionSender().sendMessage("You don't have enough money to pay the entrance fee.");
						return;
					}
					status = 29;
				}
				doDialogue(p, status);
			}
		});
		return true;
	}

	public static void doDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		switch(status) {
			case 1:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Ahoy Cap'n!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 2;
				break;
				
			case 2:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("Ahoy there!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 3;
				break;
				
			case 3:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("Avast ye scurvy swabs!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 4;
				break;
				
			case 4:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Huh?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 5;
				break;
				
			case 5:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("Don't mind me parrot, he's Cracked Jenny's Tea Cup!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 6;
				break;
				
			case 6:
				p.getActionSender().sendChatboxInterface2(235);
				p.getActionSender().modifyText("What is this place?", 235, 2);
				p.getActionSender().modifyText("What do i do in the arena?", 235, 3);
				p.getActionSender().modifyText("I'd like to use the Agility Arena, please.", 235, 4);
				p.getActionSender().modifyText("Could you sell me a Skillcape of Agility?.", 235, 5);
				p.getActionSender().modifyText("See you later.", 235, 6);
				newStatus = 7;
				break;
				
			case 7:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("What is this place?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 8;
				break;
				
			case 8:
				p.getActionSender().sendNPCHead(437, 242, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 242, 3);
				p.getActionSender().modifyText("This, me hearty, is the entrance to the Brimhaven", 242, 4);
				p.getActionSender().modifyText("Agility Arena!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 2);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 9;
				break;
				
			case 9:
				p.getActionSender().sendNPCHead(437, 242, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 242, 3);
				p.getActionSender().modifyText("I were diggin for buried treasure when I found it!", 242, 4);
				p.getActionSender().modifyText("Amazed I was! It was a sight to behold!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 2);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 10;
				break;
				
			case 10:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("Buried treasure!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 11;
				break;
				
			case 11:
				p.getActionSender().sendNPCHead(437, 242, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 242, 3);
				p.getActionSender().modifyText("It were the biggest thing i'd ever seen! It must've been", 242, 4);
				p.getActionSender().modifyText("atleast a league from side to side!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 2);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 12;
				break;
				
			case 12:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("It made me list, I were that excited!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 13;
				break;
				
			case 13:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("Get on with it!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 14;
				break;
				
			case 14:
				p.getActionSender().sendNPCHead(437, 244, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 244, 3);
				p.getActionSender().modifyText("I'd found a huge cave with all these platforms. I reckon", 244, 4);
				p.getActionSender().modifyText("it be an ancient civilisation that made it. I had to be", 244, 5);
				p.getActionSender().modifyText("mighty careful as there was these traps everywhere!", 244, 6);
				p.getActionSender().modifyText("Dangerous it was!", 244, 7);
				p.getActionSender().animateInterface(9827, 244, 2);
				p.getActionSender().sendChatboxInterface2(244);
				newStatus = 15;
				break;
				
			case 15:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("Danger! Danger!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 16;
				break;
				
			case 16:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("Entrance is only " + NumberFormat.getInstance().format(AGILITY_ARENA_PRICE) + " coins!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 6;
				break;
				
			case 17:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("What do I do in the arena?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 18;
				break;
				
			case 18:
				p.getActionSender().sendNPCHead(437, 244, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 244, 3);
				p.getActionSender().modifyText("Well, me hearty, it's simple. Ye can cross between two", 244, 4);
				p.getActionSender().modifyText("platforms by using the traps or obstacles strung across", 244, 5);
				p.getActionSender().modifyText("'em. Try and make your way to the pillar that is", 244, 6);
				p.getActionSender().modifyText("indicated by the flashing arrow.", 244, 7);
				p.getActionSender().animateInterface(9827, 244, 2);
				p.getActionSender().sendChatboxInterface2(244);
				newStatus = 19;
				break;
				
			case 19:
				p.getActionSender().sendNPCHead(437, 243, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 243, 3);
				p.getActionSender().modifyText("Ye receive tickets for tagging more than one pillar in a", 243, 4);
				p.getActionSender().modifyText("row. So ye won't get a ticket from the first pillar but", 243, 5);
				p.getActionSender().modifyText("ye will for every platform ye tag in a row after that.", 243, 6);
				p.getActionSender().animateInterface(9827, 243, 2);
				p.getActionSender().sendChatboxInterface2(243);
				newStatus = 20;
				break;
				
			case 20:
				p.getActionSender().sendNPCHead(437, 244, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 244, 3);
				p.getActionSender().modifyText("If ye miss a platform ye will miss out on the next ticket", 244, 4);
				p.getActionSender().modifyText("so try and get every platform you can! When ye be", 244, 5);
				p.getActionSender().modifyText("done, take the tickets to Jackie over there and she'll", 244, 6);
				p.getActionSender().modifyText("exchange them for experience or items.", 244, 7);
				p.getActionSender().animateInterface(9827, 244, 2);
				p.getActionSender().sendChatboxInterface2(244);
				newStatus = 21;
				break;
				
			case 21:
				p.getActionSender().sendNPCHead(4535, 242, 1);
				p.getActionSender().modifyText("Parrot", 242, 3);
				p.getActionSender().modifyText("Tag when green light means tickets!", 242, 4);
				p.getActionSender().modifyText("Tag when red light means green light!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 1);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 22;
				break;
				
			case 22:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("Thanks me hearty!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 23;
				break;
				
			case 23:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Thanks!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 6;
				break;
				
			case 24:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("I'd like to use the Agility Arena, please.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 25;
				break;
				
			case 25:
				String message = "";
				if (p.getSettings().hasPaidAgilityArena()) {
					message = "Ye've already paid, so down ye goes...";
				} else {
					message = "Aye, Entrance be " + NumberFormat.getInstance().format(AGILITY_ARENA_PRICE) + " coins.";
					newStatus = 26;
				}
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText(message, 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 26:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("Pieces of eight!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 27;
				break;
				
			case 27:
				p.getActionSender().sendNPCHead(437, 242, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 242, 3);
				p.getActionSender().modifyText("A word of warning me hearty! There are dangerous", 242, 4);
				p.getActionSender().modifyText("traps down there!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 2);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 28;
				break;
				
			case 28:
				if (!p.getInventory().hasItemAmount(995, AGILITY_ARENA_PRICE)) {
					p.getActionSender().sendPlayerHead(64, 2);
					p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
					p.getActionSender().modifyText("I don't have enough money on me at the moment..", 64, 4);
					p.getActionSender().animateInterface(9827, 64, 2);
					p.getActionSender().sendChatboxInterface2(64);
					newStatus = 31;
					break;
				}
				p.getActionSender().modifyText("Ok, here's " + NumberFormat.getInstance().format(AGILITY_ARENA_PRICE) + " coins.", 228, 2);
				p.getActionSender().modifyText("Never mind.", 228, 3);
				p.getActionSender().sendChatboxInterface(228);
				newStatus = 29;
				break;
				
			case 29:
				if (p.getInventory().deleteItem(995, AGILITY_ARENA_PRICE)) {
					p.getSettings().setPaidAgilityArena(true);
				}
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Ok, here's " + NumberFormat.getInstance().format(AGILITY_ARENA_PRICE) + " coins.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 30;
				break;
				
			case 30:
				p.getActionSender().sendMessage("You pay Cap'n Izzy " + NumberFormat.getInstance().format(AGILITY_ARENA_PRICE) + " coins.");
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("May the wind be in ye sails!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 31:
				p.getActionSender().sendNPCHead(4535, 241, 1);
				p.getActionSender().modifyText("Parrot", 241, 3);
				p.getActionSender().modifyText("*Squawk*", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 1);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 32;
				break;
				
			case 32:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("No coins, no entrance!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 33:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Never mind.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				break;
				
			case 34:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you sell me a Skillcape of Agility?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 35;
				break;
				
			case 35:
				p.getActionSender().sendNPCHead(437, 244, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 244, 3);
				if (p.getLevels().getLevelForXp(AGILITY) < 99) {
					p.getActionSender().modifyText("Unfortunatly not. I may only sell the Skillcape of Agility", 244, 4);
					p.getActionSender().modifyText("to those that have conquered the obstacles of Xenorune,", 244, 5);
					p.getActionSender().modifyText("can climb like a cat and run like the wind! which err..", 244, 6);
					p.getActionSender().modifyText("isnt you. Is there anything else?", 244, 7);
					newStatus = 6;
				} else {
					p.getActionSender().modifyText("Indeed! You have reached level 99 Agility and have", 244, 4);
					p.getActionSender().modifyText("become a master of dexterity. It would be a pleasure", 244, 5);
					p.getActionSender().modifyText("to sell you an Agility skillcape and hood for a sum of", 244, 6);
					p.getActionSender().modifyText(NumberFormat.getInstance().format(Skills.SKILLCAPE_PRICE) + " coins.", 244, 7);
					newStatus = 36;
				}
				p.getActionSender().animateInterface(9827, 244, 2);
				p.getActionSender().sendChatboxInterface2(244);
				break;
				
			case 36:
				p.getActionSender().modifyText("I'll pay " + NumberFormat.getInstance().format(Skills.SKILLCAPE_PRICE) + " coins.", 228, 2);
				p.getActionSender().modifyText(NumberFormat.getInstance().format(Skills.SKILLCAPE_PRICE) + " is a crazy price!", 228, 3);
				p.getActionSender().sendChatboxInterface(228);
				newStatus = 37;
				break;
				
			case 37:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("I'll pay " + NumberFormat.getInstance().format(Skills.SKILLCAPE_PRICE) + " coins.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 38;
				break;
				
			case 38:
				if (p.getInventory().getTotalFreeSlots() < 2) {
					p.getActionSender().sendNPCHead(437, 241, 2);
					p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
					p.getActionSender().modifyText("Ye require 2 free inventory spaces!", 241, 4);
					p.getActionSender().animateInterface(9827, 241, 2);
					p.getActionSender().sendChatboxInterface2(241);
					break;
				}
				if (p.getInventory().deleteItem(995, Skills.SKILLCAPE_PRICE)) {
					int cape = p.getLevels().hasMultiple99s() ? 9772 : 9771;
					int hood = 9773;
					p.getInventory().addItem(cape);
					p.getInventory().addItem(hood);
					p.getActionSender().sendNPCHead(437, 242, 2);
					p.getActionSender().modifyText("Cap'n Izzy No-Beard", 242, 3);
					p.getActionSender().modifyText("One Agility Skillcape & hood.", 242, 4);
					p.getActionSender().modifyText("Wear it with pride.", 242, 5);
					p.getActionSender().animateInterface(9827, 242, 2);
					p.getActionSender().sendChatboxInterface2(242);
				} else {
					p.getActionSender().sendNPCHead(437, 241, 2);
					p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
					p.getActionSender().modifyText("Ye don't have enough coins!", 241, 4);
					p.getActionSender().animateInterface(9827, 241, 2);
					p.getActionSender().sendChatboxInterface2(241);
				}
				break;
				
			case 39:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText(NumberFormat.getInstance().format(Skills.SKILLCAPE_PRICE) + " is a crazy price!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 40;
				break;
				
			case 40:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("I'm sure ye will change your mind eventually..", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 41:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("See you later.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 42;
				break;
				
			case 42:
				p.getActionSender().sendNPCHead(437, 241, 2);
				p.getActionSender().modifyText("Cap'n Izzy No-Beard", 241, 3);
				p.getActionSender().modifyText("Aye, goodbye " + p.getPlayerDetails().getDisplayName() + ".", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			/*
			 * NOW TALKING TO JACKIE THE FRUIT
			 */
				
			case 43:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Ahoy there!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 44;
				break;
				
			case 44:
				p.getActionSender().sendNPCHead(1055, 241, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 241, 3);
				p.getActionSender().modifyText("Ahoy!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 45;
				break;
				
			case 45:
				p.getActionSender().sendChatboxInterface2(232);
				p.getActionSender().modifyText("What is this place?", 232, 2);
				p.getActionSender().modifyText("What do you do?", 232, 3);
				p.getActionSender().modifyText("I'd like to trade in my tickets, please.", 232, 4);
				p.getActionSender().modifyText("See you later.", 232, 5);
				newStatus = 46;
				break;
				
			case 46:
				p.getActionSender().sendNPCHead(1055, 241, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 241, 3);
				p.getActionSender().modifyText("Welcome to the Brimhaven Agility Arena!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 47;
				break;
				
			case 47:
				p.getActionSender().sendNPCHead(1055, 242, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 242, 3);
				p.getActionSender().modifyText("If ye want to know more, talk to Cap'n Izzy, after", 242, 4);
				p.getActionSender().modifyText("all... he did find it!", 242, 5);
				p.getActionSender().animateInterface(9827, 242, 2);
				p.getActionSender().sendChatboxInterface2(242);
				newStatus = 45;
				break;
				
			case 48:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("What do you do?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 49;
				break;
				
			case 49:
				p.getActionSender().sendNPCHead(1055, 244, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 244, 3);
				p.getActionSender().modifyText("I be the jack o' tickets. I exchange the tickets ye", 244, 4);
				p.getActionSender().modifyText("Collect in the Agility arena for more stuff. Ye can", 244, 5);
				p.getActionSender().modifyText("obtain more Agility experience or items ye won't", 244, 6);
				p.getActionSender().modifyText("find anywhere else!", 244, 7);
				p.getActionSender().animateInterface(9827, 244, 2);
				p.getActionSender().sendChatboxInterface2(244);
				newStatus = 50;
				break;
				
			case 50:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Sounds good!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 45;
				break;
				
			case 51:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("I'd like to trade in my tickets, please.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 52;
				break;
				
			case 52:
				p.getActionSender().sendNPCHead(1055, 241, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 241, 3);
				p.getActionSender().modifyText("Aye, ye be on the right track.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 53;
				break;
				
			case 53:
				p.getActionSender().displayInterface(6);
				break;
				
			case 54:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("See you later.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 55;
				break;
				
			case 55:
				p.getActionSender().sendNPCHead(1055, 241, 2);
				p.getActionSender().modifyText("Pirate Jackie the Fruit", 241, 3);
				p.getActionSender().modifyText("Goodbye.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}
