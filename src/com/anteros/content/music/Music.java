package com.anteros.content.music;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

/**
 * 
 * @author Abexlry
 */
public class Music {
    Player player; WorldData a = new WorldData(); MusicData s = new MusicData();
    public Music(Player player) {
    	this.player = player;
    }
    
	/**
	 * Plays a region track
	 */
	public static void playRegionTrack(Player player) {
		int areaId = WorldData.getAreaID(player);
		int musicId = MusicData.getSongID(areaId);
		playTrack(player, musicId);
	}
	
    /**
     * Plays a random music track
     */
	public static void playRandomTrack(Player player) {
		player.randomMusic = true;
		int id = MusicData.RandomMusic[(int) (Math.random()*MusicData.RandomMusic.length)];
		player.getActionSender().sendMusic(id, MusicData.getTrackNames(id));
	}
	
	/**
	 * Sets a music timer countdown
	 */
	public static void musicTimer(final Player player) {
		int time = player.musicTimer;
		World.getInstance().registerEvent(new Event(time) {
			@Override
			public void execute() {
				if (player.randomMusic == true) {
					playRandomTrack(player);
				} else {
					
				}
				this.stop();
			}
		});
	}
	
	/**
	 * Plays a levelup tune
	 */
	public static void playLevelupSound(Player player, int skill) {
		int id = MusicData.levelUpMusic(skill);
		player.getActionSender().sendMusicEffect(id);
	}
	
	/**
	 * Plays the track [id]
	 */
	public static void playTrack(Player player, int id) {
		player.getActionSender().sendMusic(id, MusicData.getTrackNames(id));
	}
}