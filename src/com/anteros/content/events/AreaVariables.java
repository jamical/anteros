package com.anteros.content.events;

import com.anteros.content.minigames.agilityarena.AgilityArena;
import com.anteros.content.minigames.barrows.Barrows;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Area;

public class AreaVariables extends Event {

	public AreaVariables() {
		super(500);
	}

	@Override
	public void execute() {
		for (Player p : World.getInstance().getPlayerList()) {
			if (p != null) {
				updateVariables(p);
			}
		}
	}
	
	/*
	 * NOTE: Anything that goes in here and varies between HD and LD, 
	 * reset the variable in ActionSender.configureGameScreen
	 */
	public void updateVariables(Player p) {
		int currentLevel = wildernessLevel(p.getLocation());
		if (currentLevel != p.getLastWildLevel()) {
			if (currentLevel > 0) {
				p.setLastwildLevel(currentLevel);
				if (p.getTemporaryAttribute("inWild") == null) {
					p.getActionSender().sendPlayerOption("Attack", 1, 1);
					p.getActionSender().sendOverlay(381);
					p.setTemporaryAttribute("inWild", true);
				}
			} else {
				if (p.getTemporaryAttribute("inWild") != null) {
					p.getActionSender().sendPlayerOption("null", 1, 1);
					p.getActionSender().sendRemoveOverlay();
					p.setLastwildLevel(0);
					p.removeTemporaryAttribute("inWild");
				}
			}
		}
		if (Area.inMultiCombat(p.getLocation())) {
			if (p.getTemporaryAttribute("inMulti") == null) {
				p.getActionSender().displayMultiIcon();
				p.setTemporaryAttribute("inMulti", true);
			}
		} else {
			if (p.getTemporaryAttribute("inMulti") != null) {
				p.getActionSender().removeMultiIcon();
				p.removeTemporaryAttribute("inMulti");
			}
		}
		if (Area.atDuelArena(p.getLocation())) {
			if (p.getDuel() != null) {
				if (p.getDuel().getStatus() == 5 || p.getDuel().getStatus() == 6) {
					p.getActionSender().sendPlayerOption("Fight", 1, 1);
				}
			}
			if (p.getTemporaryAttribute("challengeUpdate") != null) {
				p.getActionSender().sendPlayerOption("Challenge", 1, 0);
				p.removeTemporaryAttribute("challengeUpdate");
			}
			if (p.getTemporaryAttribute("atDuelArea") == null) {
				p.getActionSender().sendPlayerOption("Challenge", 1, 0);
				p.getActionSender().sendOverlay(638);
				p.setTemporaryAttribute("atDuelArea", true);
			}
		} else {
			if (p.getTemporaryAttribute("atDuelArea") != null) {
				p.getActionSender().sendPlayerOption("null", 1, 0);
				p.getActionSender().sendRemoveOverlay();
				p.removeTemporaryAttribute("atDuelArea");
			}
		}
		if (Area.atBarrows(p.getLocation())) {
			if (p.getTemporaryAttribute("atBarrows") == null) {
				p.getActionSender().sendOverlay(24);
				p.getActionSender().setMinimapStatus(2);
				p.getActionSender().sendConfig2(452, 2652256); // doors
				if (p.getTemporaryAttribute("betweenDoors") == null) {
					if (Barrows.betweenDoors(p)) {
						p.setTemporaryAttribute("betweenDoors", true);
						p.getActionSender().sendConfig(1270,1);
					}
				}
				p.getActionSender().modifyText("Kill Count: "+p.getSettings().getBarrowKillCount(), 24, 0);
				p.setTemporaryAttribute("atBarrows", true);
				Barrows.prayerDrainEvent(p);
				boolean allBrothersKilled = true;
				for (int i = 0; i < 6; i++) {
					if (!p.getSettings().getBarrowBrothersKilled(i)) {
						allBrothersKilled = false;
					}
				}
				if (allBrothersKilled) {
					Barrows.startEarthQuake(p);
				}
			}
		} else {
			if (p.getTemporaryAttribute("atBarrows") != null) {
				boolean allBrothersKilled = true;
				for (int i = 0; i < 6; i++) {
					if (!p.getSettings().getBarrowBrothersKilled(i)) {
						allBrothersKilled = false;
					}
				}
				if (allBrothersKilled) {
					for (int i = 0; i < 6; i++) {
						p.getSettings().setBarrowBrothersKilled(i, false);
					}
					p.getSettings().setBarrowTunnel(-1);
					p.getSettings().setBarrowKillCount(0);
					p.getActionSender().resetCamera();
					p.removeTemporaryAttribute("lootedBarrowChest");
				}
				p.getActionSender().resetCamera();
				p.getActionSender().sendRemoveOverlay();
				p.removeTemporaryAttribute("atBarrows");
				p.removeTemporaryAttribute("barrowTunnel");
				p.getActionSender().setMinimapStatus(0);
				Barrows.removeBrotherFromGame(p);
			}
		}
		if (Area.atGodwars(p.getLocation())) {
			if (p.getTemporaryAttribute("atGodwars") == null) {
				p.getActionSender().sendOverlay(601);
				p.setTemporaryAttribute("atGodwars", true);
			}
		} else {
			if (p.getTemporaryAttribute("atGodwars") != null) {
				p.getActionSender().sendRemoveOverlay();
				p.removeTemporaryAttribute("atGodwars");
			}
		}
		if (Area.atAgilityArena(p.getLocation())) {
			if (p.getTemporaryAttribute("atAgilityArena") == null) {
				p.getActionSender().sendOverlay(5);
				AgilityArena.updatePillarForPlayer(p);
				p.setTemporaryAttribute("atAgilityArena", true);
			}
			if (p.getLocation().getZ() == 0) {
				p.removeTemporaryAttribute("atAgilityArena");
				p.getActionSender().sendRemoveOverlay();
				p.getSettings().setAgilityArenaStatus(0);
				p.getSettings().setTaggedLastAgilityPillar(false);
			}
		} else {
			if (p.getTemporaryAttribute("atAgilityArena") != null) {
				p.getActionSender().sendRemoveOverlay();
				p.getSettings().setAgilityArenaStatus(0);
				p.getSettings().setTaggedLastAgilityPillar(false);
				p.removeTemporaryAttribute("atAgilityArena");
			}
		}
		/*
		 * We check the cantDoAnything variable to determine whether they're using the orb.
		 */
		if (Area.inFightPitsWaitingArea(p.getLocation())) {
			if (p.getTemporaryAttribute("waitingForFightPits") == null) {
				World.getInstance().getMinigames().getFightPits().addWaitingPlayer(p);
				p.setTemporaryAttribute("waitingForFightPits", true);
			}
		} else {
			if (p.getTemporaryAttribute("waitingForFightPits") != null && p.getTemporaryAttribute("cantDoAnything") == null) {
				World.getInstance().getMinigames().getFightPits().removeWaitingPlayer(p);
				p.removeTemporaryAttribute("waitingForFightPits");
			}
		}
		if (Area.inFightPits(p.getLocation())) {
			if (p.getTemporaryAttribute("cantDoAnything") == null) {
				if (p.getTemporaryAttribute("inFightPits") == null) {
					p.getActionSender().sendPlayerOption("Attack", 1, 1);
					World.getInstance().getMinigames().getFightPits().displayFightPitsInterface(p);
					p.setTemporaryAttribute("inFightPits", true);
				}
			}
		} else {
			if (p.getTemporaryAttribute("inFightPits") != null) {
				p.getActionSender().sendPlayerOption("null", 1, 1);
				p.getActionSender().sendRemoveOverlay();
				p.removeTemporaryAttribute("inFightPits");
			}
		}
		if (Area.onWaterbirthIsle(p.getLocation())) {
			if (p.getTemporaryAttribute("snowInterface") == null) {
				p.getActionSender().sendOverlay(370);
				p.setTemporaryAttribute("snowInterface", true);
			}
		} else {
			if (p.getTemporaryAttribute("snowInterface") != null) {
				p.getActionSender().sendRemoveOverlay();
				p.removeTemporaryAttribute("snowInterface");
			}
		}
	}

	public int wildernessLevel(Location l) {
		int y = l.getY();
		if (!Area.inWilderness(l)) {
			return -1;
		}
		if(y > 3523 && y < 4000) {
			return (((int)(Math.ceil((double)(y)-3520D) / 8D) + 1));
		}
		return -1;
	}

}
