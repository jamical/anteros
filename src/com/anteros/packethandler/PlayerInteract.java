package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.Constants;
import com.anteros.content.combat.Combat;
import com.anteros.content.combat.MagicCombat;
import com.anteros.content.minigames.duelarena.DuelSession;
import com.anteros.content.skills.Skills;
import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.FaceLocation;
import com.anteros.model.player.Player;
import com.anteros.model.player.TradeSession;
import com.anteros.net.Packet;
import com.anteros.util.Area;
import com.anteros.world.Trade;

public class PlayerInteract implements PacketHandler {

	private static final int ATTACK = 68;
	private static final int FOLLOW = 71;
	private static final int TRADE = 180;
	private static final int MAGIC_ON_PLAYER = 195;
	
	@Override
	public void handlePacket(Player player, IoSession session, Packet packet) {
		switch(packet.getId()) {
			case ATTACK:
				handleAttackPlayer(player, packet);
				break;
				
			case FOLLOW:
				handleFollowPlayer(player, packet);
				break;
				
			case TRADE:
				handleTradePlayer(player, packet);
				break;
				
			case MAGIC_ON_PLAYER:
				handleMagicOnPlayer(player, packet);
				break;
		}
	}

	private void handleAttackPlayer(final Player player, Packet packet) {
		int index = packet.readLEShortA();
		if(index < 0 || index >= Constants.PLAYER_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final Player p2 = World.getInstance().getPlayerList().get(index);
		if (p2 == null) {
			return;
		}
		player.setFaceLocation(new FaceLocation(p2.getLocation()));
		player.getActionSender().closeInterfaces();
		if (Area.atDuelArena(player.getLocation())) {
			if(player.getDuel() != null) {
				if (player.getDuel().getStatus() < 4) {
					player.getDuel().declineDuel();
					return;
				} else if (player.getDuel().getStatus() == 5 && player.getDuel().getPlayer2().equals(p2)) {
					player.getActionSender().sendMessage("The duel has not yet started!");
					return;
				} else if ((player.getDuel().getStatus() == 5 || player.getDuel().getStatus() == 6) && !player.getDuel().getPlayer2().equals(p2)) {
					return;
				} else if (player.getDuel().getStatus() == 6) {
					Combat.checkAttack(player, p2);
					return;
				}
			}
			if (!player.getLocation().withinDistance(p2.getLocation(), 1)) {
				int x = p2.getLocation().getX();
				int y = p2.getLocation().getY();
				World.getInstance().registerCoordinateEvent(new AreaEvent(player, x-1, y-1, x+1, y+1) {
					@Override
					public void run() {
						player.getWalkingQueue().reset();
						player.getActionSender().clearMapFlag();
						if (p2.getGESession() != null || (p2.getDuel() != null && !p2.getDuel().getPlayer2().equals(player)) || p2.getTrade() != null || p2.getShopSession() != null || p2.getBank().isBanking()) {
							player.getActionSender().sendMessage("That player is busy at the moment.");
							return;
						}
						if (p2.wantsToDuel(player)) {
							p2.setFaceLocation(new FaceLocation(player.getLocation()));
							player.getActionSender().closeInterfaces();
							p2.getActionSender().closeInterfaces();
							player.setDuelSession(new DuelSession(player, p2));
							p2.setDuelSession(new DuelSession(p2, player));
							return;
						}
						player.setFaceLocation(new FaceLocation(p2.getLocation()));
						p2.getActionSender().sendMessage(player.getPlayerDetails().getDisplayName() + ":duelstake:");
						player.getActionSender().sendMessage("Sending duel request...");
						player.newDuelRequest(p2);
					}
				});
				return;
			}
			if (p2.getGESession() != null || (p2.getDuel() != null && !p2.getDuel().getPlayer2().equals(player)) || p2.getTrade() != null || p2.getShopSession() != null || p2.getBank().isBanking()) {
				player.getActionSender().sendMessage("That player is busy at the moment.");
				return;
			}
			if (p2.wantsToDuel(player)) {
				player.getActionSender().closeInterfaces();
				p2.getActionSender().closeInterfaces();
				p2.setFaceLocation(new FaceLocation(player.getLocation()));
				player.setDuelSession(new DuelSession(player, p2));
				p2.setDuelSession(new DuelSession(p2, player));
				return;
			}
			player.newDuelRequest(p2);
			p2.getActionSender().sendMessage(player.getPlayerDetails().getDisplayName() + ":duelstake:");
			player.getActionSender().sendMessage("Sending duel request...");
			return;
		}
		Combat.checkAttack(player, p2);
	}

	private void handleFollowPlayer(Player player, Packet packet) {
		int index = packet.readLEShortA();
		if(index < 0 || index >= Constants.PLAYER_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final Player p2 = World.getInstance().getPlayerList().get(index);
		if (p2 == null) {
			return;
		}
	    player.FollowingId = index;
			//player.setTickEvent(null); // you might need to remove this
			player.Following(player, p2);
		player.getFollow().setFollowing(p2);
	}
	
	public static void handleFollowPlayer1(Player player, Packet packet) {
		int index = packet.readLEShortA();
		if(index < 0 || index >= Constants.PLAYER_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final Player p2 = World.getInstance().getPlayerList().get(index);
		if (p2 == null) {
			return;
		}
	    player.FollowingId = index;
			//player.setTickEvent(null); // you might need to remove this
			player.Following(player, p2);
		player.getFollow().setFollowing(p2);
	}

	private void handleTradePlayer(final Player player, Packet packet) {
		int index = packet.readLEShortA();
		if(index < 0 || index >= Constants.PLAYER_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final Player p2 = World.getInstance().getPlayerList().get(index);
		if (p2 == null) {
			return;
		}
		player.setFaceLocation(new FaceLocation(p2.getLocation()));
		if(player.getTrade() != null) {
			player.getTrade().decline();
			return;
		}
		player.getActionSender().closeInterfaces();
		if (!player.getLocation().withinDistance(p2.getLocation(), 1)) {
			int x = p2.getLocation().getX();
			int y = p2.getLocation().getY();
			World.getInstance().registerCoordinateEvent(new AreaEvent(player, x-1, y-1, x+1, y+1) {
				@Override
				public void run() {
					player.getWalkingQueue().reset();
					player.getActionSender().clearMapFlag();
					if (p2.getGESession() != null || p2.getTrade() != null || p2.getDuel() != null || p2.getShopSession() != null || p2.getBank().isBanking()) {
						player.getActionSender().sendMessage("That player is busy at the moment.");
						return;
					}
					if (p2.wantsToTrade(player)) {
						player.getActionSender().closeInterfaces();
						p2.getActionSender().closeInterfaces();
						p2.setFaceLocation(new FaceLocation(player.getLocation()));
						player.setTrade(new TradeSession(new Trade(player, p2)));
						p2.setTrade(new TradeSession(new Trade(p2, player)));
						return;
					}
					player.setFaceLocation(new FaceLocation(p2.getLocation()));
					p2.getActionSender().sendMessage(player.getPlayerDetails().getDisplayName() + ":tradereq:");
					player.getActionSender().sendMessage("Sending trade offer...");
					player.newTradeRequest(p2);
				}
			});
			return;
		}
		if (p2.getGESession() != null || p2.getTrade() != null || p2.getDuel() != null || p2.getShopSession() != null || p2.getBank().isBanking()) {
			player.getActionSender().sendMessage("That player is busy at the moment.");
			return;
		}
		if (p2.wantsToTrade(player)) {
			player.getActionSender().closeInterfaces();
			p2.getActionSender().closeInterfaces();
			p2.setFaceLocation(new FaceLocation(player.getLocation()));
			player.setTrade(new TradeSession(new Trade(player, p2)));
			p2.setTrade(new TradeSession(new Trade(p2, player)));
			return;
		}
		player.newTradeRequest(p2);
		p2.getActionSender().sendMessage(player.getPlayerDetails().getDisplayName() + ":tradereq:");
		player.getActionSender().sendMessage("Sending trade offer...");
	}
	
	private void handleMagicOnPlayer(Player player, Packet packet) {
		int junk = packet.readShortA();
		int id = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int index = packet.readLEShortA();
		if(index < 0 || index >= Constants.PLAYER_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final Player p2 = World.getInstance().getPlayerList().get(index);
		if (p2 == null) {
			return;
		}
		player.getActionSender().closeInterfaces();
		player.setTarget(p2);
		MagicCombat.newMagicAttack(player, p2, id,  interfaceId == 193);
	}

}
