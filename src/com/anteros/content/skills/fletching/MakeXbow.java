package com.anteros.content.skills.fletching;

import com.anteros.content.skills.SkillItem;
import com.anteros.event.Event;
import com.anteros.model.ItemDefinition;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class MakeXbow extends FletchData {

	public static void displayOptionInterface(Player p, int type, boolean stringing) {
		String s = "<br><br><br><br>";
		p.getActionSender().sendChatboxInterface(309);
		if (!stringing) {
			p.getActionSender().itemOnInterface(309, 2, 150, UNFINISHED_XBOW[type]);
			p.getActionSender().modifyText(s + ItemDefinition.forId(UNFINISHED_XBOW[type]).getName(), 309, 6);
			return;
		}
		p.getActionSender().itemOnInterface(309, 2, 150, FINISHED_XBOW[type]);
		p.getActionSender().modifyText(s + ItemDefinition.forId(FINISHED_XBOW[type]).getName(), 309, 6);
	}

	public static void createXbow(final Player p, int amount, int xbowType, boolean isStringing, boolean newFletch) {
		SkillItem item = null;
		if (newFletch || Fletching.getFletchItem(p) == null) {
			item = getXbow(xbowType, isStringing, amount);
			Fletching.setFletchItem(p, item);
		}
		item = (SkillItem) Fletching.getFletchItem(p);
		if (item == null || p == null) {
			return;
		}
		boolean stringing = item.getItemTwo() == XBOW_STRING ? true : false;
		if (!canFletch(p, item)) {
			p.getActionSender().closeInterfaces();
			return;
		}
		if (p.getInventory().deleteItem(item.getItemOne()) &&  p.getInventory().deleteItem(item.getItemTwo())) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(FLETCHING, item.getXp());
			item.decreaseAmount();
			p.getActionSender().closeInterfaces();
			if (!stringing) {
				p.getActionSender().sendMessage("You attach some limbs to the Crossbow.");
			} else {
				p.animate(6677);
				p.getActionSender().sendMessage("You add a Crossbow String to the Crossbow, you have completed the " + ItemDefinition.forId(item.getFinishedItem()).getName() + ".");
			}
		}
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {
				@Override
				public void execute() {
					createXbow(p, -1, -1, false, false);
					this.stop();
				}
			});
		}
	}

	private static boolean canFletch(Player p, SkillItem item) {
		if (item == null || item.getAmount() <= 0) {
			return false;
		}
		if (p.getLevels().getLevel(FLETCHING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Fletching level of " + item.getLevel() + " to make that.");
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemOne())) {
			p.getActionSender().sendMessage("You don't have a " + ItemDefinition.forId(item.getItemOne()).getName());
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemTwo())) {
			p.getActionSender().sendMessage("You don't have any " + ItemDefinition.forId(item.getItemTwo()).getName());
			return false;
		}
		return true;
	}

	private static SkillItem getXbow(int xbowType, boolean stringing, int amount) {
		int i = xbowType;
		if (!stringing) {
			return new SkillItem(UNFINISHED_XBOW[i], CROSSBOW_STOCK[0], XBOW_LIMB[i], XBOW_LVL[i], FLETCHING, XBOW_XP[i], amount);
		} else {
			return new SkillItem(FINISHED_XBOW[i], UNFINISHED_XBOW[i], XBOW_STRING, XBOW_LVL[i], FLETCHING, XBOW_XP[i], amount);
		}
	}		
}
