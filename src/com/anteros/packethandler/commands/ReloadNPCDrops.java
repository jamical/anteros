package com.anteros.packethandler.commands;

import java.io.FileNotFoundException;

import com.anteros.model.World;
import com.anteros.model.player.Player;

public class ReloadNPCDrops implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
			try {
				World.getInstance().reloadDrops();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public int minimumRightsNeeded() {
		return 2;
	}

}
