package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.model.player.Player;
import com.anteros.net.Packet;
import com.anteros.packethandler.commands.CommandManager;

/**
 * 
 * @author Luke132
 */
public class Command implements PacketHandler {

	@Override
	public void handlePacket(Player player, IoSession session, Packet packet) {
		String command = packet.readRS2String().toLowerCase();
		CommandManager.execute(player, command);
	}

}
