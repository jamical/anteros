package com.anteros.content.holiday;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class Easter {

	public static void interactWithBunny(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.easter2014 == 0) {
					showChats(p, 331);
				} else {
					showChats(p, 338);
				}
			}
		});
	}
	
	public static void showChats(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {
		case 331:
			p.getActionSender().sendNpcChat1("Happy Easter " + p.getUsername() + "!", 1835, "Easter Bunny", Dialogue.HAPPY_JOYFUL);
			newStatus = 332;
			break;
		case 332:
			p.getActionSender().sendOption2("Who are you?", "Right back at you bunny!");
			newStatus = 333;
			break;
		case 333:
			p.getActionSender().sendPlayerChat1("Who are you?", Dialogue.THINKING);
			newStatus = 334;
			break;
		case 334:
			p.getActionSender().sendNpcChat2("Why, I'm the Easter Bunny!", "Why don't you take this as an Easter gift.", 1835, "Easter Bunny", Dialogue.HAPPY_JOYFUL);
			newStatus = 335;
			break;
		case 335:
			if (p.getInventory().getTotalFreeSlots() != 0) {
				p.getActionSender().sendStatement("The Easter Bunny hands you a nicely wrapped package.");
				p.getInventory().addItem(6199);
				p.easter2014 = 1;
			} else {
				p.getActionSender().sendNpcChat2("Oh dear, why don't you clear a space so", "you can carry my gift", 1835, "Easter Bunny", Dialogue.HAPPY_JOYFUL);
			}
			newStatus = -1;
			break;
		case 336:
			p.getActionSender().sendPlayerChat1("Right back at you bunny!", Dialogue.HAPPY_JOYFUL);
			newStatus = 337;
			break;
		case 337:
			p.getActionSender().sendNpcChat2("That's the Easter spirit! Here, take this", "as an Easter gift from me to you!", 1835, "Easter Bunny", Dialogue.HAPPY_JOYFUL);
			newStatus = 335;
			break;
		case 338:
			p.getActionSender().sendNpcChat1("Happy Easter! I hope you're enjoying your gift!", 1835, "Easter Bunny", Dialogue.HAPPY_JOYFUL);
			newStatus = -1;
			break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void openBox(Player player) {
		if (player.getInventory().getTotalFreeSlots() > 1) { 
				player.getInventory().deleteItem(6199);
				player.getInventory().addItem(7927);
				player.getActionSender().sendMessage("Inside the box you find an Easter Ring!");
		}
	}

}
