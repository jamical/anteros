package com.anteros.content.minigames.fightcave;

import com.anteros.content.combat.Combat;
import com.anteros.content.skills.prayer.Prayer;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class FightCaveSession {
	
	private transient Player p;
	private byte currentWave;
	private transient byte mobAmount;
	private transient boolean completed;
	private transient boolean healersSpawned;
	private transient boolean gamePaused;

	public FightCaveSession(Player player) {
		this.p = player;
		this.currentWave = 0;
		this.mobAmount = 0;
		this.completed = false;
		this.healersSpawned = false;
		this.gamePaused = false;
		startGame();
	}

	private void startGame() {
		World.getInstance().registerEvent(new Event(3000) {

			@Override
			public void execute() {
				if (completed) {
					this.stop();
					return;
				}
				if (mobAmount > 0 || currentWave > 63) {
					return;
				}
				if (gamePaused && currentWave != 63) {
					this.stop();
					p.getActionSender().forceLogout();
					return;
				}
				if (currentWave == 62) {
					this.setTick(8000);
					currentWave++;
					showJadMessage();
					return;
				} else if (currentWave < 62){
					currentWave++;
				}
				int[] mobs = decryptWave(currentWave);
				int amount = 0;
				for (int i = 0; i < mobs.length; i++) {
					if (mobs[i] > 0) {
						NPC npc = new NPC(mobs[i]);
						Location minCoords = Location.location((20000 + 2363) + (200 * p.getIndex()), 25051, 0);
						Location maxCoords = Location.location((20000 + 2430) + (200 * p.getIndex()), 25123, 0);
						npc.readResolve();
						npc.setMinimumCoords(minCoords);
						npc.setMaximumCoords(maxCoords);
						npc.setLocation(Location.location((20000 + 2387) + (200 * p.getIndex()) + Misc.random(22), 20000 + 5069 + Misc.random(33), 0));
						npc.setEntityFocus(p.getClientIndex());
						npc.setOwner(p);
						npc.setTarget(p);
						npc.getFollow().setFollowing(p);
						World.getInstance().getNpcList().add(npc);
						amount++;
					}
				}
				mobAmount = (byte) amount;
			}	
		});
	}
	
	private void showJadMessage() {
		p.getActionSender().sendNPCHead(2617, 241, 1);
		p.getActionSender().modifyText("TzHaar-Mej-Jal", 241, 3);                    
		p.getActionSender().modifyText("Look out, here comes TzTok-Jad!", 241, 4);
		p.getActionSender().animateInterface(9827, 241, 1);
		p.getActionSender().sendChatboxInterface2(241);
	}

	public void teleFromCave(final boolean quit) {
		p.teleport(Location.location(2439, 5169, 0));
		World.getInstance().removeAllPlayersNPCs(p);
		World.getInstance().registerEvent(new Event(600) {
			@Override
			public void execute() {
				this.stop();
				String s = "You have defeated TzTok-Jad, I am most impressed!";
				String s1 = "Please accept this gift as a reward.";
				if (quit) {
					if (currentWave > 1) {
						s = "Well done in the cave, here, take TokKul as a reward.";
						s1 = null;
						p.getInventory().addItemOrGround(6529, getTokkulReward());
					} else {
						s = "Well I suppose you tried... better luck next time.";
						s1 = null;
					}
				} else {
					p.getInventory().addItemOrGround(6570);
					p.getInventory().addItemOrGround(6529, 16064);
				}
				if (s1 != null) {
					p.getActionSender().sendNPCHead(2617, 242, 1);
					p.getActionSender().modifyText("TzHaar-Mej-Jal", 242, 3);
					p.getActionSender().modifyText(s, 242, 4);
					p.getActionSender().modifyText(s1, 242, 5);
					p.getActionSender().animateInterface(9827, 242, 1);
					p.getActionSender().sendChatboxInterface2(242);
				} else {
					p.getActionSender().sendNPCHead(2617, 241, 1);
					p.getActionSender().modifyText("TzHaar-Mej-Jal", 241, 3);                    
					p.getActionSender().modifyText(s, 241, 4);
					p.getActionSender().animateInterface(9827, 241, 1);
					p.getActionSender().sendChatboxInterface2(241);
				}
				p.clearKillersHits();
				p.setLastAttackType(1);
				p.setLastAttack(0);
				p.setTarget(null);
				p.setAttacker(null);
				p.setHp(p.getMaxHp());
				p.getSettings().setSkullCycles(0);
				p.setEntityFocus(65535);
				p.getSpecialAttack().resetSpecial();
				p.getEquipment().setWeapon();
				p.setLastkiller(null);
				Combat.resetCombat(p, 1);
				p.setDead(false);
				p.getSettings().setLastVengeanceTime(0);
				p.getSettings().setVengeance(false);
				p.removeTemporaryAttribute("willDie");
				p.setFrozen(false);
				p.removeTemporaryAttribute("unmovable");
				p.getSettings().setAntifireCycles(0);
				p.getSettings().setSuperAntipoisonCycles(0);
				Prayer.deactivateAllPrayers(p);
				p.getSettings().setTeleblockTime(0);
				p.removeTemporaryAttribute("teleblocked");
				p.removeTemporaryAttribute("autoCastSpell");
				for (int i = 0; i < 24; i++) {
					p.getLevels().setLevel(i, p.getLevels().getLevelForXp(i));
				}
				p.getActionSender().sendSkillLevels();
				p.getSettings().setFightCave(null);
			}
		});
	}
	
	public void resumeGame() {
		gamePaused = false;
		Location instanceLocation = Location.location((20000 + 2413) + (200 * p.getIndex()), 20000 + 5116, 0);
		p.teleport(Location.location(instanceLocation));
		p.getActionSender().sendNPCHead(2617, 242, 1);
		p.getActionSender().modifyText("TzHaar-Mej-Jal", 242, 3);
		p.getActionSender().modifyText("Welcome back to the fight cave, JalYt.", 242, 4);
		p.getActionSender().modifyText("Pepare to fight for your life!", 242, 5);
		p.getActionSender().animateInterface(9827, 242, 1);
		p.getActionSender().sendChatboxInterface2(242);
		startGame();
	}

	private int getTokkulReward() {
		return (int) ((currentWave * 6) * (currentWave * 0.34));
	}

	public int[] decryptWave(int waveId) {
        int[] mobs = new int[6];
        int mobsAdded = 0;
        if(waveId/63 > 0) {
            for(int i = 0; i <= waveId/63; i++) {
                mobs[mobsAdded++] = 2745;
                waveId -=63;
            }
        }
        if(waveId/31 > 0) {
            for(int i = 0; i <= waveId/31; i++) {
                mobs[mobsAdded++] = 2743;//+Misc.random(1);
                waveId -=31;
            }        
        }
        if(waveId/15 > 0) {
            for(int i = 0; i <= waveId/15; i++) {
                mobs[mobsAdded++] = 2741;//+Misc.random(1);
                waveId -=15;
            }        
        }
        if(waveId/7 > 0) {
            for(int i = 0; i <= waveId/7; i++) {
                mobs[mobsAdded++] = 2739;//+Misc.random(1);
                waveId -=7;
            }        
        }
        if(waveId/3 > 0) {
            for(int i = 0; i <= waveId/3; i++) {
                mobs[mobsAdded++] = 2736;//+Misc.random(1);
                waveId -=3;
            }        
        }
        if(waveId > 0) {
            for(int i = 0; i <= waveId; i++) {
                mobs[mobsAdded++] = 2734;//+Misc.random(1);
                waveId--;
            }        
        }
        return mobs;
    }
    
	public byte getMobAmount() {
		return mobAmount;
	}
	
	public void decreaseMobAmount(boolean killedJad) {
		this.mobAmount--;
		if (killedJad) {
			completed = true;
			teleFromCave(false);
		}
	}
	
    public void setPlayer(Player p) {
		this.p = p;
	}

	public boolean healersSpawned() {
		return healersSpawned;
	}
	
	public void setHealersSpawned(boolean healersSpawned) {
		this.healersSpawned = healersSpawned;
	}
	
	public boolean isGamePaused() {
		return gamePaused;
	}

	public void setGamePaused(boolean gamePaused) {
		this.gamePaused = gamePaused;
	}
}
