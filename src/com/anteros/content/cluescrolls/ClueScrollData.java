package com.anteros.content.cluescrolls;

import com.anteros.model.player.Player;

public class ClueScrollData {
	
		protected static int easyClueNpcs[] = { //this is done
			1257, 1258, 1261, 1262, 2684, 18, 1612, 12, 3246, 3247, 3248, 3249, 3250, 3251, 
			3252, 3253, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 5909, 1831, 1832, 
			5750, 7159, 3237, 2454, 7, 1757, 1758, 100, 101, 102, 444, 445, 1769, 1770, 1771, 1772, 
			1774, 1775, 1776, 2274, 2275, 2276, 2277, 2278, 2279, 2280, 2281, 1710, 1711, 1712, 
			122, 123, 2685, 2686, 2687, 2688, 3583, 4898, 6275, 2683, 1088, 1089, 3406, 6217, 
			7714, 7715, 3240, 1, 2, 3, 7877, 7878, 7875, 4404, 4405, 4406, 5751, 175, 2697, 7161, 
			7162, 3238, 1265, 1267, 187, 3239, 8, 282, 2674, 2696, 5926, 5927, 5928, 5929, 186, 
			7106, 7107, 7108, 7109, 7110, 7111, 7112, 7113, 7114, 4, 5, 6, 
			
		};
		protected static int mediumClueNpcs[] = { //didnt do level 80+ skeletons, but it's done
			2263, 2265, 3276, 3277, 3278, 3279, 4397, 4398, 4399, 1620, 1621, 4227, 1338, 1339, 
			1340, 1341, 1342, 1343, 1344, 1345, 1346, 1347, 2455, 2456, 2887, 2888, 3591, 6761, 
			6762, 6763, 6380, 2452, 2885, 3943, 9, 32,206, 253, 254, 256, 257, 296, 297, 298, 299, 
			837, 838, 1076, 1077, 1317, 2236, 2571, 2699, 2700, 2701, 2702, 2703, 3228, 3229, 
			3230, 3231, 3232, 3233, 3241, 3407, 3408, 5920, 3153, 125, 145, 3073, 113, 6268, 
			4348, 4349, 4350, 4351, 4352, 1219, 490, 1958, 1959, 1961, 1962, 1963, 1964, 1965, 
			1966, 1967, 1968, 2015, 2016, 2017, 2018, 2019, 6753, 6754, 6755, 6756, 6757, 6758, 
			6759, 6760, 20, 365, 2256, 1633, 1634, 1635, 1636, 6216, 3939, 204, 191, 2496, 2497, 
			6103, 4774, 4775, 4776, 4777, 4782, 2457, 2884, 
		};
		protected static int hardClueNpcs[] = { //this is done
			1604, 1605, 1606, 1607, 7801, 7802, 7803, 7804, 1615, 4230, 4381, 4382, 4383, 4673, 
			4674, 4675, 4676, 54, 1618, 1619, 6215, 4681, 4682, 4683, 4684, 5178, 55, 1590, 
			5362, 4353, 4354, 4355, 4356, 4357, 3200, 6269, 6270, 116, 4291, 4292, 6078, 6079, 
			6080, 6081, 2882, 2883, 2881, 2783, 1183, 1184, 1201, 2359, 2360, 2361, 2362, 7438, 
			7439, 7440, 7441, 5993, 1610, 1827, 6389, 3340, 4418, 6218, 83, 4698, 4699, 4700, 
			4701, 941, 4677, 4678, 4679, 4680, 49, 3586, 6210, 1591, 1637, 1638, 1639, 1640, 
			1641, 1642, 1158, 1159, 1160, 3835, 3836, 4234, 2642, 50, 4229, 7811, 1608, 1609, 
			5363, 1613, 53, 4669, 4670, 4671, 4672, 3068, 3069, 3070, 3071, 1592, 3590, 4527, 
			4528, 4529, 4530, 4531, 4532, 4533, 5417, 5418, 7814, 1622, 1623, 1626, 1627, 1628, 
			1629, 1630, 1200, 1203, 1204, 1206, 4649, 7120, 5361,  
		};
		protected static int JunkReward[] = {
			1077, 1125, 1165, 1195, 1297, 1367, 853, 7390,
			7392, 7394, 7396, 7386, 7388, 1099, 1135, 1065, 851
		};
		protected static int UniversalRewards[] = {
			3827, 3828, 3829, 3830, 3831, 3832, 3833, 3834, 3835, 3836, 3837, 3838,
			7329, 7330, 7331, 10326, 10327, 10476, 1727, 1729, 1725, 1731, 1704,
			1694, 1696, 1698, 1700, 1702, 10280, 10282, 10284 
		};
	 	protected static int EasyStackables[] = {
	 		995, 380, 561, 886, 
	 	};
	 	protected static int MediumStackables[] = {
	 		995, 374, 561, 563, 890,
	 	};
	 	protected static int HardStackables[] = {
	 		995, 386, 561, 563, 560, 892
	 	};
	 	protected static int EasyReward[] = { 
	 	    2583, 2585, 2587, 2589, 3472, 2591, 2593, 2595, 2597, 3473,  7392, 7396, 7390, 7394, 
	 	    2631, 2633, 2635, 2637, 7362, 7364, 7366, 7368, 7332, 10306, 7338, 10308, 7344, 10314, 
	 	    7350, 10320, 7356, 10326, 10400, 10402, 10420, 10421, 10404, 10406, 10424, 10426, 10412, 
	 	    10414, 10432, 10434, 10408, 10410, 10428, 10430, 10416, 10418, 10436, 10438, 10316, 10318, 
	 	    10320, 10322, 10324, 10394, 10398, 10392, 10396, 10452, 10446, 10458, 10464, 10470, 10450, 
	 	    10456, 10460, 10468, 10474, 10448, 10454, 10462, 10466, 10472, 
	 	};
	 	protected static int MediumReward[] = { 
	 		2599, 2601, 2603, 2605, 2607, 2609, 2611, 2613,
	 	    7334, 7340, 7346, 7352, 7358, 7319, 7321, 7323, 7325, 7327, 7372,
	 	    7370, 7380, 7378, 2645, 2647, 2649, 2577, 2579, 1073, 1091, 1099,
	 	    1111, 1135, 1124, 1145, 1161, 1169, 1183, 1199, 1211, 1245, 1271,
	 	    1287, 1301, 1317, 1332, 1357, 1371, 1430, 6916, 6918, 6920, 6922,
	 	    6924, 10400, 10402, 10416, 10418, 10420, 10422, 10436, 10438,
	 	    10446, 10448, 10450, 10452, 10454, 10456, 6889
	 	};
	 	protected static int HardReward[] = { 
			1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201,
			1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861, 859,
			2581, 2577, 2651, 1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201, 1275,
			1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861, 859, 2581, 2577,
			2651, 2615, 2617, 2619, 2621, 2623, 2625, 2627, 2629, 2639, 2641,
			2643, 2651, 2653, 2655, 2657, 2659, 2661, 2663, 2665, 2667, 2669,
			2671, 2673, 2675, 7342, 7348, 7454, 7460, 7374, 7376, 7382,
			7384, 7398, 7399, 7400, 3481, 3483, 3485, 3486, 3488, 1079, 1093,
			1113, 1127, 1148, 1164, 1185, 1201, 1213, 1247, 1275, 1289, 1303,
			1319, 1333, 1347, 1359, 1374, 1432, 2615, 2617, 2619, 2621, 2623,
			10330, 10338, 10348, 10332, 10340, 10346, 10334, 10342, 10350,
			10336, 10344, 10352, 10368, 10376, 10384, 10370, 10378, 10386,
			10372, 10380, 10388, 10374, 10382, 10390, 10470, 10472, 10474,
			10440, 10442, 10444, 6914, 1050, 1038, 1040, 1042, 1044, 1046, 1048
	 	};
	    public static int AnagramClue[] = {
	    	2682, 2683, 2684, 2685, 2686,
	    };
	    public static int AnagramNpcs[] = {
	    	962, 1070, 171, 550, 469,
	    };
	    public static int CoordinateClue[] = {
	    	2687, 2688, 2689, 2690, 2691
	    };
	    public static int CrypticClue[] = {
	    	-1
	    };
	    public static int EmoteClue[] = {
	    	-1
	    };
	    public static int SimpleClue[] = {
	    	-1
	    };
	    public static int MapClue[] = {
	    	2677, 2678, 2679, 2680, 2681, //Easy
	    	2692, 2693, 2694, 2695, 2696, //Medium
	    };
	    public static int PuzzleBox[] = {
	    	-1
	    };
	    public static int EasyClue[] = { //Add ALL easy clue ids here
	    	2677, 2678, 2679, 2680, 2681, //Maps
	    };
	    public static int MediumClue[] = { //Add ALL medium clue ids here
	    	2682, 2683, 2684, 2685, 2686, //Anagrams
	    	2692, 2693, 2694, 2695, 2696, //Maps
	    };
	    public static int HardClue[] = { //Add ALL hard clue ids here
	    	//2687, 2688, 2689, 2690, 2691, //Coordinates
	    	2697, 2698, 2699, 2700, 2701, //Maps
	    };

	    public static int RandomEasyClue() {
	    	return EasyClue[(int)(Math.random()*EasyClue.length)];
	    }
	    
	    public static int RandomMediumClue() {
	    	return MediumClue[(int)(Math.random()*MediumClue.length)];
	    }
	    
	    public static int RandomHardClue() {
	    	return HardClue[(int)(Math.random()*HardClue.length)];
	    }
	    
	    public static void setCurrentClue(Player p, int itemId) { //done
	    	for (int i = 0; i < EasyClue.length; i++) {
	    		if (itemId == EasyClue[i]) {
	    			p.clueDifficulty = 1;
	    		}
	    	}
	    	for (int i = 0; i < MediumClue.length; i++) {
	    		if (itemId == MediumClue[i]) {
	    			p.clueDifficulty = 2;
	    		}
	    	}
	    	for (int i = 0; i < HardClue.length; i++) {
	    		if (itemId == HardClue[i]) {
	    			p.clueDifficulty = 3;
	    		}
	    	}
	    }
	    
	    private static void showInterface(Player p, int itemId) {
	    	for (int i = 0; i < AnagramClue.length; i++) {
	    		if (itemId == AnagramClue[i]) {
	    			ClueScrollChecks.showAnagramClue(p, itemId);
	    		}
	    	}
	    	for (int i = 0; i < CoordinateClue.length; i++) {
	    		if (itemId == CoordinateClue[i]) {
	    			ClueScrollChecks.showCoordinateClue(p, itemId);
	    		}
	    	}
	    	for (int i = 0; i < CrypticClue.length; i++) {
	    		if (itemId == CrypticClue[i]) {
	    			ClueScrollChecks.showCrypticClue(p, itemId);
	    		}
	    	}
	    	for (int i = 0; i < EmoteClue.length; i++) {
	    		if (itemId == EmoteClue[i]) {
	    			ClueScrollChecks.showEmoteClue(p, itemId);
	    		}
	    	}
	    	for (int i = 0; i < SimpleClue.length; i++) {
	    		if (itemId == SimpleClue[i]) {
	    			ClueScrollChecks.showSimpleClue(p, itemId);
	    		}
	    	}
	    	for (int i = 0; i < MapClue.length; i++) {
	    		if (itemId == MapClue[i]) {
	    			ClueScrollChecks.showMapClue(p, itemId);
	    		}
	    	}
	    }
	    
	    public static boolean isClue(Player p, int itemId) {
	    	if (isEasyClue(itemId) || isMediumClue(itemId) || isHardClue(itemId)) {
	    		showInterface(p, itemId);
	    		return true;
	    	}
	    	return false;
	    }
	    
	    public static boolean isEasyClue(int itemId) {
	    	for (int i = 0; i < EasyClue.length; i++) {
	    		if (itemId == EasyClue[i]) {
	    			return true;
	    		}
	    	}
	    	return false;
	    }
	    
	    public static boolean isMediumClue(int itemId) {
	    	for (int i = 0; i < MediumClue.length; i++) {
	    		if (itemId == MediumClue[i]) {
	    			return true;
	    		}
	    	}
	    	return false;
	    }
	    
	    public static boolean isHardClue(int itemId) {
	    	for (int i = 0; i < HardClue.length; i++) {
	    		if (itemId == HardClue[i]) {
	    			return true;
	    		}
	    	}
	    	return false;
	    }
}
