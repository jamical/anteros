package com.anteros.io;

import com.anteros.model.player.Player;
import com.anteros.model.player.PlayerDetails;

/**
 * Player load/save interface.
 * @author Graham
 *
 */
public interface PlayerLoader {
	
	public PlayerLoadResult load(PlayerDetails p);
	public boolean save(Player p);

}
