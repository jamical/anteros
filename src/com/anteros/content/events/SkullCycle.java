package com.anteros.content.events;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class SkullCycle extends Event {

	public SkullCycle() {
		super(60000);
	}

	@Override
	public void execute() {
		for (Player p : World.getInstance().getPlayerList()) {
			if (p != null) {
				if (p.getSettings().isSkulled() && !p.isDead()) {
					p.getSettings().setSkullCycles(p.getSettings().getSkullCycles() - 1);
				}
			}
		}
	}

}
