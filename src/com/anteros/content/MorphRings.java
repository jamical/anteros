package com.anteros.content;

import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class MorphRings {
	
	public static void easterRing(Player player) {
		int randomEgg = Misc.random(5);
			player.getWalkingQueue().reset();
			player.setMorphed(true);
			player.getActionSender().sendMessage("As you put on the ring you turn into an egg!");
			player.getAppearance().setNpcId(3689 + randomEgg);
			player.getUpdateFlags().setAppearanceUpdateRequired(true);
	}
	
	public static void unequipEaster(Player player) {
		player.setMorphed(false);
		player.getAppearance().setNpcId(-1);
		player.getUpdateFlags().setAppearanceUpdateRequired(true);
	}

}
