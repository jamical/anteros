package com.anteros.content.skills.agility;

import java.util.ArrayList;
import java.util.List;

import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceMovement;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class GnomeCourse extends AgilityData {

	private static List<NPC> shoutNPCs = new ArrayList<NPC>();
	
	private static final String[] SHOUT_MESSAGES = {
		"Okay get over that log, quick quick!",
		"Move it, move it, move it!",
		"That's it - straight up!",
		"Come on scaredy cat, get across that rope!",
		"My Granny can move faster than you!",
		
	};
	
	public GnomeCourse() {

	}

	static {
		shoutNPCs = new ArrayList<NPC>();
		shoutNPCs.add(0, World.getInstance().getNpcList().get(21));
		shoutNPCs.add(1, World.getInstance().getNpcList().get(22));
		shoutNPCs.add(2, World.getInstance().getNpcList().get(28));
		shoutNPCs.add(3, World.getInstance().getNpcList().get(29));
		shoutNPCs.add(4, World.getInstance().getNpcList().get(26));
	}

	@SuppressWarnings("unused")
	public static void doCourse(final Player p, final int objectX, final int objectY, final Object[] objectArray) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		int agilityStage = (Integer) (p.getTemporaryAttribute("agilityStage") == null ? 0 : p.getTemporaryAttribute("agilityStage"));
		switch((Integer)objectArray[0]) {
			case 2295: // Log
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location((Integer)objectArray[3], (Integer)objectArray[4], 0)) {
	
					@Override
					public void run() {
						shoutNPCs.get(0).setForceText(new ForceText(SHOUT_MESSAGES[0]));
						p.getActionSender().sendMessage("You walk carefully across the slippery log...");
						final boolean running = p.getWalkingQueue().isRunToggled();
						p.getWalkingQueue().setRunToggled(false);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setTemporaryAttribute("unmovable", true);
						p.getAppearance().setWalkAnimation(155);
						p.getUpdateFlags().setAppearanceUpdateRequired(true);
						p.getWalkingQueue().forceWalk(0, -7);
						World.getInstance().registerEvent(new Event(4300) {
	
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("...and make it safely to the other side.");
								p.removeTemporaryAttribute("unmovable");
								p.getAppearance().setWalkAnimation(-1);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
								p.getWalkingQueue().setRunToggled(running);
							}
						});	
					}
				});
				break;
	
			case 2285: // Net #1
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2471, 3426, 2476, 3426) {
					@Override
					public void run() {
						shoutNPCs.get(1).setForceText(new ForceText(SHOUT_MESSAGES[1]));
						p.getActionSender().sendMessage("You climb the netting...");
						p.animate(828);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY()-1, 0));
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1000) {
	
							@Override
							public void execute() {
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(2473, p.getLocation().getY()-2, 1));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
	
			case 35970: // Tree climb
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2472, 3422, 2474, 3423) {
	
					@Override
					public void run() {
						shoutNPCs.get(2).setForceText(new ForceText(SHOUT_MESSAGES[2]));
						p.getActionSender().sendMessage("You climb the tree...");
						p.animate(828);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setFaceLocation(Location.location(2473, 3422, 1));
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1000) {
	
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("...to the platform above.");
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(2473, 3420, 2));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
	
			case 2312: // Rope balance
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location((Integer)objectArray[3], (Integer)objectArray[4], 2)) {
	
					@Override
					public void run() {
						shoutNPCs.get(3).setForceText(new ForceText(SHOUT_MESSAGES[3]));
						p.getActionSender().sendMessage("You carefully cross the tightrope.");
						final boolean running = p.getWalkingQueue().isRunToggled();
						p.getWalkingQueue().setRunToggled(false);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setTemporaryAttribute("unmovable", true);
						p.getAppearance().setWalkAnimation(155);
						p.getUpdateFlags().setAppearanceUpdateRequired(true);
						p.getWalkingQueue().forceWalk(+6, 0);
						World.getInstance().registerEvent(new Event(3700) {
	
							@Override
							public void execute() {
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.getAppearance().setWalkAnimation(-1);
								p.getUpdateFlags().setAppearanceUpdateRequired(true);
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
								p.getWalkingQueue().setRunToggled(running);
							}
	
						});	
					}
				});
				break;
	
			case 2314: // Tree climb #2
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2485, 3418, 2486, 3420) {
	
					@Override
					public void run() {
						p.getActionSender().sendMessage("You climb down the tree...");
						p.animate(828);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setFaceLocation(Location.location(2486, 3419, 2));
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1000) {
	
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("You land on the ground.");
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(2486, 3420, 0));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
	
			case 2286: // Net #2
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2483, 3425, 2488, 3425) {
					@Override
					public void run() {
						shoutNPCs.get(4).setForceText(new ForceText(SHOUT_MESSAGES[4]));
						p.getActionSender().sendMessage("You climb the netting...");
						p.animate(828);
						p.getWalkingQueue().reset();
						p.getActionSender().clearMapFlag();
						p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY()+1, 0));
						p.setTemporaryAttribute("unmovable", true);
						World.getInstance().registerEvent(new Event(1000) {
	
							@Override
							public void execute() {
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY()+2, 0));
								p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
							}		
						});
					}
				});
				break;
	
			case 4058: // Right obstacle pipe
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2486, 3430, 2488, 3431) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX + 1 && pY == objectY) {
							newMove = 1;
						} else if (pX == objectX - 1 && pY == objectY) {
							newMove = 2;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (i == 0) {
										p.getWalkingQueue().forceWalk(0, -1);
									} else if (i == 1) {
										p.getWalkingQueue().forceWalk(path == 1 ? -1 : +1, 0);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("You squeeze into the pipe...");
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY + 4, 10, 60, 0);
								p.setForceMovement(movement);		
								p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() + 1, 0));
								p.animate(10578);
								final boolean running = p.getWalkingQueue().isRunToggled();
								p.getWalkingQueue().setRunToggled(false);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								World.getInstance().registerEvent(new Event(1150) {
									int i = 0;
									@Override
									public void execute() {
										if (i == 0) {
											p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() + 3, 0));
											this.setTick(450);
											i++;
											return;
										} else if (i == 1){
											ForceMovement movement = new ForceMovement(lX, lY + 3, lX, lY + 6, 10, 60, 0);
											p.setForceMovement(movement);
											this.setTick(900);
											i++;
										} else if (i == 2) {
											this.setTick(500);
											p.animate(10579);
											ForceMovement movement = new ForceMovement(lX, lY + 6, lX, lY + 7, 10, 40, 0);
											p.setForceMovement(movement);
											i++;
										} else {
											p.getActionSender().sendMessage("...You squeeze out of the pipe.");
											this.stop();
											p.removeTemporaryAttribute("unmovable");
											p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
											p.getWalkingQueue().setRunToggled(running);
											p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() + 5, 0));
										}
									}					
								});	
							}
						});
					}
				});
				break;
				
			case 154: // Left obstacle pipe
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2483, 3430, 2485, 3431) {
					@Override
					public void run() {
						int newMove = 0;
						int pX = p.getLocation().getX();
						int pY = p.getLocation().getY();
						if (pX == objectX + 1 && pY == objectY) {
							newMove = 1;
						} else if (pX == objectX - 1 && pY == objectY) {
							newMove = 2;
						} else if (pX == 2483) {
							newMove = 3;
						} else if (pX == 2485) {
							newMove = 4;
						}
						if (newMove > 0) {
							final int path = newMove;
							World.getInstance().registerEvent(new Event(500) {
								int i = 0;
								@Override
								public void execute() {
									if (path == 3 || path == 4) {
										p.getWalkingQueue().forceWalk(path == 3 ? +1 : -1, 0);
										i = 2;
									}
									if (i == 0) {
										p.getWalkingQueue().forceWalk(0, -1);
									} else if (i == 1) {
										p.getWalkingQueue().forceWalk(path == 1 ? -1 : +1, 0);
									} else {
										doCourse(p, objectX, objectY, objectArray);
										this.stop();
									}
									i++;
								}		
							});
							return;
						}
						World.getInstance().registerEvent(new Event(0) {
							@Override
							public void execute() {
								this.stop();
								p.getActionSender().sendMessage("You squeeze into the pipe...");
								int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
								int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
								final int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
								final int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
								ForceMovement movement = new ForceMovement(lX, lY, lX, lY + 3, 10, 60, 0);
								p.setForceMovement(movement);		
								p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() + 1, 0));
								p.animate(10578);
								final boolean running = p.getWalkingQueue().isRunToggled();
								p.getWalkingQueue().setRunToggled(false);
								p.getWalkingQueue().reset();
								p.getActionSender().clearMapFlag();
								p.setTemporaryAttribute("unmovable", true);
								World.getInstance().registerEvent(new Event(1150) {
									int i = 0;
									@Override
									public void execute() {
										if (i == 0) {
											p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() + 3, 0));
											this.setTick(450);
											i++;
											return;
										} else if (i == 1){
											ForceMovement movement = new ForceMovement(lX, lY + 3, lX, lY + 6, 10, 60, 0);
											p.setForceMovement(movement);
											this.setTick(900);
											i++;
										} else if (i == 2) {
											this.setTick(500);
											p.animate(10579);
											ForceMovement movement = new ForceMovement(lX, lY + 6, lX, lY + 7, 10, 40, 0);
											p.setForceMovement(movement);
											i++;
										} else {
											p.getActionSender().sendMessage("...You squeeze out of the pipe.");
											this.stop();
											p.removeTemporaryAttribute("unmovable");
											p.getLevels().addXp(AGILITY, (Double)objectArray[7]);
											p.getWalkingQueue().setRunToggled(running);
											p.teleport(Location.location(p.getLocation().getX(), p.getLocation().getY() + 5, 0));
										}
									}	
								});	
							}
						});
					}
				});
				break;
		}
	}
}
