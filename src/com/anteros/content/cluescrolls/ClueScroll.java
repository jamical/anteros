package com.anteros.content.cluescrolls;

import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;
import com.anteros.world.GroundItem;

public class ClueScroll extends ClueScrollData {
	
	Player p;

    public static int[] clue = new int[6];
    public static int[] clueN = new int[6];
	
	public ClueScroll() {
		for (int i = 0; i < clue.length; i++) {
			clue[i] = -1;
		}
	}
	
	public static void canNpcDropClue(Player p, int npcId, Location location) { //done
		for (int i = 0; i < easyClueNpcs.length; i++) {
			if (npcId == easyClueNpcs[i]) {
				GroundItem item = new GroundItem(RandomEasyClue(), 1, location, p);
				World.getInstance().getGroundItems().newEntityDrop(item);
			}
		}
		for (int i = 0; i < mediumClueNpcs.length; i++) {
			if (npcId == mediumClueNpcs[i]) {
				GroundItem item = new GroundItem(RandomMediumClue(), 1, location, p);
				World.getInstance().getGroundItems().newEntityDrop(item);
			}
		}
		for (int i = 0; i < hardClueNpcs.length; i++) {
			if (npcId == hardClueNpcs[i]) {
				GroundItem item = new GroundItem(RandomHardClue(), 1, location, p);
				World.getInstance().getGroundItems().newEntityDrop(item);
			}
		} 
	}
    
    public static void foundClue(Player p) { //done
        p.getActionSender().sendChatboxInterface(241);
        p.getActionSender().itemOnInterface(241, 2, 250, 2678);
        p.getActionSender().modifyText("You found another clue scroll!", 241, 4);
      }

    public static void foundCasket(Player p) { //done
    	p.getActionSender().sendChatboxInterface(241);
    	p.getActionSender().itemOnInterface(41, 2, 250, 405);
    	p.getActionSender().modifyText("You found a casket!", 241, 4);
    }
    
    public static void npcGivesCasket(Player p, NPC npc) { //done
    	p.getActionSender().sendChatboxInterface(241);
    	p.getActionSender().itemOnInterface(41, 2, 250, 405);
    	p.getActionSender().modifyText(npc.getDefinition().getName() + " has given you a casket!", 241, 4);
    }
    
    public static void clueDig(Player p) { //done
    	int random = Misc.random(5);
    	if (p.clueDifficulty == 1) {
            if (random == 4) {
                p.getInventory().addItem(405, 1);
                foundCasket(p);
             } else {
                foundClue(p);
                p.getInventory().addItem(RandomEasyClue(), 1);
           }
    	} else if (p.clueDifficulty == 2) {
            if (random == 4) {
                p.getInventory().addItem(405, 1);
                foundCasket(p);
             } else {
                foundClue(p);
                p.getInventory().addItem(RandomMediumClue(), 1); 
           }
    	} else if (p.clueDifficulty == 3) {
            if (random == 4) {
                p.getInventory().addItem(405, 1);
                foundCasket(p);
             } else {
                foundClue(p);
                p.getInventory().addItem(RandomHardClue(), 1); 
           }
    	} 
      } 
    
    public static boolean playerHasClueScroll(Player p) { //done
		for (int i = 0; i < ClueScrollData.EasyClue.length; i ++) {
			if (p.getInventory().hasItem(ClueScrollData.EasyClue[i]) || 
				p.getInventory().hasItem(ClueScrollData.MediumClue[i]) ||
				p.getInventory().hasItem(ClueScrollData.HardClue[i]) ||
				p.getBank().hasItem(ClueScrollData.EasyClue[i]) ||
				p.getBank().hasItem(ClueScrollData.MediumClue[i]) ||
				p.getBank().hasItem(ClueScrollData.HardClue[i])) {
				return true;
			}
		}
		return false;
    }
	
	public static void openCasket(Player p, int slot) { //done
        if (Misc.random(5) == 1) {
        	if (p.getInventory().getTotalFreeSlots() < 1) {
        		return;
        	}
            foundClue(p);
            if (p.clueDifficulty == 1) {
                p.getInventory().addItem(RandomEasyClue(), 1);
            } else if (p.clueDifficulty == 2) {
                p.getInventory().addItem(RandomMediumClue(), 1);
            } else if (p.clueDifficulty == 3) {
                p.getInventory().addItem(RandomHardClue(), 1);
            }
            p.getInventory().deleteItem(405, slot, 1);
            } else {
                if (p.clueDifficulty == 1) {
                    easyReward(p, slot);
                } else if (p.clueDifficulty == 2) {
                	mediumReward(p, slot);
                } else if (p.clueDifficulty == 3) {
                	hardReward(p, slot);
                }
           p.getActionSender().sendMessage("You open the casket, and find...");
         }
	}
	
	public static void useSpade(Player p) { //done
        if (p.currentClue > 0) {
            ClueScrollChecks.checkClueLocation(p);
        } 
	}
	
	public static void sendClue(Player p) { //done
   		p.getActionSender().displayInterface(364);
   		Object[] invparams = new Object[] {"","","","","","","","","", -1, 0, 4, 3, 90, 364 << 16 | 4};
   		p.getActionSender().sendClientScript(150, invparams, "IviiiIsssssssss");
   		p.getActionSender().setItems(p, -1, 2, 90, clue, clueN);
   		p.getActionSender().setItems(p, -2, 60981, 90, clue, clueN);
	}

    public static void easyReward(Player p, int slot) { //done
    	int amount = Misc.random(1);
    	int amount1 = Misc.random(1000);
    	int amount2 = Misc.random(1);
    	int amount3 = Misc.random(1);
    	int amount4 = Misc.random(1);
    	int amount5 = Misc.random(1);
    	if (p.getInventory().getTotalFreeSlots() < amount + amount2 + amount3 + amount4 + amount5 + 1) {
    		return;
    	}
        int i0 = EasyReward[(int)(Math.random()*EasyReward.length)];
        int i1 = EasyStackables[(int)(Math.random()*EasyStackables.length)];
        int i2 = UniversalRewards[(int)(Math.random()*UniversalRewards.length)];
        int i3 = JunkReward[(int)(Math.random()*JunkReward.length)];
        int i4 = EasyReward[(int)(Math.random()*EasyReward.length)];
        int i5 = JunkReward[(int)(Math.random()*JunkReward.length)];
        p.getInventory().deleteItem(405, slot, 1);
        p.getInventory().addItem(i0, amount);
        p.getInventory().addItem(i1, amount1);
        p.getInventory().addItem(i2, amount2);
        p.getInventory().addItem(i3, amount3);
        p.getInventory().addItem(i4, amount4);
        p.getInventory().addItem(i5, amount5);
        if (amount != 0) {
            clue[0] = i0;
            clueN[0] = 1;
        }
        if (amount1 != 0) {
            clue[1] = i1;
            clueN[1] = amount1;
        }
        if (amount2 != 0) {
            clue[2] = i2;
            clueN[2] = 1;
        }
        if (amount3 != 0) {
            clue[3] = i3;
            clueN[3] = 1;
        }
        if (amount4 != 0) {
            clue[4] = i4;
            clueN[4] = 1;
        }
        if (amount5 != 0) {
            clue[5] = i5;
            clueN[5] = 1;
        }
       for(int i = 0; i < 6; i++) {
    	   if (clue[i] == 0) {
    			clue[i] = -1;
    	   }
       }
       sendClue(p);
       for(int i = 0; i < 6; i++) {
		clueN[i] = 1;
       }
    }
    
    public static void mediumReward(Player p, int slot) { //done
    	int amount = Misc.random(1);
    	int amount1 = Misc.random(1000);
    	int amount2 = Misc.random(1);
    	int amount3 = Misc.random(1);
    	int amount4 = Misc.random(1);
    	int amount5 = Misc.random(1);
    	if (p.getInventory().getTotalFreeSlots() < amount + amount2 + amount3 + amount4 + amount5 + 1) {
    		return;
    	}
        int i0 = MediumReward[(int)(Math.random()*MediumReward.length)];
        int i1 = MediumStackables[(int)(Math.random()*MediumStackables.length)];
        int i2 = UniversalRewards[(int)(Math.random()*UniversalRewards.length)];
        int i3 = JunkReward[(int)(Math.random()*JunkReward.length)];
        int i4 = MediumReward[(int)(Math.random()*MediumReward.length)];
        int i5 = JunkReward[(int)(Math.random()*JunkReward.length)];
        p.getInventory().deleteItem(405, slot, 1);
        p.getInventory().addItem(i0, amount);
        p.getInventory().addItem(i1, amount1);
        p.getInventory().addItem(i2, amount2);
        p.getInventory().addItem(i3, amount3);
        p.getInventory().addItem(i4, amount4);
        p.getInventory().addItem(i5, amount5);
        if (amount != 0) {
            clue[0] = i0;
            clueN[0] = 1;
        }
        if (amount1 != 0) {
            clue[1] = i1;
            clueN[1] = amount1;
        }
        if (amount2 != 0) {
            clue[2] = i2;
            clueN[2] = 1;
        }
        if (amount3 != 0) {
            clue[3] = i3;
            clueN[3] = 1;
        }
        if (amount4 != 0) {
            clue[4] = i4;
            clueN[4] = 1;
        }
        if (amount5 != 0) {
            clue[5] = i5;
            clueN[5] = 1;
        }
       for(int i = 0; i < 6; i++) {
    	   if (clue[i] == 0) {
    			clue[i] = -1;
    	   }
       }
       sendClue(p);
       for(int i = 0; i < 6; i++) {
		clueN[i] = 1;
       }
    }
    
    public static void hardReward(Player p, int slot) { //done
    	int amount = Misc.random(1);
    	int amount1 = Misc.random(1000);
    	int amount2 = Misc.random(1);
    	int amount3 = Misc.random(1);
    	int amount4 = Misc.random(1);
    	int amount5 = Misc.random(1);
    	if (p.getInventory().getTotalFreeSlots() < amount + amount2 + amount3 + amount4 + amount5 + 1) {
    		return;
    	}
        int i0 = HardReward[(int)(Math.random()*HardReward.length)];
        int i1 = HardStackables[(int)(Math.random()*HardStackables.length)];
        int i2 = UniversalRewards[(int)(Math.random()*UniversalRewards.length)];
        int i3 = JunkReward[(int)(Math.random()*JunkReward.length)];
        int i4 = HardReward[(int)(Math.random()*HardReward.length)];
        int i5 = JunkReward[(int)(Math.random()*JunkReward.length)];
        p.getInventory().deleteItem(405, slot, 1);
        p.getInventory().addItem(i0, amount);
        p.getInventory().addItem(i1, amount1);
        p.getInventory().addItem(i2, amount2);
        p.getInventory().addItem(i3, amount3);
        p.getInventory().addItem(i4, amount4);
        p.getInventory().addItem(i5, amount5);
        if (amount != 0) {
            clue[0] = i0;
            clueN[0] = 1;
        }
        if (amount1 != 0) {
            clue[1] = i1;
            clueN[1] = amount1;
        }
        if (amount2 != 0) {
            clue[2] = i2;
            clueN[2] = 1;
        }
        if (amount3 != 0) {
            clue[3] = i3;
            clueN[3] = 1;
        }
        if (amount4 != 0) {
            clue[4] = i4;
            clueN[4] = 1;
        }
        if (amount5 != 0) {
            clue[5] = i5;
            clueN[5] = 1;
        }
       for(int i = 0; i < 6; i++) {
    	   if (clue[i] == 0) {
    			clue[i] = -1;
    	   }
       }
       sendClue(p);
       for(int i = 0; i < 6; i++) {
		clueN[i] = 1;
       }
    }
}
