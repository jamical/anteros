package com.anteros.content;

import com.anteros.content.minigames.fightcave.FightCave;
import com.anteros.content.skills.magic.Teleport;
import com.anteros.event.Event;
import com.anteros.model.Item;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.util.Area;

public class JewelleryTeleport {

	public JewelleryTeleport() {
		
	}

	private static final String[][] LOCATIONS = {
		// Glory amulet
		{"Edgeville", "Karamja", "Draynor Village", "Al Kharid", "Nowhere"},
		// Trimmed Glory amulet
		{"Edgeville", "Karamja", "Draynor Village", "Al Kharid", "Nowhere"},
		// Ring of dueling
		{"Duel Arena", "Castle Wars", "Nowhere"},
		// Games amulet
		{"Burthorpe Games Room", "Pest Control", "Clan Wars", "Wilderness Volcano", "Nowhere"},
	};
	
	private static final int[][] COORDS_X = {
		// Glory amulet
		{3087, // Edgeville
		2918, // Karamja
		3105, // Draynor
		3293, // Al Kharid
		},
		// Trimmed glory amulet
		{3087, // Edgeville
		2918, // Karamja
		3105, // Draynor
		3293, // Al Kharid
		},
		//Duel ring
		{3316, // Duel arena
		2442, // Castle wars
		},
		//Games amulet
		{2207, // Games room
		2658, // Pest control
		3268, // Clan wars
		3154, // Bounty hunter
		}
	};
	
	private static final int[][] COORDS_Y = {
		// Glory amulet
		{3496, // Edgeville	
		3176, // Karamja
		3251, // Draynor
		3163, // Al Kharid
		},
		// Trimmed glory amulet
		{3496, // Edgeville	
		3176, // Karamja
		3251, // Draynor
		3163, // Al Kharid
		},
		//Duel ring
		{3235, // Duel arena
		3092, // Castle wars
		},
		//Games amulet
		{4940, // Games room
		2661, // Pest control
		3682, // Clan wars
		3663, // Bounty hunter
		}
	};

	private static final int[][] JEWELLERY = {
		{1704, 1706, 1708, 1710, 1712}, // Glory amulets
		{10362, 10360, 10358, 10356, 10354}, // Trimmed glory amulets
		{2566, 2564, 2562, 2560, 2558, 2556, 2554, 2552}, // Duel rings
		{3867, 3865, 3863, 3861, 3859, 3857, 3855, 3853}, // Games amulets
	};
	
	public static boolean useJewellery(Player p, int item, int slot, boolean wearingItem) {
		if (item == 1704 || item == 10362) { // Blank amulets
			p.getActionSender().sendMessage("This amulet has no charges remaining.");
			return true;
		}
		if (p.getTemporaryAttribute("unmovable") != null || p.getTemporaryAttribute("cantDoAnything") != null) {
			return true;
		}
		int index = getItemIndex(item);
		if (index == -1) {
			return false;
		}
		String s = index == 2 ? "ring" : "amulet";
		p.getActionSender().sendMessage("You rub the " + s + "...");
		p.getActionSender().closeInterfaces();
		int interfaceId = index == 2 ? 230 : 235; 
		int j = 2;
		p.getActionSender().modifyText("Teleport to where?", interfaceId, 1);
		for (int i = 0; i < LOCATIONS[index].length; i++) {
			p.getActionSender().modifyText(LOCATIONS[index][i], interfaceId, (i + j));
		}
		if (index == 2) {
			p.getActionSender().sendChatboxInterface2(interfaceId);
		} else {
			p.getActionSender().sendChatboxInterface2(interfaceId);
		}
		JewellerySlot js = new JewelleryTeleport.JewellerySlot(index, slot, wearingItem);
		p.setTemporaryAttribute("jewelleryTeleport", js);
		return true;
	}

	private static int getItemIndex(int item) {
		if (item >= 1706 && item <= 1712) { // Normal glory amulets
			return 0;
		} else if (item >= 10354 && item <= 10361) { // Trimmed glory amulets
			return 1;
		} else if (item >= 2552 && item <= 2566) { // Duel rings
			return 2;
		} else if (item >= 3853 && item <= 3868) { // Games amulet
			return 3;
		}
		return -1;
	}

	public static boolean teleport(final Player p, int opt, final JewellerySlot js) {
		if (js == null) {
			return false;
		}
		if (js.index == -1 || js.index > 3 || opt > 6) {
			return false;
		}
		if (!canTeleport(p, js)) {
			p.getActionSender().closeInterfaces();
			return true;
		}
		if ((js.index == 2 && opt == 4) || (js.index != 2 && opt == 6)) {
			p.getActionSender().sendMessage("You stay where you are.");
			p.getActionSender().closeInterfaces();
			return true;
		}
		opt -= 2; // Used to get the 'index' from the button id.
		p.graphics(1684);
		p.animate(9603);
		p.getWalkingQueue().reset();
		p.getActionSender().clearMapFlag();
		p.setTemporaryAttribute("teleporting", true);
		p.setTemporaryAttribute("unmovable", true);
		p.removeTemporaryAttribute("autoCasting");
		p.removeTemporaryAttribute("lootedBarrowChest");
		p.setTarget(null);
		changeJewellery(p, js);
		final int option = opt;
		p.getActionSender().closeInterfaces();
		World.getInstance().registerEvent(new Event(2000) {

			@Override
			public void execute() {
				this.stop();
				p.teleport(Location.location(COORDS_X[js.index][option], COORDS_Y[js.index][option], 0));
				p.animate(65535);
				Teleport.resetTeleport(p);
				p.removeTemporaryAttribute("unmovable");
			}
		});
		return true;
	}

	private static boolean canTeleport(Player p, JewellerySlot js) {
		if (p.getTemporaryAttribute("teleporting") != null) {
			return false;
		}
		if (p.getDuel() != null) {
			if (p.getDuel().getStatus() < 4) {
				p.getDuel().declineDuel();
			} else if (p.getDuel().getStatus() == 5 || p.getDuel().getStatus() == 6) {
				p.getActionSender().sendMessage("You cannot teleport whilst in a duel.");
				return false;
			} else if (p.getDuel().getStatus() == 8) {
				p.getDuel().recieveWinnings(p);
			}
		}
		int wildLvl = js.index == 1 || js.index == 2 ? 30 : 20;
		if (Area.inWilderness(p.getLocation()) && p.getLastWildLevel() >= wildLvl) {
			p.getActionSender().sendMessage("You cannot teleport above level " + wildLvl + " wilderness!");
			return false;
		}
		if (Area.inFightCave(p.getLocation())) {
			FightCave.antiTeleportMessage(p);
			return false;
		}
		if (p.getTemporaryAttribute("teleblocked") != null) {
			p.getActionSender().sendMessage("A magical force prevents you from teleporting!");
			return false;
		}
		if (Area.inFightPits(p.getLocation())) {
			p.getActionSender().sendMessage("You are unable to teleport from the fight pits.");
			return false;
		}
		if (p.getTemporaryAttribute("unmovable") != null) {
			return false;
		}
		return true;
	}

	protected static void changeJewellery(Player p, JewellerySlot js) {
		boolean gloryAmulet = js.index < 2;
		boolean newItem = true;
		String s = js.index == 2 ? "Ring of Dueling" : js.index == 3 ?  "Games necklace" : "Amulet of Glory";
		for (int i = 0; i < JEWELLERY[js.index].length; i++) {
			int charges = i;
			if (!js.wearing) {
				Item item = p.getInventory().getSlot(js.slot);
				if (item.getItemId() == JEWELLERY[js.index][i]) {
					if (gloryAmulet) {
						charges--;
					}
					String t = charges > 1 ? " charges" : " charge";
					if (charges > 0) {
						p.getActionSender().sendMessage("The " + s + " now has " + charges + t + " .");
					} else if (gloryAmulet && charges == 0) {
						p.getActionSender().sendMessage("The Amulet of Glory has run out of charges.");
					} else if (!gloryAmulet && charges <= 1) {
						newItem = false;
						p.getActionSender().sendMessage("The " + s + " crumbles to dust.");
						p.getInventory().deleteItem(item.getItemId(), js.slot, 1);
					}
					if (newItem) {
						item.setItemId(JEWELLERY[js.index][i - 1]);
						p.getActionSender().refreshInventory();
					}
					break;
				}
			} else {
				Item item = p.getEquipment().getSlot(js.slot);
				if (item.getItemId() == JEWELLERY[js.index][i]) {
					if (gloryAmulet) {
						charges--;
					}
					String t = charges > 1 ? " charges" : " charge";
					if (charges > 0) {
						p.getActionSender().sendMessage("The " + s + " now has " + charges + t + " .");
					} else if (gloryAmulet && charges == 0) {
						p.getActionSender().sendMessage("The Amulet of Glory has run out of charges.");
					} else if (!gloryAmulet && charges <= 1) {
						newItem = false;
						p.getActionSender().sendMessage("The " + s + " crumbles to dust.");
						item.setItemId(-1);
						item.setItemAmount(0);
					}
					if (newItem) {
						item.setItemId(JEWELLERY[js.index][i - 1]);
					}
					p.getActionSender().refreshEquipment();
					break;
				}
			}
		}
	}
	
	public static class JewellerySlot {
		
		public boolean wearing;
		public int slot;
		public int index;

		public JewellerySlot(int index, int slot, boolean wearing) {
			this.index = index;
			this.slot = slot;
			this.wearing = wearing;
		}
	}
}
