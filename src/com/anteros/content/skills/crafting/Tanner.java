package com.anteros.content.skills.crafting;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class Tanner extends CraftingData {

	public static void interactWithTanner(final Player player, final NPC n) {
		player.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(player.getLocation());
				player.setFaceLocation(n.getLocation());
				player.setEntityFocus(65535);
				player.getActionSender().displayInterface(324);
			}
		});
	}
	
	public static void handleButtons(Player player, int button, int amount) {
		switch(button) {
		case 1: //Soft Leather
			checkMaterials(player, button, amount);
			break;
		case 2: //Hard Leather
			checkMaterials(player, button, amount);
			break;
		case 3: //Snakeskin 1
			checkMaterials(player, button, amount);
			break;
		case 4: //Snakeskin 2
			checkMaterials(player, button, amount);
			break;
		case 5: //Green d'hide
			checkMaterials(player, button, amount);
			break;
		case 6: //Blue d'hide
			checkMaterials(player, button, amount);
			break;
		case 7: //Red d'hide
			checkMaterials(player, button, amount);
			break;
		case 8: //Black d'hide
			checkMaterials(player, button, amount);
			break;
		}
	}
	
	public static int getInvAmount(Player player, int button) {
		int leather = button - 1;
		int invAmount = player.getInventory().getItemAmount(TANNING_LEATHER[leather]); //amount in invy
		return invAmount;
	}
	
	private static void checkMaterials(Player player, int button, int amount) {
		int leather = button - 1;
		if (player.getInventory().hasItemAmount(995, TANNING_PRICES[leather]) &&
				player.getInventory().hasItem(TANNING_LEATHER[leather])) {
				
			tanLeather(player, leather, amount);
		} else if (!player.getInventory().hasItemAmount(995, TANNING_PRICES[leather])) {
			player.getActionSender().sendMessage("You need " + TANNING_PRICES[leather] + " gp to tan this.");
		} else if (!player.getInventory().hasItem(TANNING_LEATHER[leather])) {
			player.getActionSender().sendMessage("You need some " + TANNING_LEATHER_NAMES[leather] + " in order to tan it!");
		}
	}
	
	private static void tanLeather(Player player, int leather, int amount) {
		int invAmount = getInvAmount(player, leather + 1);
		if (amount == 0) {
			return;
		}
		if (amount > invAmount) {
			for (int i = 0; i < invAmount; i++) {
				removeItems(player, leather, amount);
			}
		} else {
			for (int i = 0; i < amount; i++) {
				removeItems(player, leather, amount);
			}
		}
		//System.out.println(amount);
		//System.out.println(invAmount);
	}
	
	private static void removeItems(Player player, int leather, int amount)	 {
		player.getInventory().deleteItem(TANNING_LEATHER[leather]);
		player.getInventory().deleteItem(995, TANNING_PRICES[leather]);
		player.getInventory().addItem(TANNED_LEATHER[leather], amount);
	}
}
