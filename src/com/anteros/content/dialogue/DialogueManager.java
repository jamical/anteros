package com.anteros.content.dialogue;

import com.anteros.model.player.Player;


public class DialogueManager {

	private Player player;
	private Dialogue lastDialogue;

	public DialogueManager(Player player) {
		this.player = player;
	}

	public void startDialogue(Object key, Object... parameters) {
		player.setTemporaryAttribute("dialogue", parameters);
		if (getLastDialogue() != null)
			getLastDialogue().finish();
		setLastDialogue(DialogueHandler.getDialogue(key));
		if (getLastDialogue() == null)
			return;
		getLastDialogue().parameters = parameters;
		getLastDialogue().setPlayer(player);
		getLastDialogue().start();
	}

	public void continueDialogue(int interfaceId, int componentId) {
		if (getLastDialogue() == null)
			return;
		getLastDialogue().run(interfaceId, componentId);
	}

	public void finishDialogue() {
		
		if (getLastDialogue() == null)
			return;
		getLastDialogue().finish();
		setLastDialogue(null);
		player.getActionSender().sendChatboxInterface(372);

	}

	public Dialogue getLastDialogue() {
		return lastDialogue;
	}

	public void setLastDialogue(Dialogue lastDialogue) {
		this.lastDialogue = lastDialogue;
	}

}
