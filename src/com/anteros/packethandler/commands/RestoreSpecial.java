package com.anteros.packethandler.commands;

import com.anteros.model.player.Player;

public class RestoreSpecial implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
		player.getSpecialAttack().resetSpecial();
	}
	}

	@Override
	public int minimumRightsNeeded() {
		return 1;
	}
}
