package com.anteros.content.skills.mining;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
import com.anteros.util.Misc;

public class Mining extends MiningData {

	public Mining() {
		
	}
	
	public static void tryMineRock(final Player p, final int rockId,  final Location rockLocation, final int i, final boolean newMine) {
		if (rockId != 2491) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, rockLocation.getX()-1, rockLocation.getY()-1, rockLocation.getX()+1, rockLocation.getY()+1) {
				@Override
				public void run() {
					mineRock(p, rockId, rockLocation, i, newMine);
				}
			});
		} else {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, rockLocation.getX()-1, rockLocation.getY()-1, rockLocation.getX()+5, rockLocation.getY()+5) {
				@Override
				public void run() {
					mineRock(p, rockId, rockLocation, i, newMine);
				}
			});
		}
	}

	public static void mineRock(final Player p, int rockId,  Location rockLocation, int i, boolean newMine) {
		if (!newMine && p.getTemporaryAttribute("miningRock") == null) {
			return;
		}
		if (newMine) {
			if (!World.getInstance().getObjectLocations().objectExists(rockId, rockLocation)) {
				//Logger.getInstance().info(p.getUsername() + " tried to mine a non existing rock!");
				//return;
			}
			Rock newRock = new Rock(i, rockId, rockLocation, ORES[i], ROCK_LEVEL[i], NAME[i], ROCK_XP[i]);
			p.setTemporaryAttribute("miningRock", newRock);
		}
		final Rock rockToMine = (Rock) p.getTemporaryAttribute("miningRock");
		final boolean essRock = rockToMine.getRockIndex() == 0;
		if (!canMine(p, rockToMine, null)) {
			resetMining(p);
			return;
		}
		if (newMine) {
			String s = essRock ? "You begin to mine Essence.." : "You swing your pick at the rock..";
			p.getActionSender().sendMessage(s);
		}
		p.getActionSender().closeInterfaces();
		p.animate(getPickaxeAnimation(p));
		p.setFaceLocation(rockLocation);
		int delay = getMineTime(p, rockToMine.getRockIndex());
		World.getInstance().registerEvent(new Event(delay) {
			@Override
			public void execute() {
				this.stop(); // Stop the event no matter what
				if (p.getTemporaryAttribute("miningRock") == null) {
					return;
				}
				final Rock rock = (Rock) p.getTemporaryAttribute("miningRock");
				if (!canMine(p, rockToMine, rock)) {
					return;
				}
				if (!essRock) {
					World.getInstance().getGlobalObjects().lowerHealth(rock.getRockId(), rock.getRockLocation());
					if (!World.getInstance().getGlobalObjects().originalObjectExists(rock.getRockId(), rock.getRockLocation())) {
						resetMining(p);
						stopAllOtherMiners(p, rock);
						p.animate(65535);
						this.stop();
					}
				}
				boolean addGem = (!essRock && Misc.random(getGemChance(p)) == 0) ? true : false;
				if (p.getInventory().addItem(addGem ? randomGem() : rock.getOre())) {
					p.getLevels().addXp(MINING, rock.getXp());
					if (addGem) {
						p.getActionSender().sendMessage("You manage to mine a sparkling gem!");
					} else {
						if (!essRock) {
							p.getActionSender().sendMessage("You manage to mine some " + rock.getName() + ".");
						}
					}
				}
				if (rock.isContinueMine()) {
					mineRock(p, rock.getRockId(), rock.getRockLocation(), rock.getRockIndex(), false);
				}
			}
		});
		if (delay >= 9000 && !rockToMine.isContinueMine()) {
			World.getInstance().registerEvent(new Event(9000) {	
				@Override
				public void execute() {
					this.stop();
					final Rock rock = (Rock) p.getTemporaryAttribute("miningRock");
					if (!canMine(p, rockToMine, rock)) {
						return;
					}
					if (rock != null) {
						p.setFaceLocation(rock.getRockLocation());
						p.animate(getPickaxeAnimation(p));
					} 
				}
			});
		}
	}
	
	private static int getGemChance(Player p) {
		for (int i = 0; i < GLORY_AMULETS.length; i++) {
			if (p.getEquipment().getItemInSlot(2) == GLORY_AMULETS[i]) {
				return 30;
			}
		}
		return 40;
	}
	
	public static int getMineTime(Player p, int i) {
		int standardTime = ROCK_TIME[i];
		int randomTime = standardTime + Misc.random(standardTime);
		int pickaxeTime = PICKAXE_TIME[getUsedPickaxe(p)];
		int levelTime = getLevelDelayTime(p);
		int finalTime = (randomTime -= pickaxeTime) + levelTime;
		if (finalTime <= 800 || finalTime <= 0) {
			finalTime = 800;
		}
		if (i == 0) {
			finalTime = standardTime;
		}
		return finalTime;
	}
	
	public static int getLevelDelayTime(Player p) {
		if (p.getLevels().getLevel(MINING) < 10 && p.getLevels().getLevel(MINING) > 0) {
			return LEVEL_TIME[0];
		}
		if (p.getLevels().getLevel(MINING) >= 10 && p.getLevels().getLevel(MINING) < 20) {
			return LEVEL_TIME[1];
		}
		if (p.getLevels().getLevel(MINING) >= 20 && p.getLevels().getLevel(MINING) < 30) {
			return LEVEL_TIME[2];
		}
		if (p.getLevels().getLevel(MINING) >= 30 && p.getLevels().getLevel(MINING) < 40) {
			return LEVEL_TIME[3];
		}
		if (p.getLevels().getLevel(MINING) >= 40 && p.getLevels().getLevel(MINING) < 50) {
			return LEVEL_TIME[4];
		}
		if (p.getLevels().getLevel(MINING) >= 50 && p.getLevels().getLevel(MINING) < 60) {
			return LEVEL_TIME[5];
		}
		if (p.getLevels().getLevel(MINING) >= 60 && p.getLevels().getLevel(MINING) < 70) {
			return LEVEL_TIME[6];
		}
		if (p.getLevels().getLevel(MINING) >= 70 && p.getLevels().getLevel(MINING) < 80) {
			return LEVEL_TIME[7];
		}
		if (p.getLevels().getLevel(MINING) >= 80 && p.getLevels().getLevel(MINING) < 90) {
			return LEVEL_TIME[8];
		}
		if (p.getLevels().getLevel(MINING) >= 90 && p.getLevels().getLevel(MINING) < 99) {
			return LEVEL_TIME[9];
		}
		if (p.getLevels().getLevel(MINING) == 99) {
			return LEVEL_TIME[10];
		}
		
		else {
			return 0;
		}
		
	}

	public static void prospectRock(final Player p, int x, int y, final String s) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x+1, y+1) {
			@Override
			public void run() {
				p.getActionSender().sendMessage("You examine the rock for ores...");
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(2000) {
					@Override
					public void execute() {
						p.getActionSender().sendMessage("This rock contains " + s + ".");
						p.removeTemporaryAttribute("unmovable");
						this.stop();
					}
				});
			}
		});
	}
	
	private static void stopAllOtherMiners(Player player, Rock rock) {
		for (Player p : World.getInstance().getPlayerList()) {
			if (p != null && player.getLocation().withinDistance(p.getLocation(), 5) && !p.equals(player)) {
				if (p.getTemporaryAttribute("miningRock") != null) {
					Rock otherPlayerRock = (Rock) p.getTemporaryAttribute("miningRock");
					if (otherPlayerRock.getRockLocation().equals(rock.getRockLocation())) {
						p.animate(65535);
					}
				}
			}
		}
	}

	private static boolean canMine(Player p, Rock rock, Rock rock2) {
		if (rock == null || p == null || !World.getInstance().getGlobalObjects().originalObjectExists(rock.getRockId(), rock.getRockLocation())) {
			return false;
		}
		if (rock.getRockIndex() != 0) {
			if (!p.getLocation().withinDistance(rock.getRockLocation(), 2)) {
				return false;
			}
		} else {
			// is rune ess rock
			if (!p.getLocation().inArea(rock.getRockLocation().getX()-1, rock.getRockLocation().getY()-1, rock.getRockLocation().getX()+5, rock.getRockLocation().getY()+5)) {
				return false;
			}
		}
		if (rock2 != null) {
			if (!rock.equals(rock2)) {
				return false;
			}
		}
		if (p.getLevels().getLevel(MINING) < rock.getLevel()) {
			p.getActionSender().sendMessage("You need a Mining level of " + rock.getLevel() + " to mine that rock.");
			return false;
		}
		if (!hasPickaxe(p)) {
			p.getActionSender().sendMessage("You need a pickaxe to mine a rock!");
			return false;
		}
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendChatboxInterface(210);
			p.getActionSender().modifyText("Your inventory is too full to carry any ore.", 210, 1);
			return false;
		}
		return true;
	}

	public static boolean hasPickaxe(Player p) {
		for (int i = 0; i < PICKAXES.length; i++) {
			if (p.getEquipment().getItemInSlot(3) == PICKAXES[i] || p.getInventory().hasItem(PICKAXES[i])) {
				return true;
			}
		}
		return false;
	}
	
	public static int getPickaxeAnimation(Player p) {
		int pickaxe = getUsedPickaxe(p);
		if (pickaxe == -1) {
			return 65535;
		}
		return PICKAXE_ANIMS[pickaxe];
	}
	
	private static int getUsedPickaxe(Player p) {
		int index = -1;
		for (int i = 0; i < PICKAXES.length; i++) {
			if (p.getEquipment().getItemInSlot(3) == PICKAXES[i] || p.getInventory().hasItem(PICKAXES[i])) {
				index = i;
			}
		}
		return index;
	}
	
	public static int randomGem() {
		if (Misc.random(5000) == 0) {
			return 1631; // Dragonstone
		}
		if (Misc.random(15000) == 0) {
			return 6571; // Onyx
		}
		return GEMS[(int) (Math.random() * GEMS.length)];
	}
	
	public static void displayEmptyRockMessage(final Player player, Location rockLocation) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, rockLocation.getX() - 1, rockLocation.getY() - 1, rockLocation.getX() + 1, rockLocation.getY() + 1) {

			@Override
			public void run() {
				player.getActionSender().sendMessage("There is currently no ore available from this rock.");
			}
		});	
	}

	public static void resetMining(Player p) {
		p.removeTemporaryAttribute("miningRock");
	}
}
