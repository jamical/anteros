package com.anteros.content.skills;

import com.anteros.content.skills.cooking.Cooking;
import com.anteros.content.skills.crafting.Crafting;
import com.anteros.content.skills.fishing.Fishing;
import com.anteros.content.skills.fletching.Fletching;
import com.anteros.content.skills.herblore.Herblore;
import com.anteros.content.skills.mining.Mining;
import com.anteros.content.skills.smithing.Smithing;
import com.anteros.content.skills.woodcutting.Woodcutting;
import com.anteros.model.player.Player;

public class Skills {

	public static final int SKILLCAPE_PRICE = 250000;

	public static void resetAllSkills(Player p) {
		Fletching.setFletchItem(p, null);
		Herblore.setHerbloreItem(p, null);
		Cooking.setCookingItem(p, null);
		Mining.resetMining(p);
		Smithing.resetSmithing(p);
		Woodcutting.resetWoodcutting(p);
		Fishing.resetFishing(p);
		Crafting.resetCrafting(p);
		p.removeTemporaryAttribute("harvesting");
	}
}
