package com.anteros.content.skills.runecrafting;

import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.ItemDefinition;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.FaceLocation;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.ShopSession;
import com.anteros.util.Misc;

public class RuneCraft extends RuneCraftData {

	public RuneCraft() {
		
	}
	
	public static boolean wantToRunecraft(Player p, int object, int x, int y) {
		for (int i = 0; i < ALTARS.length; i++) {
			if (object == ALTARS[i]) {
				craftRunes(p, i, x, y);
				faceAltar(p, i);
				return true;
			}
		}
		return false;
	}
	
	public static boolean fillPouch(Player p, int pouch) {
		if (pouch != SMALL_POUCH && pouch !=  MEDIUM_POUCH && pouch != LARGE_POUCH && pouch != GIANT_POUCH) {
			return false;
		}
		int amount = 0;
		int pouchAmount = 0;
		int invenEss = p.getInventory().getItemAmount(ESSENCE);
		switch(pouch) {
			case SMALL_POUCH:
				amount = 3;
				pouchAmount = p.getSettings().getSmallPouchAmount();
				break;
		
			case MEDIUM_POUCH:
				amount = 6;
				pouchAmount = p.getSettings().getMediumPouchAmount();
				break;
				
			case LARGE_POUCH:
				amount = 9;
				pouchAmount = p.getSettings().getLargePouchAmount();
				break;
				
			case GIANT_POUCH:
				amount = 12;
				pouchAmount = p.getSettings().getGiantPouchAmount();
				break;
		}
		amount = (amount - pouchAmount);
		if (invenEss <= 0) {
			return true;
		}
		if (amount >= invenEss) {
			amount = invenEss;
		}
		if (amount <= 0) {
			p.getActionSender().sendMessage("This pouch is full.");
			return true;
		}
		for (int i = 0; i < amount; i++) {
			if (!p.getInventory().deleteItem(ESSENCE)) {
				p.getActionSender().sendMessage("An error occured whilst deleting essence.");
				return true;
			}
		}
		setAddPouchAmount(p, pouch, amount);
		return true;
	}
	
	public static boolean emptyPouch(Player p, int pouch) {
		if (pouch != SMALL_POUCH && pouch !=  MEDIUM_POUCH && pouch != LARGE_POUCH && pouch != GIANT_POUCH) {
			return false;
		}
		int freeSpace = p.getInventory().getTotalFreeSlots();
		int amount = 0;
		switch(pouch) {
			case SMALL_POUCH:
				amount = p.getSettings().getSmallPouchAmount();
				break;
		
			case MEDIUM_POUCH:
				amount = p.getSettings().getMediumPouchAmount();
				break;
				
			case LARGE_POUCH:
				amount = p.getSettings().getLargePouchAmount();
				break;
				
			case GIANT_POUCH:
				amount = p.getSettings().getGiantPouchAmount();
				break;
		}
		if (amount >= freeSpace) {
			amount = freeSpace;
		}
		for (int i = 0; i < amount; i++) {
			p.getInventory().addItem(ESSENCE);
		}
		setRemovePouchAmount(p, pouch, amount);
		return true;
	}

	private static void setAddPouchAmount(Player p, int pouch, int amount) {
		switch(pouch) {
			case SMALL_POUCH:
				p.getSettings().setSmallPouchAmount(p.getSettings().getSmallPouchAmount() + amount);
				break;
		
			case MEDIUM_POUCH:
				p.getSettings().setMediumPouchAmount(p.getSettings().getMediumPouchAmount() + amount);
				break;
				
			case LARGE_POUCH:
				p.getSettings().setLargePouchAmount(p.getSettings().getLargePouchAmount() + amount);
				break;
				
			case GIANT_POUCH:
				p.getSettings().setGiantPouchAmount(p.getSettings().getGiantPouchAmount() + amount);
				break;
		}
	}
	
	private static void setRemovePouchAmount(Player p, int pouch, int amount) {
		switch(pouch) {
			case SMALL_POUCH:
				p.getSettings().setSmallPouchAmount(p.getSettings().getSmallPouchAmount() - amount);
				break;
		
			case MEDIUM_POUCH:
				p.getSettings().setMediumPouchAmount(p.getSettings().getMediumPouchAmount() - amount);
				break;
				
			case LARGE_POUCH:
				p.getSettings().setLargePouchAmount(p.getSettings().getLargePouchAmount() - amount);
				break;
				
			case GIANT_POUCH:
				p.getSettings().setGiantPouchAmount(p.getSettings().getGiantPouchAmount() - amount);
				break;
		}
	}

	private static void faceAltar(Player p, int i) {
		p.setFaceLocation(new FaceLocation(ALTAR_COORDS[i][0], ALTAR_COORDS[i][1]));	
	}

	private static void craftRunes(final Player p, final int i, final int x, final int y) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-3, y-3, x+3, y+3) {
			@Override
			public void run() {
				if (p.getTemporaryAttribute("lastRunecraftTime") != null) {
					if (System.currentTimeMillis() - (Long) p.getTemporaryAttribute("lastRunecraftTime") < 500) {
						return;
					}
				}
				if (!p.getInventory().hasItem(ESSENCE)) {
					p.getActionSender().sendMessage("You have no Pure essence.");
					return;
				}
				if (p.getLevels().getLevel(RUNECRAFTING) < CRAFT_LEVEL[i]) {
					p.getActionSender().sendMessage("You need a Runecrafting level of " + CRAFT_LEVEL[i] + " to craft " + ItemDefinition.forId(RUNES[i]).getName() + "s.");
					return;
				}
				p.animate(791);
				World.getInstance().registerEvent(new Event(250) {	
					@Override
					public void execute() {
						this.stop();
						int amount = p.getInventory().getItemAmount(ESSENCE);
						for (int j = 0 ; j < amount; j++) {
							if (!p.getInventory().deleteItem(ESSENCE)) {
								p.getActionSender().sendMessage("An error occured whilst deleting essence from your inventory.");
								return;
							}
						}
						int multiply = 1;
						for (int j = 0; j < MULTIPLY_LEVELS[i].length; j++) {
							if (p.getLevels().getLevel(RUNECRAFTING) >= MULTIPLY_LEVELS[i][j]) {
								multiply++;
							}
						}
						String s = amount > 1 || (amount == 1 && multiply > 1) ? "s." : ".";
						String s1 = amount > 1 || (amount == 1 && multiply > 1) ? "" : "a ";
						if (p.getInventory().addItem(RUNES[i], amount * multiply)) {
							p.getLevels().addXp(RUNECRAFTING, (CRAFT_XP[i] * amount));
							p.getActionSender().sendMessage("You craft the essence into " + s1 + ItemDefinition.forId(RUNES[i]).getName() + s);
						}
						p.setTemporaryAttribute("lastRunecraftTime", System.currentTimeMillis());
					}
				});
			}
		});
	}
	
	public static void enterAltar(final Player p, int i) {
		if (i == 13) {
			return;
		}
		if (i == 12) {
			p.getActionSender().sendMessage("This altar is currently unavailable due to mapdata issues.");
			return;
		}
		p.teleport(Location.location(RUIN_TELEPORT[i][0], RUIN_TELEPORT[i][1], 0));
	}

	public static boolean enterRift(final Player p, int objectId, int x, int y) {
		for (int i = 0; i < ABYSS_DOORWAYS.length; i++) {
			if (objectId == ABYSS_DOORWAYS[i]) {
				if (i == 13) {
					return true;
				}
				final int j = i;
				World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(x, y, 0)) {
					@Override
					public void run() {
						if (j == 12) {
							p.getActionSender().sendMessage("This altar is currently unavailable due to mapdata issues.");
							return;
						}
						p.teleport(Location.location(ALTAR_COORDS[j][0], (ALTAR_COORDS[j][1] + 3), 0));
						faceAltar(p, j);
					}
				});
				return true;
			}
		}
		return false;
	}
	
	public static void toggleRuin(Player p, int id,  boolean show) {
		int index = getTiaraIndex(id);
		if (index == -1) {
			return;
		}
		int ruinId = show ? RUINS2[index] : RUINS[index];
		int face = index == 6 ? 2 : index == 7 ? 3 : 0;
		p.getActionSender().createObject((ruinId), Location.location(RUIN_COORDS[index][0], RUIN_COORDS[index][1], 0), face, 10);
	}
	
	public static int getTiaraIndex(int id) {
		for (int j = 0; j < TIARAS.length; j++) {
			if (id == TIARAS[j]) {
				return j;
			}
		}
		return -1;
	}

	public static boolean wearingTiara(Player p) {
		for (int i = 0; i < TIARAS.length; i++) {
			if (p.getEquipment().getItemInSlot(0) == TIARAS[i]) {
				return true;
			}
		}
		return false;
	}

	public static boolean enterViaTiara(final Player p, int object, int x, int y) {
		if (object < 7104 || object > 7124) {
			return false;
		}
		for (int i = 0; i < RUINS2.length; i++) {
			if (object == RUINS2[i]) {
				final int j = i;
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x+4, y+4) {
					@Override
					public void run() {
						if (wearingTiara(p)) {
							enterAltar(p, j);
						}
					}
				});
				return true;
			}
		}
		return false;
	}

	public static boolean useTalisman(final Player p, int object, int x, int y) {
		if (object < 2452 || object > 2461) {
			return false;
		}
		for (int i = 0; i < RUINS.length; i++) {
			if (object == RUINS[i]) {
				final int j = i;
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x+4, y+4) {
					@Override
					public void run() {
						if (hasTalisman(p, j)) {
							enterAltar(p, j);
						}
					}
				});
				return true;
			}
		}
		return false;
	}
	
	private static boolean hasTalisman(Player p, int i) {
		for (int j = 0; j < TALISMANS.length; j++) {
			if (p.getInventory().hasItem(TALISMANS[j])) {
				return true;
			}
		}
		return false;
	}

	public static boolean leaveAltar(final Player p, int object, int x, int y) {
		if (object < 2465 || object > 2475) {
			return false;
		}
		for (int i = 0; i < PORTALS.length; i++) {
			if (object == PORTALS[i]) {
				if (i != 6) {
					if (x != PORTAL_COORDS[i][0] && y != PORTAL_COORDS[i][1]) {
						return false;
					}
				} else {
					if (i == 6 && (x != 2163 && y != 4833) && (x != 2142 && y != 4854) && (x != 2121 && y != 4833) && (x != 2142 && y != 4812)) {
						return false;
					}
				}
				final int j = i;
				World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x+1, y+1) {	
					@Override
					public void run() {
						teleportOutOfAltar(p, j);
					}
				});
				return true;
			}
		}
		return false;
	}
	
	private static void teleportOutOfAltar(Player p, int i) {
		int x = RUIN_COORDS[i][0] + 1;
		int y = RUIN_COORDS[i][1] - 1;
		p.teleport(Location.location(x, y, 0));
	}

	public static Location teleportInner() {
		int i = Misc.random(ABYSS_TELEPORT_INNER.length - 1);
		return Location.location(ABYSS_TELEPORT_INNER[i][0], ABYSS_TELEPORT_INNER[i][1], 0);
	}
	
	public static Location teleportOuter() {
		int i = Misc.random(ABYSS_TELEPORT_OUTER.length - 1);
		return Location.location(ABYSS_TELEPORT_OUTER[i][0], ABYSS_TELEPORT_OUTER[i][1], 0);
	}

	public static void teleportToEssMine(final Player p, final NPC n) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		p.setTemporaryAttribute("unmovable", true);
		n.graphics(108);
		World.getInstance().registerEvent(new Event(600) {
			int i = 0;
			@Override
			public void execute() {
				i++;
				if (i == 1) {
					p.graphics(110);
					n.setForceText(new ForceText("Senventior disthine molenko!"));
				} else if (i == 2) {
					this.stop();
					World.getInstance().registerEvent(new Event(300) {
	
						@Override
						public void execute() {
							p.teleport(getRandomMineLocation());
							p.removeTemporaryAttribute("unmovable");
							this.stop();
						}
					});
				}
			}
		});
	}

	protected static Location getRandomMineLocation() {
		switch(Misc.random(4)) {
			case 0 : return Location.location(2920 + Misc.random(2), 4845 + Misc.random(4), 0);
			case 1 : return Location.location(2897 + Misc.random(2), 4849 + Misc.random(4), 0);
			case 3 : return Location.location(2897 + Misc.random(2), 4808 + Misc.random(3), 0);
			case 4 : return Location.location(2929 + Misc.random(1), 4807 + Misc.random(6), 0);
		}
		return Location.location(2909 + Misc.random(3), 4830 + Misc.random(4), 0);
	}
	
	public static void leaveEssMine(final Player p, Location loc) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, loc.getX() - 1, loc.getY() - 1, loc.getX() + 1, loc.getY() + 1) {

			@Override
			public void run() {
				p.teleport(Location.location(3253 + Misc.random(1), 3401 + Misc.random(1), 0));
			}
		});
	}
}
