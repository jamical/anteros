package com.anteros.content.minigames.fightpits;

import java.util.ArrayList;
import java.util.List;

import com.anteros.content.skills.magic.Teleport;
import com.anteros.content.skills.prayer.Prayer;
import com.anteros.event.AreaEvent;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
import com.anteros.util.Area;
import com.anteros.util.Misc;

public class FightPits {
	
	private List<Player> playersWaiting = new ArrayList<Player>();
	private List<Player> playersPlaying = new ArrayList<Player>();
	private boolean gameInProgress = false, gameStarted = false;
	private Player lastWinner = null;
	private long gameStartedTime;
	private static final int MAX_GAME_LENGTH = 9;
	@SuppressWarnings("unused")
	private boolean monstersSpawned = false;
	
	/**
	 * Used for the reward
	 */
	private int tokkulReward;
	private int totalPlayers;
	
	private static final int[][] ORB_COORDINATES = {
		{13, 14, 15, 12, 11}, // Viewing orb interface button id's
		{2409, 2411, 2388, 2384, 2398}, // x
		{5158, 5137, 5138, 5157, 5150} // y
		// NE, SE, SW, NW, middle,
	};

	public FightPits() {
		startWaitingEvent();
	}
	
	private void startWaitingEvent() {
		World.getInstance().registerEvent(new Event(10000) {

			@Override
			public void execute() {
				if (!gameInProgress) {
					if (playersWaiting.size() >= 2 || (playersWaiting.size() >= 1 && playersPlaying.size() == 1)) {
						startGame();
						this.setTick(1000);
					}
				} else {
					if (playersPlaying.size() <= 1) {
						gameInProgress = false;
						this.setTick(40000);
						setWinner();
					} else {
						if (System.currentTimeMillis() - gameStartedTime >= (MAX_GAME_LENGTH * 60000)) {
							spawnMonsters();
						}
					}
				}
			}
		});
	}

	protected void spawnMonsters() {
		/*if (monstersSpawned || !gameInProgress) {
			return;
		}
		monstersSpawned = true;
		World.getInstance().registerEvent(new Event(3000) {

			@Override
			public void execute() {
				if (!monstersSpawned || playersPlaying.size() <= 1) {
					this.stop();
					for (NPC n : World.getInstance().getNpcList()) {
						if (Area.inFightPits(n.getLocation())) {
							n.setHidden(true);
							World.getInstance().getNpcList().remove(n);
						}
					}
					return;
				}
			}
		});*/
		for (Player p : playersPlaying) {
			teleportToWaitingRoom(p, false);
			sendNPCMessage(p, "You took to long in defeating your enemies.");
		}
		playersPlaying.clear();
	}

	protected void setWinner() {
		monstersSpawned = false;
		if (playersPlaying.isEmpty()) {
			lastWinner = null;
			return;
		}
		Player p = playersPlaying.get(0);
		playersPlaying.clear();
		if (p != null) {
			sendNPCMessage(p, "Congratulations! You are the champion!");
			p.getActionSender().sendMessage("Congratulations, you are the champion!");
			p.getSettings().setTzhaarSkull();
			playersPlaying.add(p);
			displayFightPitsInterface(p); // update the name.
			for (NPC n : World.getInstance().getNpcList()) {
				if (n != null) {
					if (n.getId() == 2618) {
						if (lastWinner != null) {
							if (lastWinner.getUsername().equals(p.getUsername())) {
								n.setForceText(new ForceText(p.getPlayerDetails().getDisplayName() + " retains champion status!"));
							} else {
								n.setForceText(new ForceText(p.getPlayerDetails().getDisplayName() + " is the new champion!"));
							}
						} else {
							n.setForceText(new ForceText(p.getPlayerDetails().getDisplayName() + " is the new champion!"));
						}
						break;
					}
				}
			}
			lastWinner = p;
		} else {
			lastWinner = null;
		}
	}

	protected void startGame() {
		gameInProgress = true;
		gameStartedTime = System.currentTimeMillis();
		synchronized(playersWaiting) {
			playersPlaying.addAll(playersWaiting);
		}
		playersWaiting.clear();
		for (Player p : playersPlaying) {
			if (p == null || p.isDead() || p.isDestroyed() || p.getTeleportTo() != null || (lastWinner != null && p.equals(lastWinner) && Area.inFightPits(p.getLocation()))) {
				continue;
			}
			p.teleport(getRandomTeleport());
			resetVariables(p);
			sendNPCMessage(p, "Wait for my signal before fighting.");
			tokkulReward += p.getLevels().getCombatLevel();
			totalPlayers++;
		}
		tokkulReward *= (totalPlayers * 2.40);
		World.getInstance().registerEvent(new Event(10000) {

			@Override
			public void execute() {
				this.stop();
				if (playersPlaying.size() == 1) {
					gameInProgress = false;
					setWinner();
					return;
				}
				for (Player p : playersPlaying) {
					if (p == null || p.isDestroyed()) {
						continue;
					}
					sendNPCMessage(p, "Fight!");
				}
				gameStarted = true;
			}
		});
	}
	
	public void useOrb(final Player p, int button) {
		if (p.getTemporaryAttribute("teleporting") != null) {
			return;
		}
		if (button == -1) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2398, 5171, 2400, 5173) {

				@Override
				public void run() {
					p.getActionSender().displayInventoryInterface(374);
					p.getAppearance().setInvisible(true);
					p.getUpdateFlags().setAppearanceUpdateRequired(true);
					p.setTemporaryAttribute("cantDoAnything", true);
					p.setTemporaryAttribute("unmovable", true);
					p.getActionSender().setMinimapStatus(2);
					World.getInstance().registerEvent(new Event(500) {

						@Override
						public void execute() {
							this.stop();
							int random = Misc.random(4);
							p.teleport(Location.location(ORB_COORDINATES[1][random], ORB_COORDINATES[2][random], 0));
						}
					});
				}
				
			});
		} else {
			if (p.getTemporaryAttribute("cantDoAnything") != null) {
				if (button == 5) {
					World.getInstance().registerEvent(new Event(500) {

						@Override
						public void execute() {
							this.stop();
							p.getAppearance().setInvisible(false);
							p.getUpdateFlags().setAppearanceUpdateRequired(true);
							p.removeTemporaryAttribute("cantDoAnything");
							p.removeTemporaryAttribute("unmovable");
							teleportToWaitingRoom(p, false);
							p.getActionSender().closeInterfaces();
							p.getActionSender().setMinimapStatus(0);
							p.getActionSender().clearMapFlag();
						}
					});
					return;
				}
				for (int i = 0; i < ORB_COORDINATES[0].length; i++) {
					if (button == ORB_COORDINATES[0][i]) {
						final int j = i;
						p.setTemporaryAttribute("teleporting", true);
						World.getInstance().registerEvent(new Event(500) {

							@Override
							public void execute() {
								this.stop();
								p.teleport(Location.location(ORB_COORDINATES[1][j], ORB_COORDINATES[2][j], 0));
								Teleport.resetTeleport(p);
							}
						});
						return;
					}
				}
			}
		}
	}

	private void sendNPCMessage(Player p, String string) {
		p.getActionSender().sendNPCHead(2618, 241, 1);
		p.getActionSender().modifyText("TzHaar-Mej-Kah", 241, 3);
		p.getActionSender().modifyText(string, 241, 4);
		p.getActionSender().animateInterface(9827, 241, 1);
		p.getActionSender().sendChatboxInterface2(241);
	}

	private void resetVariables(Player p) {
		p.getSettings().setSkullCycles(0);
		p.getSpecialAttack().resetSpecial();
		p.setLastkiller(null);
		p.setDead(false);
		p.setEntityFocus(65535);
		p.setPoisonAmount(0);
		p.clearKillersHits();
		p.getSettings().setLastVengeanceTime(0);
		p.getSettings().setVengeance(false);
		p.removeTemporaryAttribute("willDie");
		p.setFrozen(false);
		p.removeTemporaryAttribute("unmovable");
		p.getSettings().setAntifireCycles(0);
		p.getSettings().setSuperAntipoisonCycles(0);
		p.getSettings().setTeleblockTime(0);
		p.removeTemporaryAttribute("teleblocked");
		p.setTarget(null);
		p.setAttacker(null);
		p.setHp(p.getMaxHp());
		for (int i = 0; i < Skills.SKILL_COUNT; i ++) {
			p.getLevels().setLevel(i, p.getLevels().getLevelForXp(i));
		}
		p.getActionSender().sendSkillLevels();
		Prayer.deactivateAllPrayers(p);
		if (p.getTemporaryAttribute("cantDoAnything") != null) {
			p.getAppearance().setInvisible(false);
			p.getUpdateFlags().setAppearanceUpdateRequired(true);
			p.removeTemporaryAttribute("cantDoAnything");
			p.removeTemporaryAttribute("unmovable");
			teleportToWaitingRoom(p, false);
			p.getActionSender().closeInterfaces();
			p.getActionSender().setMinimapStatus(0);
			p.getActionSender().clearMapFlag();
		}
	}

	private Location getRandomTeleport() {
		switch(Misc.random(4)) {
			case 0: return Location.location(2384 + Misc.random(29), 5133 + Misc.random(4), 0);
			case 1: return Location.location(2410 + Misc.random(4), 5140 + Misc.random(18), 0);
			case 2: return Location.location(2392 + Misc.random(11), 5141 + Misc.random(26), 0);
			case 3: return Location.location(2383 + Misc.random(3), 5141 + Misc.random(15), 0);
			case 4: return Location.location(2392 + Misc.random(12), 5145 + Misc.random(20), 0);
		}
		return null;
	}
	
	public void displayFightPitsInterface(Player p) {
		if (p.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		p.getActionSender().sendConfig(560, playersPlaying.size() - 1);
		p.getActionSender().modifyText("Current champion: " + getLastWinnerName(), 373, 0);
		p.getActionSender().sendOverlay(373);
	}
	
	public void teleportToWaitingRoom(Player p, boolean removeFromList) {
		int x = 2395 + Misc.random(8);
		int y = 5170 + Misc.random(3);
		if (x == 2399 && y == 5172) { // On viewing orb
			x++; // Move to the side of it
		}
		p.teleport(Location.location(x, y, 0));
		resetVariables(p);
		if (removeFromList) {
			playersPlaying.remove(p);
			if (playersPlaying.size() >= 1) {
				for (Player player : playersPlaying) {
					displayFightPitsInterface(player);
				}
			}
		}
	}

	public void useDoor(final Player p, int doorId) {
		final boolean running =  p.getWalkingQueue().isRunToggled();
		if (doorId == 9369) { // main entrance
			final int y = p.getLocation().getY() >= 5177 ? 5177 : 5175;
			World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(2399, y, 0)) {
	
				@Override
				public void run() {
					p.getWalkingQueue().setRunToggled(false);
					p.getWalkingQueue().reset();
					p.getActionSender().clearMapFlag();
					p.setTemporaryAttribute("unmovable", true);
					p.getWalkingQueue().forceWalk(0, y == 5177 ? -2 : +2);
					World.getInstance().registerEvent(new Event(1000) {

						@Override
						public void execute() {
							this.stop();
							p.removeTemporaryAttribute("unmovable");
							p.getWalkingQueue().setRunToggled(running);
						}
						
					});
				}
			});
		} else if (doorId == 9368) { // game door
			final int y = p.getLocation().getY() >= 5169 ? 5169 : 5167;
			World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(2399, y, 0)) {
				
				@Override
				public void run() {
					if (playersPlaying.size() == 1) {
						sendNPCMessage(p, "Here is some TokKul, as a reward.");
						p.getInventory().addItemOrGround(6529, tokkulReward);
					}
					removePlayingPlayer(p);
					p.getWalkingQueue().setRunToggled(false);
					p.getWalkingQueue().reset();
					p.getActionSender().clearMapFlag();
					if (y == 5167) {
						p.getWalkingQueue().forceWalk(0, +2);
					} else {
						p.getActionSender().sendMessage("You are unable to bypass the hot barrier.");
						return;
					}
					p.setTemporaryAttribute("unmovable", true);
					World.getInstance().registerEvent(new Event(1000) {

						@Override
						public void execute() {
							this.stop();
							p.removeTemporaryAttribute("unmovable");
							p.getWalkingQueue().setRunToggled(running);
							p.getActionSender().sendMessage("You leave the fight pit.");
							resetVariables(p);
						}
						
					});
				}
			});
		}
	}
	
	public boolean hasGameStarted() {
		return gameStarted;
	}
	
	private String getLastWinnerName() {
		if (lastWinner == null) {
			return "-";
		}
		return lastWinner.getPlayerDetails().getDisplayName();
	}
	
	public void addWaitingPlayer(Player p) {
		synchronized(playersWaiting) {
			playersWaiting.add(p);
		}
	}
	
	public void removeWaitingPlayer(Player p) {
		synchronized(playersWaiting) {
			playersWaiting.remove(p);
		}
	}
	
	public void addPlayingPlayer(Player p) {
		synchronized(playersPlaying) {
			playersPlaying.add(p);
		}
	}
	
	public void removePlayingPlayer(Player p) {
		synchronized(playersPlaying) {
			playersPlaying.remove(p);
		}
	}
}
