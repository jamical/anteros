package com.anteros.content.areas;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.ShopSession;

public class TzHaar {

	public TzHaar() {
		
	}
	
	public static void exitTzhaar(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2479, 5175, 2481, 5175) {

			@Override
			public void run() {
				p.teleport(Location.location(2866, 9571, 0));
			}
		});
	}
	
	public static void enterTzhaar(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2866, 9570, 2866, 9572) {

			@Override
			public void run() {
				p.teleport(Location.location(2480, 5175, 0));
			}
		});
	}
	
	public static void useTzhaarBanker(Player p, int option) {
		
	}
	
	public static boolean interactTzhaarNPC(final Player p, final NPC n, final int option) {
		if (n.getId() != 2622 && n.getId() != 2620 && n.getId() != 2623 && n.getId() != 2619 && n.getId() != 2617 && n.getId() != 2618) {
			return false;
		}
		p.setEntityFocus(n.getClientIndex());
		int npcX = n.getLocation().getX();
		int npcY = n.getLocation().getY();
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, npcX-1, npcY-1, npcX+1, npcY+1) {

			@Override
			public void run() {
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				switch(n.getId()) {
					case 2619: // Bankers
						if (option == 1) { // Talk
							
						} else if (option == 2) { // Bank
							p.getBank().openBank();
						} else if (option == 3) { // Collect
							
						}
						break;
				
						//TODO tzhaar stores
					case 2622: // Ore shop
						if (option == 1) { // Speak
							
						} else if (option == 2) { // Trade
							p.setShopSession(new ShopSession(p, 3));
						}
						break;
				}
			}
		});
		return true;
	}
}
