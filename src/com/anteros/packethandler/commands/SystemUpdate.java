package com.anteros.packethandler.commands;

import com.anteros.content.events.SystemUpdateEvent;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class SystemUpdate implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
		if (!World.getInstance().isUpdateInProgress()) {
			World.getInstance().registerEvent(new SystemUpdateEvent());
			World.getInstance().setUpdateInProgress(true);
		}
		}
	}

	@Override
	public int minimumRightsNeeded() {
		return 2;
	}

}
