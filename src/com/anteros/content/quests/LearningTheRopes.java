package com.anteros.content.quests;

import com.anteros.content.CharacterDesign;
import com.anteros.content.Dialogue;
import com.anteros.content.minigames.fightcave.FightCaveSession;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class LearningTheRopes {
	private static int startX = 2524;
	private static int startY = 5000;
	
	private static int sirVantReadyStance = 7938;
	private static int sirVantHangingSword = 7939;
	private static int sirVantNoSword = 7940;
	private static int sirVantPaper = 7941;
	private static int sirVantSitting = 7942;
	private static int dragon1 = 7943;
	private static int dragon2 = 7944;
	private static int dragon3 = 7945;
	private static int dragonLHDead = 7946;
	private static int dragonLRHDead = 7947;
	private static int dragonLRHDeadRFace = 7948;
	
	public static void configureAppearance(Player p) {
		CharacterDesign.open(p);
		p.getActionSender().modifyText("Creating your Character", 769, 0);
		p.getActionSender().modifyText("Before your adventure begins, you should take some time to set your mouse " +
				"settings and give your character its very own look. Take your time and choose " +
				"from the option above, then confirm your changes.", 769, 1);
		p.getActionSender().sendChatboxInterface2(769);
		p.firstLogin = false;
	}
	
	public static void startArrows(Player p) {
		p.getActionSender().setArrowOnEntity(1, 7938);
	}
	
	private static void showCutscene(Player p) {
		if (p.learningTheRopesInt == 0) {
			//TODO cutscene 1
			p.getActionSender().modifyText("Cutscenes", 769, 0);
			p.getActionSender().modifyText("There are moments in RuneScape that you cannot change what is going on - " +
					"you must simply watch to see what unfolds. Some call these 'cutscenes'. Watch " +
					"the knight fight the dragon in this cutscene.", 769, 1);
			p.getActionSender().sendChatboxInterface2(769);
		}
	}
	
	public static void interactWithSirVant(final Player p, final NPC n) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				if (p.learningTheRopesInt == 0) {
					showChats(p, 151);
				}
			}
		});
	}
	
	public static void showChats(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {
		case 151:
			if (p.learningTheRopesInt == 0) {
				p.getActionSender().sendNpcChat3("My word! You scared me there, friend. I have no idea", "where you came from, but you have fantastic timing.", "You see, I have come across a dragon.", sirVantHangingSword, "Sir Vant", Dialogue.CALM_TALK_BARELY_MOVING);
				newStatus = 152;
			}
			break;
		case 152:
			p.getActionSender().sendPlayerChat1("A dragon?", Dialogue.DISBELIEF);
			newStatus = 153;
			break;
		case 153:
			p.getActionSender().sendNpcChat4("Oh yes. It's not usually a problem. After all I am a", "White Knight! I just haven't really had time to prepare:", "food, runes, legions of other White Knights - those sorts", "of things.", sirVantHangingSword, "Sir Vant", Dialogue.SAD);
			newStatus = 154;
			break;
		case 154:
			p.getActionSender().sendPlayerChat1("You didn't prepare for facing a dragon?", Dialogue.THINKING);
			newStatus = 155;
			break;
		case 155:
			p.getActionSender().sendNpcChat4("It's not as if I woke up with a dragon-slaying urge! I", "was on my way through Lumbridge when it started", "attacking. It's a rather large three-headed variant. Very", "rare.", sirVantHangingSword, "Sir Vant", Dialogue.DEFAULT_HAPPY);
			newStatus = 156;
			break;
		case 156:
			p.getActionSender().sendPlayerChat1("So, what can I do?", Dialogue.THINKING);
			newStatus = 157;
			break;
		case 157:
			p.getActionSender().sendNpcChat1("You can keep back - it's returning!", sirVantHangingSword, "Sir Vant", Dialogue.DEFAULT_HAPPY);
			newStatus = 158;
		case 158:
			showCutscene(p);
			break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}

}
