package com.anteros.io;

import com.anteros.Constants;
import com.anteros.model.player.Player;

/**
 * Player load result.
 * @author Graham
 *
 */
public class PlayerLoadResult {
	
	public int returnCode = Constants.ReturnCodes.LOGIN_OK;
	public Player player = null;

}
