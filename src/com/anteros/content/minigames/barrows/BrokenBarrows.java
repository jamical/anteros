package com.anteros.content.minigames.barrows;

import java.text.NumberFormat;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.ShopSession;

public class BrokenBarrows {

	public BrokenBarrows() {
		
	}
	private static final int BOB_ID = 519;
	private static final Object[][] DATA = {
		// Fixed id, broken id, cost, name
		{4753, 4980, 60000, "Verac helm"},
		{4755, 4986, 100000, "Verac flail"},
		{4757, 4992, 90000, "Verac brassard"},
		{4759, 4998, 80000, "Verac plateskirt"},
		{4745, 4956, 60000, "Torag helm"},
		{4747, 4962, 100000, "Torag hammers"},
		{4749, 4968, 90000, "Torag platebody"},
		{4751, 4974, 80000, "Torag platelegs"},
		{4716, 4884, 60000, "Dharok helm"},
		{4718, 4890, 100000, "Dharok greataxe"},
		{4720, 4896, 90000, "Dharok platebody"},
		{4722, 4902, 80000, "Dharok platelegs"},
		{4708, 4860, 60000, "Ahrim hood"},
		{4710, 4866, 100000, "Ahrim staff"},
		{4712, 4872, 90000, "Ahrim robetop"},
		{4714, 4878, 80000, "Ahrim robeskirt"},
		{4732, 4932, 60000, "Karil coif"},
		{4734, 4938, 100000, "Karil crossbow"},
		{4736, 4944, 90000, "Karil leathertop"},
		{4738, 4950, 80000, "Karil leatherskirt"},
		{4724, 4908, 60000, "Guthan helm"},
		{4726, 4914, 100000, "Guthan warspear"},
		{4728, 4920, 90000, "Guthan platebody"},
		{4730, 4926, 80000, "Guthan chainskirt"},
	};
	
	public static int getBrokenId(int fixedId) {
		for (int i = 0; i < DATA.length; i++) {
			if (fixedId == (Integer)DATA[i][0]) {
				return (Integer)DATA[i][1];
			}
		}
		return fixedId;
	}
	
	private static int getIndex(int item) {
		for (int i = 0; i < DATA.length; i++) {
			if (item == (Integer)DATA[i][1] || item == (Integer)DATA[i][0]) {
				return i;
			}
		}
		return -1;
	}

	public static void talkToBob(final Player p, final NPC npc, final int item, final int option) {
		p.setEntityFocus(npc.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, npc.getLocation().getX() - 1, npc.getLocation().getY() - 1, npc.getLocation().getX() + 1, npc.getLocation().getY() + 1) {

			@Override
			public void run() {
				npc.setFaceLocation(p.getLocation());
				p.setFaceLocation(npc.getLocation());
				p.setEntityFocus(65535);
				if (option == 0) { // use item on bob
					if (item > 0) {
						p.setTemporaryAttribute("bobsAxesBarrowItem", item);
						showBobDialogue(p, 101);
					}
				} else if (option == 1) { // talk
					showBobDialogue(p, 107);
				} else if (option == 2) { // trade
					p.setShopSession(new ShopSession(p, 4));
				}
			}
		});
	}
	
	public static void showBobDialogue(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		int index = -1;
		int newStatus = -1;
		if (p.getTemporaryAttribute("bobsAxesBarrowItem") != null) {
			int item = (Integer)p.getTemporaryAttribute("bobsAxesBarrowItem");
			index = getIndex(item);
			if (index == -1) {
				return;
			} else if (item == (Integer)DATA[index][0]) {
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("That item isn't broken..", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				return;
			} else if (item != (Integer)DATA[index][1]) {
				return;
			}
		}
		switch(status) {
			case 101:
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("That'll cost you " + NumberFormat.getInstance().format((Integer)DATA[index][2]) + " gold coins to fix, are you sure?", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 102;
				break;
				
			case 102:
				p.getActionSender().modifyText("Yes, I'm sure!", 557, 2);
				p.getActionSender().modifyText("On second thoughts, no thanks.", 557, 3);
				p.getActionSender().sendChatboxInterface2(557);
				newStatus = 103;
				break;
				
			case 103:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Yes, I'm sure!", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 104;
				break;
				
			case 104:
				if (!p.getInventory().hasItemAmount(995, (Integer)DATA[index][2])) {
					p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
					p.getActionSender().modifyText("Bob", 241, 3);
					p.getActionSender().modifyText("You don't have enough money to pay for the repair!", 241, 4);
					p.getActionSender().animateInterface(9827, 241, 2);
					p.getActionSender().sendChatboxInterface2(241);
					break;
				} else {
					if (!p.getInventory().hasItem((Integer)DATA[index][1])) {
						p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
						p.getActionSender().modifyText("Bob", 241, 3);
						p.getActionSender().modifyText("The item seems to have gone from your inventory.", 241, 4);
						p.getActionSender().animateInterface(9827, 241, 2);
						p.getActionSender().sendChatboxInterface2(241);
						break;
					} else if (p.getInventory().deleteItem(995, (Integer)DATA[index][2])) {
						p.getInventory().replaceSingleItem((Integer)DATA[index][1], (Integer)DATA[index][0]);
						p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
						p.getActionSender().modifyText("Bob", 241, 3);
						p.getActionSender().modifyText("There you go, happy doing business with you!", 241, 4);
						p.getActionSender().animateInterface(9827, 241, 2);
						p.getActionSender().sendChatboxInterface2(241);
						p.getActionSender().sendMessage("You pay Bob his fee and he repairs your " + (String)DATA[index][3] + ".");
						break;
					}
				}
				break;
				
			case 105:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("On second thoughts, no thanks.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 106;
				break;
				
			case 106:
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("Ok, but don't expect my prices to change anytime soon!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 107:
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("Hello there " + p.getPlayerDetails().getDisplayName() + ", what can I do for you?", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 108;
				break;
				
			case 108:
				p.getActionSender().modifyText("Could you please repair my Barrows item?", 230, 2);
				p.getActionSender().modifyText("I'm interested in buying an axe.", 230, 3);
				p.getActionSender().modifyText("Nevermind.", 230, 4);
				p.getActionSender().sendChatboxInterface2(230);
				newStatus = 109;
				break;
				
			case 109:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you please repair my Barrows item?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 110;
				break;
				
			case 110:
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("Certainly! Show me the item and I'll see what I can do.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				break;
				
			case 111:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("I'm interested in buying an axe.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 112;
				break;
				
			case 112:
				p.getActionSender().sendNPCHead(BOB_ID, 241, 2);
				p.getActionSender().modifyText("Bob", 241, 3);
				p.getActionSender().modifyText("What a coincidence! Axes are my speciality!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 113;
				break;
				
			case 113: //open bob's shop
				p.setShopSession(new ShopSession(p, 4)); //bob's shop
				break;
				
			case 114:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Nevermind.", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}
