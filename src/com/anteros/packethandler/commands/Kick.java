package com.anteros.packethandler.commands;

import com.anteros.content.skills.fishing.Barbarian;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Kick implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
		String cmd[] = command.split(" ");
		cmd[1].toLowerCase();
		for (Player p : World.getInstance().getPlayerList()) {
			if (p != null) {
				if (p.getUsername().toLowerCase().equalsIgnoreCase(cmd[1])) {
					p.getActionSender().logout();
				}
			}
		}
	}
	}

	@Override
	public int minimumRightsNeeded() {
		return 1;
	}

}
