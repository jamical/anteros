package com.anteros.content.music;

import com.anteros.model.player.Skills;
import com.anteros.util.Misc;

public class MusicData {
	/*
	 * Our last music id for revision 530 is 633
	 */
	protected static int RandomMusic[]	= {
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
		61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
		81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
		101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116,
		117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132,
		133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148,
		149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
		165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
		181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196,
		197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212,
		213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228,
		229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244,
		245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260,
		261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276,
		277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292,
		293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308,
		309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324,
		325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340,
		341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356,
		357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372,
		373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388,
		389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404,
		405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420,
		421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436,
		437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452,
		453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468,
		469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484,
		485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500,
		501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516,
		517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532,
		533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548,
		549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564,
		565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580,
		581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596,
		597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612,
		613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628,
		629, 630, 631, 632, 633, 
	};
	
	public static int levelUpMusic(int skill) {
		int rnd = Misc.random(1);
		switch (skill) {
		case Skills.AGILITY:
			return 28;
		case Skills.ATTACK:
			return 29;
		case Skills.CONSTRUCTION:
			return 31;
		case Skills.COOKING:
			return 33;
		case Skills.CRAFTING:
			return 35;
		case Skills.DEFENCE:
			return 37;
		case Skills.FARMING:
			return 11;
		case Skills.FIREMAKING:
			return 39;
		case Skills.FISHING:
			return 41;
		case Skills.FLETCHING:
			return 44;
		case Skills.HERBLORE:
			return 45;
		case Skills.HITPOINTS:
			return 48;
		case Skills.HUNTER:
			return 49;
		case Skills.MAGIC:
			if (rnd == 0) {
				return 51;
			} else {
				return 52;
			}
		case Skills.MINING:
			return 53;
		case Skills.PRAYER:
			return 55;
		case Skills.RANGE:
			return 57;
		case Skills.RUNECRAFTING:
			return 60;
		case Skills.SLAYER:
			return 62;
		case Skills.SMITHING: //TODO find the real tune for this
			return 43;
		case Skills.STRENGTH:
			return 65;
		case Skills.SUMMONING: //TODO find the real tune for this
			return 22; 
		case Skills.THIEVING: //TODO find the real tune for this
			return 34;
		case Skills.WOODCUTTING:
			return 69;
			
			
		}
		return skill;
	}
	
	public static String getTrackNames(int id) {
		switch (id) {
		case 0: 
			 return "Scape Main";
			case 1:
			 return "Iban";
			case 2:
			 return "Autumn Voyage";
			case 3:
			 return "Unknown Land";
			case 4:
			 return "Report ID: 1029455878";
			case 5:
			 return "Sad Meadow";
			case 6:
			 return "Report ID: -1154441378";
			case 7:
			 return "Overture";
			case 8:
			 return "Wildwood";
			case 9:
			 return "Kingdom";
			case 10:
			 return "Moody";
			case 11:
			 return "Spooky2";
			case 12:
			 return "Long Way Home";
			case 13:
			 return "Mage Arena";
			case 14:
			 return "Witching";
			case 15:
			 return "Workshop";
			case 16:
			 return "Report ID: 627705588";
			case 17:
			 return "Escape";
			case 18:
			 return "Horizon";
			case 19:
			 return "Arabique";
			case 20:
			 return "Lullaby";
			case 21:
			 return "Monarch Waltz";
			case 22:
			 return "Gnome King";
			case 23:
			 return "Fe Fi Fo Fum";
			case 24:
			 return "Attack1";
			case 25:
			 return "Attack2";
			case 26:
			 return "Attack3";
			case 27:
			 return "Attack4";
			case 28:
			 return "Attack5";
			case 29:
			 return "Attack6";
			case 30:
			 return "Voodoo Cult";
			case 31:
			 return "Report ID: 1984386383";
			case 32:
			 return "Voyage";
			case 33:
			 return "Gnome Village";
			case 34:
			 return "Wonder";
			case 35:
			 return "Sea Shanty2";
			case 36:
			 return "Arabian";
			case 37:
			 return "Deep Wildy";
			case 38:
			 return "Trawler";
			case 39:
			 return "Report ID: 345324413";
			case 40:
			 return "Report ID: 345324414";
			case 41:
			 return "Expecting";
			case 42:
			 return "Wilderness2";
			case 43:
			 return "Wilderness3";
			case 44:
			 return "Right On Track";
			case 45:
			 return "Venture2";
			case 46:
			 return "Harmony2";
			case 47:
			 return "Duel Arena";
			case 48:
			 return "Morytania";
			case 49:
			 return "Wander";
			case 50:
			 return "Al Kharid";
			case 51:
			 return "Trawler Minor";
			case 52:
			 return "Serene";
			case 53:
			 return "Royale";
			case 54:
			 return "Scape Soft";
			case 55:
			 return "High Seas";
			case 56:
			 return "Doorways";
			case 57:
			 return "Rune Essence";
			case 58:
			 return "Nomad";
			case 59:
			 return "Cursed";
			case 60:
			 return "Lasting";
			case 61:
			 return "Village";
			case 62:
			 return "Newbie Melody";
			case 63:
			 return "Chain Of Command";
			case 64:
			 return "Book Of Spells";
			case 65:
			 return "Miracle Dance";
			case 66:
			 return "Legion";
			case 67:
			 return "Close Quarters";
			case 68:
			 return "Cavern";
			case 69:
			 return "Egypt";
			case 70:
			 return "Upcoming";
			case 71:
			 return "Chompy Hunt";
			case 72:
			 return "Fanfare";
			case 73:
			 return "Report ID: -440204630";
			case 74:
			 return "Lightwalk";
			case 75:
			 return "Venture";
			case 76:
			 return "Harmony";
			case 77:
			 return "Splendour";
			case 78:
			 return "Reggae";
			case 79:
			 return "Report ID: 907815588";
			case 80:
			 return "Soundscape";
			case 81:
			 return "Wonderous";
			case 82:
			 return "Waterfall";
			case 83:
			 return "Big Chords";
			case 84:
			 return "Dead Quiet";
			case 85:
			 return "Vision";
			case 86:
			 return "Dimension X";
			case 87:
			 return "Ice Melody";
			case 88:
			 return "Twilight";
			case 89:
			 return "Reggae2";
			case 90:
			 return "Ambient Jungle";
			case 91:
			 return "Riverside";
			case 92:
			 return "Sea Shanty";
			case 93:
			 return "Parade";
			case 94:
			 return "Tribal2";
			case 95:
			 return "Intrepid";
			case 96:
			 return "Inspiration";
			case 97:
			 return "Hermit";
			case 98:
			 return "Forever";
			case 99:
			 return "Baroque";
			case 100:
			 return "Beyond";
			case 101:
			 return "Gnome Village2";
			case 102:
			 return "Alone";
			case 103:
			 return "Oriental";
			case 104:
			 return "Camelot";
			case 105:
			 return "Tomorrow";
			case 106:
			 return "Expanse";
			case 107:
			 return "Miles Away";
			case 108:
			 return "Starlight";
			case 109:
			 return "Theme";
			case 110:
			 return "Serenade";
			case 111:
			 return "Still Night";
			case 112:
			 return "Gnomeball";
			case 113:
			 return "Lightness";
			case 114:
			 return "Jungly1";
			case 115:
			 return "Jungly2";
			case 116:
			 return "Greatness";
			case 117:
			 return "Jungly3";
			case 118:
			 return "Faerie";
			case 119:
			 return "Fishing";
			case 120:
			 return "Shining";
			case 121:
			 return "Forbidden";
			case 122:
			 return "Shine";
			case 123:
			 return "Arabian2";
			case 124:
			 return "Arabian3";
			case 125:
			 return "Garden";
			case 126:
			 return "We Are The Fairies";
			case 127:
			 return "Nightfall";
			case 128:
			 return "Grumpy";
			case 129:
			 return "Spookyjungle";
			case 130:
			 return "Tree Spirits";
			case 131:
			 return "Understanding";
			case 132:
			 return "Breeze";
			case 133:
			 return "Report ID: 2122572442";
			case 134:
			 return "La Mort";
			case 135:
			 return "Report ID: 1642101044";
			case 136:
			 return "Report ID: 1642101045";
			case 137:
			 return "Report ID: 1642101046";
			case 138:
			 return "Emperor";
			case 139:
			 return "Report ID: -942142538";
			case 140:
			 return "Talking Forest";
			case 141:
			 return "Barbarianism";
			case 142:
			 return "Complication";
			case 143:
			 return "Down To Earth";
			case 144:
			 return "Scape Cave";
			case 145:
			 return "Yesteryear";
			case 146:
			 return "Zealot";
			case 147:
			 return "Silence";
			case 148:
			 return "Emotion";
			case 149:
			 return "Principality";
			case 150:
			 return "Mouse Trap";
			case 151:
			 return "Start";
			case 152:
			 return "Ballad Of Enchantment";
			case 153:
			 return "Expedition";
			case 154:
			 return "Bone Dance";
			case 155:
			 return "Neverland";
			case 156:
			 return "Mausoleum";
			case 157:
			 return "Medieval";
			case 158:
			 return "Quest";
			case 159:
			 return "Gaol";
			case 160:
			 return "Army Of Darkness";
			case 161:
			 return "Long Ago";
			case 162:
			 return "Tribal Background";
			case 163:
			 return "Flute Salad";
			case 164:
			 return "Landlubber";
			case 165:
			 return "Tribal";
			case 166:
			 return "Fanfare2";
			case 167:
			 return "Fanfare3";
			case 168:
			 return "Lonesome";
			case 169:
			 return "Crystal Sword";
			case 170:
			 return "Report ID: 1339486127";
			case 171:
			 return "Null";
			case 172:
			 return "Jungle Island";
			case 173:
			 return "Dunjun";
			case 174:
			 return "Desert Voyage";
			case 175:
			 return "Spirit";
			case 176:
			 return "Undercurrent";
			case 177:
			 return "Adventure";
			case 178:
			 return "Courage";
			case 179:
			 return "Underground";
			case 180:
			 return "Attention";
			case 181:
			 return "Crystal Cave";
			case 182:
			 return "Dangerous";
			case 183:
			 return "Troubled";
			case 184:
			 return "Magical Journey";
			case 185:
			 return "Magic Dance";
			case 186:
			 return "Arrival";
			case 187:
			 return "Tremble";
			case 188:
			 return "In The Manor";
			case 189:
			 return "Land Of Snow";
			case 190:
			 return "Heart And Mind";
			case 191:
			 return "Knightly";
			case 192:
			 return "Trinity";
			case 193:
			 return "Mellow";
			case 194:
			 return "Brimstail's Scales";
			case 195:
			 return "Report ID: -1349658313";
			case 196:
			 return "Report ID: -1842191205";
			case 197:
			 return "Lament Of Meiyerditch";
			case 198:
			 return "Dagannoth Dawn";
			case 199:
			 return "Report ID: -645078965";
			case 200:
			 return "Report ID: 2142215577";
			case 201:
			 return "Slug A Bug Ball";
			case 202:
			 return "Prime Time";
			case 203:
			 return "Report ID: 1381363755";
			case 204:
			 return "Roc And Roll";
			case 205:
			 return "High Spirits";
			case 206:
			 return "Floating Free";
			case 207:
			 return "Scape Hunter";
			case 208:
			 return "Jungle Island Xmas";
			case 209:
			 return "Jungle Bells";
			case 210:
			 return "Sea Shanty Xmas";
			case 211:
			 return "Pirates Of Penance";
			case 212:
			 return "Report ID: -1057729269";
			case 213:
			 return "Labyrinth";
			case 214:
			 return "Undead Dungeon";
			case 215:
			 return "Report ID: -1304031901";
			case 216:
			 return "Espionage";
			case 217:
			 return "Have An Ice Day";
			case 218:
			 return "Report ID: 1586342879";
			case 219:
			 return "Report ID: -1042007977";
			case 220:
			 return "Island Of The Trolls";
			case 221:
			 return "Jester Minute";
			case 222:
			 return "Major Miner";
			case 223:
			 return "Norse Code";
			case 224:
			 return "Ogre The Top";
			case 225:
			 return "Volcanic Vikings";
			case 226:
			 return "Report ID: 1263256805";
			case 227:
			 return "Report ID: -1061250740";
			case 228:
			 return "Garden Of Autumn";
			case 229:
			 return "Garden Of Spring";
			case 230:
			 return "Garden Of Summer";
			case 231:
			 return "Garden Of Winter";
			case 232:
			 return "Report ID: 1710400588";
			case 233:
			 return "Report ID: -490773228";
			case 234:
			 return "Creature Cruelty";
			case 235:
			 return "Magic Magic Magic";
			case 236:
			 return "Mutant Medley";
			case 237:
			 return "Work Work Work";
			case 238:
			 return "Zombiism";
			case 239:
			 return "Report ID: 1427539525";
			case 240:
			 return "Report ID: 346263512";
			case 241:
			 return "Stagnant";
			case 242:
			 return "Time Out";
			case 243:
			 return "Stratosphere";
			case 244:
			 return "Waterlogged";
			case 245:
			 return "Natural";
			case 246:
			 return "Grotto";
			case 247:
			 return "Artistry";
			case 248:
			 return "Aztec";
			case 249:
			 return "Report ID: 346288985";
			case 250:
			 return "Report ID: -1793433037";
			case 251:
			 return "Forest";
			case 252:
			 return "Elven Mist";
			case 253:
			 return "Lost Soul";
			case 254:
			 return "Meridian";
			case 255:
			 return "Woodland";
			case 256:
			 return "Overpass";
			case 257:
			 return "Sojourn";
			case 258:
			 return "Contest";
			case 259:
			 return "Crystal Castle";
			case 260:
			 return "Insect Queen";
			case 261:
			 return "Marzipan";
			case 262:
			 return "Righteousness";
			case 263:
			 return "Bandit Camp";
			case 264:
			 return "Mad Eadgar";
			case 265:
			 return "Superstition";
			case 266:
			 return "Bone Dry";
			case 267:
			 return "Sunburn";
			case 268:
			 return "Everywhere";
			case 269:
			 return "Competition";
			case 270:
			 return "Exposed";
			case 271:
			 return "Well Of Voyage";
			case 272:
			 return "Alternative Root";
			case 273:
			 return "Easter Jig";
			case 274:
			 return "Rising Damp";
			case 275:
			 return "Slice Of Station";
			case 276:
			 return "Report ID: -628963539";
			case 277:
			 return "Haunted Mine";
			case 278:
			 return "Deep Down";
			case 279:
			 return "Report ID: -276138668";
			case 280:
			 return "Report ID: 615365769";
			case 281:
			 return "Sf_exst";
			case 282:
			 return "Chamber";
			case 283:
			 return "Everlasting";
			case 284:
			 return "Miscellania";
			case 285:
			 return "Etcetera";
			case 286:
			 return "Shadowland";
			case 287:
			 return "Lair";
			case 288:
			 return "Deadlands";
			case 289:
			 return "Rellekka";
			case 290:
			 return "Saga";
			case 291:
			 return "Borderland";
			case 292:
			 return "Stranded";
			case 293:
			 return "Legend";
			case 294:
			 return "Frostbite";
			case 295:
			 return "Warrior";
			case 296:
			 return "Technology";
			case 297:
			 return "Report ID: -1564148198";
			case 298:
			 return "Illusive";
			case 299:
			 return "Inadequacy";
			case 300:
			 return "Untouchable";
			case 301:
			 return "Down And Out";
			case 302:
			 return "On The Up";
			case 303:
			 return "Monkey Madness";
			case 304:
			 return "Marooned";
			case 305:
			 return "Anywhere";
			case 306:
			 return "Island Life";
			case 307:
			 return "Temple";
			case 308:
			 return "Suspicious";
			case 309:
			 return "Looking Back";
			case 310:
			 return "Dwarf Theme";
			case 311:
			 return "Showdown";
			case 312:
			 return "Find My Way";
			case 313:
			 return "Goblin Village";
			case 314:
			 return "Castlewars";
			case 315:
			 return "Report ID: -1010529595";
			case 316:
			 return "Report ID: -1479412376";
			case 317:
			 return "Melodrama";
			case 318:
			 return "Ready For Battle";
			case 319:
			 return "Stillness";
			case 320:
			 return "Lighthouse";
			case 321:
			 return "Scape Scared";
			case 322:
			 return "Out Of The Deep";
			case 323:
			 return "Report ID: 111485446";
			case 324:
			 return "Background";
			case 325:
			 return "Cave Background";
			case 326:
			 return "Dark";
			case 327:
			 return "Dream";
			case 328:
			 return "March";
			case 329:
			 return "Regal";
			case 330:
			 return "Cellar Song";
			case 331:
			 return "Scape Sad";
			case 332:
			 return "Scape Wild";
			case 333:
			 return "Spooky";
			case 334:
			 return "Pirates Of Peril";
			case 335:
			 return "Romancing The Crone";
			case 336:
			 return "Dangerous Road";
			case 337:
			 return "Faithless";
			case 338:
			 return "Tiptoe";
			case 339:
			 return "Report ID: 563269755";
			case 340:
			 return "Masquerade";
			case 341:
			 return "Report ID: 1343200077";
			case 342:
			 return "Body Parts";
			case 343:
			 return "Monster Melee";
			case 344:
			 return "Fenkenstrain's Refrain";
			case 345:
			 return "Barking Mad";
			case 346:
			 return "Goblin Game";
			case 347:
			 return "Fruits De Mer";
			case 348:
			 return "Narnode's Theme";
			case 349:
			 return "Impetuous";
			case 350:
			 return "Report ID: -711467112";
			case 351:
			 return "Dynasty";
			case 352:
			 return "Scarab";
			case 353:
			 return "Shipwrecked";
			case 354:
			 return "Phasmatys";
			case 355:
			 return "Report ID: -720253066";
			case 356:
			 return "Settlement";
			case 357:
			 return "Cave Of Beasts";
			case 358:
			 return "Dragontooth Island";
			case 359:
			 return "Sarcophagus";
			case 360:
			 return "Report ID: 1333935085";
			case 361:
			 return "Down Below";
			case 362:
			 return "Karamja Jam";
			case 363:
			 return "7th Realm";
			case 364:
			 return "Pathways";
			case 365:
			 return "Report ID: 1837251043";
			case 366:
			 return "Eagle Peak";
			case 367:
			 return "Barb Wire";
			case 368:
			 return "Report ID: -1573228346";
			case 369:
			 return "Time To Mine";
			case 370:
			 return "In Between";
			case 371:
			 return "Report ID: 1343649581";
			case 372:
			 return "Far Away";
			case 373:
			 return "Claustrophobia";
			case 374:
			 return "Knightmare";
			case 375:
			 return "Fight Or Flight";
			case 376:
			 return "Temple Of Light";
			case 377:
			 return "Report ID: 2110556093";
			case 378:
			 return "Forgotten";
			case 379:
			 return "Throne Of The Demon";
			case 380:
			 return "Dance Of The Undead";
			case 381:
			 return "Dangerous Way";
			case 382:
			 return "Lore And Order";
			case 383:
			 return "City Of The Dead";
			case 384:
			 return "Hypnotized";
			case 385:
			 return "Report ID: 1480073951";
			case 386:
			 return "Bandos Battalion";
			case 387:
			 return "Sphinx";
			case 388:
			 return "Mirage";
			case 389:
			 return "Cave Of The Goblins";
			case 390:
			 return "Romper Chomper";
			case 391:
			 return "Zamorak Zoo";
			case 392:
			 return "Zogre Dance";
			case 393:
			 return "Path Of Peril";
			case 394:
			 return "Wayward";
			case 395:
			 return "Tale Of Keldagrim";
			case 396:
			 return "Land Of The Dwarves";
			case 397:
			 return "Tears Of Guthix";
			case 398:
			 return "Report ID: 1267356434";
			case 399:
			 return "Armageddon";
			case 400:
			 return "Report ID: -43712789";
			case 401:
			 return "Report ID: -521895311";
			case 402:
			 return "Report ID: -1588113323";
			case 403:
			 return "Report ID: 375695247";
			case 404:
			 return "Armadyl Alliance";
			case 405:
			 return "Report ID: 155289058";
			case 406:
			 return "Report ID: 1498787169";
			case 407:
			 return "Report ID: 1326424637";
			case 408:
			 return "Strength Of Saradomin";
			case 409:
			 return "Frogland";
			case 410:
			 return "Report ID: 1921157304";
			case 411:
			 return "Report ID: 1364992651";
			case 412:
			 return "Into The Abyss";
			case 413:
			 return "Report ID: 196677638";
			case 414:
			 return "Report ID: 1426066149";
			case 415:
			 return "Report ID: -1642689926";
			case 416:
			 return "Healin' Feelin'";
			case 417:
			 return "Terrorbird Tussle";
			case 418:
			 return "Corporal Punishment";
			case 419:
			 return "Pheasant Peasant";
			case 420:
			 return "Report ID: 465278529";
			case 421:
			 return "Storeroom Shuffle";
			case 422:
			 return "Report ID: 1574786856";
			case 423:
			 return "Report ID: -2094237825";
			case 424:
			 return "Report ID: -1042911649";
			case 425:
			 return "Report ID: 881850881";
			case 426:
			 return "Waste Defaced";
			case 427:
			 return "Bolrie's Diary";
			case 428:
			 return "Altar Ego";
			case 429:
			 return "Roots And Flutes";
			case 430:
			 return "Report ID: 200221348";
			case 431:
			 return "Report ID: -1406116675";
			case 432:
			 return "Grimly Fiendish";
			case 433:
			 return "Dusk In Yu'biusk";
			case 434:
			 return "Have A Blast";
			case 435:
			 return "Wilderness";
			case 436:
			 return "Forgettable Melody";
			case 437:
			 return "Zanik's Theme";
			case 438:
			 return "Report ID: -2012159921";
			case 439:
			 return "Catacombs And Tombs";
			case 440:
			 return "Temple Of Tribes";
			case 441:
			 return "Tournament";
			case 442:
			 return "Report ID: 687938017";
			case 443:
			 return "Bounty Hunter Level 2";
			case 444:
			 return "Bounty Hunter Level 1";
			case 445:
			 return "Bounty Hunter Level 3";
			case 446:
			 return "Report ID: 1736514924";
			case 447:
			 return "Over To Nardah";
			case 448:
			 return "Report ID: 1250935993";
			case 449:
			 return "Wild Isle";
			case 450:
			 return "Venomous";
			case 451:
			 return "Tune From The Dune";
			case 452:
			 return "Copris Lunaris";
			case 453:
			 return "Jungle Hunt";
			case 454:
			 return "Home Sweet Home";
			case 455:
			 return "Scarabaeoidea";
			case 456:
			 return "Animal Apogee";
			case 457:
			 return "Scape Summon";
			case 458:
			 return "Report ID: -2049632553";
			case 459:
			 return "Report ID: -1916806250";
			case 460:
			 return "Joy Of The Hunt";
			case 461:
			 return "Report ID: 1915718129";
			case 462:
			 return "Spirits Of Elid";
			case 463:
			 return "Fire And Brimstone";
			case 464:
			 return "Report ID: 2110260221";
			case 465:
			 return "Desert Heat";
			case 466:
			 return "Ground Scape";
			case 467:
			 return "Report ID: -150357971";
			case 468:
			 return "Shaping Up";
			case 469:
			 return "In The Pits";
			case 470:
			 return "Strange Place";
			case 471:
			 return "Brew Hoo Hoo";
			case 472:
			 return "Spa Bizarre";
			case 473:
			 return "Report ID: -858121616";
			case 474:
			 return "Bish Bash Bosh";
			case 475:
			 return "Wild Side";
			case 476:
			 return "Dead Can Dance";
			case 477:
			 return "Report ID: 1174254130";
			case 478:
			 return "Report ID: 1705947058";
			case 479:
			 return "Jungle Troubles";
			case 480:
			 return "Report ID: 693567775";
			case 481:
			 return "Catch Me If You Can";
			case 482:
			 return "Rat A Tat Tat";
			case 483:
			 return "Jungle Community";
			case 484:
			 return "Report ID: -1825588455";
			case 485:
			 return "Report ID: -200702983";
			case 486:
			 return "Report ID: -650459553";
			case 487:
			 return "Charmin' Farmin'";
			case 488:
			 return "Report ID: -280100409";
			case 489:
			 return "Bubble And Squeak";
			case 490:
			 return "Sarim's Vermin";
			case 491:
			 return "Rat Hunt";
			case 492:
			 return "Exam Conditions";
			case 493:
			 return "Safety In Numbers";
			case 494:
			 return "Incarceration";
			case 495:
			 return "Report ID: -40155624";
			case 496:
			 return "Report ID: 1179379180";
			case 497:
			 return "Aye Car Rum Ba";
			case 498:
			 return "Blistering Barnacles";
			case 499:
			 return "Creepy";
			case 500:
			 return "A New Menace";
			case 501:
			 return "Distant Land";
			case 502:
			 return "Bittersweet Bunny";
			case 503:
			 return "Waiting For The Hunt";
			case 504:
			 return "Fangs For The Memory";
			case 505:
			 return "Report ID: 284435223";
			case 506:
			 return "Land Down Under";
			case 507:
			 return "Report ID: -329713253";
			case 508:
			 return "Meddling Kids";
			case 509:
			 return "Corridors Of Power";
			case 510:
			 return "Slither And Thither";
			case 511:
			 return "In The Clink";
			case 512:
			 return "Report ID: -1207444135";
			case 513:
			 return "Report ID: 1124871605";
			case 514:
			 return "Report ID: 1129072823";
			case 515:
			 return "Mudskipper Melody";
			case 516:
			 return "Report ID: 1124871606";
			case 517:
			 return "Subterranea";
			case 518:
			 return "Report ID: 300077565";
			case 519:
			 return "Incantation";
			case 520:
			 return "Grip Of The Talon";
			case 521:
			 return "Report ID: 1826933756";
			case 522:
			 return "Icy A Worried Gnome";
			case 523:
			 return "Icy Trouble Ahead";
			case 524:
			 return "Xenophobe";
			case 525:
			 return "Title Fight";
			case 526:
			 return "Arma Gonna Get You";
			case 527:
			 return "Dillo-gence Is Key";
			case 528:
			 return "Victory Is Mine";
			case 529:
			 return "Woe Of The Wyvern";
			case 530:
			 return "In The Brine";
			case 531:
			 return "Toktz-ket-ek-mack";
			case 532:
			 return "Diango's Little Helpers";
			case 533:
			 return "Roll The Bones";
			case 534:
			 return "Mind Over Matter";
			case 535:
			 return "Golden Touch";
			case 536:
			 return "Cool For Ali Cats";
			case 537:
			 return "Dogs Of War";
			case 538:
			 return "Report ID: -1683079062";
			case 539:
			 return "Under The Sand";
			case 540:
			 return "Desert Smoke";
			case 541:
			 return "Report ID: -926977577";
			case 542:
			 return "Lament";
			case 543:
			 return "Slain To Waste";
			case 544:
			 return "Making Waves";
			case 545:
			 return "Cabin Fever";
			case 546:
			 return "Last Stand";
			case 547:
			 return "Scape Santa";
			case 548:
			 return "Poles Apart";
			case 549:
			 return "Report ID: 760396927";
			case 550:
			 return "Something Fishy";
			case 551:
			 return "Report ID: 173246926";
			case 552:
			 return "Report ID: 1165343312";
			case 553:
			 return "Report ID: -1736543319";
			case 554:
			 return "Report ID: -1005698524";
			case 555:
			 return "Report ID: 1919110318";
			case 556:
			 return "Report ID: -1464738642";
			case 557:
			 return "Report ID: -1069601863";
			case 558:
			 return "Food For Thought";
			case 559:
			 return "Malady";
			case 560:
			 return "Dance Of Death";
			case 561:
			 return "Second Vision";
			case 562:
			 return "Circus";
			case 563:
			 return "Report ID: 405819542";
			case 564:
			 return "Report ID: -2125002578";
			case 565:
			 return "Wrath And Ruin";
			case 566:
			 return "Report ID: 1644982783";
			case 567:
			 return "Report ID: -1289131398";
			case 568:
			 return "Storm Brew";
			case 569:
			 return "Report ID: -1569181344";
			case 570:
			 return "Report ID: 364331367";
			case 571:
			 return "Report ID: 1084042962";
			case 572:
			 return "Report ID: 1927019011";
			case 573:
			 return "Report ID: -710515142";
			case 574:
			 return "Report ID: 1531147948";
			case 575:
			 return "Chickened Out";
			case 576:
			 return "Report ID: 851641665";
			case 577:
			 return "Mastermindless";
			case 578:
			 return "Report ID: 2063976492";
			case 579:
			 return "Report ID: -1908972024";
			case 580:
			 return "Report ID: -341747031";
			case 581:
			 return "Report ID: -228100280";
			case 582:
			 return "Too Many Cooks...";
			case 583:
			 return "Chef Surprize";
			case 584:
			 return "Report ID: -610897909";
			case 585:
			 return "Report ID: -726771179";
			case 586:
			 return "Everlasting Fire";
			case 587:
			 return "Null And Void";
			case 588:
			 return "Pest Control";
			case 589:
			 return "Report ID: 1640251258";
			case 590:
			 return "Report ID: -1967330545";
			case 591:
			 return "Tomb Raider";
			case 592:
			 return "Report ID: -586830894";
			case 593:
			 return "Report ID: 1371970793";
			case 594:
			 return "No Way Out";
			case 595:
			 return "Report ID: -2019857272";
			case 596:
			 return "Report ID: 219811040";
			case 597:
			 return "Report ID: 192519476";
			case 598:
			 return "Report ID: -1931522920";
			case 599:
			 return "Report ID: 157350490";
			case 600:
			 return "Method Of Madness";
			case 601:
			 return "Report ID: -638949674";
			case 602:
			 return "Fear And Loathing";
			case 603:
			 return "Funny Bunnies";
			case 604:
			 return "Assault And Battery";
			case 605:
			 return "Report ID: -1462273044";
			case 606:
			 return "Report ID: 907740319";
			case 607:
			 return "Report ID: 152537119";
			case 608:
			 return "Report ID: 421221421";
			case 609:
			 return "Report ID: 645359674";
			case 610:
			 return "Distillery Hilarity";
			case 611:
			 return "Trouble Brewing";
			case 612:
			 return "Head To Head";
			case 613:
			 return "Report ID: 495163692";
			case 614:
			 return "Pinball Wizard";
			case 615:
			 return "Beetle Juice";
			case 616:
			 return "Back To Life";
			case 617:
			 return "Report ID: 689804761";
			case 618:
			 return "Report ID: 1758764057";
			case 619:
			 return "Report ID: 1250412381";
			case 620:
			 return "Where Eagles Lair";
			case 621:
			 return "Homescape";
			case 622:
			 return "Waking Dream";
			case 623:
			 return "Dreamstate";
			case 624:
			 return "Report ID: -517202799";
			case 625:
			 return "Report ID: 673424924";
			case 626:
			 return "Way Of The Enchanter";
			case 627:
			 return "Isle Of Everywhere";
			case 628:
			 return "Report ID: -1819419169";
			case 629:
			 return "Report ID: -1811473240";
			case 630:
			 return "Report ID: 619237947";
			case 631:
			 return "Life's A Beach!";
			case 632:
			 return "Little Cave Of Horrors";
			case 633:
			 return "On The Wing";
		}
		return "TODO: Name for " + id;
	}
	
	
	 WorldData a = new WorldData();
	    /**
	     * takes area IDs as switch, and returns the song ID for that area
	     * @param area
	     * @return
	     */
	    public static int getSongID(int area) {
	    
	        switch(area){
	            case 1:
	                return 62;
	            case 2:
	                return 318;
	            case 3:
	            	return 381;
	            case 4:
	            	return 380;
	            case 5:
	            	return 96;
	            case 6:
	            	return 99;
	            case 7:
	            	return 98;
	            case 8:
	            	return 3;
	            case 9:
	            	return 587;
	            case 10:
	            	return 50;
	            case 11:
	                return 76;
	            case 12:
	            	return 72;
	            case 13:
	            	return 473;
	            case 14:
	            	//TODO find pirate music
	            case 15:
	            	return 141;
	            case 16:
	            	//TODO find abbys music
	            case 17:
	            	return 172;
	            case 18:
	            	//TODO find bandit camp music
	            case 19:
	            	return 66;
	            case 20:
	            	//TODO find bedibin camp music lol wut
	            case 21:
	            	//TODO find the song "morooned"
	            case 22:
	            	return 119;
	            case 23:
	            	return 87;
	            case 24:
	            	//TODO find burthrope music
	            case 25:
	            	return 104;
	            case 26:
	            	//TODO find canifis "village" music
	            case 27:
	            	//TODO find maze music (random
	            case 28:
	            	//TODO find cranador music lol
	            case 29:
	            	return 151;
	            case 30:
	            	return 47;
	            case 31:
	            	return 179;
	            case 32:
	            	return 150;
	            case 33:
	            	return 23;
	            case 34:
	            	//TODO find ham hideout music
	            case 35:
	            	return 114;
	            case 36:
	            	return 412;
	            case 37:
	            	//TODO find misc music
	            case 38:
	            	return 286;
	            case 39:
	            	//TODO find port Phasmatys music
	            case 40:
	            	return 35;
	            case 41:
	            	//TODO find long way home song
	            case 42:
	            	return 7;
	            case 43:
	            	return 90;
	            case 44:
	            	return 18;
	            case 45:
	            	return 23;
	            case 46:
	            	return 469;
	            case 47:
	            	return 125;
	            case 48:
	            	return 185;
	            case 49:
	            	return 314;
	            case 50:
	            	return 318;
	            case 51:
	            	return 318;
	            case 52:
	            	return 28;
	            case 53:
	            	//TODO find kalphite queen music
	            case 54:
	            	return 2;
	            case 55:
	            	return 111;
	            case 56:
	            	return 123;
	            case 57:
	            	return 36;
	            case 58:
	            	return 122;
	            case 59:
	            	return 541;
	            case 60:
	            	return 64;
	            case 61:
	            	return 327;
	            case 62:
	            	return 163;
	            case 63:
	            	return 333;
	            case 64:
	            	return 116;
	            case 65:
	            	return 157;
	            case 66:
	            	return 177;
	            case 67:
	            	return 93;
	            case 68:
	            	return 48;
	            case 69:
	            	return 107;
	            case 70:
	            	return 49;
	            case 71:
	            	return 186;
	            case 72:
	            	return 493;
	            default:
	                return 3;
	        }
	    }
}
