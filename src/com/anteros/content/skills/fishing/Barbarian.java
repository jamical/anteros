package com.anteros.content.skills.fishing;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.model.player.Skills;
import com.anteros.util.Misc;

/**
 * Handles Barehanded Fishing of Tuna, Swordfish, and Sharks
 * 
 * @author Abexlry
 *
 */

public class Barbarian extends Fishing {
	
	public static int checkFish(Player p) {
		if (p.getLevels().getLevel(Skills.FISHING) >= 55 && p.getLevels().getLevel(Skills.STRENGTH) >= 35) {
			if (p.getLevels().getLevel(Skills.FISHING) >= 70 && p.getLevels().getLevel(Skills.STRENGTH) >= 50) {
				if (p.getLevels().getLevel(Skills.FISHING) >= 96 && p.getLevels().getLevel(Skills.STRENGTH) >= 76) {
					return 3;
				}
				return 2;
			} 
			return 1;
		}
		return 0;
	}
	
	public static boolean canUseSpot(Player p, Spot fishingSpot) {
		if (fishingSpot.isSecondOption()) {
			if (fishingSpot.getSpotId() == 324 && Barbarian.checkFish(p) > 0) {
				return true;
			}
			if (fishingSpot.getSpotId() == 313 && Barbarian.checkFish(p) > 2) {
				return true;
			}
		}
		return false;
	}
	
	private static int getCatchAnimationAndLoot(Player p, Spot fishingSpot) {
		int fishingFor = checkFish(p);
		switch(fishingSpot.getSpotId()) {
		case 324:
			switch(fishingFor) {
			case 1:
				p.animate(6710);
				p.getLevels().addXp(Skills.FISHING, 80);
				p.getLevels().addXp(Skills.STRENGTH, 8);
				p.getInventory().addItem(359);
				break;
			case 2:
			case 3:
				if (Misc.random(1) == 1) {
					p.animate(6710);
					p.getLevels().addXp(Skills.FISHING, 80);
					p.getLevels().addXp(Skills.STRENGTH, 8);
					p.getInventory().addItem(359);
				} else {
					p.animate(6707);
					p.getLevels().addXp(Skills.FISHING, 100);
					p.getLevels().addXp(Skills.STRENGTH, 10);
					p.getInventory().addItem(371);
				}
			}
			break;
		case 313:
			p.animate(6705);
			p.getLevels().addXp(Skills.FISHING, 110);
			p.getLevels().addXp(Skills.STRENGTH, 11);
			p.getInventory().addItem(383);
			break;
		}
		return 0;
	}
	
	public static void startFishing(final Player p, final int i, NPC npc, boolean newFish, final boolean secondOption) {
		if (p.isDead() || p.isDisconnected() || p.isDestroyed()) {
			resetFishing(p);
			return;
		}
		final long animTime = 18000 - Misc.random(16000);
		p.animate(6709);
		p.animate(BARBARIAN_ANIMATION[2][0]);
		World.getInstance().registerEvent(new Event(animTime) {
			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("fishingSpot") == null) {
					resetFishing(p);
					p.animate(65535);
					return;
				}
				p.getActionSender().closeInterfaces();
				if (p.barbarianFishingDelay < System.currentTimeMillis()) {
					caughtFish(p, i, null, false, secondOption);
					return;
				}
				p.animate(65535);
				p.animate(BARBARIAN_ANIMATION[2][0]);
				startFishing(p, i, null, false, secondOption);
			}
		});	
	}
	
	private static void caughtFish(final Player p, final int i, NPC npc, boolean newFish, final boolean secondOption) {
		final Spot fishingSpot = (Spot) p.getTemporaryAttribute("fishingSpot");
		if (p.isDead() || p.isDisconnected() || p.isDestroyed()) {
			resetFishing(p);
			return;
		}
		final int catchAnimTime = 10000 - Misc.random(5000);
		p.barbarianFishingDelay = System.currentTimeMillis() + Fishing.getFishingDelay(p, fishingSpot);
		getCatchAnimationAndLoot(p, fishingSpot);
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendChatboxInterface(210);
			p.getActionSender().modifyText("Your inventory is too full to catch any more fish.", 210, 1);
			resetFishing(p);
			return;
		}
		World.getInstance().registerEvent(new Event(catchAnimTime) {
			@Override
			public void execute() {
				this.stop();
				if (p.getTemporaryAttribute("fishingSpot") == null) {
					return;
				} else {
					startFishing(p, i, null, false, secondOption);
					return;
				}
			}
		});	
		}
	}
