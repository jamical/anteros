package com.anteros.content.skills.herblore;

import com.anteros.content.skills.SkillItem;
import com.anteros.event.Event;
import com.anteros.model.ItemDefinition;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Herblore extends HerbloreData {

	public Herblore() {
		
	}
	
	public static boolean doingHerblore(Player p, int itemUsed, int usedWith) {
		int itemOne = itemUsed;
		int itemTwo = usedWith;
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				itemOne = usedWith;
				itemTwo = itemUsed;
			}
			for (int j = 0; j < SECONDARY.length; j++) {
				if (itemOne == SECONDARY[j] && itemTwo == UNFINISHED[j]) {
					setHerbloreItem(p, null);
					shopPotionOptions(p, j);
					return true;
				}
			}
			for (int j = 0; j < UNCRUSHED.length; j++) {
				if (itemOne == UNCRUSHED[j] && itemTwo == PESTLE_AND_MORTAR) {
					setHerbloreItem(p, null);
					showGrindOptions(p, j);
					return true;
				}
			}
			for (int j = 0; j < CLEAN_HERBS.length; j++) {
				if (itemOne == CLEAN_HERBS[j] && itemTwo == VIAL_OF_WATER) {
					setHerbloreItem(p, null);
					shopUnfinishedOptions(p, j);
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean emptyPotion(Player p, int item, int slot) {
		if (item == VIAL_OF_WATER) {
			if (p.getInventory().replaceItemSlot(VIAL_OF_WATER, VIAL, slot)) {
				return true;
			}
		}
		for (int i = 0; i < DOSES.length; i++) {
			for (int j = 0; j < DOSES[0].length; j++) {
				if (item == DOSES[i][j]) {
					p.getInventory().replaceItemSlot(item, VIAL, slot);
					return true;
				}
			}
		}
		return false;
	}

	private static void shopPotionOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getActionSender().sendChatboxInterface(309);
		p.getActionSender().itemOnInterface(309, 2, 110, END_POTION[index]);
		p.getActionSender().modifyText(s + ItemDefinition.forId(END_POTION[index]).getName(), 309, 6);
		p.setTemporaryAttribute("completePotion", index);			
	}

	private static void shopUnfinishedOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		String s1 = index == 6 ? "Spirit weed" : ItemDefinition.forId(CLEAN_HERBS[index]).getName();
		p.getActionSender().sendChatboxInterface(309);
		p.getActionSender().itemOnInterface(309, 2, 110, UNFINISHED_POTION[index]);
		p.getActionSender().modifyText(s + " " + s1 + " potion (unf) ", 309, 6);
		p.setTemporaryAttribute("unfinishedPotion", index);	
	}

	private static void showGrindOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getActionSender().sendChatboxInterface(309);
		p.getActionSender().itemOnInterface(309, 2, 150, CRUSHED[index]);
		p.getActionSender().modifyText(s + ItemDefinition.forId(CRUSHED[index]).getName(), 309, 6);
		p.setTemporaryAttribute("herbloreGrindItem", index);
	}
	
	public static void completePotion(final Player p, int amount, boolean newMix) {
		if (newMix && p.getTemporaryAttribute("completePotion") == null) {
			return;
		}
		if (!newMix && p.getTemporaryAttribute("herbloreItem") == null) {
			return;
		}
		if (newMix) {
			if (p.getTemporaryAttribute("completePotion") == null) {
				return;
			}
			int index = (Integer) p.getTemporaryAttribute("completePotion");
			p.setTemporaryAttribute("herbloreItem", new Potion(END_POTION[index], UNFINISHED[index], SECONDARY[index], POTION_LEVEL[index], POTION_XP[index], amount));
		}
		final Potion item = (Potion) p.getTemporaryAttribute("herbloreItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			resetAllHerbloreVariables(p);
			return;
		}
		if (!p.getInventory().hasItem(item.getSecondary()) || !p.getInventory().hasItem(item.getUnfinished())) {
			resetAllHerbloreVariables(p);
			return;
		}
		if (p.getLevels().getLevel(HERBLORE) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Herblore level of " + item.getLevel() + " to make that potion.");
			resetAllHerbloreVariables(p);
			return;
		}
		String s = ItemDefinition.forId(item.getFinished()).getName().replaceAll("(3)", "");
		if (p.getInventory().deleteItem(item.getUnfinished()) && p.getInventory().deleteItem(item.getSecondary())) {
			if (p.getInventory().addItem(item.getFinished())) {
				item.decreaseAmount();
				p.animate(MIX_ANIMATION);
				p.getLevels().addXp(HERBLORE, item.getXp());
				p.getActionSender().sendMessage("You add the ingredient into the murky vial, you have completed the potion.");
				p.getActionSender().closeInterfaces();
			}
		}
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(750) {
				@Override
				public void execute() {
					completePotion(p, item.getAmount(), false);
					stop();
				}
			});
		}	
	}
	
	public static void makeUnfinishedPotion(final Player p, int amount, boolean newMix) {
		if (newMix && p.getTemporaryAttribute("unfinishedPotion") == null) {
			return;
		}
		if (!newMix && p.getTemporaryAttribute("herbloreItem") == null) {
			return;
		}
		if (newMix) {
			if (p.getTemporaryAttribute("unfinishedPotion") == null) {
				return;
			}
			int index = (Integer) p.getTemporaryAttribute("unfinishedPotion");
			p.setTemporaryAttribute("herbloreItem", new Potion(UNFINISHED_POTION[index], -1, CLEAN_HERBS[index], -1, -1, amount));
		}
		final Potion item = (Potion) p.getTemporaryAttribute("herbloreItem");
		if (!p.getInventory().hasItem(item.getSecondary()) || !p.getInventory().hasItem(VIAL_OF_WATER)) {
			return;
		}
		if (p.getInventory().deleteItem(VIAL_OF_WATER) && p.getInventory().deleteItem(item.getSecondary())) {
			if (p.getInventory().addItem(item.getFinished())) {
				item.decreaseAmount();
				p.animate(MIX_ANIMATION);
				p.getActionSender().sendMessage("You mix the " + ItemDefinition.forId(item.getSecondary()).getName() + " into a vial of water.");
				p.getActionSender().closeInterfaces();
			}
		}
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(750) {
				@Override
				public void execute() {
					makeUnfinishedPotion(p, item.getAmount(), false);
					stop();
				}
			});
		}	
	}
	
	public static void grindIngredient(final Player p, int amount, boolean newGrind) {
		if (newGrind && p.getTemporaryAttribute("herbloreGrindItem") == null) {
			return;
		}
		if (!newGrind && p.getTemporaryAttribute("herbloreItem") == null) {
			return;
		}
		if (newGrind) {
			if (p.getTemporaryAttribute("herbloreGrindItem") == null) {
				return;
			}
			int index = (Integer) p.getTemporaryAttribute("herbloreGrindItem");
			p.setTemporaryAttribute("herbloreItem", new SkillItem(CRUSHED[index], UNCRUSHED[index], -1, -1, -1, -1, amount));
		}
		final SkillItem item = (SkillItem) p.getTemporaryAttribute("herbloreItem");
		if (!p.getInventory().hasItem(PESTLE_AND_MORTAR)) {
			p.getActionSender().sendMessage("You do not have a Pestle and mortar.");
			return;
		}
		if (p.getInventory().replaceSingleItem(item.getItemOne(), item.getFinishedItem())) {
			item.decreaseAmount();
			p.animate(GRIND_ANIMATION);
			p.getActionSender().sendMessage("You grind the " + ItemDefinition.forId(item.getItemOne()).getName() + " into a fine dust.");
			p.getActionSender().closeInterfaces();
		}
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(750) {
				@Override
				public void execute() {
					grindIngredient(p, item.getAmount(), false);
					stop();
				}
			});
		}
	}

	private static void identifyHerb(Player p, int herb) {
		if (p.getTemporaryAttribute("identifyHerbTimer") != null) {
			if (System.currentTimeMillis() - (Long) p.getTemporaryAttribute("identifyHerbTimer") < 250) {
				return;
			}
		}
		if (p.getLevels().getLevel(HERBLORE) < HERB_LVL[herb]) {
			p.getActionSender().sendMessage("You need a Herblore level of " + HERB_LVL[herb] + " to clean that herb.");
			return;
		}
		if (p.getInventory().replaceSingleItem(GRIMY_HERBS[herb], CLEAN_HERBS[herb])) {
			p.getActionSender().sendMessage("You clean the " + HERB_NAME[herb] + ".");
			p.getLevels().addXp(HERBLORE, HERB_XP[herb]);
			p.setTemporaryAttribute("identifyHerbTimer", (Long) System.currentTimeMillis());
		}		
	}
	
	public static boolean idHerb(Player p, int item) {
		for (int j = 0; j < GRIMY_HERBS.length; j++) {
			if (item == GRIMY_HERBS[j]) {
				identifyHerb(p, j);
				setHerbloreItem(p, null);
				return true;
			}
		}
		return false;
	}
	
	private static void resetAllHerbloreVariables(Player p) {
		p.removeTemporaryAttribute("herbloreGrindItem");
		p.removeTemporaryAttribute("fillingVials");
		p.removeTemporaryAttribute("unfinishedPotion");
		p.removeTemporaryAttribute("completePotion");
	}

	public static void setHerbloreItem(Player p, Object a) {
		if (a == null) {
			resetAllHerbloreVariables(p);
			p.removeTemporaryAttribute("herbloreItem");
			return;
		}
		p.setTemporaryAttribute("herbloreItem", (Object) a);
	}
	
	public static boolean mixDoses(Player p, int itemUsed, int usedWith, int usedSlot, int withSlot) {
		int ONE = 0, TWO = 1, THREE = 2, FOUR = 3;
		for (int i = 0; i < DOSES.length; i++) {
			for (int j = 0; j < DOSES[ONE].length; j++) {
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[ONE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[ONE][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[ONE][j], DOSES[TWO][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[TWO][j]) {	
					p.getInventory().replaceItemSlot(DOSES[TWO][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[TWO][j], DOSES[THREE][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[THREE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[THREE][j], DOSES[TWO][j], usedSlot);
					p.getInventory().replaceItemSlot(DOSES[THREE][j], DOSES[FOUR][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[TWO][j]) {	
					p.getInventory().replaceItemSlot(DOSES[ONE][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[TWO][j], DOSES[THREE][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[ONE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[TWO][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[ONE][j], DOSES[THREE][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[THREE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[ONE][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[THREE][j], DOSES[FOUR][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[ONE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[THREE][j], VIAL, usedSlot);
					p.getInventory().replaceItemSlot(DOSES[ONE][j], DOSES[FOUR][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[THREE][j]) {	
					p.getInventory().replaceItemSlot(DOSES[TWO][j], DOSES[ONE][j], usedSlot);
					p.getInventory().replaceItemSlot(DOSES[THREE][j], DOSES[FOUR][j], withSlot);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[TWO][j]) {	
					p.getInventory().replaceItemSlot(DOSES[THREE][j], DOSES[ONE][j], usedSlot);
					p.getInventory().replaceItemSlot(DOSES[TWO][j], DOSES[FOUR][j], withSlot);
					return true;
				}
			}
		}
		return false;
	}
}
