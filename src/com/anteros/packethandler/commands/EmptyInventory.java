package com.anteros.packethandler.commands;

import com.anteros.model.player.Player;

public class EmptyInventory implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
		player.getInventory().deleteAll();
	}
	}

	@Override
	public int minimumRightsNeeded() {
		return 1;
	}

}
