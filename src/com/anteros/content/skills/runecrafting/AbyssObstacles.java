package com.anteros.content.skills.runecrafting;

import com.anteros.content.skills.mining.Mining;
import com.anteros.content.skills.woodcutting.Woodcutting;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.FaceLocation;
import com.anteros.model.player.Player;

public class AbyssObstacles {

	public AbyssObstacles() {
		
	}
	
	public static void mineRock(final Player p, final int x, final int y) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x +3, y+1) {
			@Override
			public void run() {
				if (!Mining.hasPickaxe(p)) {
					p.getActionSender().sendMessage("You need a pickaxe to get past this obstacle.");
					return;
				}
				p.getWalkingQueue().reset();
				p.setFaceLocation(new FaceLocation(x + 1, y));
				p.animate(Mining.getPickaxeAnimation(p));
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(1900) {
					int status = 0;
					@Override
					public void execute() {
						int[] ROCKS = {7158, 7159, 7160};
						if (status < 3) {
							p.getActionSender().createObject(ROCKS[status], Location.location(x, y, 0), 0, 10);
						}
						status++;
						if (status == 1) {
							this.setTick(1300);
						}
						if (status == 3) {
							p.animate(65535);
							this.setTick(800);
						}
						if (status == 4) {
							this.stop();
							teleportPastObstacle(p);
							p.removeTemporaryAttribute("unmovable");
						}
					}
				});
				return;
			}
		});
	}
	
	public static void burnBoil(final Player p, final int x, final int y) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x +3, y+2) {
			@Override
			public void run() {
				if (!p.getInventory().hasItem(590)) {
					p.getActionSender().sendMessage("You need a tinderbox to get past this obstacle.");
					return;
				}
				p.setFaceLocation(new FaceLocation(x + 1, y));
				p.animate(733);
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(1900) {
					int status = 0;
					@Override
					public void execute() {
						int[] BOIL = {7165, 7166, 7167};
						if (status < 3) {
							p.getActionSender().createObject(BOIL[status], Location.location(x, y, 0), x == 3060 ? 3 : 1, 10);
						}
						status++;
						if (status == 1) {
							this.setTick(1300);
						}
						if (status == 3) {
							p.animate(65535);
							this.setTick(1000);
						}
						if (status == 4) {
							this.stop();
							teleportPastObstacle(p);
							p.removeTemporaryAttribute("unmovable");
						}
					}
					
				});
				return;
			}
		});
	}
	
	public static void useAgilityTunnel(final Player p, final int x, final int y) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-2, y-2, y+2, y+2) {
			@Override
			public void run() {
				p.getWalkingQueue().reset();
				p.animate(844);
				p.setTemporaryAttribute("unmovable", true);
				p.setFaceLocation(new FaceLocation(x, y));
				World.getInstance().registerEvent(new Event(1000) {
					@Override
					public void execute() {
							p.getActionSender().sendMessage("You squeeze through the gap.");
							teleportPastObstacle(p);
							p.removeTemporaryAttribute("unmovable");
							this.stop();
					}
				});
				return;
			}
		});
	}
	
	public static void passEyes(final Player p, final int  x, final int y) {
		int var = x == 3058 ? x+2 : x-1;
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, var, y, var, y+2) {
			@Override
			public void run() {
				p.getWalkingQueue().reset();
				p.setFaceLocation(new FaceLocation(x, y));
				p.setTemporaryAttribute("unmovable", true);
				p.getActionSender().sendMessage("You attempt to distract the eyes...");
				World.getInstance().registerEvent(new Event(1900) {
					int status = 0;
					@Override
					public void execute() {
						int[] EYES = {7168, 7169, 7170};
						if (status == 0) {
							p.getActionSender().createObject(EYES[1], Location.location(x, y, 0), x == 3058 ? 3 : 1, 10);
						}
						status++;
						if (status == 1) {
							this.setTick(1300);
						}
						if (status == 2) {
							p.animate(65535);
							this.setTick(800);
						}
						if (status == 3) {
							this.stop();
							p.getActionSender().sendMessage("You distract the eyes and pass them.");
							teleportPastObstacle(p);
							p.removeTemporaryAttribute("unmovable");
						}
					}
				});
				return;
			}
		});
	}
	
	public static void chopTendrils(final Player p, final int x, final int y) {
		int var = x == 3057 ? x+2 : x-1;
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, var, y, var, y+2) {
			@Override
			public void run() {
				if (!Woodcutting.hasAxe(p)) {
					p.getActionSender().sendMessage("You need an axe to get past this obstacle.");
					return;
				}
				p.getWalkingQueue().reset();
				p.setFaceLocation(new FaceLocation(x + 1, y));
				p.animate(Woodcutting.getAxeAnimation(p));
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(1900) {
					int status = 0;
					@Override
					public void execute() {
						int[] TENDRILS = {7161, 7162, 7163};
						if (status < 3) {
							p.getActionSender().createObject(TENDRILS[status], Location.location(x, y, 0), x == 3057 ? 3 : 1, 10);
						}
						status++;
						if (status == 1) {
							p.animate(Woodcutting.getAxeAnimation(p));
							this.setTick(1300);
						}
						if (status == 3) {
							p.getActionSender().sendMessage("You clear your way through the tendrils.");
							p.animate(65535);
							this.setTick(800);
						}
						if (status == 4) {
							this.stop();
														teleportPastObstacle(p);
							p.removeTemporaryAttribute("unmovable");
						}
					}
				});
				return;
			}
		});
	}
	
	private static void teleportPastObstacle(Player p) {
		p.teleport(RuneCraft.teleportInner());
	}
	
}