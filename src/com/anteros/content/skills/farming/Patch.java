package com.anteros.content.skills.farming;

import com.anteros.content.skills.farming.FarmingData.PatchType;

public class Patch {

	private int status;
	private int index;
	private int configId;
	private int[] configArray;
	private long timeToGrow;
	private long lastUpdate;
	public String ownerName;
	private int seedIndex;
	private int multiplyer;
	private PatchType patchType;
	private boolean hasWeeds;
	private boolean sapling;
	private boolean healthChecked;
	private boolean weeding;
	
	public Patch(String owner, int index) {
		this.ownerName = owner;
		this.index = index;
		this.sapling = true;
		this.status = 0;
	}
	
	public Patch(String owner, PatchType type, int index, int multiplyer, int config) {
		this.hasWeeds = true;
		this.status = 0;
		this.lastUpdate = System.currentTimeMillis();
		this.ownerName = owner;
		this.patchType = type;
		this.index = index;
		this.multiplyer = multiplyer;
		this.configId = config;
	}
	
	public Patch(String owner, PatchType type, int index, int configId, int[] configArray, long timeToGrow, int crop, int multiplyer) {
		this.ownerName = owner;
		this.patchType = type;
		this.index = index;
		this.configId = configId;
		this.configArray = configArray;
		this.timeToGrow = timeToGrow;
		this.multiplyer = multiplyer;
		this.hasWeeds = true;
		this.status = 0;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	
	public int getPatchIndex() {
		return index;
	}

	public int getConfigId() {
		return configId;
	}
	
	public void setConfig(int i) {
		this.configId = i;
	}
	
	public int getConfigElement(int status) {
		return configArray[status];
	}
	
	public long getTimeToGrow() {
		return timeToGrow;
	}

	public void setTimeToGrow(long time) {
		this.timeToGrow = time;
	}

	public int getConfigLength() {
		return configArray.length;
	}

	public boolean isFullyGrown() {
		if (!patchOccupied()) {
			return false;
		}
		if (!isFruitTree()) {
			return status >= configArray.length - 1;
		}
		return false;
	}
	
	public int getMultiplyer() {
		return multiplyer;
	}

	public PatchType getPatchType() {
		return patchType;
	}

	public void setConfigArray(int[] is) {
		this.configArray = is;
	}
	
	public boolean hasWeeds() {
		return hasWeeds;
	}
	
	public void setHasWeeds(boolean b) {
		this.hasWeeds = b;
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(long time) {
		this.lastUpdate = time;
	}

	public boolean patchOccupied() {
		return configArray.length != 3;
	}

	public void setSeedIndex(int seedIndex) {
		this.seedIndex = seedIndex;
	}

	public int getSeedIndex() {
		return seedIndex;
	}

	public boolean isSapling() {
		return sapling;
	}
	
	public boolean isFruitTree() {
		return patchType.equals(FarmingData.PatchType.FRUIT_TREE);
	}
	
	public boolean isTree() {
		return patchType.equals(FarmingData.PatchType.TREE);
	}

	public void setHealthChecked(boolean healthChecked) {
		this.healthChecked = healthChecked;
	}

	public boolean isHealthChecked() {
		return healthChecked;
	}

	public boolean isStump() {
		return false;
	}
	
	public boolean isBlankPatch() {
		return !hasWeeds && configArray.length == 3 && status == 2;
	}

	public void setWeeding(boolean b) {
		this.weeding = b;
	}
	
	public boolean isWeeding() {
		return weeding;
	}

	public int checkHealthStatus() {
		if (isTree()) {
			return configArray.length - 3;
		} else if (isFruitTree()) {
			return configArray.length - 3;
		}
		return 0;
	}

	public int chopStatus() {
		if (isTree()) {
			return configArray.length - 2;
		} else if (isFruitTree()) {
			return configArray.length - 2;
		}
		return 0;
	}

	public int stumpStatus() {
		if (isTree()) {
			return configArray.length - 1;
		} else if (isFruitTree()) {
			return configArray.length - 1;
		}
		return 0;
	}
}
