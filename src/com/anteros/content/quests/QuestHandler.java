package com.anteros.content.quests;

import com.anteros.model.player.Player;
/**
 * 
 * @author Abexlry
 *
 */
/*
 * Colors for quest tab
 * Yellow: FFFF00
 * Green: 00FF00
 * 
 * Colors for quest interface
 * Blue = 342D7E
 * Red = 800517
 * 
 * Enter colors like this: <col=342D7E>TEXT HERE
 */

public class QuestHandler {
	
	public void sendQuestInterface(Player player) {
		for (int i = 10; i < 301; i++) {
			player.getActionSender().modifyText("", 275, i);
		}
	}
	
	public void sendQuestCompleted(Player player) {
		for (int i = 10; i < 18; i++) {
			player.getActionSender().modifyText("", 277, i);
		}
	}
	
	public void loadList(Player player) { // updates quest text color
		player.getActionSender().sendConfig(101, player.questPoints);
		if (player.doricsQuestInt > 0 && player.doricsQuestInt < 3) {
			player.getActionSender().modifyText("<col=FFFF00>Doric's Quest", 274, 16);
		}
		if (player.doricsQuestInt == 3) {
			player.getActionSender().modifyText("<col=00FF00>Doric's Quest", 274, 16);
		}
		if (player.runeMysteriesInt > 0 && player.runeMysteriesInt < 4) {
			player.getActionSender().modifyText("<col=FFFF00>Rune Mysteries", 274, 26);
		}
		if (player.runeMysteriesInt == 4) {
			player.getActionSender().modifyText("<col=00FF00>Rune Mysteries", 274, 26);
		}
		if (player.druidicRitualInt > 0 && player.druidicRitualInt < 4) {
			player.getActionSender().modifyText("<col=FFFF00>Druidic Ritual", 274, 47);
		}
		if (player.druidicRitualInt == 4) {
			player.getActionSender().modifyText("<col=00FF00>Druidic Ritual", 274, 47);
		}
		
	}
	
	public int addQuestPoint(Player player, int amount) {
		return player.questPoints = player.questPoints + amount;
	}

}
