package com.anteros.packethandler.commands;

import com.anteros.content.CharacterDesign;
import com.anteros.model.player.Player;

public class CharacterAppearance implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded() && player.getRights() < 4) {
		CharacterDesign.open(player);
		}
	}

	@Override
	public int minimumRightsNeeded() {
		return 1;
	}

}
