package com.anteros.content.minigames.agilityarena;

import com.anteros.content.skills.agility.AgilityData;
import com.anteros.event.CoordinateEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceMovement;
import com.anteros.model.player.Player;

public class Obstacles extends AgilityData {

	public Obstacles() {
		
	}
	
	public static void doObstacle(final Player p, final int index) {
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(index) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			int x = index <= 2 ? (Integer)AGILITY_ARENA_OBJECTS[index][1] + 1 : (Integer)AGILITY_ARENA_OBJECTS[index][1] - 1;
			final int dirX = index <= 2 ? -7 : +7;
			World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location(x, (Integer)AGILITY_ARENA_OBJECTS[index][2], 3)) {
				
				@Override
				public void run() {
					final boolean running = p.getWalkingQueue().isRunToggled();
					p.getWalkingQueue().setRunToggled(false);
					p.getWalkingQueue().reset();
					p.getActionSender().clearMapFlag();
					p.setTemporaryAttribute("unmovable", true);
					p.getAppearance().setWalkAnimation(155);
					p.getUpdateFlags().setAppearanceUpdateRequired(true);
					p.getWalkingQueue().forceWalk(dirX, 0);
					World.getInstance().registerEvent(new Event(4300) {

						@Override
						public void execute() {
							this.stop();
							p.removeTemporaryAttribute("unmovable");
							p.getAppearance().setWalkAnimation(-1);
							p.getUpdateFlags().setAppearanceUpdateRequired(true);
							p.getLevels().addXp(AGILITY, (Double)AGILITY_ARENA_OBJECTS[index][3]);
							p.getWalkingQueue().setRunToggled(running);
						}
					});	
				}
			});
			break;
			
		case 6:
			World.getInstance().registerCoordinateEvent(new CoordinateEvent(p, Location.location((Integer)AGILITY_ARENA_OBJECTS[index][1], (Integer)AGILITY_ARENA_OBJECTS[index][2], 3)) {
				
				@Override
				public void run() {
					final boolean running = p.getWalkingQueue().isRunToggled();
					p.getWalkingQueue().setRunToggled(false);
					p.getWalkingQueue().reset();
					p.getActionSender().clearMapFlag();
					p.setTemporaryAttribute("unmovable", true);
					p.animate(1121);
					p.setFaceLocation(Location.location(p.getLocation().getX(), p.getLocation().getY() + 1, 3));
					World.getInstance().registerEvent(new Event(700) {
						int i = 0;
						@Override
						public void execute() {
							p.animate(1122);
							int regionX = p.getUpdateFlags().getLastRegion().getRegionX();
							int regionY = p.getUpdateFlags().getLastRegion().getRegionY();
							int lX = (p.getLocation().getX() - ((regionX - 6) * 8));
							int lY = (p.getLocation().getY() - ((regionY - 6) * 8));
							ForceMovement movement = new ForceMovement(lX, lY, lX-1, lY, 0, 5, 0);
							p.setForceMovement(movement);
							if (i++ >= 7) {
								p.animate(65535);
								this.stop();
								p.removeTemporaryAttribute("unmovable");
								p.getLevels().addXp(AGILITY, (Double)AGILITY_ARENA_OBJECTS[index][3]);
								p.getWalkingQueue().setRunToggled(running);
								return;
							}
							World.getInstance().registerEvent(new Event(500) {

								@Override
								public void execute() {
									this.stop();
									p.teleport(Location.location(p.getLocation().getX()-1, p.getLocation().getY(), 3));
								}
							});
						}
					});	
				}
			});
			break;
		}
	}
}
