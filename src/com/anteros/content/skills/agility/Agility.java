package com.anteros.content.skills.agility;

import com.anteros.content.minigames.agilityarena.AgilityArena;
import com.anteros.content.minigames.agilityarena.Obstacles;
import com.anteros.model.player.Player;
import com.anteros.util.Area;

public class Agility extends AgilityData {

	public Agility() {

	}

	public static boolean doAgility(Player p, int object, int x, int y) {
		for (int i = 0; i < GNOME_COURSE.length; i++) {
			if (object == (Integer)GNOME_COURSE[i][0]) {
				GnomeCourse.doCourse(p, x, y, GNOME_COURSE[i]);
				return true;
			}
		}
		for (int i = 0; i < BARBARIAN_COURSE.length; i++) {
			if (object == (Integer)BARBARIAN_COURSE[i][0]) {
				BarbarianCourse.doCourse(p, x, y, BARBARIAN_COURSE[i]);
				return true;
			}
		}
		for (int i = 0; i < WILDERNESS_COURSE.length; i++) {
			if (object == (Integer)WILDERNESS_COURSE[i][0]) {
				WildernessCourse.doCourse(p, x, y, WILDERNESS_COURSE[i]);
				return true;
			}
		}
		for (int i = 0; i < APE_ATOLL_COURSE.length; i++) {
			if (object == (Integer)APE_ATOLL_COURSE[i][0]) {
				ApeAtollCourse.doCourse(p, x, y, APE_ATOLL_COURSE[i]);
				return true;
			}
		}
		for (int i = 0; i < AGILITY_ARENA_PILLARS.length; i++) {
			if (x == AGILITY_ARENA_PILLARS[i][1] && y == AGILITY_ARENA_PILLARS[i][2]) {
				if (object == AGILITY_ARENA_PILLARS[i][0]) {
					AgilityArena.tagPillar(p, i);
					return true;
				}
			}
		}
		if (Area.atAgilityArena(p.getLocation())) {
			for (int i = 0; i < AGILITY_ARENA_OBJECTS.length; i++) {
				if (x == (Integer)AGILITY_ARENA_OBJECTS[i][1] && y == (Integer)AGILITY_ARENA_OBJECTS[i][2]) {
					if (object == (Integer)AGILITY_ARENA_OBJECTS[i][0]) {
						Obstacles.doObstacle(p, i);
						return true;
					}
				}
			}
		}
		if (object == 3205 && x == 2532 && y == 3545) {
			BarbarianCourse.useLadder(p);
			return true;
		}
		return false;
	}
}
