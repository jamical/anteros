package com.anteros.util;

import com.thoughtworks.xstream.XStream;

/**
 * Util class to get the xstream object.
 * @author Graham
 *
 */
public class XStreamUtil {
	
	private XStreamUtil() {}
	
	private static XStream xstream = null;
	
	public static XStream getXStream() {
		if(xstream == null) {
			xstream = new XStream();
			/*
			 * Set up our aliases.
			 */
			xstream.alias("packet", com.anteros.packethandler.PacketHandlerDef.class);
			xstream.alias("player", com.anteros.model.player.Player.class);
			xstream.alias("itemDefinition", com.anteros.model.ItemDefinition.class);
			xstream.alias("item", com.anteros.model.Item.class);
			xstream.alias("npcDefinition", com.anteros.model.npc.NPCDefinition.class);
			xstream.alias("npc", com.anteros.model.npc.NPC.class);
			xstream.alias("shop", com.anteros.world.Shop.class);
			xstream.alias("npcDrop", com.anteros.model.npc.NPCDrop.class);
		}
		return xstream;
	}

}
