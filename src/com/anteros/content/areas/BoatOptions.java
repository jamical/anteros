package com.anteros.content.areas;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class BoatOptions {
	
	private static final int[][] LOCATIONS = {
		{3508, 3471, 0}, //Canifis
		{2548, 3758, 0}, //Waterbirth isle
		{2659, 2676, 0}, //Pest control
		{2875, 3546, 0}, //Warrior guild
		{2414, 3893, 0}, //Fremmenik shore
		{3027, 3216, 0}, //Port Sarim from Musa point
		{2954, 3153, 0}, //Musa point
	};
	
	private static String[] DESTINATION_NAMES = {
		"Canifis",
		"Waterbirth Isle",
		"Pest Control",
		"The Warrior Guild",
		"Fremmenik Shore",
		"Port Sarim",
		"Musa Point",
	};
	
	private static String[] DESTINATION_MESSAGES = {
		"It's a tight squeeze through the swamp, but the boat soon arrives in Canifis.",
		"The boat docks at Waterbirth Isle.",
		"The Squire welcomes you to Pest Control Island.",
		"The boat drops you off at shore, you walk for a short distance to The Warrior Guild.",
		"Fremmenik Shore, an icy tundra, you see dungeon entrances in the distance.",
		"You step off the boat into Port Sarim.",
		"The boat arrives in Musa Point.",
	};

	public BoatOptions() {
		
	}
	
	public static boolean interactWithBoatNPC(final Player p, final NPC n) {
		final int id = n.getId();
		if (id != 4540 && id != 1304 && id != 2436 && id != 3781 && id != 1361 && id != 4962 && id != 376 && id != 377
				&& id != 378 && id != 380) {
			return false;
		}
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				switch(n.getId()) {
					case 4540: // Home boat
						showBentleyDialogue(p, 240);
						break;
						
					case 1304: // Canifis sailor
						showCanifisSailorDialogue(p, 280);
						break;
						
					case 2436: // Waterbirth isle
						showJarvaldDialogue(p, 300);
						break;
						
					case 3781: // Pest control squire
						showSquireDialogue(p, 340);
						break;
						
					case 1361: // Warrior guild
						showArnorDialogue(p, 370);
						break;
					
					case 4962: // fremmenik shore
						showCaptainBarnabyDialogue(p, 410);
						break;
						
					case 376:
					case 377:
					case 378:
						showTobLorThrDialogue(p, id, 271);
						break;
					case 380:
						showTobLorThrDialogue(p, id, 275);
						break;
				}
			}
		});
		return true;
	}
	
	private static String getMultiNpcNames(int npcId) {
		if (npcId == 376) {
			return "Captain Tobias";
		} else if (npcId == 377) {
			return "Seaman Lorris";
		} else if (npcId == 378) {
			return "Seaman Thresnor";
		} else if (npcId == 380) {
			return "Customs Officer";
		} else {
			return null;
		}
		
	}
	
	public static void showTobLorThrDialogue(Player p, int npcId, int status) {
		String name = getMultiNpcNames(npcId);
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 271:
				Dialogue.setInteractingNpcId(p, npcId);
				p.getActionSender().sendNpcChat1("The trip to Karamja will cost you 30 coins.", npcId, name, Dialogue.DEFAULT_HAPPY);
				newStatus = 272;
				break;
			case 272:
				p.getActionSender().sendOption2("Yes please.", "No, thank you.");
				newStatus = 273;
				break;
			case 273:
				if (p.getInventory().hasItemAmount(995, 30)) {
					p.getInventory().deleteItem(995, 30);
					travel(p, 6, false);
				} else {
					p.getActionSender().sendPlayerChat1("Oh dear, I don't seem to have enough money.", Dialogue.SAD);
				}
				break;
			case 274:
				p.getActionSender().sendPlayerChat1("No, thank you.", Dialogue.DEFAULT_HAPPY);
				newStatus = -1;
				break;
			case 275:
				Dialogue.setInteractingNpcId(p, npcId);
				p.getActionSender().sendNpcChat1("Would you like to return to Port Sarim?", npcId, name, Dialogue.DEFAULT_HAPPY);
				newStatus = 276;
				break;
			case 276:
				p.getActionSender().sendOption2("Yes please.", "No, thank you.");
				newStatus = 277;
				break;
			case 277:
				if (p.getInventory().hasItem(431)) {
					p.getActionSender().sendNpcChat1("Sorry, you can't board with rum in your pack.", npcId, name, Dialogue.CALM_TALK_BARELY_MOVING);
				} else {
					travel(p, 5, false);
				}
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void showCanifisSailorDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 280:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you take me back to Oo'glog please?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 281;
				break;
				
			case 281:
				p.getActionSender().sendNPCHead(1304, 241, 2);
				p.getActionSender().modifyText("Sailor", 241, 3);
				p.getActionSender().modifyText("As you wish, i'll fetch the boat.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 282;
				break;
				
			case 282:
				travel(p, 0, true);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void showJarvaldDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 300:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you take me back to Oo'glog please?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 301;
				break;
				
			case 301:
				p.getActionSender().sendNPCHead(2436, 241, 2);
				p.getActionSender().modifyText("Jarvald", 241, 3);
				p.getActionSender().modifyText("No problem. Off we go!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 302;
				break;
				
			case 302:
				travel(p, 1, true);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void showSquireDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 340:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you take me back to Oo'glog please?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 341;
				break;
				
			case 341:
				p.getActionSender().sendNPCHead(3781, 241, 2);
				p.getActionSender().modifyText("Squire", 241, 3);
				p.getActionSender().modifyText("Certainly! Please visit Pest Control again soon.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 342;
				break;
				
			case 342:
				travel(p, 2, true);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void showArnorDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 370:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you take me back to Oo'glog please?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 371;
				break;
				
			case 371:
				p.getActionSender().sendNPCHead(1361, 241, 2);
				p.getActionSender().modifyText("Arnor", 241, 3);
				p.getActionSender().modifyText("Of course, follow me.", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 372;
				break;
				
			case 372:
				travel(p, 3, true);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
	
	public static void showCaptainBarnabyDialogue(Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 410:
				p.getActionSender().sendPlayerHead(64, 2);
				p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
				p.getActionSender().modifyText("Could you take me back to Oo'glog please?", 64, 4);
				p.getActionSender().animateInterface(9827, 64, 2);
				p.getActionSender().sendChatboxInterface2(64);
				newStatus = 411;
				break;
				
			case 411:
				p.getActionSender().sendNPCHead(4962, 241, 2);
				p.getActionSender().modifyText("Captain Barnaby", 241, 3);
				p.getActionSender().modifyText("Yes! it's freezing here, let's go!", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 412;
				break;
				
			case 412:
				travel(p, 4, true);
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}

	public static void showBentleyDialogue(final Player p, int status) {
		int newStatus = -1;
		p.getActionSender().softCloseInterfaces();
		if (p.getTemporaryAttribute("unmovable") != null) {
			return;
		}
		switch(status) {
			case 240:
				p.getActionSender().sendNPCHead(4540, 241, 2);
				p.getActionSender().modifyText("Captain Bentley", 241, 3);
				p.getActionSender().modifyText("Well, hello there " + p.getPlayerDetails().getDisplayName() + ", ready to set sail?", 241, 4);
				p.getActionSender().animateInterface(9827, 241, 2);
				p.getActionSender().sendChatboxInterface2(241);
				newStatus = 241;
				break;
				
			case 241:
				p.getActionSender().modifyText("I'd like to travel to..", 235, 1);
				p.getActionSender().modifyText("Canifis", 235, 2);
				p.getActionSender().modifyText("Waterbirth Isle", 235, 3);
				p.getActionSender().modifyText("Pest Control", 235, 4);
				p.getActionSender().modifyText("Warrior Guild", 235, 5);
				p.getActionSender().modifyText("Fremmenik Shore", 235, 6);
				p.getActionSender().sendChatboxInterface2(235);
				newStatus = 242;
				break;
				
			case 242: // Canifis
				travel(p, 0, false);
				break;
				
			case 243: // Waterbirth isle
				travel(p, 1, false);
				break;
				
			case 244: // Pest control
				travel(p, 2, false);
				break;
				
			case 245: // Warrior guild
				int attackLevel = p.getLevels().getLevelForXp(0);
				int strengthLevel = p.getLevels().getLevelForXp(2);
				boolean hasA99 = attackLevel == 99 || strengthLevel == 99;
				if (((attackLevel + strengthLevel) >= 130) || hasA99) {
					travel(p, 3, false);
				} else {
					p.getActionSender().sendNPCHead(4540, 243, 2);
					p.getActionSender().modifyText("Captain Bentley", 243, 3);
					p.getActionSender().modifyText("I'm sorry " + p.getPlayerDetails().getDisplayName() + ", I cannot take you there.", 243, 4);
					p.getActionSender().modifyText("A combined Attack & Strength level of 130 is ", 243, 5);
					p.getActionSender().modifyText("required to use The Warrior Guild.", 243, 6);
					p.getActionSender().animateInterface(9827, 243, 2);
					p.getActionSender().sendChatboxInterface2(243);
				}
				break;
				
			case 246: // Fremmenik shore
				travel(p, 4, false);
				break;
				
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}

	private static void travel(final Player p, final int index, final boolean returning) {
		p.setTemporaryAttribute("unmovable", true);
		p.getActionSender().displayInterface(120);
		if (returning) {
			p.getActionSender().sendMessage("You sail off back to Oo'glog..");
		} else {
			p.getActionSender().sendMessage("You climb aboard Captain Bentley's boat and set sail to " + DESTINATION_NAMES[index] + ".");
		}
		World.getInstance().registerEvent(new Event(2000) {
			int i = 0;
			@Override
			public void execute() {
				if (i == 0) {
					i++;
					this.setTick(600);
					if (returning) {
						p.teleport(Location.location(2622, 2857, 0));
					} else {
						p.teleport(Location.location(LOCATIONS[index][0], LOCATIONS[index][1], LOCATIONS[index][2]));
					}
				} else {
					this.stop();
					p.getActionSender().sendOverlay(170);
					p.removeTemporaryAttribute("unmovable");
					p.getActionSender().sendMessage(returning ? "The boat arrives back in Oo'glog." : DESTINATION_MESSAGES[index]);
					p.getActionSender().closeInterfaces();
					World.getInstance().registerEvent(new Event(2000) {

						@Override
						public void execute() {
							this.stop();
							p.getActionSender().sendRemoveOverlay();
							if (index == 1) {
								p.removeTemporaryAttribute("snowInterface");
							}
						}
					});
				}
			}		
		});
	}
}
