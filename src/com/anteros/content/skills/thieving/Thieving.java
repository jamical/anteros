package com.anteros.content.skills.thieving;

import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;
import com.anteros.util.log.Logger;

public class Thieving extends ThievingData {

	public Thieving() {
		
	}
	
	public static boolean wantToThieveNpc(Player p, NPC npc) {
		for (int i = 0; i < NPCS.length; i++) {
			for (int j = 0; j < NPCS[i].length; j++) {
				if (npc.getId() == NPCS[i][j]) {
					thieveNpc(p, npc , i);
					return true;
				}
			}
		}
		return false;
	}

	private static void thieveNpc(final Player p, final NPC npc, final int index) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, npc.getLocation().getX()-1, npc.getLocation().getY()-1, npc.getLocation().getX()+1, npc.getLocation().getY()+1) {
			@Override
			public void run() {
				if (!canThieveNpc(p, npc, index)) {
					return;
				}
				p.setFaceLocation(npc.getLocation());
				p.animate(881);
				p.getActionSender().sendMessage("You attempt to pick the " + NPC_NAMES[index] + " pocket...");
				p.setTemporaryAttribute("lastPickPocket", System.currentTimeMillis());
				World.getInstance().registerEvent(new Event(1000) {
		
					@Override
					public void execute() {
						this.stop();
						if (!p.getLocation().withinDistance(npc.getLocation(), 2)) {
							return;
						}
						if (successfulThieve(p, index, false)) {
							int rewardIndex = Misc.random(NPC_REWARD[index].length - 1);
							int reward = NPC_REWARD[index][rewardIndex];
							int rewardAmount = NPC_REWARD_AMOUNT[index][rewardIndex];
							if (index == 7) { // Master farmer.
								if (Misc.random(15) == 0) {
									reward = HERB_SEEDS[Misc.random(HERB_SEEDS.length - 1)];
								}
							}
							p.getLevels().addXp(THIEVING, NPC_XP[index]);
							p.getInventory().addItem(reward, rewardAmount);
							p.getActionSender().sendMessage("You pick the " + NPC_NAMES[index] + " pocket.");
						} else {
							p.getWalkingQueue().reset();
							p.getActionSender().sendMessage("You fail to pick the " + NPC_NAMES[index] + " pocket.");
							p.getActionSender().sendMessage("You've been stunned!");
							npc.setForceText(new ForceText("What do you think you're doing?"));
							p.setTemporaryAttribute("unmovable", true);
							p.setTemporaryAttribute("stunned", true);
							p.graphics(80, 0, 100);
							p.animate(p.getDefenceAnimation());
							p.hit(1);
							npc.setFaceLocation(p.getLocation());
							World.getInstance().registerEvent(new Event(5000) {
								@Override
								public void execute() {
									this.stop();
									p.removeTemporaryAttribute("unmovable");
									p.removeTemporaryAttribute("stunned");
									p.graphics(65535);
								}
							});
						}
					}
				});
			}
		});
	}
	
	private static boolean successfulThieve(Player p, int index, boolean stall) {
		int thieveLevel = p.getLevels().getLevel(THIEVING);
		int levelNeeded = stall ? STALL_LVL[index] : NPC_LVL[index];
		int difference = thieveLevel - levelNeeded;
		if ((difference > 6 && index >= 12)) {
			difference = 6;
		}
		if ((difference > 14 && index < 12)) {
			difference = 14;
		}
		int chance = difference < 3 ? 1 : (int) (difference * 0.9);
		return Misc.random(chance) != 0;
	}

	private static boolean canThieveNpc(Player p, NPC npc, int index) {
		if (p == null || npc == null || npc.isDead() || npc.isHidden() || npc.isDestroyed() || p.isDead() || p.isDestroyed()) {
			return false;
		}
		if (!p.getLocation().withinDistance(npc.getLocation(), 2)) {
			return false;
		}
		if (p.getLevels().getLevel(THIEVING) < NPC_LVL[index]) {
			p.getActionSender().sendMessage("You need a Thieving level of " + NPC_LVL[index] + " to rob this NPC.");
			p.setFaceLocation(npc.getLocation());
			return false;
		}
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendMessage("You need a free inventory space for any potential loot.");
			return false;
		}
		if (p.getTemporaryAttribute("stunned") != null) {
			return false;
		}
		if (p.getTemporaryAttribute("lastPickPocket") != null) {
			if (System.currentTimeMillis() - (Long)p.getTemporaryAttribute("lastPickPocket") < 1500) {
				return false;
			}
		}
		return true;
	}

	public static boolean wantToThieveStall(Player p, int id, int x, int y) {
		for (int i = 0; i < STALLS.length; i++) {
			for (int j = 0; j < STALLS[i].length; j++) {
				if (id == STALLS[i][j]) {
					thieveStall(p, i, id, x, y);
					return true;
				}
			}
		}
		return false;
	}
	public static boolean wantToThieveChest(Player p, int id, int x, int y) {
		for (int i = 0; i < CHESTS.length; i++) {
			if (id == CHESTS[i]) {
				thieveChest(p, i, id, x, y);
				return true;
			}
		}
		return false;
	}

	private static void thieveChest(final Player p, final int index, final int chestId, final int x, final int y) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, x-1, y-1, x+1, y+1) {
			@Override
			public void run() {
				p.setFaceLocation(Location.location(x, y, p.getLocation().getZ()));
				if (!canThieveChest(p, index, chestId, x, y)) {
					return;
				}
				p.getActionSender().sendMessage("You attempt to pick the chest lock..");
				p.animate(833);
				World.getInstance().registerEvent(new Event(1000) {
					@Override
					public void execute() {
						this.stop();
						if (Misc.random(5) == 0) {
							p.hit(p.getLevels().getLevelForXp(3) / 10);
							p.forceChat("Ouch!");
							p.getActionSender().sendMessage("You activate a trap whilst trying to pick the lock!");
							return;
						}
						if (World.getInstance().getGlobalObjects().originalObjectExists(chestId, Location.location(x, y, 0))) {
							World.getInstance().getGlobalObjects().lowerHealth(chestId, Location.location(x, y, 0));
							for (int i = 0; i < CHEST_REWARD[index].length; i++) {
								p.getInventory().addItem(CHEST_REWARD[index][i], CHEST_REWARD_AMOUNTS[index][i]);
							}
							p.getLevels().addXp(THIEVING, CHEST_XP[index]);
							p.getActionSender().sendMessage("You successfully pick the lock and loot the chest!");
						}
					}
				});
			}
		});
	}

	protected static boolean canThieveChest(Player p, int index, int chestId, int x, int y) {
		if (p == null || p.isDead() || p.isDestroyed() || p.isDisconnected()) {
			return false;
		}
		if (!World.getInstance().getObjectLocations().objectExists(chestId, Location.location(x, y, 0))) {
			Logger.getInstance().info(p.getUsername() + " tried to steal from a non existing chest!");
			return false;
		}
		if (p.getInventory().getTotalFreeSlots() < CHEST_REWARD[index].length) {
			p.getActionSender().sendMessage("You don't have enough free inventory space for the loot from that chest.");
			return false;
		}
		if (!World.getInstance().getGlobalObjects().originalObjectExists(chestId, Location.location(x, y, 0))) {
			return false;
		}
		if (p.getLevels().getLevel(THIEVING) < CHEST_LVL[index]) {
			p.getActionSender().sendMessage("You need a Thieving level of " + CHEST_LVL[index] + " to steal from this chest.");
			return false;
		}
		return true;
	}

	private static void thieveStall(final Player p, final int index, final int stallId, final int x, final int y) {
		int stallFace = World.getInstance().getObjectLocations().getFace(stallId, Location.location(x, y, 0));
		int[] areaCoords = new int[4];
		if (index == 2 || index == 4 || index == 5 || index == 12 || index == 13) { // Ape atoll stalls
			areaCoords[0] = x - 1;
			areaCoords[1] = y - 1;
			areaCoords[2] = x + 1;
			areaCoords[3] = y + 1;
		} else {
			areaCoords = getAreaCoords(stallFace, x, y);
		}
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, areaCoords[0], areaCoords[1], areaCoords[2], areaCoords[3]) {
			@Override
			public void run() {
				if (!canThieveStall(p, index, stallId, x, y)) {
					return;
				}
				p.animate(833);
				World.getInstance().registerEvent(new Event(1000) {
					@Override
					public void execute() {
						this.stop();
						if (World.getInstance().getGlobalObjects().originalObjectExists(stallId, Location.location(x, y, 0))) {
							World.getInstance().getGlobalObjects().lowerHealth(stallId, Location.location(x, y, 0));
							p.getLevels().addXp(THIEVING, STALL_XP[index]);
							int rewardIndex = Misc.random(STALL_REWARD[index].length - 1);
							int reward = STALL_REWARD[index][rewardIndex];
							if (index == 7) { // Seed stall
								if (Misc.random(15) == 0) {
									reward = HERB_SEEDS[Misc.random(HERB_SEEDS.length - 1)];
								}
							} else if (index == 13) { // Scimitar stall
								if (Misc.random(75) == 0) {
									reward = 1333; // Rune scimitar.
								} else if (Misc.random(250) == 0) {
									reward = 4587; // Dragon scimitar.
								}
							}
							int amount = Misc.random(STALL_REWARD_AMOUNTS[index][rewardIndex]);
							if (amount <= 0) {
								amount = 1;
							}
							p.getInventory().addItem(reward, amount);
						}
					}
				});
			}
		});
	}
	
	private static int[] getAreaCoords(int face, int x, int y) {
		int[] coords = new int[4];
		switch(face) {
			case 0: 
			case 3:
				coords[0] = x - 1;
				coords[1] = y - 1;
				coords[2] = x + 2;
				coords[3] = y + 2;
				break;
				
			case 1:
			case 2:
				coords[0] = x - 1;
				coords[1] = y - 1;
				coords[2] = x + 2;
				coords[3] = y + 2;
				break;

		}
		return coords;
	}

	private static boolean canThieveStall(Player p, int index, int stallId, int x, int y) {
		if (p == null || p.isDead() || p.isDestroyed() || p.isDisconnected()) {
			return false;
		}
		if (!World.getInstance().getObjectLocations().objectExists(stallId, Location.location(x, y, 0))) {
			Logger.getInstance().info(p.getUsername() + " tried to steal from a non existing stall!");
			return false;
		}
		if (p.getInventory().findFreeSlot() == -1) {
			p.getActionSender().sendMessage("You need a free inventory space for any potential loot.");
			return false;
		}
		if (!World.getInstance().getGlobalObjects().originalObjectExists(stallId, Location.location(x, y, 0))) {
			return false;
		}
		if (p.getLevels().getLevel(THIEVING) < STALL_LVL[index]) {
			p.getActionSender().sendMessage("You need a Thieving level of " + STALL_LVL[index] + " to steal from this stall.");
			return false;
		}
		return true;
	}
}
