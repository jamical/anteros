package com.anteros.content.skills.crafting;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Jewellery extends CraftingData {

	public Jewellery() {
		
	}
	
	public static void showCutGemOption(Player p, int index) {
		int amount = p.getInventory().getItemAmount((Integer)GEMS[index][0]);
		if (amount > 1) {
			String s = "<br><br><br><br>";
			p.getActionSender().itemOnInterface(309, 2, 190, (Integer) GEMS[index][1]);
			p.getActionSender().modifyText(s + (String) GEMS[index][4], 309, 6);
			p.getActionSender().sendChatboxInterface(309);
			p.setTemporaryAttribute("craftType", index + 50);
			return;
		}
		cutGem(p, index + 50, 1, true);
	}

	public static void cutGem(final Player p, int index, int amount, boolean newCut) {
		index -= 50;
		if (newCut) {
			p.setTemporaryAttribute("craftItem", new CraftItem(5, index, amount, (Double) GEMS[index][3], (Integer) GEMS[index][1], (String) GEMS[index][4], (Integer) GEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 5) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		if (!p.getInventory().hasItem(CHISEL)) {
			p.getActionSender().sendMessage("You cannot cut gems without a chisel.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem((Integer)GEMS[item.getCraftItem()][0])) {
			if (newCut) {
				p.getActionSender().sendMessage("You have no " + item.getMessage() + " to cut.");
			} else {
				p.getActionSender().sendMessage("You have no more " + item.getMessage() + "'s to cut.");
			}
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to cut that gem.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem((Integer)GEMS[item.getCraftItem()][0])) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			p.animate((Integer) GEMS[item.getCraftItem()][5]);
			p.getActionSender().sendMessage("You cut the " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {

				@Override
				public void execute() {
					cutGem(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}

	public static void displayJewelleryInterface(Player p) {
		for (int i = 0; i < JEWELLERY_INTERFACE_VARS.length; i++) {
			if (p.getInventory().hasItem(JEWELLERY_INTERFACE_VARS[i][0])) {
				p.getActionSender().showChildInterface(675, JEWELLERY_INTERFACE_VARS[i][1], false);
				displayJewellery(p, i);
			}
		}
		p.getActionSender().displayInterface(675);
	}

	private static void displayJewellery(Player p, int index) {
		Object[][] items = getItemArray(p, index);
		if (items == null) {
			return;
		}
		int SIZE = 100;
		int interfaceSlot = JEWELLERY_INTERFACE_VARS[index][2];
		p.getActionSender().itemOnInterface(675, interfaceSlot, SIZE, (Integer)items[0][0]); // display the gold item since we know we have the bar
		interfaceSlot += 2;
		for (int i = 1; i < items.length; i++) { // i is set to 1 to ignore the gold
			for (int j = 0; j < items[i].length; j++) {
				if (p.getInventory().hasItem((Integer) GEMS[i - 1][1]) && p.getLevels().getLevel(CRAFTING) >= (Integer)items[i][1]) { // lower it down 1 because of the gold..0 in jeweller is 0, but in GEMS is a sapphire
					p.getActionSender().itemOnInterface(675, interfaceSlot, SIZE, (Integer)items[i][0]);
				} else {
					p.getActionSender().itemOnInterface(675, interfaceSlot, SIZE, NULL_JEWELLERY[index]);
				}
			}
			interfaceSlot += 2;
		}
	}

	private static Object[][] getItemArray(Player p, int index) {
		switch(index) {
			case 0: return RINGS;
			case 1: return NECKLACES;
			case 2: return AMULETS;
			case 3: return BRACELETS;
		}
		return null;
	}
	
	public static void makeJewellery(final Player p, int buttonId, int amount, boolean newCraft) {
		int index = -1;
		if (newCraft) {
			int itemType = getIndex(buttonId, true);
			Object[][] items = getItemArray(p, itemType);
			index = getIndex(buttonId, false);
			if (index == -1) {
				p.getActionSender().closeInterfaces();
				return;
			}
			p.setTemporaryAttribute("craftItem", new CraftItem(itemType, index, amount, (Double)items[index][2], (Integer)items[index][0], (String)items[index][3], (Integer)items[index][1]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			p.getActionSender().closeInterfaces();
			return;
		}
		p.getActionSender().closeInterfaces();
		index = item.getCraftItem();
		System.out.println(index);
		int index2 = index;
		String gemType = (String) GEMS[index2-1][4];
		String s = index == 3 ? "an" : "a";
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft a " + gemType + " " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(JEWELLERY_INTERFACE_VARS[item.getCraftType()][0])) {
			p.getActionSender().sendMessage("You need " + s + " " + item.getMessage() + " mould to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(GOLD_BAR)) {
			p.getActionSender().sendMessage("You need a Gold bar to craft an item of jewellery.");
			Crafting.resetCrafting(p);
			return;
		}
		if (index2 > 0) { // We dont need gems for gold bars
			if (!p.getInventory().hasItem((Integer) GEMS[index2-1][1])) {
				p.getActionSender().sendMessage("You don't have a cut " + (String) GEMS[index2-1][4] + ".");
				Crafting.resetCrafting(p);
				return;
			}
		}
		if (p.getInventory().deleteItem(GOLD_BAR)) {
			if (index2 > 0) {
				if (!p.getInventory().deleteItem((Integer) GEMS[index2-1][1])) {
					return;
				}
			}
			p.animate(3243);
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			String message = index2 == 0 ? "You smelt the gold bar into a Gold " + item.getMessage() : "You fuse the Gold bar and " + (String) GEMS[index2-1][4] + " together, and create a " + (String) GEMS[index2-1][4] + " " + item.getMessage();
			p.getActionSender().sendMessage(message + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			
			World.getInstance().registerEvent(new Event(1500) {
				@Override
				public void execute() {
					makeJewellery(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}

	private static int getIndex(int buttonId, boolean getItemType) {
		int[][] BUTTONS = {
			{20, 22, 24, 26, 28, 30, 32},
			{39, 41, 43, 45, 47, 49, 51},
			{58, 60, 62, 64, 66, 68, 70},
			{77, 79, 81, 83, 85, 87, 89},
		};
		for (int i = 0; i < BUTTONS.length; i++) {
			for (int j = 0; j < BUTTONS[i].length; j++) {
				if (buttonId == BUTTONS[i][j]) {
					return getItemType ? i : j;
				}
			}			
		}
		return -1;
	}

	public static void showStringAmulet(Player p, int index) {
		int amount = p.getInventory().getItemAmount(BALL_OF_WOOL);
		int amount2 = p.getInventory().getItemAmount(STRUNG_AMULETS[index][1]);
		if (amount > 1 && amount2 > 1) {
			String s = "<br><br><br><br>";
			p.getActionSender().itemOnInterface(309, 2, 80, STRUNG_AMULETS[index][0]);
			p.getActionSender().modifyText(s + (String) GEMS[index - 1][4] + " Amulet", 309, 6);
			p.getActionSender().sendChatboxInterface(309);
			p.setTemporaryAttribute("craftType", index + 100);
			return;
		}
		stringAmulet(p, index + 100, 1, true);
	}

	public static void stringAmulet(final Player p, int index, int amount, boolean newString) {
		index -= 100;
		if (newString) {
			p.setTemporaryAttribute("craftItem", new CraftItem(4, index, amount, STRINGING_XP, STRUNG_AMULETS[index][0], (String) GEMS[index][4] + " amulet", 1));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 4) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		if (!p.getInventory().hasItem(BALL_OF_WOOL)) {
			p.getActionSender().sendMessage("You do not have a Ball of wool.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(STRUNG_AMULETS[item.getCraftItem()][1])) {
			String s = item.getCraftItem() == 1 || item.getCraftItem() == 5 ? "an" : "a";
			p.getActionSender().sendMessage("You don't have " + s + " " + GEMS[item.getCraftItem()][4] + " unstrung amulet to string.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(STRUNG_AMULETS[item.getCraftItem()][1]) && p.getInventory().deleteItem(BALL_OF_WOOL)) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, STRINGING_XP);
			p.getActionSender().sendMessage("You add a string to the amulet.");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1000) {

				@Override
				public void execute() {
					stringAmulet(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
