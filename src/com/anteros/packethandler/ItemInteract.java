package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.content.Consumables;
import com.anteros.content.DestroyItem;
import com.anteros.content.DwarfCannon;
import com.anteros.content.JewelleryTeleport;
import com.anteros.content.MorphRings;
import com.anteros.content.Skillcape;
import com.anteros.content.cluescrolls.ClueScroll;
import com.anteros.content.cluescrolls.ClueScrollChecks;
import com.anteros.content.holiday.Easter;
import com.anteros.content.minigames.barrows.Barrows;
import com.anteros.content.minigames.warriorguild.WarriorGuild;
import com.anteros.content.quests.DruidicRitual;
import com.anteros.content.skills.Skills;
import com.anteros.content.skills.cooking.Cooking;
import com.anteros.content.skills.crafting.Crafting;
import com.anteros.content.skills.farming.Farming;
import com.anteros.content.skills.farming.FarmingAmulet;
import com.anteros.content.skills.firemaking.Firemaking;
import com.anteros.content.skills.fletching.Fletching;
import com.anteros.content.skills.herblore.FillVial;
import com.anteros.content.skills.herblore.Herblore;
import com.anteros.content.skills.magic.Teleport;
import com.anteros.content.skills.prayer.Prayer;
import com.anteros.content.skills.runecrafting.RuneCraft;
import com.anteros.content.skills.slayer.Slayer;
import com.anteros.content.skills.smithing.Smelting;
import com.anteros.content.skills.smithing.Smithing;
import com.anteros.content.skills.woodcutting.Woodcutting;
import com.anteros.event.CoordinateEvent;
import com.anteros.model.ItemDefinition;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.masks.FaceLocation;
import com.anteros.model.player.Player;
import com.anteros.net.Packet;
import com.anteros.util.ItemData;
import com.anteros.util.log.Logger;
import com.anteros.world.GroundItem;

/**
 * All item related packets.
 * @author Luke132
 * @author Graham
 */
public class ItemInteract implements PacketHandler {

	private static final int EQUIP = 55; // d
	private static final int ITEM_ON_ITEM = 27; // d
	private static final int INVEN_CLICK = 156; // d
	private static final int ITEM_ON_OBJECT = 134; // d
	private static final int OPERATE = 206; // d
	private static final int DROP = 135; // d
	private static final int PICKUP = 66; // d
	private static final int SWAP_SLOT = 231; // d
	private static final int SWAP_SLOT2 = 79; // d
	private static final int RIGHT_CLICK_OPTION1 = 161; // d
	private static final int RIGHT_CLICK_OPTION2 = 153; // d
	private static final int EXAMINE_ITEM = 92; // d
	private static final int MAGIC_ON_ITEM = 253;
	
	@Override
	public void handlePacket(Player player, IoSession session, Packet packet) {
		switch(packet.getId()) {
			case EQUIP:
				handleEquipItem(player, packet);
				break;
				
			case ITEM_ON_ITEM:
				handleItemOnItem(player, packet);
				break;
				
			case INVEN_CLICK:
				handleInvenClickItem(player, packet);
				break;
				
			case ITEM_ON_OBJECT:
				handleItemOnObject(player, packet);
				break;
				
			case OPERATE:
				handleOperateItem(player, packet);
				break;
				
			case DROP:
				handleDropItem(player, packet);
				break;
				
			case PICKUP:
				handlePickupItem(player, packet);
				break;
				
			case SWAP_SLOT:
				handleSwapSlot(player, packet);
				break;
				
			case SWAP_SLOT2:
				handleSwapSlot2(player, packet);
				break;
				
			case RIGHT_CLICK_OPTION1:
				handleRightClickOne(player, packet);
				break;
				
			case RIGHT_CLICK_OPTION2:
				handleRightClickTwo(player, packet);
				break;
				
			case EXAMINE_ITEM:
				handleExamineItem(player, packet);
				break;
				
			case MAGIC_ON_ITEM:
				handleMagicOnItem(player, packet);
				break;
		}
	}

	private void handleEquipItem(Player player, Packet packet) {
		int item = packet.readLEShort();
		int slot = packet.readShortA();
		int interfaceId = packet.readInt(); // actually readInt1
		if (slot > 28 || slot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		if (player.getInventory().getItemInSlot(slot) == item) {
			player.getActionSender().closeInterfaces();
			if (RuneCraft.emptyPouch(player, player.getInventory().getItemInSlot(slot))) {
				return;
			}
			if (item == 7927) {
				MorphRings.easterRing(player); //TODO
			}
			//player.getEquipment().equipItem(player.getInventory().getItemInSlot(slot), slot);
			player.getEquipment().checkLevel(item, slot); //checks to see if you meet the requirements to equip the item
		}
	}
	
	private void handleItemOnItem(Player player, Packet packet) {
		int itemSlot = packet.readShort();
		int unused = packet.readInt();
		int withSlot = packet.readLEShort();
		int unused2 = packet.readInt();
		int itemUsed = packet.readLEShortA();
		int usedWith = packet.readLEShortA();
		if (itemSlot > 28 || itemSlot < 0 || withSlot > 28 || withSlot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		player.getActionSender().closeInterfaces();
		if (player.getInventory().getSlot(itemSlot).getItemId() == itemUsed && player.getInventory().getSlot(withSlot).getItemId() == usedWith) {
			if (Fletching.isFletching(player, itemUsed, usedWith)) {
				return;
			} else if (Herblore.doingHerblore(player, itemUsed, usedWith)) {
				return;
			} else if (Herblore.mixDoses(player, itemUsed, usedWith, itemSlot, withSlot)) {
				return;
			} else if (Crafting.wantsToCraft(player, itemUsed, usedWith)) {
				return;
			} else if (Firemaking.isFiremaking(player, itemUsed, usedWith, itemSlot, withSlot)) {
				return;
			} else if (Farming.plantSapling(player, itemUsed, usedWith)) {
				return;
			} else if (Woodcutting.crushNest(player, itemUsed, usedWith)) {
				return;
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		
	}
	
	private void handleInvenClickItem(Player player, Packet packet) {
		int slot = packet.readLEShortA();
		int item = packet.readShortA();
		int childId = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		if (slot > 28 || slot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		if (player.getInventory().getItemInSlot(slot) == item) {
			player.getActionSender().closeInterfaces();
			if (Consumables.isEating(player, player.getInventory().getItemInSlot(slot), slot)) {
				return;
			} else if (Herblore.idHerb(player, player.getInventory().getItemInSlot(slot))) {
				return;
			} else if (RuneCraft.fillPouch(player, player.getInventory().getItemInSlot(slot))) {
				return;
			} else if (Prayer.wantToBury(player,  player.getInventory().getItemInSlot(slot), slot)) {
				return;
			} else if (Teleport.useTeletab(player, player.getInventory().getItemInSlot(slot), slot)) {
				return;
			} else if (FarmingAmulet.showOptions(player, player.getInventory().getItemInSlot(slot))) {
				return;
			} else if (ClueScroll.isClue(player, item)) {
				return;
			}
			switch(item) {
			case 405: 
				ClueScroll.openCasket(player, slot);
				break;
			case 6199:
				Easter.openBox(player);
				break;
				case 4155: // Slayer gem
					Slayer.doDialogue(player, 1051);
					break;
					
				case 6: // Cannon base
					if (player.getSettings().getCannon() != null) {
						player.getActionSender().sendMessage("You already have a cannon set up!");
						break;
					}
					player.getSettings().setCannon(new DwarfCannon(player));
					break;
			
				case 5073: // Nest with seeds.
				case 5074: // Nest with jewellery.
					//TODO egg nests
					Woodcutting.randomNestItem(player, item);
					break;
					
				case 952: // Spade
					ClueScrollChecks.checkClueLocation(player);
					player.animate(830);
					if (Barrows.enterCrypt(player)) {
						player.getActionSender().sendMessage("You've broken into a crypt!");
						break;
					}
					//player.getActionSender().sendMessage("You find nothing.");
					break;
			}
		}
	}
	
	private void handleItemOnObject(Player player, Packet packet) {
		int objectX = packet.readShortA();
		int item = packet.readShort();
		int objectY = packet.readLEShort();
		int slot = packet.readShort();
		int interfaceId = packet.readLEShort();
		int child = packet.readShort();
		int objectId = packet.readShortA();
		if (slot > 28 || slot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			System.out.println("Item on object = " + objectId + " " + objectX + " " + objectY);
		}
		Skills.resetAllSkills(player);
		player.getActionSender().closeInterfaces();
		player.setFaceLocation(new FaceLocation(objectX, objectY));
		if (player.getInventory().getItemInSlot(slot) == item) {
			if (Crafting.wantsToCraftOnObject(player, player.getInventory().getItemInSlot(slot), objectId)) {
				return;
			} else if (Farming.interactWithPatch(player, objectId, objectX, objectY, player.getInventory().getItemInSlot(slot))) {
				return;
			} else if (WarriorGuild.useAnimator(player, player.getInventory().getItemInSlot(slot), objectId, objectX, objectY)) {
				return;
			}
			if (player.getInventory().getItemInSlot(slot) == 7936) {
				if (RuneCraft.wantToRunecraft(player, objectId, objectX, objectY)) {
					return;
				}
				if (RuneCraft.useTalisman(player, objectId, objectX, objectY)) {
					return;
				}
			}
			switch(objectId) {
				case 6: // Cannon:
					DwarfCannon cannon = player.getSettings().getCannon();
					Location l = Location.location(objectX, objectY, player.getLocation().getZ());
					if (cannon == null || (cannon != null & !l.equals(cannon.getLocation()))) {
						player.getActionSender().sendMessage("This isn't your cannon!");
						break;
					}
					cannon.loadCannon();
					break;
					
				case -28755: // Lumbridge fountain.
				case 24214:	// Fountain in east Varrock.
				case 24265:	// Varrock main fountain.
				case 11661:	// Falador waterpump.
				case 11759:	// Falador south fountain.
				case 879:	// Camelot fountains.
				case 29529:	// Sink.
				case 874:	// Sink.
					if (FillVial.fillingVial(player, Location.location(objectX, objectY, player.getLocation().getZ())) && player.getInventory().getItemInSlot(slot) == 229) {
						break;
					}
					break;

				case 114:
				case 2728: // Range in Catherby
				case 2859:
				case 3039:
				case 4172:
				case 5275:
				case 8750:
				case 9682:
				case 14919:
				case 40110:
				case 41264:
				case 42345:
				case 43081:
				case 44078:
				case 45319:
				case 47633:
				case 49035:
					if (Cooking.isCooking(player, player.getInventory().getItemInSlot(slot), false, -1, -1)) {
						break;	
					}
					break;
					
				case 2732: // Fire
					if (Cooking.isCooking(player, player.getInventory().getItemInSlot(slot), true, objectX, objectY)) {
						break;	
					}
					break;
					
				case -28580: // Lumbridge furnace
				case 11666: // Falador furnace
					if (Smelting.wantToSmelt(player, player.getInventory().getItemInSlot(slot))) {
						break;
					} else if (Crafting.wantsToCraftOnObject(player, player.getInventory().getItemInSlot(slot), objectId)) {
						break;
					}
					break;
					
				case 2783: // Anvil
					if (Smithing.wantToSmithOnAnvil(player, player.getInventory().getItemInSlot(slot), Location.location(objectX, objectY, player.getLocation().getZ()))) {
						break;
					}
					break;
				case 2142:
					if (player.druidicRitualInt == 2) {
						DruidicRitual.itemOnCauldron(player, slot);
					}
					break;
					
				default:
					player.getActionSender().sendMessage("Nothing interesting happens.");
					break;
			}
		}
	}
	
	private void handleOperateItem(Player player, Packet packet) {
		int item = packet.readShortA();
		int slot = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int childId = packet.readLEShort();
		if (slot < 0 || slot > 28 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		if (player.getEquipment().getItemInSlot(slot) == item) {
			Skills.resetAllSkills(player);
			player.getActionSender().closeInterfaces();
			if (JewelleryTeleport.useJewellery(player, player.getEquipment().getItemInSlot(slot), slot, true)) {
				return;
			} else
			if (slot == 1 && Skillcape.emote(player)) {
				return;
			}
			player.getActionSender().sendMessage("This item isn't operable.");
		}
	}
	
	private void handleDropItem(Player player, Packet packet) {
		int item = packet.readShortA();
		int slot = packet.readShortA();
		int interfaceId = packet.readLEShort();
		int childId = packet.readShort();
		if (slot > 28 || slot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		if (player.getInventory().getItemInSlot(slot) == item) {
			player.getActionSender().closeInterfaces();
			if (ItemData.isPlayerBound(player.getInventory().getItemInSlot(slot))) {
				DestroyItem.displayInterface(player, player.getInventory().getItemInSlot(slot));
				//return;
			}
			int id = player.getInventory().getItemInSlot(slot);
			int amt = player.getInventory().getAmountInSlot(slot);
			GroundItem i = new GroundItem(id, amt, Location.location(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()), player);
			if (player.getInventory().deleteItem(id, slot, amt)) {
				if (!World.getInstance().getGroundItems().addToStack(id, amt, player.getLocation(), player)) {
					World.getInstance().getGroundItems().newEntityDrop(i);
				}
			}
		}
	}
	
	private void handlePickupItem(final Player player, Packet packet) {
		final int x = packet.readLEShort();
		final int id  = packet.readShort();
		final int y  = packet.readLEShortA();
		Location l = Location.location(x, y, player.getLocation().getZ());
		GroundItem item = World.getInstance().getGroundItems().itemExists(l, id);
		Skills.resetAllSkills(player);
		if (x < 1000 || y < 1000 | id < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		player.getActionSender().closeInterfaces();
		if(player.getLocation().equals(l)) {
			World.getInstance().getGroundItems().pickupItem(player, id, player.getLocation());
			//return;
		}
		World.getInstance().registerCoordinateEvent(new CoordinateEvent(player, l) {
			@Override
			public void run() {
				World.getInstance().getGroundItems().pickupItem(player, id, player.getLocation());
			}
		});
		switch (id) {
		case 1931:
		case 1935:
		case 1205:
		case 1923:
		case 946:
			World.getInstance().getGroundItems().pickupCloseItem(item, player, id, x, y);
			break;
		}
	}
	
	private void handleSwapSlot(Player player, Packet packet) {
		int oldSlot = packet.readShort();
		int childId = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int newSlot = packet.readShortA();
		int swapType = packet.readByteS();
		int oldItem = player.getInventory().getItemInSlot(oldSlot);
		int oldAmount = player.getInventory().getAmountInSlot(oldSlot);
		int newItem = player.getInventory().getItemInSlot(newSlot);
		int newAmount = player.getInventory().getAmountInSlot(newSlot);
		if (oldSlot > 28 || oldSlot < 0 || newSlot > 28 || oldSlot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		switch (interfaceId) {
			case 149:
				if (swapType == 0 && childId == 0) {
					player.getInventory().getSlot(oldSlot).setItemId(newItem);
					player.getInventory().getSlot(oldSlot).setItemAmount(newAmount);
					player.getInventory().getSlot(newSlot).setItemId(oldItem);
					player.getInventory().getSlot(newSlot).setItemAmount(oldAmount);
				}
				break;
			
			default:
				logger.info("UNHANDLED ITEM SWAP 1 : interface = " + interfaceId);
				break;
		}
		//No need to update the screen since the client does it for us!
	}
	
	private void handleSwapSlot2(Player player, Packet packet) {
		int interfaceId = packet.readLEShort();
		int child = packet.readShort();
		int newSlot = packet.readLEShort();
		int interface2 = packet.readShort();
		int child2 = packet.readShort();
		int oldSlot = packet.readLEShort();
		int oldItem = player.getInventory().getItemInSlot(oldSlot);
		int oldAmount = player.getInventory().getAmountInSlot(oldSlot);
		int newItem = player.getInventory().getItemInSlot(newSlot);
		int newAmount = player.getInventory().getAmountInSlot(newSlot);
		if (oldSlot > 28 || oldSlot < 0 || newSlot > 28 || oldSlot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		switch (interfaceId) {
			case 621: // Shop.
			case 763: // Bank.
			case 336: // Duel
				player.getInventory().getSlot(oldSlot).setItemId(newItem);
				player.getInventory().getSlot(oldSlot).setItemAmount(newAmount);
				player.getInventory().getSlot(newSlot).setItemId(oldItem);
				player.getInventory().getSlot(newSlot).setItemAmount(oldAmount);
				break;
			
			default:
				logger.info("UNHANDLED ITEM SWAP 2 : interface = " + interfaceId);
				break;
		}
		//No need to update the screen since the client does it for us!
		player.getActionSender().refreshInventory();
	}
	
	private void handleRightClickOne(Player player, Packet packet) {
		int childId = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int item = packet.readLEShortA();
		int slot = packet.readLEShortA();
		if (slot > 28 || slot < 0 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		if (player.getInventory().getItemInSlot(slot) == item) {
			player.getActionSender().closeInterfaces();
			if (interfaceId == 149 && childId == 0) {
				if (Herblore.emptyPotion(player, player.getInventory().getItemInSlot(slot), slot)) {
					return;
				} else if (JewelleryTeleport.useJewellery(player, player.getInventory().getItemInSlot(slot), slot, false)) {
					return;
				}
			}
		}
	}
	
	private void handleRightClickTwo(Player player, Packet packet) {
		int childId = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int slot = packet.readLEShort();
		int item = packet.readLEShort();
		if (slot < 0 || slot > 28 || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		if (player.getInventory().getItemInSlot(slot) == item) {
			player.getActionSender().closeInterfaces();
			switch(player.getInventory().getItemInSlot(slot)) {
				case 5509: // Small pouch.
					player.getActionSender().sendMessage("There is " + player.getSettings().getSmallPouchAmount() + " Pure essence in your small pouch. (holds 3).");
					break;
					 
				case 5510: // Medium pouch.
					player.getActionSender().sendMessage("There is " + player.getSettings().getMediumPouchAmount() + " Pure essence in your medium pouch. (holds 6).");
					 break;
					 
				 case 5512: // Large pouch.
					 player.getActionSender().sendMessage("There is " + player.getSettings().getLargePouchAmount() + " Pure essence in your large pouch. (holds 9).");
					 break;
					 
				 case 5514: // Giant pouch.
					 player.getActionSender().sendMessage("There is " + player.getSettings().getGiantPouchAmount() + " Pure essence in your giant pouch. (holds 12).");
					 break;
			}
		}
	}
	
	private void handleExamineItem(Player player, Packet packet) {
		int item = packet.readLEShortA();
		if (item < 0 || item > 14630) {
			return;
		}
		String examine = ItemDefinition.forId(item).getExamine();
		player.getActionSender().sendMessage(examine);
	}
	
	private void handleMagicOnItem(Player player, Packet packet) {
		int slot = packet.readShort();
		int spellId = packet.readShortA();
		int item = packet.readShort();
		//if (item < 0 || item > 14630) { //TODO find packet format
		//	return;
		//}
		//System.out.println(item);
	}


	private Logger logger = Logger.getInstance();
}
