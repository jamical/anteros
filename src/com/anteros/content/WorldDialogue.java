package com.anteros.content;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Misc;

public class WorldDialogue {
	
	
	/**
	 * Use this for random npc dialogue that isn't related to quests, etc.
	 */
	public static void handleNpcDialogue(final Player p, final NPC n, final int npcId, final int status) {
		p.setEntityFocus(n.getClientIndex());
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

			@Override
			public void run() {
				n.setFaceLocation(p.getLocation());
				p.setFaceLocation(n.getLocation());
				p.setEntityFocus(65535);
				getNpcDialogue(p, npcId, status);
			}
		});
	}
	
	public static void getNpcDialogue(Player p, int npcId, int status) {
		Dialogue.setInteractingNpcId(p, npcId);
		p.getActionSender().softCloseInterfaces();
		int newStatus = -1;
		switch(status) {
		/*
		 * START OF MEN/WOMEN
		 */
	    case 750:
	    	int randomChat = Misc.random(6);
	    	p.getActionSender().sendPlayerChat1("Hello, how's it going?", Dialogue.DEFAULT_HAPPY);
	    	//newStatus = 751;
	    	if (randomChat == 1) {
	    		newStatus = 751;
	    	}
	    	if (randomChat == 2) {
	    		newStatus = 753;
	    	}
	    	if (randomChat == 3) {
	    		newStatus = 756;
	    	}
	    	if (randomChat == 4) {
	    		newStatus = 757;
	    	}
	    	if (randomChat == 5) {
	    		newStatus = 758;
	    	}
	    	if (randomChat == 6) {
	    		newStatus = 759;
	    	}
        break;
	    case 751: //first random
            p.getActionSender().sendNpcChat1("I'm fine, how are you?", npcId, "Man", Dialogue.DEFAULT_HAPPY);
            newStatus = 752;
        break;
	    case 752:
	    	p.getActionSender().sendPlayerChat1("Very well thank you.", Dialogue.DEFAULT_HAPPY);
            newStatus = -1;
        break;
	    case 753: //second random
            p.getActionSender().sendNpcChat1("Who are you?", npcId, "Man", Dialogue.THINKING);
            newStatus = 754;
        break;
	    case 754:
	    	p.getActionSender().sendPlayerChat1("I'm a bold adventurer.", Dialogue.DEFAULT_HAPPY);
            newStatus = 755;
        break;
	    case 755:
            p.getActionSender().sendNpcChat1("Ah, a very noble profession.", npcId, "Man", Dialogue.CALM_TALK);
            newStatus = -1;
        break;
	    case 756: //third random
            p.getActionSender().sendNpcChat1("I'm very well thank you.", npcId, "Man", Dialogue.DEFAULT_HAPPY);
            newStatus = -1;
        break;
	    case 757: //fourth random
            p.getActionSender().sendNpcChat2("I think we need a new king. The one we've got isn't", "very good. ", npcId, "Man", Dialogue.ANGRY);
            newStatus = -1;
        break;
	    case 758: //fifth random
            p.getActionSender().sendNpcChat1("I'm busy right now.", npcId, "Man", Dialogue.CALM_TALK_BARELY_MOVING);
            newStatus = -1;
        break;
	    case 759: //sixth random
            p.getActionSender().sendNpcChat1("Not too bad thanks.", npcId, "Man", Dialogue.CALM_TALK);
            newStatus = -1;
        break;
        /*
         * END OF MEN/WOMEN, START OF
         */
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}

}
