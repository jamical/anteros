package com.anteros.content.areas;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class AlKharid {
	
	public static void talkToGate(final Player p, int objectId, final Location loc) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, loc.getX() - 1, loc.getY() - 1, loc.getX() + 1, loc.getY() + 1) {

			@Override
			public void run() {
				if (p.getLocation().getX() < loc.getX()) {
					payToll(p, 126);
				} else {
					p.teleport(Location.location(loc.getX() - 1, loc.getY(), 0));
				}
			}
		});
	}
	
	public static void talkToNpc(final Player p, int npcId, Location loc) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, loc.getX() - 1, loc.getY() - 1, loc.getX() + 1, loc.getY() + 1) {

			@Override
			public void run() {
				if (p.getLocation().getX() < 3268) {
					payToll(p, 126);
				}
			}
		});
	}
	
	public static void payToll(Player p, int status) {
		p.getActionSender().softCloseInterfaces();
		int newStatus = -1;
		//al-kharid gate
		switch(status) {
		case 126:
			p.getActionSender().sendPlayerHead(64, 2);
			p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
			p.getActionSender().modifyText("Can I come through this gate?", 64, 4);
			p.getActionSender().animateInterface(9828, 64, 2);
			p.getActionSender().sendChatboxInterface2(64);
			newStatus = 127;
			break;
		case 127:
			p.getActionSender().sendNPCHead(925, 241, 2);
			p.getActionSender().modifyText("Border Guard", 241, 3);
			p.getActionSender().modifyText("You must pay a toll of 10 gold coins to pass.", 241, 4);
			p.getActionSender().animateInterface(9772, 241, 2);
			p.getActionSender().sendChatboxInterface2(241);
			newStatus = 128;
		break;
		case 128:
			p.getActionSender().modifyText("Okay, I'll pay.", 231, 2);
			p.getActionSender().modifyText("Who does my money go to?", 231, 3);
			p.getActionSender().modifyText("No thanks, I'll walk around.", 231, 4);
			p.getActionSender().sendChatboxInterface2(231);
			newStatus = 129;
		break;
		case 129:
			p.getActionSender().sendPlayerHead(64, 2);
			p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
			p.getActionSender().modifyText("Okay, I'll pay.", 64, 4);
			p.getActionSender().animateInterface(9847, 64, 2);
			p.getActionSender().sendChatboxInterface2(64);
			p.teleport(Location.location(3268, 3227, 0));
			p.getInventory().deleteItem(995, 10);
			newStatus = 135;
			break;
		case 130:
			p.getActionSender().sendPlayerHead(64, 2);
			p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
			p.getActionSender().modifyText("Who does my money go to?", 64, 4);
			p.getActionSender().animateInterface(9828, 64, 2);
			p.getActionSender().sendChatboxInterface2(64);
			newStatus = 131;
			break;
		case 131:
			p.getActionSender().sendNPCHead(925, 242, 2);
			p.getActionSender().modifyText("Border Guard", 242, 3);
			p.getActionSender().modifyText("The money goes to the city of Al-Kharid.", 242, 4);
			p.getActionSender().modifyText("Will you pay the toll?", 242, 5);
			p.getActionSender().animateInterface(9772, 242, 2);
			p.getActionSender().sendChatboxInterface2(242);
			newStatus = 128;
		break;
		case 132:
			p.getActionSender().sendPlayerHead(64, 2);
			p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
			p.getActionSender().modifyText("No thanks, I'll walk around.", 64, 4);
			p.getActionSender().animateInterface(9847, 64, 2);
			p.getActionSender().sendChatboxInterface2(64);
			newStatus = 133;
			break;
		case 133:
			p.getActionSender().sendNPCHead(925, 241, 2);
			p.getActionSender().modifyText("Border Guard", 241, 3);
			p.getActionSender().modifyText("As you wish. Don't go too near the scorpions.", 241, 4);
			p.getActionSender().animateInterface(9772, 241, 2);
			p.getActionSender().sendChatboxInterface2(241);
			newStatus = -1;
		break;
		case 134:
			p.getActionSender().sendPlayerHead(64, 2);
			p.getActionSender().modifyText(p.getPlayerDetails().getDisplayName(), 64, 3);
			p.getActionSender().modifyText("I haven't got that much.", 64, 4);
			p.getActionSender().animateInterface(9828, 64, 2);
			p.getActionSender().sendChatboxInterface2(64);
			newStatus = -1;
		break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}
