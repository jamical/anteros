package com.anteros.content.skills.crafting;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Silver extends CraftingData {

	public Silver() {
		
	}

	public static void displaySilverOptions(Player p) {
		String s = "<br><br><br><br>";
		p.getActionSender().sendChatboxInterface(303);
		p.getActionSender().itemOnInterface(303, 2, 175, (Integer)SILVER_ITEMS[0][0]);
		p.getActionSender().itemOnInterface(303, 3, 175, (Integer)SILVER_ITEMS[1][0]);
		p.getActionSender().modifyText(s + (String)SILVER_ITEMS[0][4], 303, 7);
		p.getActionSender().modifyText(s + (String)SILVER_ITEMS[1][4], 303, 11);
		p.setTemporaryAttribute("craftType", 120);
	}
	
	public static void newSilverItem(final Player p, int amount, int index, boolean newCraft) {
		index -= 120;
		if (newCraft) {
			p.setTemporaryAttribute("craftItem", new CraftItem(3, index, amount, (Double) SILVER_ITEMS[index][3], (Integer) SILVER_ITEMS[index][0], (String) SILVER_ITEMS[index][4], (Integer) SILVER_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 3) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		String s = item.getCraftItem() == 0 ? "an" : "a";
		if (!p.getInventory().hasItem((Integer)SILVER_ITEMS[item.getCraftItem()][1])) {
			p.getActionSender().sendMessage("You need " + s + " " + item.getMessage() + " mould to make that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(SILVER_BAR)) {
			p.getActionSender().sendMessage("You don't have a Silver bar.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to smelt that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(SILVER_BAR)) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			p.animate(3243);
			p.getActionSender().sendMessage("You smelt the Silver bar in to " + s + " " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {

				@Override
				public void execute() {
					newSilverItem(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
