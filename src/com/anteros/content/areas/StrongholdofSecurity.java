package com.anteros.content.areas;
import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;
import com.anteros.net.Packet;
public class StrongholdofSecurity {
	
	public static boolean goingDown;
	

public static void entranceEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() {
			if (StrongholdofSecurity.goingDown == true) {
				player.getActionSender().modifyText("", 242, 3);
				player.getActionSender().modifyText("You squeeze through the hole and find a ladder a few feet down", 242, 4);
				player.getActionSender().modifyText("leading into the Stronghold of Security.", 242, 5);
				player.getActionSender().sendChatboxInterface2(242);
				StrongholdofSecurity.goingDown = false;
			}
			
			if (StrongholdofSecurity.goingDown == false) {
				player.teleport(Location.location(1860, 5244, 0));
			}
		}
	});
}
public static void climbExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3081, 3421, 0));
		}
	});
}
public static void entranceEnter2(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3081, 3421, 0));
		}
	});
}
public static void gateEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3081, 3421, 0));
		}
	});
}
public static void gateEnter2(final Player player, int objectId, Location loc, final int oX, final int oY) {
	World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

		@Override
		public void run() { 
			player.teleport(Location.location(3081, 3421, 0));
		}
	});
}

 public static void EntranceEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
  World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {

   @Override
   public void run() {
    player.teleport(Location.location(3082, 4229, 0));
   }
  });
 }
 
 public static void openGateRight(final Player player, Packet packet, Location loc, final int oX, final int oY) {
		final int playerZ = player.getLocation().getZ();
	  World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
	   @Override
	   public void run() {
		   player.teleport(Location.location(oX + 1, oY, playerZ));
	   }
	  });
	 }
 
 public static void openGateUp(final Player player, Packet packet, Location loc, final int oX, final int oY) {
		final int playerZ = player.getLocation().getZ();
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			  
	   @Override
	   public void run() {
		   player.teleport(Location.location(oX, oY + 1, playerZ));
	   }
	  });
	 }
 
 public static void openGateLeft(final Player player, Packet packet, Location loc, final int oX, final int oY) {
		final int playerZ = player.getLocation().getZ();
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			  
	   @Override
	   public void run() {
		   player.teleport(Location.location(oX - 1, oY, playerZ));
	   }
	  });
	 }
 
 public static void openGateDown(final Player player, Packet packet, Location loc, final int oX, final int oY) {
		final int playerZ = player.getLocation().getZ();
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			  
	   @Override
	   public void run() {
		   player.teleport(Location.location(oX, oY - 1, playerZ));
	   }
	  });
	 }
 
 public static int checkFace(Player player, int oX, int oY) {
	 switch (oX) {
	 case 1859:
	 case 1858:
	 case 1860:
	 case 1861:
	 case 1876:
	 case 1877:
	 case 1874:
	 case 1875:
	 case 1890:
	 case 1889:
	 case 1879:
	 case 1878:
	 case 1904:
	 case 1905:
	 case 1912:
	 case 1911:
		 
		 switch (oY) {
		 case 5235:
		 case 5238:
		 case 5212:
		 case 5209:
		 case 5208:
		 case 5211:
		 case 5207:
		 case 5204:
		 case 5192:
		 case 5195:
		 case 5198:
		 case 5226:
		 case 5223:
		 case 5233:
		 case 5230:
		 case 5206:
				 
			 openGateNS(player, oX, oY);
			 return 1;
		 }
		 
	 }
	 openGateWE(player, oX, oY);
	 return 0;
 }
 
 public static void openGateNS(Player player, int oX, int oY) {
		final int pY = player.getLocation().getY();
	 if (pY > oY) {
		 openGateDown(player, null, null, oX, oY);
	 } else {
		 openGateUp(player, null, null, oX, oY);
	 }
 }
 
 public static void openGateWE(Player player, int oX, int oY) {
		final int pX = player.getLocation().getX();
	 if (pX < oX) {
		 openGateRight(player, null, null, oX, oY);
	 } else {
		 openGateLeft(player, null, null, oX, oY);
	 }
 }
}
