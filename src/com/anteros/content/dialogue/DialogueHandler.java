package com.anteros.content.dialogue;

import java.util.HashMap;

import com.anteros.util.Utils;

public final class DialogueHandler {

	private static final HashMap<Object, Class<Dialogue>> handledDialogues = new HashMap<Object, Class<Dialogue>>();
	
	@SuppressWarnings("unchecked")
	public static final void init() {
		try {
			
			Class<Dialogue>[] classes = Utils.getClasses("com.able.content.dialogue.impl");
			Class<Dialogue>[] lumbridge = Utils.getClasses("org.fb.game.content.dialogues.impl.cities.lumbridge");
			Class<Dialogue>[] varrock = Utils.getClasses("org.fb.game.content.dialogues.impl.cities.varrock");
			Class<Dialogue>[] alkharid = Utils.getClasses("org.fb.game.content.dialogues.impl.cities.alkharid");
			Class<Dialogue>[] cooksassistant = Utils.getClasses("org.fb.game.content.quest.impl.cooksassistant.dialogues");
			Class<Dialogue>[] blackknightsfortress = Utils.getClasses("org.fb.game.content.quest.impl.blackknightsfortress.dialogues");
			Class<Dialogue>[] impcatcher = Utils.getClasses("org.fb.game.content.quest.impl.impcatcher.dialogues");
			Class<Dialogue>[] piratestreasure = Utils.getClasses("org.fb.game.content.quest.impl.piratestresure.dialogues");
			Class<Dialogue>[] druidicritual = Utils.getClasses("org.fb.game.content.quest.impl.druidicritual.dialogues");
			Class<Dialogue>[] doricsquest = Utils.getClasses("org.fb.game.content.quest.impl.doricsquest.dialogues");
			Class<Dialogue>[] ernestthechicken = Utils.getClasses("org.fb.game.content.quest.impl.ernestthechicken.dialogues");
			
			for (Class<Dialogue> c : classes) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : lumbridge) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : varrock) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			for (Class<Dialogue> c : alkharid) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : cooksassistant) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : blackknightsfortress) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : impcatcher) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : piratestreasure) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : druidicritual) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : doricsquest) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			for (Class<Dialogue> c : ernestthechicken) {
				if (c.isAnonymousClass()) // next
					continue;
				handledDialogues.put(c.getSimpleName(), c);
			}
			
			
			
		} catch (Throwable e) {
		}
	}

	public static final void reload() {
		handledDialogues.clear();
		init();
	}

	public static final Dialogue getDialogue(Object key) {
		if (key instanceof Dialogue)
			return (Dialogue) key;
		Class<Dialogue> classD = handledDialogues.get(key);
		if (classD == null)
			return null;
		try {
			return classD.newInstance();
		} catch (Throwable e) {
		}
		return null;
	}

	private DialogueHandler() {

	}
}
