package com.anteros.packethandler.commands;

import com.anteros.model.Location;
import com.anteros.model.player.Player;

public class Coordinates implements Command {

	@Override
	public void execute(Player player, String command) {
		if (player.getRights() >= minimumRightsNeeded()) {
		Location loc = player.getLocation();
		player.getActionSender().sendMessage("X = " + loc.getX() + " Y = " + loc.getY());
		}
	}

	@Override
	public int minimumRightsNeeded() {
		return 0;
	}

}
