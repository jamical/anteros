package com.anteros.content.dialogue.impl;

import com.anteros.content.dialogue.Dialogue;
import com.anteros.content.dialogue.Mood;
import com.anteros.model.masks.ForceText;
import com.anteros.model.npc.NPC;


public class Hans extends Dialogue {
	
	private NPC npc;
	private int npcId;

	@Override
	public void start() {
		npcId = (Integer) parameters[0];
		sendNPCChat(Mood.NORMAL, "Hello. What are you doing here?");
		stage = -1;
	}

	@Override
	public void run(int interfaceId, int componentId) {
		switch (stage) {
		case -1:
			sendOptionsDialogue(DEFAULT,
					"I'm looking for whoever is in charge of this place.",
					"I have come to kill everyone in this castle!",
					"I don't know. I'm lost. Where am I?",
					"Can you tell me how long I've been here?", "Nothing.");
			stage = 1;
			break;

		case 1:
			switch(componentId) {
			case OPTION_1:
				sendNPCDialogue(npcId, PLAIN_TALKING,
						"Who, the Duke? He's in his study, on the first floor.");
				stage = -2;
				break;
			case OPTION_2:
				if (npc.getId() == 0) {
					npc.setForceText(new ForceText("Help! Help!"));
				}
				break;
			case OPTION_3:
				sendNPCDialogue(npcId, PLAIN_TALKING, "You are in Lumbridge Castle.");
				stage = -2;
				break;
			case OPTION_4:
				sendNPCDialogue(
						npcId,
						NORMAL,
						"Ahh, I see all the newcomers arriving in Lumbridge, fresh-faced and eager for adventure. I remember you...");
				stage = 2;
				break;
			case OPTION_5:
				sendPlayerDialogue(NORMAL, "Nothing.");
				stage = -2;
				break;
			}
			break;
		case 2:
			sendNPCDialogue(npcId, PLAIN_TALKING,
					"You've spent x in the world since you arrived x days ago.");
			stage = -2;
			break;
		case -2:
			end();
			break;
		}
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}
}
