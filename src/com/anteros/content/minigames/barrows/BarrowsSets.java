package com.anteros.content.minigames.barrows;

import com.anteros.model.player.Player;

public class BarrowsSets {

	public BarrowsSets() {
		
	}
	
	public static boolean wearingDharok(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4722 &&
		p.getEquipment().getItemInSlot(3) == 4718 &&
		p.getEquipment().getItemInSlot(4) == 4720 &&
		p.getEquipment().getItemInSlot(0) == 4716;
	}
	
	public static boolean wearingAhrim(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4714 &&
		p.getEquipment().getItemInSlot(3) == 4710 &&
		p.getEquipment().getItemInSlot(4) == 4712 &&
		p.getEquipment().getItemInSlot(0) == 4708;
	}
	
	public static boolean wearingGuthan(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4730 &&
		p.getEquipment().getItemInSlot(3) == 4726 &&
		p.getEquipment().getItemInSlot(4) == 4728 &&
		p.getEquipment().getItemInSlot(0) == 4724;
	}
	
	public static boolean wearingTorag(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4751 &&
		p.getEquipment().getItemInSlot(3) == 4747 &&
		p.getEquipment().getItemInSlot(4) == 4749 &&
		p.getEquipment().getItemInSlot(0) == 4745;
	}
	
	public static boolean wearingKaril(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4738 &&
		p.getEquipment().getItemInSlot(3) == 4734 &&
		p.getEquipment().getItemInSlot(4) == 4736 &&
		p.getEquipment().getItemInSlot(0) == 4732;
	}
	
	public static boolean wearingVerac(Player p) {
		return p.getEquipment().getItemInSlot(7) == 4759 &&
		p.getEquipment().getItemInSlot(3) == 4755 &&
		p.getEquipment().getItemInSlot(4) == 4757 &&
		p.getEquipment().getItemInSlot(0) == 4753;
	}
}
