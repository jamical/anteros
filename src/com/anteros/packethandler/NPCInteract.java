package com.anteros.packethandler;

import org.apache.mina.common.IoSession;

import com.anteros.Constants;
import com.anteros.content.CharacterDesign;
import com.anteros.content.Shops;
import com.anteros.content.WorldDialogue;
import com.anteros.content.areas.AlKharid;
import com.anteros.content.areas.BoatOptions;
import com.anteros.content.areas.ShipCharter;
import com.anteros.content.areas.TzHaar;
import com.anteros.content.areas.Varrock;
import com.anteros.content.cluescrolls.ClueScroll;
import com.anteros.content.cluescrolls.ClueScrollChecks;
import com.anteros.content.combat.Combat;
import com.anteros.content.combat.MagicCombat;
import com.anteros.content.holiday.Easter;
import com.anteros.content.minigames.agilityarena.AgilityArena;
import com.anteros.content.minigames.barrows.BrokenBarrows;
import com.anteros.content.minigames.warriorguild.WarriorGuild;
import com.anteros.content.quests.DoricsQuest;
import com.anteros.content.quests.DruidicRitual;
import com.anteros.content.quests.RuneMysteries;
import com.anteros.content.skills.Skills;
import com.anteros.content.skills.crafting.Tanner;
import com.anteros.content.skills.fishing.Fishing;
import com.anteros.content.skills.slayer.Slayer;
import com.anteros.content.skills.thieving.Thieving;
import com.anteros.model.Entity;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.npc.NPCDefinition;
import com.anteros.model.player.Player;
import com.anteros.net.Packet;

/**
 * 
 * NPC clicking packets.
 * @author Luke132
 */
public class NPCInteract implements PacketHandler {

	private static final int EXAMINE_NPC = 72;
	private static final int FIRST_CLICK = 3; // d
	private static final int SECOND_CLICK = 78; // d
	private static final int THIRD_CLICK = 148; // d
	private static final int FOURTH_CLICK = 30;
	private static final int FIFTH_CLICK = 218;
	private static final int MAGIC_ON_NPC = 239; // d
	private static final int ITEM_ON_NPC = 115; // d
	
	@Override
	public void handlePacket(Player player, IoSession session, Packet packet) {
		switch(packet.getId()) {
			case FIRST_CLICK:
				handleFirstClickNPC(player, packet);
				break;
				
			case SECOND_CLICK:
				handleSecondClickNPC(player, packet);
				break;
				
			case THIRD_CLICK:
				handleThirdClickNPC(player, packet);
				break;
				
			case FOURTH_CLICK:
				handleFourthClickNPC(player, packet);
				return;
				
			case FIFTH_CLICK:
				handleFifthClickNPC(player, packet);
				break;
				
			case MAGIC_ON_NPC:
				handleMagicOnNPC(player, packet);
				break;
				
			case ITEM_ON_NPC:
				handleItemOnNPC(player, packet);
				break;
				
			case EXAMINE_NPC:
				handleExamine(player, packet);
				break;
		}
	}

	private void handleFirstClickNPC(Player player, Packet packet) {
		int npcIndex = packet.readLEShortA();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		Skills.resetAllSkills(player);
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("1st Click NPC: " + npc.getId());
			//System.out.println("NPC ID = " + npc.getId());
		}
		Combat.checkAttack(player, npc);
	}
	
	private void handleSecondClickNPC(Player player, Packet packet) {
		int npcIndex = packet.readLEShort();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("2nd Click NPC: " + npc.getId());
			//System.out.println("Second click NPC " + npc.getId());
		}
		Skills.resetAllSkills(player);
		if (Fishing.wantToFish(player, npc, false)) {
			return;
		} else if (Slayer.talkToMaster(player, npc)) {
			return;
		} else if (AgilityArena.dialogue(player, npc, false)) {
			return;
		} else if (TzHaar.interactTzhaarNPC(player, npc, 1)) {
			return;
		} else if (WarriorGuild.talkToWarriorGuildNPC(player, npc, 1)) {
			return;
		} else if (BoatOptions.interactWithBoatNPC(player, npc)) {
			return;
		} else if (ClueScrollChecks.currentClueNpc(player, npc)) {
			return;
		}
		if(npc.getId() == 0) {
			player.getDialogueManager().startDialogue("Hans", npc.getId());
		}
		
		switch(npc.getId()) {
			case 1835:
				Easter.interactWithBunny(player, npc);
			break;
			case 7938:
			//LearningTheRopes.interactWithSirVant(player, npc);
			break;
			
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				WorldDialogue.handleNpcDialogue(player, npc, npc.getId(), 750);
				break;
			/*case 278:
				if (player.cookStage == 0) { //TODO redo cook's assistant
					CooksAssistant.interactWithCook(player, npc);
				}
				break;*/
			case 519: // Bob
				BrokenBarrows.talkToBob(player, npc, -1, 1);
				break;
			case 553: // Aubury
				//TODO aubury dialogue
				break;
			case 925: 
				AlKharid.talkToNpc(player, npcIndex, Location.location(npc.getLocation().getX(), npc.getLocation().getY(), player.getLocation().getZ()));
				break;
			case 284:
				DoricsQuest.interactWithDoric(player, npc);
				break;
			case 741:
				RuneMysteries.interactWithDuke(player, npc);
				break;
			case 2258:
				RuneMysteries.interactWithMage(player, npc);
				break;
			case 460:
				RuneMysteries.interactWithFrum(player, npc);
				break;
			case 455:
				DruidicRitual.interactWithKaqemeex(player, npc);
				break;
			case 454:
				DruidicRitual.interactWithSanfew(player, npc);
				break;
		}
	}

	@SuppressWarnings("unused")
	private void handleThirdClickNPC(Player player, Packet packet) {
		int npcIndex = packet.readShortA();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		int npcX = npc.getLocation().getX();
		int npcY = npc.getLocation().getY();
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("3rd Click NPC: " + npc.getId());
			//System.out.println("Third click NPC " + npc.getId());
		}
		Shops.openShop(player, npc, npc.getId());
		Skills.resetAllSkills(player);
		if (Thieving.wantToThieveNpc(player, npc)) {
			return;
		} else if (Fishing.wantToFish(player, npc, true)) {
			return;
		} else if (AgilityArena.dialogue(player, npc, true)) {
			return;
		} else if (TzHaar.interactTzhaarNPC(player, npc, 2)) {
			return;
		} else if (WarriorGuild.talkToWarriorGuildNPC(player, npc, 2)) {
			return;
		}
		switch(npc.getId()) {
			case 44:
			case 45:
			case 166:
			case 494:
			case 495:
			case 496:
			case 497:
			case 498:
			case 499:
			case 953:
			case 1036:
			case 1360:
			case 1702:
			case 2163:
			case 2164:
			case 2354:
			case 2355:
			case 2568:
			case 2569:
			case 2570:
			case 3046:
			case 3198:
			case 3199:
			case 4519:
			case 5258:
			case 5260:
			case 5776:
			case 5777:
			case 5912:
			case 5913:
			case 6200:
			case 6532:
			case 6533:
			case 6534:
			case 6535:
			case 6538:
			case 7049:
			case 7050:
			case 7445:
			case 7446:
			case 7605:
				player.getBank().openNpcBank(player, npc);
				break;
			case 804:
				Tanner.interactWithTanner(player, npc);
				break;
				
		}
	}
	
	private void handleFourthClickNPC(Player player, Packet packet) {
		int npcIndex = packet.readShort();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("4th Click NPC: " + npc.getId());
			//System.out.println("Fourth click NPC " + npc.getId());
		}
		Skills.resetAllSkills(player);
		if (Slayer.openSlayerShop(player, npc)) {
			return;
		}
		switch(npc.getId()) {
			case 553: // Aubury
				Varrock.teleportAubury(player, npc);
				break;
			case 548:
				CharacterDesign.open(player);
				break;
			case 4651:
			case 4652:
			case 4653:
			case 4654:
			case 4655:
			case 4656:
				player.getActionSender().displayInterface(ShipCharter.SHIP_CHARTER);
				break;
		}
	}
	
	private void handleFifthClickNPC(Player player, Packet packet) {
		int npcIndex = packet.readLEShort();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("5th Click NPC: " + npc.getId());
			//System.out.println("Fifth click NPC " + npc.getId());
		}
		Skills.resetAllSkills(player);
		if (Slayer.openPointsInterface(player, npc)) {
			return;
		}
	}
	
	@SuppressWarnings("unused")
	private void handleMagicOnNPC(Player player, Packet packet) {
		int childId = packet.readLEShort();
		int interfaceId = packet.readLEShort();
		int junk = packet.readShortA();
	    int npcIndex = packet.readLEShortA();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		Skills.resetAllSkills(player);
		player.setTarget(npc);
		Entity target = player.getTarget();
		if (Location.projectilePathBlocked(player, target) && npc.getMaxHit() != 0) {
			MagicCombat.newMagicAttack(player, npc, childId, interfaceId == 193);
		} else {
			if (npc.getMaxHit() == 0) {
				player.getActionSender().sendMessage("I can't attack this npc!");
			} else if (npc.getMaxHit() != 0) {
				player.getActionSender().sendMessage("I can't reach that!");
				player.getWalkingQueue().reset();
			}
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("Spell " + childId + " on NPC: " + npc.getId());
			//System.out.println("Spell ID = " + childId);
		}
	}
	
	@SuppressWarnings("unused")
	private void handleItemOnNPC(Player player, Packet packet) {
		int interfaceId = packet.readInt();
		int slot = packet.readLEShort();
		int npcIndex = packet.readLEShort();
		int item = packet.readLEShortA();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		Skills.resetAllSkills(player);
		player.getActionSender().closeInterfaces();
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("Item " + item + " on NPC: " + npc.getId());
			//System.out.println("Item on NPC " + npc.getId());
		}
		if (player.getInventory().getItemInSlot(slot) == item) {
			switch(npc.getId()) {
				case 519: // Bob
					BrokenBarrows.talkToBob(player, npc, player.getInventory().getItemInSlot(slot), 0);
					break;
			}
		}
	}
	
	private void handleExamine(Player player, Packet packet) {
		NPCDefinition def;
		int npcIndex = packet.readShort();
		if (npcIndex < 0 || npcIndex > Constants.NPC_CAP || player.isDead() || player.getTemporaryAttribute("cantDoAnything") != null) {
			return;
		}
		def = NPCDefinition.forId(npcIndex);
		final NPC npc = World.getInstance().getNpcList().get(npcIndex);
		if (npc == null || npc.isDestroyed()) {
			return;
		}
		if (player.getRights() == 1 || player.getRights() == 2) {
			player.getActionSender().sendMessage("Examine NPC: " + npc.getId());
			//System.out.println("Examine NPC " + npc.getId());
		}
		Skills.resetAllSkills(player);
		player.getActionSender().sendMessage(def.getExamine());
	}

}
