package com.anteros.content.skills.crafting;

import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class Leather extends CraftingData {

	public Leather() {
		
	}
	
	public static void openLeatherInterface(Player p, int type) {
		p.setTemporaryAttribute("leatherCraft", type);
		if (type == 4) { // Cowhide
			p.getActionSender().displayInterface(154);
			return;
		}
		int i = type;
		int k = 2;
		int l = 8;
		String s = "<br><br><br><br>";
		for (int j = 0; j < 3; j++) {
			p.getActionSender().itemOnInterface(304, k, 180, (Integer) LEATHER_ITEMS[i][0]);
			p.getActionSender().modifyText(s + (String) LEATHER_ITEMS[i][4], 304, l);
			l += 4;
			i += 4;
			k++;
		}
		p.getActionSender().sendChatboxInterface(304);
	}
	
	public static void craftDragonHide(final Player p, int amount, int itemIndex, int leatherType, boolean newCraft) {
		if (newCraft) {
			itemIndex = leatherType != 0 ? itemIndex += leatherType : itemIndex;
			p.setTemporaryAttribute("craftItem", new CraftItem(leatherType, itemIndex, amount, (Double) LEATHER_ITEMS[itemIndex][2], (Integer) LEATHER_ITEMS[itemIndex][0], (String) LEATHER_ITEMS[itemIndex][4], (Integer) LEATHER_ITEMS[itemIndex][1]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		int index = item.getCraftItem();
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItemAmount(TANNED_HIDE[item.getCraftType()], (Integer) LEATHER_ITEMS[index][3])) {
			p.getActionSender().sendMessage("You need " + (Integer) LEATHER_ITEMS[index][3] + " dragonhide to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(NEEDLE)) {
			p.getActionSender().sendMessage("You need a needle if you wish to craft leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItemAmount(THREAD, (Integer)LEATHER_ITEMS[index][3])) {
			p.getActionSender().sendMessage("You need " + (Integer)LEATHER_ITEMS[index][3] + " thread to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		String s = index < 4 ? "a" : "a pair of";
		for (int j = 0; j < (Integer) LEATHER_ITEMS[index][3]; j++) {
			if (!p.getInventory().deleteItem(TANNED_HIDE[item.getCraftType()])) {
				return; 
			}
		}
		p.getInventory().deleteItem(THREAD, (Integer) LEATHER_ITEMS[index][3]);
		p.getInventory().addItem(item.getFinishedItem());
		p.getLevels().addXp(CRAFTING, item.getXp());
		p.animate(1249);
		p.getActionSender().sendMessage("You craft " + s + " " + item.getMessage() + ".");
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			
			World.getInstance().registerEvent(new Event(1500) {
				@Override
				public void execute() {
					craftDragonHide(p, -1, -1, -1, false);
					this.stop();
				}
			});
		}
	}

	public static void craftNormalLeather(final Player p, int index, int amount, boolean newCraft) {
		index -= 28;
		if (newCraft) {
			p.setTemporaryAttribute("craftItem", new CraftItem(4, index, amount, (Double) NORMAL_LEATHER[index][2], (Integer) NORMAL_LEATHER[index][0], (String) NORMAL_LEATHER[index][3], (Integer) NORMAL_LEATHER[index][1]));
		}
		CraftItem item = (CraftItem) p.getTemporaryAttribute("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 4 || item.getCraftItem() < 0) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getActionSender().closeInterfaces();
		if (!p.getInventory().hasItem(TANNED_HIDE[4])) {
			p.getActionSender().sendMessage("You have no Leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(NEEDLE)) {
			p.getActionSender().sendMessage("You need a needle if you wish to craft leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(THREAD)) {
			p.getActionSender().sendMessage("You have no thread.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getLevels().getLevel(CRAFTING) < item.getLevel()) {
			p.getActionSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		int i = item.getCraftItem();
		String s = i == 0 || i == 5 || i == 6 ? "a" : "a pair of";
		if (p.getInventory().deleteItem(THREAD) && p.getInventory().deleteItem(TANNED_HIDE[4])) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getLevels().addXp(CRAFTING, item.getXp());
			p.animate(1249);
			p.getActionSender().sendMessage("You make " + s + " " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getInstance().registerEvent(new Event(1500) {

				@Override
				public void execute() {
					craftNormalLeather(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
