package com.anteros.content.quests;

import com.anteros.event.AreaEvent;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;

public class RuneMysteries { //TODO This is half-assed, maybe redo it
	
	Player c;

	public RuneMysteries(Player c) {
		this.c = c;
	}

	public static void showInformation(Player p) {
		p.getActionSender().displayInterface(275);
		p.getActionSender().modifyText("Rune Mysteries", 275, 2);
		p.getQuests().sendQuestInterface(p);
		p.getActionSender().displayInterface(275);
	if(p.runeMysteriesInt == 0) {
		p.getActionSender().modifyText("Rune Mysteries", 275, 2);
		p.getActionSender().modifyText("", 275, 11);
		p.getActionSender().modifyText("<col=342D7E>To start this quest", 275, 12);
		p.getActionSender().modifyText("<col=342D7E>Go talk to Duke Horacio", 275, 13);
		p.getActionSender().modifyText("<col=342D7E>Which is located in Lumbridge Castle", 275, 14);
	}else if(p.runeMysteriesInt == 1) {
		p.getActionSender().displayInterface(275);
        p.getActionSender().modifyText( "Rune Mysteries", 275, 2);
		p.getActionSender().modifyText( "", 275, 11);
		p.getActionSender().modifyText( "The duke as asked you to", 275, 12);
		p.getActionSender().modifyText( "Take the air talisman he found", 275, 13);
		p.getActionSender().modifyText( "To the head Wizard at the Wizard's Tower", 275, 14);
	}else if(p.runeMysteriesInt == 2) {
		p.getActionSender().displayInterface(275);
        p.getActionSender().modifyText( "Rune Mysteries", 275, 2);
		p.getActionSender().modifyText( "Wizard Frumscone has given you some notes", 275, 11);
		p.getActionSender().modifyText( "He told you to take them to Mage of Zamorak", 275, 12);
		p.getActionSender().modifyText( "Located in Varrock.", 275, 13);

	}else if(p.runeMysteriesInt == 3) {
		p.getActionSender().displayInterface(275);
        p.getActionSender().modifyText( "Rune Mysteries", 275, 2);
		p.getActionSender().modifyText( "", 275, 11);
		p.getActionSender().modifyText( "Mage Of Zamorak told you to take the notes", 275, 12);
		p.getActionSender().modifyText( "To Wizard Frumscone. To complete the quest.", 275, 13);

	}else if(p.runeMysteriesInt == 4) {
		p.getActionSender().displayInterface(275);
        p.getActionSender().modifyText( "Rune Mysteries", 275, 2);
		p.getActionSender().modifyText( "", 275, 11);
		p.getActionSender().modifyText( "You have completed the quest!", 275, 12);
	
    }
	
	}
	
    	public static boolean hasReq(Player p, int ItemId, int amt) {
    		return p.getInventory().hasItemAmount(ItemId, amt);
    }
	
	    public static void deleteItems(Player p, int itemId, int amt) {
	    	p.getInventory().getItemInSlot(itemId);
            p.getInventory().deleteItem(itemId, amt);
    }
	    
		public static void interactWithFrum(final Player p, final NPC n) {
			p.setEntityFocus(n.getClientIndex());
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

				@Override
				public void run() {
					n.setFaceLocation(p.getLocation());
					p.setFaceLocation(n.getLocation());
					p.setEntityFocus(65535);
					if (p.runeMysteriesInt == 1) {
						showChats(p, 627);
					} else if (p.runeMysteriesInt == 3) {
						showChats(p, 629);
					} else if (p.runeMysteriesInt == 4) {
						
					}
				}
			});
		}
		
		public static void interactWithDuke(final Player p, final NPC n) {
			p.setEntityFocus(n.getClientIndex());
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

				@Override
				public void run() {
					n.setFaceLocation(p.getLocation());
					p.setFaceLocation(n.getLocation());
					p.setEntityFocus(65535);
					if (p.runeMysteriesInt == 0) {
						showChats(p, 626);
					} else if (p.runeMysteriesInt == 4) {
						
					}
				}
			});
		}
		
		public static void interactWithMage(final Player p, final NPC n) {
			p.setEntityFocus(n.getClientIndex());
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, n.getLocation().getX() - 1, n.getLocation().getY() - 1, n.getLocation().getX() + 1, n.getLocation().getY() + 1) {

				@Override
				public void run() {
					n.setFaceLocation(p.getLocation());
					p.setFaceLocation(n.getLocation());
					p.setEntityFocus(65535);
					if (p.runeMysteriesInt == 2) {
						showChats(p, 628);
					} else if (p.runeMysteriesInt == 4) {
						
					}
				}
			});
		}
	
	public static void showChats(Player p, int status) {
		int airT = 1438;
		int pack = 290;
		int note = 291;
		p.getActionSender().softCloseInterfaces();
		p.getQuests().loadList(p);
		int newStatus = -1;
		switch (status) {	
			case 626:
				if (p.runeMysteriesInt == 0) {
				p.getActionSender().sendNpcChat4("Hi I have found a major discovery.", "I found that we can runecraft,", "but there's still more to be done.", "Give this to Wizard Frumscone and hurry.", 741, "Duke Horacio", 9850);
				p.getInventory().addItem(1438, 1);
				p.runeMysteriesInt = 1;
				p.getQuests().loadList(p);
			}
			break;
			case 627:
				if (p.runeMysteriesInt == 1 && hasReq(p, airT, 1)) {
					p.getActionSender().sendNpcChat3("Oh you got one of those cool neety Air Talismans.", "Now give me it and take this", "Research Package to the Mage Of Zamorak.", 460, "Wizard Frumscone", 9850);
					deleteItems(p, airT, 1);
					p.getInventory().addItem(290, 1);
					p.runeMysteriesInt = 2;
				}
			break;
			case 628:
				if (p.runeMysteriesInt == 2 && hasReq(p, pack, 1)) {
					p.getActionSender().sendNpcChat4("Ok you've got the package,", "good give me it.", "Now take these notes to", "Wizard Frumscone to complete this quest.", 2258, "Mage Of Zamorak", 9847);
					deleteItems(p, pack, 1);
					p.getInventory().addItem(291, 1);
					p.runeMysteriesInt = 3;
				}
			break;
			case 629:
				if (p.runeMysteriesInt == 3 && hasReq(p, note, 1)) {
					p.getActionSender().sendNpcChat2("Ok you got the notes,", "give me them.", 460, "Wizard Frumscone", 9847);
						deleteItems(p, note, 1);
						p.runeMysteriesInt = 4;
						p.getQuests().addQuestPoint(p, 1);
						p.getQuests().sendQuestCompleted(p);
						p.getQuests().loadList(p);
						p.getActionSender().questCompleted3(1, 1438, "You have completed Rune Mysteries", "1 Quest Point", "Runecrafting skill", "Air Talisman");
				}
				break;
		}
		if (newStatus != -1) {
			p.setTemporaryAttribute("dialogue", newStatus);
		}
	}
}