package com.anteros.world;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.anteros.content.skills.farming.Farming;
import com.anteros.content.skills.farming.Patch;
import com.anteros.event.Event;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class FarmingPatches {

	private List<Patch> patches;
	
	public FarmingPatches() {
		patches = new ArrayList<Patch>();
	}

	public void processPatches() {
		/*
		 * If something has to be removed from the ArrayList in this loop, use it.remove() not patches.remove()
		 * or else we end up with a ConcurrentModificationException and it'll break out the loop :$.
		 */
		World.getInstance().registerEvent(new Event(3000) {
			@Override
			public void execute() {
				Iterator<Patch> it = patches.iterator();
				while (it.hasNext()) {
					Patch patch = (Patch) it.next();
					if (patch == null) {
						it.remove();
					} else if (patch.isSapling()) {
						if (Farming.growSapling(patch)) {
							it.remove();
						}
					} else if (!patch.patchOccupied()) {
						if (Farming.regrowWeeds(patch)) { // true if we should remove from the list
							it.remove();
						}
					} else if (!patch.isFullyGrown() && patch.patchOccupied()) {
						long currentTime = System.currentTimeMillis();
						long lastUpdatedTime = patch.getLastUpdate();
						int delay = (int) (patch.getTimeToGrow() / patch.getConfigLength());
						//if (currentTime - lastUpdatedTime >= delay) {
							Farming.growPatch(patch);
						//}
					}
				}
			}
		});
	}
	
	public Patch[] getPatchesForPlayer(Player p, int min, int max) {
		int i = 0;
		Patch[] array = new Patch[4];
		for (int j = min; j <= max; j++) {
			Patch patch = patchExists(p, j);
			array[i++] = patch;
		}
		return array;
	}
	
	public Patch patchExists(Player p, int index) {
		for (Patch patch : patches) {
			if (patch.getOwnerName().equals(p.getUsername())) {
				if (patch.getPatchIndex() == index) {
					return patch;
				}
			}
		}
		return null;
	}
	
	public void removePatch(Patch patch) {
		synchronized(patches) {
			patches.remove(patch);
		}
	}
	
	public void addPatch(Patch patch) {
		synchronized(patches) {
			patches.add(patch);
		}
	}
}
