package com.anteros.content.minigames.fightcave;

import com.anteros.content.skills.prayer.PrayerData;
import com.anteros.event.AreaEvent;
import com.anteros.event.Event;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.npc.NPC;
import com.anteros.model.player.Player;
import com.anteros.util.Area;
import com.anteros.util.Misc;

public class FightCave {

	public FightCave() {
		
	}
	
	public static void enterCave(final Player p) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2438, 5168, 2439, 5168) {

			@Override
			public void run() {
				/*
				 * Fight cave is 20k squares from the original place, then another (200 * playerIndex) squares west.
				 */
				Location instanceLocation = Location.location((20000 + 2413) + (200 * p.getIndex()), 20000 + 5116, 0);
				p.teleport(Location.location(instanceLocation));
				p.getSettings().setFightCave(new FightCaveSession(p));
				
				World.getInstance().registerEvent(new Event(600) {

					@Override
					public void execute() {
						this.stop();
						p.getActionSender().sendNPCHead(2617, 242, 1);
						p.getActionSender().modifyText("TzHaar-Mej-Jal", 242, 3);
						p.getActionSender().modifyText("You're on your own now, JalYt.", 242, 4);
						p.getActionSender().modifyText("Pepare to fight for your life!", 242, 5);
						p.getActionSender().animateInterface(9827, 242, 1);
						p.getActionSender().sendChatboxInterface2(242);
					}

				});
			}
		});
	}
	
	public static void exitCave(final Player p, int objectX, int objectY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(p, objectX, objectY - 1, objectX + 2, objectY - 1) {

			@Override
			public void run() {
				p.setTemporaryAttribute("unmovable", true);
				World.getInstance().registerEvent(new Event(600) {

					@Override
					public void execute() {
						this.stop();
						p.getSettings().getFightCave().teleFromCave(true);
					}
				});
			}
		});
	}
	
	public static void fightCaveAttacks(final NPC npc, final Player p) {
		if (npc.isDead() || npc.isDestroyed() || p.isDead() || p.isDestroyed() || p.isDead() || !Area.inFightCave(p.getLocation()) || p.getTeleportTo() != null) {
			return;
		}
		int damage = Misc.random(npc.getMaxHit());
		final int prayer = p.getPrayers().getHeadIcon();
		int hitDelay = npc.getHitDelay();
		int animation = npc.getAttackAnimation();
		switch(npc.getId()) {
			case 2734: // Tz-Kih (lvl 22)
			case 2735:
				if (prayer == PrayerData.MELEE) {
					damage = 0;
				}
				break;
				
			case 2739: // Tz-Xil (lvl 90)
			case 2740:
				if (prayer == PrayerData.RANGE) {
					damage = 0;
				}
				p.getActionSender().sendProjectile(npc.getLocation(), p.getLocation(), 32, 1616, 50, 40, 34, 50, p);
				break;
				
			case 2741: // Yt-MejKot (lvl 180)
			case 2742:
				if (prayer == PrayerData.MELEE) {
					damage = 0;
				}
				// TODO healing
				break;
				
			case 2743: // Ket-Zek (lvl 360)
			case 2744:
				if (!p.getLocation().withinDistance(npc.getLocation(), 2)) {
					hitDelay = 1600;
					animation = 9266;
					npc.graphics(1622);
					damage = Misc.random(49);
					if (prayer == PrayerData.MAGIC) {
						damage = 0;
					}
					World.getInstance().registerEvent(new Event(300) {
						@Override
						public void execute() {
							this.stop();
							p.getActionSender().sendProjectile(npc.getLocation(), p.getLocation(), 32, 1623, 50, 40, 34, 80, p);
						}
					});
				} else {
					damage = Misc.random(64);
					if (prayer == PrayerData.MELEE) {
						damage = 0;
					}
				}
				break;
				
			case 2745: // TzTok Jad (lvl 702)
				doJadAttacks(p, npc);
				break;
		}
		if (npc.getId() == 2745){
			return;
		}
		if (animation != 65535) {
			npc.animate(animation);
		}
		p.setLastAttacked(System.currentTimeMillis());
		npc.setLastAttack(System.currentTimeMillis());
		p.setAttacker(npc);
		npc.resetCombatTurns();
		if (damage > p.getHp()) {
			damage = p.getHp();
		}
		final int hit = damage;
		final int npcId = npc.getId();
		World.getInstance().registerEvent(new Event(hitDelay) {
			@Override
			public void execute() {
				this.stop();
				if (!Area.inFightCave(p.getLocation()) || p.getTeleportTo() != null) {
					return;
				}
				if (npcId == 2734 || npcId == 2735) {
					int prayerLevel = p.getLevels().getLevel(5);
					int newPrayerLevel = prayerLevel -= (hit + 1);
					if (newPrayerLevel <= 0) {
						newPrayerLevel = 0;
					}
					p.getLevels().setLevel(5, newPrayerLevel);
					p.getActionSender().sendSkillLevel(5);
				} else if (npcId == 2743 || npcId == 2744) {
					if (Misc.random(1) == 0) {
						p.graphics(1624);
					}
				}
				if ((p.getCombatTurns() > 2 || p.getCombatTurns() < 0)) {
					p.animate(p.getDefenceAnimation());
				}
				p.hit(hit);
			}
		});
	}

	private static void doJadAttacks(final Player p, final NPC npc) {
		if (npc.getHp() <= (npc.getMaxHp() * 0.50)) {
			if (p.getSettings().getFightCave() != null) {
				if (!p.getSettings().getFightCave().healersSpawned()) {
					summonJadHealers(p, npc);
					p.getSettings().getFightCave().setHealersSpawned(true);
				}
			}
		}
		npc.resetCombatTurns();
		npc.setEntityFocus(p.getClientIndex());
		switch(Misc.random(1)) {
			case 0: // Range
				npc.animate(9276);
				npc.graphics(1625);
				World.getInstance().registerEvent(new Event(1600) {
					int hit = 0;
					int status = 0;
					@Override
					public void execute() {
						int prayer = p.getPrayers().getHeadIcon();
						if (this.status == 0) {
							this.status++;
							this.setTick(1500);
							p.graphics(451);
							if (prayer == PrayerData.RANGE) {
								hit = 0;
							} else {
								hit = Misc.random(96);
							}
						} else {
							if (prayer != PrayerData.RANGE) {
								hit = Misc.random(96);
							}
							this.stop();
							p.setLastAttacked(System.currentTimeMillis());
							npc.setLastAttack(System.currentTimeMillis());
							p.setAttacker(npc);
							if (hit > p.getHp()) {
								hit = p.getHp();
							}
							if (!Area.inFightCave(p.getLocation()) || p.getTeleportTo() != null) {
								return;
							}
							if ((p.getCombatTurns() > 2 || p.getCombatTurns() < 0)) {
								p.animate(p.getDefenceAnimation());
							}
							p.hit(hit);
							World.getInstance().registerEvent(new Event(100) {
								@Override
								public void execute() {
									this.stop();
									p.graphics(157, 0, 100);
								}
							});
						}
					}
				});
				break;
	
			case 1: // Magic
				npc.graphics(1626);
				World.getInstance().registerEvent(new Event(300) {
					int hit = 0;
					int status = 0;
					@Override
					public void execute() {
						int prayer = p.getPrayers().getHeadIcon();
						npc.animate(9278);
						if (this.status == 0) {
							this.status++;
							this.setTick(1600);
							p.getActionSender().sendProjectile(npc.getLocation(), p.getLocation(), 32, 1627, 50, 40, 34, 90, p);
						} else {
							this.stop();
							if (prayer == PrayerData.MAGIC) {
								hit = 0;
							} else {
								hit = Misc.random(96);
							}
							p.setLastAttacked(System.currentTimeMillis());
							npc.setLastAttack(System.currentTimeMillis());
							p.setAttacker(npc);
							if (hit > p.getHp()) {
								hit = p.getHp();
							}
							if (!Area.inFightCave(p.getLocation()) || p.getTeleportTo() != null) {
								return;
							}
							if ((p.getCombatTurns() > 2 || p.getCombatTurns() < 0)) {
								p.animate(p.getDefenceAnimation());
							}
							p.hit(hit);
							World.getInstance().registerEvent(new Event(100) {
								@Override
								public void execute() {
									this.stop();
									p.graphics(157, 0, 100);
								}
							});
						}
					}
				});
				break;
		}
	}
	
	public static void antiTeleportMessage(Player p) {
		p.getActionSender().sendNPCHead(2617, 242, 1);
		p.getActionSender().modifyText("TzHaar-Mej-Jal", 242, 3);
		p.getActionSender().modifyText("I cannot allow you to teleport from the fight cave.", 242, 4);
		p.getActionSender().modifyText("In Tzhaar, you either win, or die!", 242, 5);
		p.getActionSender().animateInterface(9827, 242, 1);
		p.getActionSender().sendChatboxInterface2(242);
		p.getActionSender().sendMessage("You are unable to teleport from the fight cave.");
	}

	private static void summonJadHealers(Player p, final NPC jad) {
		for (int i = 0; i < 4; i++) {
			final NPC npc = new NPC(2746);
			Location minCoords = Location.location((20000 + 2363) + (200 * p.getIndex()), 25502, 0);
			Location maxCoords = Location.location((20000 + 2430) + (200 * p.getIndex()), 25123, 0);
			npc.readResolve();
			npc.setMinimumCoords(minCoords);
			npc.setMaximumCoords(maxCoords);
			npc.setLocation(Location.location((20000 + 2387) + (200 * p.getIndex()) + Misc.random(22), 20000 + 5069 + Misc.random(33), 0));
			npc.setEntityFocus(jad.getClientIndex());
			npc.setOwner(p);
			npc.getFollow().setFollowing(jad);
			npc.setTarget(null);
			World.getInstance().getNpcList().add(npc);
			World.getInstance().registerEvent(new Event(2000) {

				@Override
				public void execute() {
					if (npc.isDead() || npc.isHidden() || npc.isDestroyed()) {
						this.stop();
						return;
					}
					if (npc.getLocation().withinDistance(jad.getLocation(), 2) && !npc.inCombat()) {
						if (Misc.random(7) == 0) {
							jad.graphics(444);
							npc.animate(9254);
							int jadMaxHp = jad.getMaxHp();
							jad.heal((int) (jadMaxHp * 0.5));
						}
					}
				}
			});
		}
	}
}
