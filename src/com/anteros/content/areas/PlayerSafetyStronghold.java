package com.anteros.content.areas;

import com.anteros.content.Dialogue;
import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class PlayerSafetyStronghold {

	public static void schoolDoor(Player player) {
		//TODO school door
	}
	
	public static void safetyStronghold(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 2, oY - 2, oX + 2, oY + 2) {
			@Override
			public void run() {
				if (player.pSafetyStronghold == 0 || player.pSafetyStronghold == 1) {
					player.getActionSender().sendPlayerChat1("I'm too scared to go down there!", Dialogue.WORRIED);
				} else if (player.pSafetyStronghold == 2) {
					player.teleport(Location.location(3159, 4279, 3));
				}
			}
		});
	}
	
	public static void exitSafetyStronghold(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3077, 3462, 0));
			}
		});
	}
	
	public static void trapdoorEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3082, 4229, 0));
			}
		});
	}
	
	public static void trapdoorExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3074, 3456, 0));
			}
		});
	}
	public static void magicEnter(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3140, 4230, 2));
			}
		});
	}
	public static void tunnelExit(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3077, 4234, 0));
			}
		});
	}
	public static void toChestRoom(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				if (player.pSafetyStronghold == 1 || player.pSafetyStronghold == 2) {
					player.teleport(Location.location(3177, 4266, 4));
				} else {
					player.getActionSender().sendPlayerChat1("This door is locked.", Dialogue.THINKING);
				}
			}
		});
	}
	public static void leaveChestRoom(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3177, 4269, 2));
			}
		});
	}
	//stairs
	//
	public static void Laddertothirdup(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3171, 4271, 3));
			}
		});
	}
	public static void Laddertothirddown(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3174, 4273, 2));
			}
		});
	}
	//
	//
	public static void Ladderbottomrightup(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3157, 4244, 2));
			}
		});
	}
	public static void Ladderbottomrightdown(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3160, 4246, 1));
			}
		});
	}
	public static void Ladderbottomleftup(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3149, 4244, 2));
			}
		});
	}
	public static void Ladderbottomeftdown(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3146, 4246, 1));
			}
		});
	}
	public static void Laddertoprightup(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3157, 4251, 2));
			}
		});
	}
	public static void Laddertoprightdown(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3160, 4249, 1));
			}
		});
	}
	public static void Laddertopleftup(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3149, 4251, 2));
			}
		});
	}
	public static void Laddertopleftdown(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				player.teleport(Location.location(3146, 4249, 1));
			}
		});
	}
	//end stairs
	//
	//
	//
	//
	
	public static void pullLever(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				if(player.pSafetyStronghold == 0) {
					player.pSafetyStronghold = 1;
					player.animate(798);
					player.getActionSender().sendMessage("You hear a faint clicking noise like a door unlocking...");
				}
			}
		});
	}
	
	public static void openChest(final Player player, int objectId, Location loc, final int oX, final int oY) {
		World.getInstance().registerCoordinateEvent(new AreaEvent(player, oX - 1, oY - 1, oX + 1, oY + 1) {
			@Override
			public void run() {
				if(player.pSafetyStronghold == 1) {
					if (player.getInventory().getTotalFreeSlots() > 3) {
						player.pSafetyStronghold = 2;
						player.animate(536);
						player.getInventory().addItem(995, 10000);
						player.getInventory().addItem(12629, 1);
						player.getInventory().addItem(12627, 1);
						player.getInventory().addItem(12628, 1);
						player.getActionSender().sendStatement3("You open the chest to find a large pile of gold, along with a " +
								"pair", "of safety gloves and two antique lamps. Also in the chest is the", "secret of the " +
										"'Safety First' emote.");
					}
				}
			}
		});
	}
}