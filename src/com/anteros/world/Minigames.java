package com.anteros.world;

import com.anteros.content.minigames.agilityarena.AgilityArena;
import com.anteros.content.minigames.fightpits.FightPits;

public class Minigames {

	private AgilityArena agilityArena;
	private FightPits fightPits;
	
	public Minigames() {
		this.agilityArena = new AgilityArena();
		this.fightPits = new FightPits();
	}

	public AgilityArena getAgilityArena() {
		return agilityArena;
	}

	public FightPits getFightPits() {
		return fightPits;
	}
}
