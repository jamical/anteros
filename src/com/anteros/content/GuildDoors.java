package com.anteros.content;

import com.anteros.event.AreaEvent;
import com.anteros.model.Location;
import com.anteros.model.World;
import com.anteros.model.player.Player;

public class GuildDoors {
	
	public static void fishingGuild(final Player p, final int objectX, final int objectY) {
			World.getInstance().registerCoordinateEvent(new AreaEvent(p, 2611, 3393, 2611, 3394) {

				@Override
				public void run() {
					if (p.getLocation().getY() >= objectY) {
						p.teleport(Location.location(objectX, objectY - 1, 0));
					} else {
						p.teleport(Location.location(objectX, objectY + 1, 0));
					}
				}
			});
	}

}
